---
title: "#"
date: {{ dateFormat "2006-01-02" .Date }}
categories: [kdeexpress]
tags: [temporada4]
author: "David Marzal"
img: 
podcast:
  audio: https://op3.dev/e,pg=$GUIDv5/$URL
  olength : 
  mlength : 
  iduration : "00:MM:SS"
  capitulos :   # [guid, guid]
si :
  uri :        # [URL a root toot]
transcript :
  #- trurl : "$URL.vtt"
  #  trtype : text/vtt    # text/plain text/html text/vtt application/json application/x-subrip
  #- trurl : "$URL.srt"
  #  trtype : text/srt 
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : "https://mastodon.escepticos.es/system/accounts/avatars/110/741/007/953/649/588/original/4422d774516e1cc5.jpg"
    href : https://masto.es/@DavidMarzalC
  - person : Jorge Lama
    role : Audio Editor
    group : Audio Post-Production
    img : https://files.mastodon.social/accounts/avatars/001/106/656/original/0c35a1ce0f0cec6b.jpg
    href : https://mastodon.social/@raivenra
location :
  - text: 
    GEO:
    OSM:
    rel:
    country:
itunes:
  explicit:   # por defecto false
comments: false
aliases:
  - /
---


___
{{% licencia %}}
