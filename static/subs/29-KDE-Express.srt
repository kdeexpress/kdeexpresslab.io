1
00:00:00,980 --> 00:00:03,850
 Buenas, episodio 29 de KDE Express,

2
00:00:03,850 --> 00:00:06,430
 grabado por la noche, así que

3
00:00:06,430 --> 00:00:08,100
 si me escucháis un poco flojo

4
00:00:08,100 --> 00:00:09,760
 es que tengo a la cría durmiendo.

5
00:00:09,760 --> 00:00:12,000
 Hoy es 10 de junio y estamos de

6
00:00:12,000 --> 00:00:13,780
 vuelta de la Akademy, así que

7
00:00:13,780 --> 00:00:15,670
 se me han juntado unas cuantas

8
00:00:15,670 --> 00:00:17,490
 noticias y finalizaré con un

9
00:00:17,490 --> 00:00:19,360
 par de reflexiones sobre el

10
00:00:19,360 --> 00:00:20,000
 evento.

11
00:00:20,000 --> 00:00:22,620
 Para empezar tenemos una nueva

12
00:00:22,620 --> 00:00:25,240
 iniciativa de KDE Eco que se

13
00:00:25,240 --> 00:00:27,560
 llama Opt Green, de elige verde

14
00:00:27,560 --> 00:00:29,580
 o elige sostenible.

15
00:00:29,580 --> 00:00:31,440
 Que es la contrapartida a lo

16
00:00:31,440 --> 00:00:33,390
 que habían hecho con el Blue

17
00:00:33,390 --> 00:00:35,440
 Angel de certificar software

18
00:00:35,440 --> 00:00:37,720
 para que sea más eficiente.

19
00:00:37,720 --> 00:00:39,610
 Pues ahora esto va enfocado

20
00:00:39,610 --> 00:00:41,540
 más al usuario y va a ser una

21
00:00:41,540 --> 00:00:43,380
 campaña publicitaria y de

22
00:00:43,380 --> 00:00:45,250
 medios en el que informar a la

23
00:00:45,250 --> 00:00:46,930
 gente de que usar software

24
00:00:46,930 --> 00:00:49,060
 libre es eficiente porque alarga

25
00:00:49,060 --> 00:00:50,900
 la vida de los dispositivos y

26
00:00:50,900 --> 00:00:51,900
 cosas así.

27
00:00:51,900 --> 00:00:53,950
 Os dejo un enlace de MuyLinux

28
00:00:53,950 --> 00:00:56,310
 que está bastante detallado,

29
00:00:56,310 --> 00:00:58,860
 aunque en la página de KDE Eco.

30
00:00:58,860 --> 00:00:59,560
 ¿Cómo estoy yo?

31
00:00:59,560 --> 00:01:00,600
 También tendréis la

32
00:01:00,600 --> 00:01:01,750
 información oficial en

33
00:01:01,750 --> 00:01:02,400
 español.

34
00:01:02,400 --> 00:01:04,170
 Luego, sin salir de MuyLinux,

35
00:01:04,170 --> 00:01:05,890
 también tengo una noticia que

36
00:01:05,890 --> 00:01:07,600
 va sobre las webapps en KDE.

37
00:01:07,600 --> 00:01:09,150
 En concreto, cómo mejorar sus

38
00:01:09,150 --> 00:01:09,720
 iconos.

39
00:01:09,720 --> 00:01:11,080
 Hablan un poco de que en

40
00:01:11,080 --> 00:01:13,160
 Firefox sigue siendo imposible

41
00:01:13,160 --> 00:01:14,830
 usarlas, que hay que hacer un

42
00:01:14,830 --> 00:01:16,620
 derivado de Chromium para que

43
00:01:16,620 --> 00:01:18,020
 funcione la webapps.

44
00:01:18,020 --> 00:01:19,710
 Y que si estás en Wayland no

45
00:01:19,710 --> 00:01:21,040
 se suelen ver bien.

46
00:01:21,040 --> 00:01:23,090
 Y entonces hay un truco que es

47
00:01:23,090 --> 00:01:25,240
 meterse en las flags de Chrome

48
00:01:25,240 --> 00:01:27,200
 y poner algo del Preferred o

49
00:01:27,200 --> 00:01:29,280
 Zone Platform Wayland.

50
00:01:29,280 --> 00:01:31,100
 Y entonces empiezan a funcionar.

51
00:01:31,100 --> 00:01:32,280
 Ahí os dejo el enlace por si

52
00:01:32,280 --> 00:01:33,530
 la usáis y queréis mejorar

53
00:01:33,530 --> 00:01:34,280
 los iconos.

54
00:01:34,280 --> 00:01:36,230
 Y hablando de iconos, tenemos

55
00:01:36,230 --> 00:01:38,100
 otra noticia en la que habla de

56
00:01:38,100 --> 00:01:39,920
 que en KDE se han dado cuenta

57
00:01:39,920 --> 00:01:41,950
 que si estás usando aplicaciones

58
00:01:41,950 --> 00:01:44,070
 del ecosistema KDE dentro de

59
00:01:44,070 --> 00:01:44,800
 Gnome,

60
00:01:44,800 --> 00:01:46,530
 hay algunas que se ven muy mal,

61
00:01:46,530 --> 00:01:48,120
 sobre todo en los iconos.

62
00:01:48,120 --> 00:01:50,120
 Y eso es porque Gnome tiene un

63
00:01:50,120 --> 00:01:52,380
 tema, Adaguita y Libadaguita,

64
00:01:52,380 --> 00:01:54,320
 que ha tenido varios cambios.

65
00:01:54,320 --> 00:01:55,900
 Y la cosa es que no sigue el

66
00:01:55,900 --> 00:01:57,850
 estándar de ser un tema usado

67
00:01:57,850 --> 00:01:59,260
 para todo el mundo.

68
00:01:59,260 --> 00:02:01,600
 Sino que es un subconjunto de

69
00:02:01,600 --> 00:02:04,200
 assets, de herramientas, para

70
00:02:04,200 --> 00:02:07,180
 su propio entorno de escritorio.

71
00:02:07,180 --> 00:02:09,020
 Hay otros temas y no puedes depender

72
00:02:09,020 --> 00:02:09,580
 de ese.

73
00:02:09,580 --> 00:02:11,560
 Entonces han hecho unos cambios

74
00:02:11,560 --> 00:02:13,570
 para que todas las aplicaciones

75
00:02:13,570 --> 00:02:15,350
 de KDE puedan tener un tema por

76
00:02:15,350 --> 00:02:15,980
 defecto.

77
00:02:15,980 --> 00:02:17,380
 Para que si cuando buscas el

78
00:02:17,380 --> 00:02:19,100
 tema del escritorio no encuentras

79
00:02:19,100 --> 00:02:21,090
 los iconos que necesites, busques

80
00:02:21,090 --> 00:02:22,240
 otros por defecto.

81
00:02:22,240 --> 00:02:23,430
 Y a las malas siempre tengas

82
00:02:23,430 --> 00:02:24,760
 uno instalado, que supongo que

83
00:02:24,760 --> 00:02:25,760
 serán los de Breeze.

84
00:02:25,760 --> 00:02:27,630
 Al final es las aplicaciones

85
00:02:27,630 --> 00:02:29,240
 que vayan usando esto.

86
00:02:29,240 --> 00:02:30,510
 Se verán mejor en Gnome

87
00:02:30,510 --> 00:02:32,620
 conforme se vayan actualizando.

88
00:02:32,620 --> 00:02:35,010
 Luego tenemos el lanzamiento

89
00:02:35,010 --> 00:02:37,590
 habitual de KDE Gear, que es el

90
00:02:37,590 --> 00:02:39,870
 24.05, que nos trae nuevas

91
00:02:39,870 --> 00:02:41,680
 aplicaciones y varias mejoras.

92
00:02:41,680 --> 00:02:43,220
 Las mejoras las voy a dejar

93
00:02:43,220 --> 00:02:45,180
 para que las podáis ver vosotros

94
00:02:45,180 --> 00:02:46,860
 en la lista de los enlaces que

95
00:02:46,860 --> 00:02:47,580
 os dejo.

96
00:02:47,580 --> 00:02:48,640
 Pero sí os voy a decir las

97
00:02:48,640 --> 00:02:49,920
 aplicaciones nuevas por si os

98
00:02:49,920 --> 00:02:50,600
 interesan.

99
00:02:50,600 --> 00:02:52,960
 Una es Audex, que es una aplicación

100
00:02:52,960 --> 00:02:54,920
 de conversión de CD de audio

101
00:02:54,920 --> 00:02:56,600
 en diferentes formatos.

102
00:02:56,600 --> 00:02:58,980
 Lo que antes llamábamos Ripeo.

103
00:02:59,340 --> 00:03:00,430
 Un inspector de accesibilidad,

104
00:03:00,430 --> 00:03:01,770
 que es una herramienta para que

105
00:03:01,770 --> 00:03:03,210
 los desarrolladores comprueben

106
00:03:03,210 --> 00:03:05,100
 la accesibilidad de su aplicación.

107
00:03:05,100 --> 00:03:06,410
 Lo cual me viene muy bien para

108
00:03:06,410 --> 00:03:07,940
 accesibilidad con tecnologías

109
00:03:07,940 --> 00:03:09,390
 libres, que se ponga el acento

110
00:03:09,390 --> 00:03:10,280
 en estas cosas.

111
00:03:10,280 --> 00:03:11,740
 Francis, que es una aplicación

112
00:03:11,740 --> 00:03:13,020
 de productividad basada en la

113
00:03:13,020 --> 00:03:14,120
 técnica Pomodoro.

114
00:03:14,120 --> 00:03:16,600
 Kalm, con K, por supuesto.

115
00:03:16,600 --> 00:03:18,000
 Una aplicación que te guía a

116
00:03:18,000 --> 00:03:19,530
 través de diversas técnicas

117
00:03:19,530 --> 00:03:20,500
 de respiración.

118
00:03:20,500 --> 00:03:22,480
 Y, a ver esta, ¿cómo la pronuncio?

119
00:03:22,480 --> 00:03:25,290
 Es Klaznik, un juego nuevo

120
00:03:25,290 --> 00:03:28,840
 inspirado en el japonés Shokoban.

121
00:03:28,840 --> 00:03:30,840
 Que no tengo ni idea cómo va.

122
00:03:30,840 --> 00:03:32,840
 Pero lo podéis probar.

123
00:03:32,840 --> 00:03:34,310
 Creo que es de estilo rompecabezas

124
00:03:34,310 --> 00:03:34,840
 o algo así.

125
00:03:34,840 --> 00:03:37,450
 Luego, por supuesto, KDE Gear trae

126
00:03:37,450 --> 00:03:39,750
 mejoras en todas las aplicaciones

127
00:03:39,750 --> 00:03:40,840
 del ecosistema.

128
00:03:40,840 --> 00:03:43,290
 Y luego, en especial, tenemos

129
00:03:43,290 --> 00:03:44,950
 dentro de esas aplicaciones,

130
00:03:44,950 --> 00:03:46,200
 pero que no siempre va de la

131
00:03:46,200 --> 00:03:46,840
 mano de KDE Gear,

132
00:03:46,840 --> 00:03:48,160
 sino que llega a su propio

133
00:03:48,160 --> 00:03:50,840
 calendario, Kdenlive.

134
00:03:50,840 --> 00:03:52,840
 Que nos trae efectos en grupo,

135
00:03:52,840 --> 00:03:55,840
 renderizado, multiformato.

136
00:03:55,840 --> 00:03:57,340
 Es decir, que puedes publicar

137
00:03:57,340 --> 00:03:57,840
 un vídeo.

138
00:03:57,840 --> 00:04:01,610
 De golpe, en 4/3, 16/9 o algún

139
00:04:01,610 --> 00:04:02,840
 formato raro.

140
00:04:02,840 --> 00:04:04,840
 Y trae subtítulos con Whisper.

141
00:04:04,840 --> 00:04:06,790
 Kdenlive ya tenía la opción

142
00:04:06,790 --> 00:04:08,840
 de hacer subtítulos automáticos.

143
00:04:08,840 --> 00:04:10,840
 Pero creo que era con Bosch.

144
00:04:10,840 --> 00:04:12,840
 Y el resultado no era el mismo.

145
00:04:12,840 --> 00:04:14,380
 Ahora lo han hecho súper

146
00:04:14,380 --> 00:04:14,840
 fácil.

147
00:04:14,840 --> 00:04:16,840
 Para que puedas usar Whisper.

148
00:04:16,840 --> 00:04:18,320
 Que ya sabemos que si tienes

149
00:04:18,320 --> 00:04:19,840
 una máquina que lo soporte.

150
00:04:19,840 --> 00:04:22,840
 Pues da mejores resultados.

151
00:04:22,840 --> 00:04:23,920
 Yo lo uso por fuera y va

152
00:04:23,920 --> 00:04:24,840
 bastante bien.

153
00:04:24,840 --> 00:04:26,070
 Tengo pendiente usarlo dentro

154
00:04:26,070 --> 00:04:26,840
 de Kdenlive.

155
00:04:26,840 --> 00:04:28,840
 Pero puede ser una manera sencilla.

156
00:04:28,840 --> 00:04:30,570
 Sin que tengas que montar tu

157
00:04:30,570 --> 00:04:32,840
 modelo y cosas por tu cuenta.

158
00:04:32,840 --> 00:04:34,840
 Luego también tenemos unas cuantas

159
00:04:34,840 --> 00:04:35,840
 noticias a futuro.

160
00:04:35,840 --> 00:04:37,730
 Que se está desarrollando

161
00:04:37,730 --> 00:04:39,840
 mucho sobre Plasma 6.1.

162
00:04:39,840 --> 00:04:42,180
 Y una de las mejoras será el

163
00:04:42,180 --> 00:04:42,840
 HDR.

164
00:04:42,840 --> 00:04:44,800
 Ya se introdujo en Plasma 6 un

165
00:04:44,800 --> 00:04:45,840
 poco de soporte.

166
00:04:45,840 --> 00:04:47,680
 Pero en 6.1 tiene pinta de que

167
00:04:47,680 --> 00:04:48,840
 va a ir mucho mejor.

168
00:04:48,840 --> 00:04:51,220
 También en Plasma 6.1 será

169
00:04:51,220 --> 00:04:52,840
 más sencillo editar tu escritorio.

170
00:04:52,840 --> 00:04:55,840
 Añadir Plasmoide y esas cosas.

171
00:04:55,840 --> 00:04:57,840
 Lo que se ha hecho ahora es que

172
00:04:57,840 --> 00:04:57,840
 cuando tú le das a editar.

173
00:04:57,840 --> 00:04:59,840
 Se te abren unas barras.

174
00:04:59,840 --> 00:05:01,840
 En mi caso están en el lateral.

175
00:05:01,840 --> 00:05:03,840
 Pero una barra arriba y abajo.

176
00:05:03,840 --> 00:05:05,120
 Con opciones para personalizar

177
00:05:05,120 --> 00:05:05,840
 el escritorio.

178
00:05:05,840 --> 00:05:07,200
 Pero a veces esa barra te tapa

179
00:05:07,200 --> 00:05:08,840
 alguna parte del escritorio.

180
00:05:08,840 --> 00:05:10,840
 Porque sube un poco la de abajo.

181
00:05:10,840 --> 00:05:12,840
 Y baja un poco la de arriba.

182
00:05:12,840 --> 00:05:14,840
 Y entre que es un poco lío.

183
00:05:14,840 --> 00:05:16,080
 Y que te tapa el fondo de

184
00:05:16,080 --> 00:05:16,840
 pantalla.

185
00:05:16,840 --> 00:05:18,000
 Han pensado que para que la

186
00:05:18,000 --> 00:05:18,840
 gente no se líe.

187
00:05:18,840 --> 00:05:20,840
 Pueda ver completamente el escritorio.

188
00:05:20,840 --> 00:05:22,840
 Donde dejas el Plasmoide.

189
00:05:22,840 --> 00:05:24,840
 Y esté cristalino cristalino.

190
00:05:24,840 --> 00:05:26,840
 Lo que han hecho es.

191
00:05:26,840 --> 00:05:28,840
 Cuando tú le das a editar.

192
00:05:28,840 --> 00:05:30,840
 Se rescala el fondo de pantalla.

193
00:05:30,840 --> 00:05:32,840
 Lo que viene siendo tu escritorio.

194
00:05:32,840 --> 00:05:34,370
 Y entonces tú vas a ver unos

195
00:05:34,370 --> 00:05:34,840
 bordes.

196
00:05:34,840 --> 00:05:36,050
 Que imagino que serán grises o

197
00:05:36,050 --> 00:05:36,840
 de algún color.

198
00:05:36,840 --> 00:05:38,840
 Y ya dentro de esos bordes.

199
00:05:38,840 --> 00:05:40,840
 Ves tu escritorio.

200
00:05:40,840 --> 00:05:41,860
 En los bordes van a estar las

201
00:05:41,860 --> 00:05:42,840
 barras de herramientas.

202
00:05:42,840 --> 00:05:44,840
 Para la edición.

203
00:05:44,840 --> 00:05:46,840
 Y cuando termines de editar.

204
00:05:46,840 --> 00:05:47,910
 Volverás otra vez a escalar a

205
00:05:47,910 --> 00:05:48,840
 tu tamaño original.

206
00:05:48,840 --> 00:05:49,910
 Y así queda muy claro que es

207
00:05:49,910 --> 00:05:50,840
 lo que estás haciendo.

208
00:05:50,840 --> 00:05:52,180
 Esto estará en la siguiente

209
00:05:52,180 --> 00:05:52,840
 versión.

210
00:05:52,840 --> 00:05:54,340
 Que saldrá creo que a mitad de

211
00:05:54,340 --> 00:05:54,840
 junio.

212
00:05:54,840 --> 00:05:56,840
 Luego tengo una noticia.

213
00:05:56,840 --> 00:05:58,840
 Que no es exclusivamente de Plasma.

214
00:05:58,840 --> 00:06:00,150
 Pero que sí puede afectar a

215
00:06:00,150 --> 00:06:00,840
 KDE Neon.

216
00:06:00,840 --> 00:06:02,840
 Que hay un debate interesante.

217
00:06:02,840 --> 00:06:04,840
 En la web de TheRegister.com

218
00:06:04,840 --> 00:06:06,840
 Sobre si los kernels super viejos.

219
00:06:06,840 --> 00:06:08,340
 Son más inseguros que los LTS

220
00:06:08,340 --> 00:06:08,840
 estables.

221
00:06:08,840 --> 00:06:10,840
 Hay distribuciones.

222
00:06:10,840 --> 00:06:12,840
 Como Red Hat.

223
00:06:12,840 --> 00:06:14,840
 O Centos.

224
00:06:14,840 --> 00:06:16,840
 Que cogen una versión del

225
00:06:16,840 --> 00:06:16,840
 kernel.

226
00:06:16,840 --> 00:06:18,840
 Y la aguantan 10 años.

227
00:06:18,840 --> 00:06:20,840
 O 15.

228
00:06:20,840 --> 00:06:22,840
 Y encima ni siquiera es.

229
00:06:22,840 --> 00:06:24,840
 La LTS más larga.

230
00:06:24,840 --> 00:06:26,230
 Sino que es la que a ellos les

231
00:06:26,230 --> 00:06:26,840
 va bien.

232
00:06:26,840 --> 00:06:28,840
 En ese momento.

233
00:06:28,840 --> 00:06:30,170
 Y entonces se dedican a hacer

234
00:06:30,170 --> 00:06:30,840
 backporting.

235
00:06:30,840 --> 00:06:32,280
 A coger errores que son solucionados

236
00:06:32,280 --> 00:06:32,840
 en nuevos.

237
00:06:32,840 --> 00:06:34,840
 Y a ponerlos en el viejo.

238
00:06:34,840 --> 00:06:36,840
 Han hecho algunos análisis.

239
00:06:36,840 --> 00:06:38,840
 Y ven que las versiones nuevas.

240
00:06:38,840 --> 00:06:40,840
 Suelen tener más errores corregidos.

241
00:06:40,840 --> 00:06:42,840
 Y menos vulnerabilidades.

242
00:06:42,840 --> 00:06:44,280
 Porque en las versiones super

243
00:06:44,280 --> 00:06:44,840
 viejas.

244
00:06:44,840 --> 00:06:46,210
 Al final hay tanto parche que

245
00:06:46,210 --> 00:06:46,840
 aplicar.

246
00:06:46,840 --> 00:06:48,180
 Que algunos no se llegan a

247
00:06:48,180 --> 00:06:48,840
 aplicar.

248
00:06:48,840 --> 00:06:50,840
 Y te gusta el tema de seguridad.

249
00:06:50,840 --> 00:06:52,840
 Y luego volviendo ya a KDE y Plasma.

250
00:06:52,840 --> 00:06:54,840
 Os hago un repaso.

251
00:06:54,840 --> 00:06:56,840
 De algunas distros.

252
00:06:56,840 --> 00:06:58,480
 Que poco a poco van cogiendo el

253
00:06:58,480 --> 00:06:58,840
 ritmo.

254
00:06:58,840 --> 00:07:00,840
 Y ya tienen Plasma 6 disponible.

255
00:07:00,840 --> 00:07:02,840
 Para los usuarios.

256
00:07:02,840 --> 00:07:04,220
 Ya sabemos que KDE Neon es la

257
00:07:04,220 --> 00:07:04,840
 primera.

258
00:07:04,840 --> 00:07:06,840
 Que Arch Linux va muy rápida.

259
00:07:06,840 --> 00:07:08,050
 Y que hay algunas que también

260
00:07:08,050 --> 00:07:08,840
 van super rápidas.

261
00:07:08,840 --> 00:07:10,840
 Y algunas que tardarán años.

262
00:07:10,840 --> 00:07:12,840
 En estilo Debian y cosas así.

263
00:07:12,840 --> 00:07:14,840
 Y luego las intermedias.

264
00:07:14,840 --> 00:07:16,480
 Pues tienes por ejemplo Alpine

265
00:07:16,480 --> 00:07:16,840
 Linux la 320.

266
00:07:16,840 --> 00:07:18,840
 Que ya lo tiene.

267
00:07:18,840 --> 00:07:20,840
 Y te puedes preguntar.

268
00:07:20,840 --> 00:07:22,840
 Que Alpine Linux que hace con KDE.

269
00:07:22,840 --> 00:07:24,200
 Si lo que más se usa es para

270
00:07:24,200 --> 00:07:24,840
 docker.

271
00:07:24,840 --> 00:07:26,220
 Y es una distribución super

272
00:07:26,220 --> 00:07:26,840
 liviana.

273
00:07:26,840 --> 00:07:28,840
 Que se usa incluso en móviles.

274
00:07:28,840 --> 00:07:30,840
 Pues ahí está el truco.

275
00:07:30,840 --> 00:07:31,910
 Algunos móviles sí que tienen

276
00:07:31,910 --> 00:07:32,840
 estos nodes de escritorio.

277
00:07:32,840 --> 00:07:34,840
 Y ahí sí que lo vas a usar.

278
00:07:34,840 --> 00:07:35,940
 En un docker no creo que vayas

279
00:07:35,940 --> 00:07:36,840
 a instalar Plasma.

280
00:07:36,840 --> 00:07:38,840
 Pero en un móvil.

281
00:07:38,840 --> 00:07:40,840
 Pues sí que necesitas.

282
00:07:40,840 --> 00:07:42,840
 Y tanto Plasma como Gnome.

283
00:07:42,840 --> 00:07:44,840
 Tienen su variante para móviles.

284
00:07:44,840 --> 00:07:46,310
 Luego Manjaro ya se ha puesto

285
00:07:46,310 --> 00:07:46,840
 al día.

286
00:07:46,840 --> 00:07:48,840
 Y Nixos 2405.

287
00:07:48,840 --> 00:07:50,840
 También tiene Plasma 6.

288
00:07:50,840 --> 00:07:52,220
 Así que los que seáis usuarios

289
00:07:52,220 --> 00:07:52,840
 de esas distros.

290
00:07:52,840 --> 00:07:53,920
 Podéis disfrutar de nuestro

291
00:07:53,920 --> 00:07:54,840
 escritorio favorito.

292
00:07:54,840 --> 00:07:56,840
 Y espero que pronto tengáis 6.1.

293
00:07:56,840 --> 00:07:58,250
 Que también viene con muchas

294
00:07:58,250 --> 00:07:58,840
 mejoras.

295
00:07:58,840 --> 00:08:00,840
 Luego os traigo.

296
00:08:00,840 --> 00:08:02,840
 La reflexión de Baltasar.

297
00:08:02,840 --> 00:08:04,840
 Sobre el Akademy-es 2024.

298
00:08:04,840 --> 00:08:06,840
 Que ha escrito un artículo en

299
00:08:06,840 --> 00:08:06,840
 su blog.

300
00:08:06,840 --> 00:08:08,840
 Y aparte de compartir todo lo

301
00:08:08,840 --> 00:08:08,840
 que escribe.

302
00:08:08,840 --> 00:08:10,440
 Yo la verdad que tengo que

303
00:08:10,440 --> 00:08:10,840
 decir.

304
00:08:10,840 --> 00:08:12,840
 Que lo disfruté un montón.

305
00:08:12,840 --> 00:08:14,840
 Tuvimos una sala para nosotros.

306
00:08:14,840 --> 00:08:16,840
 El viernes casi toda la mañana.

307
00:08:16,840 --> 00:08:18,840
 Y toda la tarde.

308
00:08:18,840 --> 00:08:20,040
 En la que tuvimos charlas muy

309
00:08:20,040 --> 00:08:20,840
 interesantes.

310
00:08:20,840 --> 00:08:22,840
 Con bastante buena audiencia.

311
00:08:22,840 --> 00:08:26,840
 Estuvo todo muy bien dirigido.

312
00:08:26,840 --> 00:08:28,840
 Y gestionado.

313
00:08:28,840 --> 00:08:30,840
 Por nuestro querido presidente.

314
00:08:30,840 --> 00:08:32,840
 Que estuvo a los mandos técnicos.

315
00:08:32,840 --> 00:08:34,840
 Gestionando la sala.

316
00:08:34,840 --> 00:08:36,200
 Y la verdad que fue un intercambio

317
00:08:36,200 --> 00:08:36,840
 estupendo.

318
00:08:36,840 --> 00:08:38,840
 De pareceres, de información.

319
00:08:38,840 --> 00:08:40,840
 De compadreos.

320
00:08:40,840 --> 00:08:42,280
 De gente que le gusta lo mismo

321
00:08:42,280 --> 00:08:42,840
 que a ti.

322
00:08:42,840 --> 00:08:44,840
 Me lo pasé muy bien.

323
00:08:44,840 --> 00:08:46,230
 No pude estar todo el rato en

324
00:08:46,230 --> 00:08:46,840
 la sala.

325
00:08:46,840 --> 00:08:48,070
 No pude estar todo esLibre de

326
00:08:48,070 --> 00:08:48,840
 voluntarios.

327
00:08:48,840 --> 00:08:50,840
 Pero la verdad que sí que pude

328
00:08:50,840 --> 00:08:50,840
 disfrutar.

329
00:08:50,840 --> 00:08:52,840
 Entre el viernes y el sábado.

330
00:08:52,840 --> 00:08:54,840
 Más que en la de Málaga.

331
00:08:54,840 --> 00:08:56,840
 Que estuvimos a piñon.

332
00:08:56,840 --> 00:08:58,840
 Y los voluntarios.

333
00:08:58,840 --> 00:09:00,840
 Venga a atender las charlas.

334
00:09:00,840 --> 00:09:02,840
 Y luego esLibre.

335
00:09:02,840 --> 00:09:04,840
 Aunque lo cierto es que cuesta

336
00:09:04,840 --> 00:09:04,840
 muchísimo.

337
00:09:04,840 --> 00:09:06,840
 Hacer un evento de este tipo.

338
00:09:06,840 --> 00:09:08,840
 En el que se dio comida.

339
00:09:08,840 --> 00:09:10,840
 Se hicieron regalos.

340
00:09:10,840 --> 00:09:12,840
 Se hicieron sorteos.

341
00:09:12,840 --> 00:09:14,840
 Hubo fruta disponible.

342
00:09:14,840 --> 00:09:16,840
 Todo el evento.

343
00:09:16,840 --> 00:09:18,840
 Café.

344
00:09:18,840 --> 00:09:20,840
 Todo con vistas de sostenibilidad.

345
00:09:20,840 --> 00:09:22,840
 Cuatro tracks.

346
00:09:22,840 --> 00:09:24,840
 Un montón de charlas.

347
00:09:24,840 --> 00:09:26,840
 Muchísimos ponentes.

348
00:09:26,840 --> 00:09:28,840
 Mucho trabajo en general.

349
00:09:28,840 --> 00:09:30,580
 Pero la verdad es que yo creo

350
00:09:30,580 --> 00:09:30,840
 que mereció la pena.

351
00:09:30,840 --> 00:09:32,490
 Porque ver a tanta gente ahí

352
00:09:32,490 --> 00:09:32,840
 juntas.

353
00:09:32,840 --> 00:09:34,840
 Hablando, disfrutando.

354
00:09:34,840 --> 00:09:36,120
 Las charlas tan interesantes

355
00:09:36,120 --> 00:09:36,840
 que habían.

356
00:09:36,840 --> 00:09:38,840
 Yo os tengo que recomendar que

357
00:09:38,840 --> 00:09:38,840
 si tenéis la oportunidad.

358
00:09:38,840 --> 00:09:40,840
 Vengáis a uno de estos.

359
00:09:40,840 --> 00:09:42,840
 Con el tiempo, tener paciencia.

360
00:09:42,840 --> 00:09:44,840
 Como la mayoría de los vídeos.

361
00:09:44,840 --> 00:09:46,840
 Pero como presencial.

362
00:09:46,840 --> 00:09:48,840
 No hay nada.

363
00:09:48,840 --> 00:09:50,840
 Yo os recomiendo que.

364
00:09:50,840 --> 00:09:51,910
 En tal que salgan las fechas

365
00:09:51,910 --> 00:09:52,840
 del siguiente Akademy.

366
00:09:52,840 --> 00:09:54,840
 Y del siguiente libre.

367
00:09:54,840 --> 00:09:55,950
 Os las apuntéis y hagáis lo

368
00:09:55,950 --> 00:09:56,840
 que podáis por ir.

369
00:09:56,840 --> 00:09:58,840
 Yo lo haré.

370
00:09:58,840 --> 00:10:00,840
 Porque se disfrutan mucho.

371
00:10:00,840 --> 00:10:02,110
 Y luego es muy fácil hablar

372
00:10:02,110 --> 00:10:02,840
 por internet.

373
00:10:02,840 --> 00:10:04,840
 Estar en salas de Matrix.

374
00:10:04,840 --> 00:10:06,310
 O en el canal o el grupo de

375
00:10:06,310 --> 00:10:06,840
 Telegram.

376
00:10:06,840 --> 00:10:08,840
 Pero no es lo mismo.

377
00:10:08,840 --> 00:10:10,840
 Hay que verse de vez en cuando.

378
00:10:10,840 --> 00:10:12,840
 Y con esta nueva tradición.

379
00:10:12,840 --> 00:10:14,840
 Voy a terminar con un atajo de

380
00:10:14,840 --> 00:10:14,840
 teclado.

381
00:10:14,840 --> 00:10:16,840
 El que más usaba.

382
00:10:16,840 --> 00:10:18,840
 Es el Meta+e para Dolphin.

383
00:10:18,840 --> 00:10:20,840
 Pero muy seguido.

384
00:10:20,840 --> 00:10:22,840
 Casi empatado.

385
00:10:22,840 --> 00:10:24,840
 Lo que más uso es Alt Espacio.

386
00:10:24,840 --> 00:10:26,000
 Que también te sirve para

387
00:10:26,000 --> 00:10:26,840
 abrir Dolphin.

388
00:10:26,840 --> 00:10:28,190
 Aunque tienes que teclear un

389
00:10:28,190 --> 00:10:28,840
 poco más.

390
00:10:28,840 --> 00:10:30,330
 Pero es que te sirve para casi

391
00:10:30,330 --> 00:10:30,840
 todo.

392
00:10:30,840 --> 00:10:32,840
 Te sirve para invocar aplicaciones.

393
00:10:32,840 --> 00:10:34,840
 Abrir carpetas.

394
00:10:34,840 --> 00:10:36,840
 Abrir páginas web.

395
00:10:36,840 --> 00:10:38,840
 Hacer cálculos matemáticos.

396
00:10:38,840 --> 00:10:40,840
 Definiciones.

397
00:10:40,840 --> 00:10:42,840
 Puedes activar.

398
00:10:42,840 --> 00:10:44,840
 Puedes priorizar.

399
00:10:44,840 --> 00:10:46,040
 Que tipo de resultados te salgan

400
00:10:46,040 --> 00:10:46,840
 antes o después.

401
00:10:46,840 --> 00:10:48,300
 Si lo primero que quieres son

402
00:10:48,300 --> 00:10:48,840
 carpetas o ficheros.

403
00:10:48,840 --> 00:10:50,840
 Resultados de internet o aplicaciones.

404
00:10:50,840 --> 00:10:52,840
 Es una gozada.

405
00:10:52,840 --> 00:10:53,810
 Es la misma tecnología que

406
00:10:53,810 --> 00:10:54,840
 está en el botón de inicio.

407
00:10:54,840 --> 00:10:56,840
 Que se lo llaman Kick Off.

408
00:10:56,840 --> 00:10:58,840
 Pero con Alt Espacio.

409
00:10:58,840 --> 00:11:00,840
 Para mí es súper rápido.

410
00:11:00,840 --> 00:11:02,840
 Y súper productivo.

411
00:11:02,840 --> 00:11:04,510
 Si es verdad que como Baltasar

412
00:11:04,510 --> 00:11:04,840
 dijo.

413
00:11:04,840 --> 00:11:06,840
 El Alt F2.

414
00:11:06,840 --> 00:11:08,840
 Yo todo mi respeto.

415
00:11:08,840 --> 00:11:10,840
 Pero hacer Alt Espacio.

416
00:11:10,840 --> 00:11:12,840
 Y poner 22+28.

417
00:11:12,840 --> 00:11:14,840
 Partido de 55.

418
00:11:14,840 --> 00:11:16,300
 La raíz cuadrada de no sé

419
00:11:16,300 --> 00:11:16,840
 qué.

420
00:11:16,840 --> 00:11:18,840
 Y que me dé el resultado y lo

421
00:11:18,840 --> 00:11:18,840
 pueda copiar.

422
00:11:18,840 --> 00:11:20,840
 O abrir una aplicación.

423
00:11:20,840 --> 00:11:22,840
 Para mí es una maravilla.

424
00:11:22,840 --> 00:11:24,840
 Con este consejo os dejo.

425
00:11:24,840 --> 00:11:26,840
 Y espero seguir dando noticias.

426
00:11:26,840 --> 00:11:28,470
 Sin que se me vayan acumulando

427
00:11:28,470 --> 00:11:28,840
 tanto.

428
00:11:28,840 --> 00:11:30,840
 Por estos eventos tan demandantes.

429
00:11:30,840 --> 00:11:32,840
 Pero tan disfrutados.

