 Buenas, soy David Marzal, estoy
 grabando el 28 de marzo, y esto
 es KDE Express 25.
 Hoy voy a hablaros sobre los
 eventos que tenemos por delante,
 algo sobre Plasma 6,
 y ponernos al día de un
 montón de noticias que se me
 han ido acumulando este mes.
 Akademy-es está a la vuelta de
 la esquina, cuando escuchéis
 esto se habrá acabado el plazo
 para enviar propuestas,
 pero todavía está abierto el
 plazo para ser patrocinador.
 En esLibre, que es donde se va a
 realizar Akademy, ahora mismo
 hay un montón de charlas aprobadas.
 Todavía se puede enviar alguna
 ponencia, pero la verdad es que
 hay un montón, un montón,
 y si no os dais prisa, yo no
 sé si va a ser posible porque
 en las naves de Valencia no sé
 si van a caber tantos ponentes.
 Lo cual es muy buena señal,
 significa que hay buena salud
 en el mundo organizativo de
 eventos de GNU/Linux y Software
 Libre, y cultura en general.
 Os voy a poner en las notas del
 programa un enlace a Mancomún
 Podcast,
 donde hemos grabado un episodio
 especial hablando del evento, y
 así no tengo que repetirlo y
 enrollarme por aquí.
 También quiero poneros al día,
 porque como hace bastante que
 no grabo porque me han estado
 liando en otros programas,
 que estamos con KDE Gears 24.02.1,
 que si ya todo el mundo sabréis
 que salió Plasma 6 con todo el
 mega lanzamiento de los
 frameworks y los Gears portados
 a Qt 6,
 y ya empezamos a tener las
 versiones de arreglo, que es
 donde podéis estar más seguros
 de que el software es todavía
 más estable.
 En la 24.02.1 por ejemplo, Dolphin
 recupera el cambio de color en
 su icono, según el escritorio,
 esto que tiene KDE, de que si
 configuras que tú quieres que
 los colores, el tema del color,
 se ajuste al fondo de pantalla
 que tienes,
 pues te va cambiando los colorinchis
 que hay en la barra de tarea o
 en las barras de menú.
 A mí la verdad es que me gusta
 mucho. Eso parece ser que dejó
 de funcionar y ya rápidamente
 lo han arreglado.
 Por supuesto hay muchísimas
 más arreglas, pero yo dejo en
 las notas del programa el enlace.
 Yo la verdad es que no he
 encontrado prácticamente
 ningún bug, pero según por lo
 que han escrito, ha habido unos
 cuantos.
 Y hablando de bugs, pues KDE Plasma
 después del lanzamiento grandioso
 que tuvimos,
 que fue bastante bien, quitando
 algún errorcillo en KDE Neon
 que se solucionó pronto,
 está ya por la 6.0.3, que eso
 significa que ya tienen dos
 semanas de arreglo de toda la
 gente que rápidamente instaló
 con prisa, como yo.
 Ya han ido reportando, los
 desarrolladores han puesto mano
 a la obra, y en la 6.0.3 yo lo
 defino como la versión que le
 podría poner ya a mi madre.
 Es evidente que después de un
 cambio tan gordo, algunos bugs
 tenían que salir que no lo habíamos
 podido sacar en la fase de beta,
 pero ahora, como no hemos tirado
 dos semanas, venga a reportar y
 venga a arreglar, yo creo que
 esto ya es como una versión
 normal y corriente,
 y hemos pasado el pico de que
 acabamos de cambiar prácticamente
 el motor entero del coche, y
 podemos decir que esto está
 marchando.
 Luego sobre Plasma 6, no quiero
 entrar mucho en detalle porque
 ahora mismo hay un montón de
 gente haciendo vídeos, la
 gente lo está probando,
 pero como yo siempre digo,
 aquí estamos los Early Adopters,
 los que nos gusta mucho mucho
 la tecnología y estar a la
 última.
 Pero hay un montón de gente
 que Plasma 6 no lo verá hasta
 dentro de un año, entonces yo
 prefiero ir poco a poco
 hablando algo de las novedades,
 pero no hacer un pedazo así de
 episodio exclusivo en el que
 hago uno a uno cuáles son las
 cosas que han cambiado.
 Sí que quiero reseñar que os
 dejo el enlace a la publicación
 oficial donde vienen un montón
 de cambios que se han hecho.
 He grabado hace poco en PeerTube
 un vídeo enseñándoles a
 José Jiménez de 24H24L un
 poco alguna novedad y alguna
 cosa que se ha hecho,
 y hablando de cómo ha ido la
 migración. Os dejo también el
 enlace en las notas.
 Y en las notas van a ver un
 montón de URLs en las que tenéis
 "User Facing Changes", que esto
 es la página oficial de Plasma,
 la oficial hacia adentro, no es
 la que promoción publicita, en
 la que vienen todos los cambios
 uno a uno con enlace a por qué
 se ha hecho y cómo,
 por si tenéis realmente curiosidad
 por saber hasta la última miga
 de cambios que hemos tenido.
 Yo querría destacar, por
 ejemplo, aparte de que todo el
 mundo está muy contento con
 que ha vuelto el cubo,
 que ahora hay un efecto nuevo
 que se llama como agitar para
 encontrar el ratón, que yo lo
 he activado,
 que es que si una vez que lo
 tienes activado mueves muy
 rápido el ratón, el ratón
 empieza a crecer, a crecer, a
 crecer...
 Bueno, el ratón no, el icono
 del ratón. Y entonces lo ves
 muy fácil.
 Si no sabes por dónde está
 por la pantalla, tú empiezas a
 menear, a menear, a menear y se
 hace gigante y dices "aquí
 está, sin duda".
 Y luego desaparece en un
 segundo, vuelve a su tamaño
 original.
 También os dejo un artículo
 sobre cómo fue la migración
 de KDE Neon a Plasma 6.
 El 90% de la gente que está
 usando Plasma 6 está contento
 con él y no tiene demasiados
 problemas.
 Siempre están los que si tengo
 envidia y tengo esta versión
 de los drivers que es muy antigua
 porque mi placa ya no lo soportan...
 Y entonces ahí con Wyland
 puede ser... Siempre hay casos,
 pero vamos, para la mayoría de
 la gente tendría que funcionar.
 OpenSUSE, openSUSE, Tumbleweed
 y Kalpa también están ya en
 Plasma 6.
 También ha llegado a PostMarket
 OS, que es una versión para
 móviles.
 Y luego también os dejo un enlace
 a una charla que tuve con José
 Picón y su equipo, que tiene
 un nuevo podcast que se llama "WeHacks"
 sobre privacidad.
 Y me invitaron para hablar un
 poco sobre KDE, sobre Plasma 6
 y en ese momento también hablamos
 sobre el drama que había con
 Telegram, que ya lo tenemos
 superado.
 Eso era el montón de enlaces y
 aquí os lo quería mencionar,
 que os he dejado en las notas
 del programa.
 Y luego quería entrar en una
 serie de noticias que no van
 directamente sobre Plasma 6,
 pero que sí van sobre el ecosistema.
 Por ejemplo, "Muy Luino" hizo
 un artículo que a mí me gustó
 mucho sobre cómo cambiar el
 tamaño de los archivos en Dolphin.
 De aparte entraba en cómo Dolphin
 es una aplicación super útil
 de productividad y que en el
 momento que la usas te enamoras
 de ella, si eres de los que te
 gusta personalizar e intentar
 conseguir ser más productivo
 cuando estás haciendo trabajo
 ofimático.
 Luego también la NLNET, que es
 una pata, una sección que hay
 de la Unión Europea que se dedica
 a fundar proyectos de soberanía
 tecnológica.
 Ha aceptado una propuesta para
 mejorar la accesibilidad de KDE Plasma
 con Wayland, que es una cosa
 que a mí me alegra mucho
 porque KDE Plasma es maravilloso,
 la versión 6 es la mejor que
 hemos tenido, pero tenemos
 mucho trabajo que hacer en
 accesibilidad.
 Y esto nos va a ayudar mucho
 porque el trabajo de accesibilidad
 es duro, requiere gente que
 reporte, gente que pueda probarlo
 y desarrolladores que puedan
 tener el tiempo de hacerlo
 cuando no es de los casos más
 fáciles de programar.
 Con lo cual el dinero siempre
 es bien recibido y la ayuda de
 voluntarios también, claro.
 Yo tengo previsto hacer un
 vídeo, no sé cuándo, pero
 quiero hacer uno con todas las
 posibilidades de KDE Plasma que
 tiene en materia de accesibilidad.
 Yo estoy probando ya algunas
 aquí en mi casa, algunas son
 combinaciones de teclas, otras
 son configuraciones que dejas
 puestas.
 Esto mismo que comentaba antes
 de mover el ratón, la verdad
 es que puede ayudar, pero haré
 un listado y haré una demostración
 una a una de cómo se activan y
 cómo funcionan.
 También tenemos que nuestro
 querido Slimbook, que no es patrocinador
 del programa, pero sí que es
 una empresa comprometida con el
 software libre y con KDE en
 particular, ha sacado su
 versión de portátil del Slimbook
 que siempre hemos tenido, una
 versión renovada.
 Es muy lindo hacer un reportaje
 sobre él, el resumen es porque
 es la actualización, mejor que
 el que había antes, igual de
 bueno, con cosas más modernas.
 También quería contaros que
 es algo que yo uso de vez en
 cuando con mi hija, de hecho es
 lo único que usa del ordenador
 ella, es GCompris.
 La aplicación o programa de
 educación infantil han sacado
 ocho nuevas actividades, la
 verdad es que tienen más de
 las que yo he sido capaz de
 probar.
 Está bien porque viene por
 categorías, por edades o por
 dificultad, y luego por sección
 de si es gramática, si son puzles,
 si es escritura.
 Si tenéis críos o personas
 mayores, que también es viable,
 os recomiendo que le echéis un
 vistazo, aparte GCompris
 está en Android, Mac, Windows,
 Flatpak, vamos, lo podéis
 instalar prácticamente en
 cualquier sitio, no vais a
 tener problemas.
 Luego digiKam 8.3 incorpora un
 descodificador RAW interno. digiKam
 es la aplicación de organización
 fotográfica, organización
 plus plus diría yo, porque
 tienes que hacer muchas cosas.
 Yo tengo ahí organizado mis
 fotos y tengo etiquetadas en
 las caras para saber en todas
 las fotos que salgo yo con mi
 mujer, en todas las fotos que
 sale mi hija con no sé qué.
 Y como está en local, pues no
 es esto de Google Fotos de que
 sabe hasta dónde has pasado
 las navidades. Funciona muy
 bien, se van actualizando cada
 cierto tiempo.
 Esta característica en concreto
 es para gente más aficionada a
 la fotografía, porque es raro
 que lo tengáis si hacéis foto
 con el móvil, aunque ya hay
 móviles que también lo hacen.
 Pero bueno, está bien que
 sigue mejorando en todos los
 aspectos.
 Luego tengo una noticia que
 para quien no lo sepa puede ser
 un poco así chocante, estaba
 dedicada José Jiménez, que es
 que Fedora 40 vendrá pronto
 con Plasma 6, pero sin opción
 de X11.
 Y entonces aquí, en algunos
 otros sitios ya he hecho la advertencia,
 pero quería recordarlo.
 Plasma 6 soporta X11 de manera
 nativa, o sea, no es que en Wayland
 puedas abrir aplicaciones de X11,
 que eso lo tienen todos los
 escritorios con Wayland.
 Plasma 6 está para X11 y
 funciona con X11, de hecho yo
 tengo un portátil en el que
 arranco sesión de Plasma 6 con
 X11 porque me pasa algo con las
 gráficas que son duales de
 interna y externa.
 La cosa es que Fedora, la
 distribución, ha decidido que
 X11 ya es demasiado antiguo y
 que ellos no van a empaquetar
 el soporte de X11.
 No significa que Plasma 6 no
 vaya a funcionar en cualquier
 otra distribución con X11. Sí,
 seguramente lo van a hacer en
 muchas, pero este es el pistoletazo
 de salida para tener en cuenta
 que X11, más pronto que tarde,
 va a ir desapareciendo de
 algunas distribuciones.
 En algunas, por ejemplo, Debian,
 yo creo que dentro de 10 años
 lo mismo todavía podemos arrancar
 porque Plasma 6 tiene soporte
 para mucho, mucho tiempo.
 Pero sí es verdad que ya vamos
 a tener que ir teniendo en
 cuenta que si tenemos un
 hardware específico, como una
 NVIDIA muy antigua, que los
 drivers propietarios no lo soportan
 porque dejaron de dar soporte,
 valga la redundancia.
 El driver Open Source sí que
 soporta que vaya bien con Wayland,
 pero tengo 3 monitores...
 La cosa es que tendréis que ir
 probando, yo espero que Wayland
 haya mejorado suficientemente y
 los drivers hayan mejorado suficientemente
 para que le funcione a todo el
 mundo, pero habrá algún caso
 en el que lo mismo hay que
 cambiar de distribución.
 O lo mismo, como esto es
 software libre, alguien saca un
 repositorio, creo que en Fedora
 es Kopr o algo así, y le da soporte.
 Sé que en Fedora 40 es posible
 que Gnome todavía lo tenga,
 pero Gnome también por lo que
 he leído está pensando en la
 41 cargarse el soporte X11.
 Así que probar y ir teniendo
 una distribución de cabecera
 como plan B por si queréis
 seguir usando Plasma y X11 ya
 dejar de ser una opción.
 Yo, aunque tengo pendiente de
 probarla, ya directamente recomiendo
 que probéis OpenSUSE SlowRoll,
 porque sobre el papel tiene
 todo lo que yo le pido a una
 distribución para poner en
 masa.
 Tiene muy buen soporte, viene
 de una compañía y de una
 comunidad muy grande, como es SUSE
 y como es la comunidad de OpenSUSE,
 es una distribución de las
 primeras, de las grandes, con
 lo cual tiene un montón de
 desarrollo detrás, son punteros.
 No es tan tan tan tan tan moderna
 y rolling release como Tumbleweed,
 con lo cual va a ser más
 estable y va a ser más sencillo
 de manejar, no vais a tener que
 estar tan pendiente de actualizaciones.
 Pero no va a ser tan tan tan
 lentas como otras que vas a
 tener que estar viendo si
 instalas esto en Flatpak, que si
 instalas esto en no sé qué,
 aunque yo la verdad es que instalarlo
 en Flatpak me va bastante bien.
 Pero no sé, creo que tiene una
 cosa ahí, un intermedio que
 merece la pena probarlo.
 Y luego, aunque yo no soy muy
 fan, estoy a favor de la libertad,
 el software libre, no se puede
 guardar que haya nuevas distribuciones
 porque si no esto no sería
 software libre.
 Pero si es verdad que dentro de
 que tiene que haber libertad y
 si no hubiera habido libertad,
 una de las que hoy son grandes,
 ayer no se hubieran podido
 crear, no estoy por ir probando
 todas las nuevas distros que
 aparecen.
 Pero aún así, yo las menciono
 y cada uno es libre de probar y
 ver si les va mejor.
 Y lo mismo el día de mañana,
 una de estas pega el petardazo
 y se convierte en una de las
 principales.
 Y todo este rollo venía porque
 hay una nueva distro que se
 llama X-Ray OS, que por lo que
 he podido leer, y por supuesto
 tenéis en las notas del
 programa el enlace,
 es una especie de Arch Linux
 Vanilla, que son los repositorios
 de Arch sin meterle muchos repositorios
 propios y hacer grandes cambios,
 que está enfocada a juegos y
 que por supuesto viene con
 Plasma por defecto.
 Y al final, que como en Devor,
 lo que hace es que puedas tener
 un Plasma sin tener que hacer
 el proceso de instalación
 desde cero por línea de comando
 o con el menú TUI por el que
 se puede hacer.
 O sea, es un Arch Linux para
 humanos de a pie, que algo debe
 tener especial para juegos, por
 si lo queréis probar.
 Pues todo este popurrí de noticias
 es lo que me he ido apuntando
 este mes.
 La verdad es que esto viene
 siendo una lectura del artículo
 que voy a tener,
 porque la verdad es que el
 episodio de hoy tiene mucha
 más gracia si tenéis delante
 los enlaces, porque son una parrada
 de enlaces por si queréis
 profundizar.
 Pero no quería dejar de comentarlos
 para que tuvierais ahí un resumen
 por encima, por si alguno os
 interesa, ya podréis vosotros
 investigar más.
 Y así ponernos al día y una
 vez que haya terminado el periplo
 de pasar por Mancomún, por Wihax
 y por accesibilidad con tecnologías
 libres,
 recupero un poco de tiempo y el
 siguiente lo puedo hacer antes
 y os traigo las novedades un
 poco antes.
 Recordar que esLibre está a
 la vuelta de la esquina, que yo
 iría reservando ya un hotel si
 queréis ir,
 porque Valencia por esas fechas,
 si lo hacéis con muy poca antelación,
 va a ser caro y va a haber pocas
 opciones.
 Que Akademy-es estará allí por
 si nos queréis ver en una mesa
 que pondremos muy chula con un
 montón de cosas de probar.
 Y que nos vemos en las redes.
 Un saludo, hasta luego.
