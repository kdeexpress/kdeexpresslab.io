1
00:00:00,000 --> 00:00:02,620
 Buenas, KDE Express 22, grabado

2
00:00:02,620 --> 00:00:04,250
 el 11 de diciembre, por si queréis

3
00:00:04,250 --> 00:00:05,500
 una referencia temporal.

4
00:00:05,500 --> 00:00:07,740
 Hoy os traigo, entre otras noticias,

5
00:00:07,740 --> 00:00:08,940
 una reflexión que parte de una

6
00:00:08,940 --> 00:00:10,440
 conversación de Mastodon,

7
00:00:10,440 --> 00:00:12,360
 en la que Roberto, desde disperso

8
00:00:12,360 --> 00:00:13,260
 tiempo escaso,

9
00:00:13,260 --> 00:00:14,790
 un podcast muy recomendable,

10
00:00:14,790 --> 00:00:16,530
 que al final del episodio dejamos

11
00:00:16,530 --> 00:00:17,120
 una promo,

12
00:00:17,120 --> 00:00:18,930
 preguntaba que qué distro le

13
00:00:18,930 --> 00:00:21,120
 recomendábamos para usar con KDE.

14
00:00:21,120 --> 00:00:23,070
 Yo ahí soy de la opinión de

15
00:00:23,070 --> 00:00:24,720
 que es muy difícil recomendar

16
00:00:24,720 --> 00:00:25,800
 una distro de Linux,

17
00:00:25,800 --> 00:00:27,290
 o una distro de Linux para un

18
00:00:27,290 --> 00:00:28,650
 entorno de escritorio en

19
00:00:28,650 --> 00:00:29,300
 completo,

20
00:00:29,300 --> 00:00:31,800
 porque necesitas saber el caso

21
00:00:31,800 --> 00:00:33,200
 de uso exacto.

22
00:00:33,200 --> 00:00:35,120
 Cada persona es un mundo, y hay

23
00:00:35,120 --> 00:00:36,840
 quien usa una distro para jugar,

24
00:00:36,840 --> 00:00:37,880
 quien la usa para ofimática,

25
00:00:37,880 --> 00:00:39,260
 quien la usa para servidores...

26
00:00:39,260 --> 00:00:40,540
 Entonces, quería traeros la

27
00:00:40,540 --> 00:00:42,610
 reflexión de que no hay una

28
00:00:42,610 --> 00:00:45,180
 mejor distro para usar con KDE,

29
00:00:45,180 --> 00:00:47,200
 igual que no hay una mejor distro

30
00:00:47,200 --> 00:00:48,600
 para usar de Linux.

31
00:00:48,600 --> 00:00:50,170
 Habrá una mejor distro para

32
00:00:50,170 --> 00:00:52,400
 vosotros, por muchas circunstancias.

33
00:00:52,400 --> 00:00:54,560
 Y habrá, puede que, una mejor

34
00:00:54,560 --> 00:00:57,040
 distro para una cosa en concreta.

35
00:00:57,040 --> 00:00:58,840
 O sea, la distro que esté muy

36
00:00:58,840 --> 00:01:00,760
 actualizada, que sea muy

37
00:01:00,760 --> 00:01:01,320
 estable,

38
00:01:01,320 --> 00:01:02,850
 pero una distro que lo tenga

39
00:01:02,850 --> 00:01:04,960
 todo, y que cubra todo, y que

40
00:01:04,960 --> 00:01:06,420
 sirva para todo el mundo,

41
00:01:06,420 --> 00:01:07,460
 no creo que exista.

42
00:01:07,460 --> 00:01:09,140
 O sea, yo por ejemplo le dije

43
00:01:09,140 --> 00:01:11,260
 que si buscaba mucha estabilidad,

44
00:01:11,260 --> 00:01:13,180
 y ya tenía experiencia con Debian,

45
00:01:13,180 --> 00:01:14,540
 y que no le apetecía liarse,

46
00:01:14,540 --> 00:01:15,800
 aprender cómo funciona el

47
00:01:15,800 --> 00:01:16,220
 sistema,

48
00:01:16,220 --> 00:01:17,670
 pues que KDE Neon podría ser

49
00:01:17,670 --> 00:01:18,820
 una buena opción.

50
00:01:18,820 --> 00:01:20,970
 Tiene base Debian, se basa en

51
00:01:20,970 --> 00:01:21,540
 Ubuntu,

52
00:01:21,540 --> 00:01:23,600
 y tiene KDE a la última, y te

53
00:01:23,600 --> 00:01:25,090
 olvidas más o menos de

54
00:01:25,090 --> 00:01:26,680
 administrar el sistema.

55
00:01:26,680 --> 00:01:28,880
 En cambio, si no tiene una base

56
00:01:28,880 --> 00:01:29,620
 de Debian,

57
00:01:29,620 --> 00:01:31,160
 y le apetece probar algo

58
00:01:31,160 --> 00:01:31,820
 diferente,

59
00:01:31,820 --> 00:01:34,780
 puede usar openSUSE SlowRoll.

60
00:01:34,780 --> 00:01:36,690
 Que es una cosa intermedia

61
00:01:36,690 --> 00:01:38,420
 entre una rolling pura,

62
00:01:38,420 --> 00:01:40,140
 y una estable pura,

63
00:01:40,140 --> 00:01:42,340
 y que openSUSE tiene un ecosistema

64
00:01:42,340 --> 00:01:45,260
 estupendo que está muy infravalorado.

65
00:01:45,260 --> 00:01:46,460
 Luego, si le gusta estar a la

66
00:01:46,460 --> 00:01:47,660
 última, a la última, a la

67
00:01:47,660 --> 00:01:48,020
 última,

68
00:01:48,020 --> 00:01:49,140
 pero no quiere liarse la mente

69
00:01:49,140 --> 00:01:50,960
 a la cabeza y ponerse una Arch Linux,

70
00:01:50,960 --> 00:01:52,500
 pues un Manjaro.

71
00:01:52,500 --> 00:01:54,140
 Y ahí tienes los últimos de KDE,

72
00:01:54,140 --> 00:01:55,220
 los últimos paquetes.

73
00:01:55,220 --> 00:01:56,940
 Aunque sí, es verdad que llevo

74
00:01:56,940 --> 00:01:59,480
 mucho tiempo escuchando maravillas

75
00:01:59,480 --> 00:02:00,920
 y cosas horribles de Manjaros.

76
00:02:00,920 --> 00:02:02,610
 Arch Linux es si tienes espíritu

77
00:02:02,610 --> 00:02:04,320
 de administrador de sistemas,

78
00:02:04,320 --> 00:02:05,630
 Gentoo, si te has pasado del

79
00:02:05,630 --> 00:02:07,040
 espíritu de administrador de

80
00:02:07,040 --> 00:02:08,720
 sistemas a más allá todavía,

81
00:02:08,720 --> 00:02:09,780
 y que si fuera un servidor

82
00:02:09,780 --> 00:02:10,600
 sería Debian.

83
00:02:10,600 --> 00:02:12,540
 Eso fue mi recomendación en

84
00:02:12,540 --> 00:02:13,320
 genérica,

85
00:02:13,320 --> 00:02:14,420
 con la advertencia de que

86
00:02:14,420 --> 00:02:15,640
 primero hay que conocer muy

87
00:02:15,640 --> 00:02:17,560
 bien a quién estás recomendando.

88
00:02:17,560 --> 00:02:18,870
 Lo bueno de Linux es que

89
00:02:18,870 --> 00:02:20,400
 siempre puedes probarla,

90
00:02:20,400 --> 00:02:21,550
 pero cuando no le vas a estar

91
00:02:21,550 --> 00:02:23,080
 diciendo a tu padre "prueba 7,

92
00:02:23,080 --> 00:02:23,960
 distribuciones,

93
00:02:23,960 --> 00:02:25,120
 y mira a ver cuál es la que

94
00:02:25,120 --> 00:02:25,960
 más te gusta",

95
00:02:25,960 --> 00:02:27,630
 pues sabiendo qué es lo que va

96
00:02:27,630 --> 00:02:28,720
 a hacer un usuario,

97
00:02:28,720 --> 00:02:30,720
 se puede dar una mejor recomendación.

98
00:02:30,720 --> 00:02:31,800
 Y también quería dejar un

99
00:02:31,800 --> 00:02:32,960
 poco así, caer,

100
00:02:32,960 --> 00:02:34,440
 de que tenemos que intentar ser

101
00:02:34,440 --> 00:02:35,680
 un poco menos fanáticos,

102
00:02:35,680 --> 00:02:37,300
 que está muy bien querer y

103
00:02:37,300 --> 00:02:39,400
 admirar lo que nosotros usamos,

104
00:02:39,400 --> 00:02:42,220
 pero cuidar un poco el "no hay

105
00:02:42,220 --> 00:02:43,280
 una mejor distro",

106
00:02:43,280 --> 00:02:45,200
 porque no es en esta conversación

107
00:02:45,200 --> 00:02:47,400
 de Mastodon, pero sí en otras,

108
00:02:47,400 --> 00:02:48,890
 he visto pestes sobre otras

109
00:02:48,890 --> 00:02:50,350
 distribuciones porque no son

110
00:02:50,350 --> 00:02:51,400
 las que nos gustan.

111
00:02:51,400 --> 00:02:52,890
 Y a veces esa distribución que

112
00:02:52,890 --> 00:02:54,400
 para nosotros es una castaña

113
00:02:54,400 --> 00:02:55,820
 es muy buena para otra persona

114
00:02:55,820 --> 00:02:57,120
 por otras circunstancias.

115
00:02:57,120 --> 00:02:58,940
 Y además esas distribuciones

116
00:02:58,940 --> 00:03:00,440
 normalmente son gratis,

117
00:03:00,440 --> 00:03:02,120
 no pagamos nada por ellas,

118
00:03:02,120 --> 00:03:03,670
 con lo cual también yo me quejaría

119
00:03:03,670 --> 00:03:04,790
 un poco con la boca un poco

120
00:03:04,790 --> 00:03:05,440
 más pequeña

121
00:03:05,440 --> 00:03:06,950
 sabiendo que eso lo hace gente

122
00:03:06,950 --> 00:03:08,040
 en su tiempo libre.

123
00:03:08,040 --> 00:03:09,430
 Una pequeña reflexión que

124
00:03:09,430 --> 00:03:11,240
 quería dejaros pre-navideña.

125
00:03:11,240 --> 00:03:12,630
 Luego quería anunciar, por si

126
00:03:12,630 --> 00:03:13,680
 hay algún oyente

127
00:03:13,680 --> 00:03:15,240
 aquí de este podcast que sea

128
00:03:15,240 --> 00:03:16,520
 sucio y que no lea la lista de

129
00:03:16,520 --> 00:03:17,320
 distribución,

130
00:03:17,320 --> 00:03:18,750
 que tenemos la reunión anual

131
00:03:18,750 --> 00:03:20,330
 el 20 de diciembre, que es el

132
00:03:20,330 --> 00:03:21,240
 miércoles que viene,

133
00:03:21,240 --> 00:03:22,430
 para que se lo apunten en el

134
00:03:22,430 --> 00:03:23,000
 calendario.

135
00:03:23,000 --> 00:03:26,000
 Y si no sois socios, pues yo me

136
00:03:26,000 --> 00:03:26,840
 haría sucio ya

137
00:03:26,840 --> 00:03:28,230
 y preguntaría si puedo asistir

138
00:03:28,230 --> 00:03:30,160
 y así iría conociendo a la

139
00:03:30,160 --> 00:03:30,160
 gente,

140
00:03:30,160 --> 00:03:31,410
 porque esta es una asociación

141
00:03:31,410 --> 00:03:32,200
 que está muy bien

142
00:03:32,200 --> 00:03:34,710
 y cuanta más gente os apuntéis,

143
00:03:34,710 --> 00:03:35,920
 mejor será.

144
00:03:35,920 --> 00:03:37,650
 Y luego también quería comentaros

145
00:03:37,650 --> 00:03:38,560
 que Grita,

146
00:03:38,560 --> 00:03:41,560
 una aplicación del proyecto KDE,

147
00:03:41,560 --> 00:03:43,570
 que es la leche y que tiene

148
00:03:43,570 --> 00:03:45,000
 mucha acogida

149
00:03:45,000 --> 00:03:48,160
 en el mundo empresarial, artístico,

150
00:03:48,160 --> 00:03:49,200
 en las universidades,

151
00:03:49,200 --> 00:03:51,160
 una de las que tiene la promoción

152
00:03:51,160 --> 00:03:51,800
 bien hecha

153
00:03:51,800 --> 00:03:53,080
 y no es de las que no se conoce,

154
00:03:53,080 --> 00:03:54,200
 que hace maravillas

155
00:03:54,200 --> 00:03:55,280
 y está ahí escondida

156
00:03:55,280 --> 00:03:56,680
 esperando que la descubran.

157
00:03:56,680 --> 00:03:58,160
 Pues ha conseguido un nuevo

158
00:03:58,160 --> 00:03:59,970
 sponsor que se llama Broken

159
00:03:59,970 --> 00:04:00,480
 Rules

160
00:04:00,480 --> 00:04:01,980
 y es un ejemplo de cómo

161
00:04:01,980 --> 00:04:03,880
 algunos proyectos de software

162
00:04:03,880 --> 00:04:04,200
 libre

163
00:04:04,200 --> 00:04:06,670
 sí que consiguen la visibilidad

164
00:04:06,670 --> 00:04:08,600
 y la sostenibilidad económica

165
00:04:08,600 --> 00:04:10,350
 que nos gustaría tener en

166
00:04:10,350 --> 00:04:11,000
 otros.

167
00:04:11,000 --> 00:04:12,970
 Luego también quería comentaros

168
00:04:12,970 --> 00:04:14,560
 que si lleváis mucho tiempo

169
00:04:14,560 --> 00:04:16,990
 sin que os aparezca en la aplicación

170
00:04:16,990 --> 00:04:17,760
 de podcast,

171
00:04:17,760 --> 00:04:20,180
 un episodio de Compilando

172
00:04:20,180 --> 00:04:20,960
 Podcast,

173
00:04:20,960 --> 00:04:22,700
 que sepáis que Paco, después

174
00:04:22,700 --> 00:04:23,560
 de un parón grande,

175
00:04:23,560 --> 00:04:25,340
 volvió a publicar episodios,

176
00:04:25,340 --> 00:04:26,950
 pero ha cambiado la URL del

177
00:04:26,950 --> 00:04:27,560
 feed.

178
00:04:27,560 --> 00:04:29,470
 Entonces, si todavía tenéis

179
00:04:29,470 --> 00:04:30,840
 Compilando.audio,

180
00:04:30,840 --> 00:04:32,130
 por ejemplo, en Antennapod, tú

181
00:04:32,130 --> 00:04:33,840
 puedes cambiar la URL del feed,

182
00:04:33,840 --> 00:04:35,640
 te avisa de que te puedes cargar

183
00:04:35,640 --> 00:04:36,000
 el feed,

184
00:04:36,000 --> 00:04:38,220
 pero vamos, yo lo hice, quitas

185
00:04:38,220 --> 00:04:41,080
 .audio, pones .es

186
00:04:41,080 --> 00:04:42,740
 y ya tendrías el feed de nuevo

187
00:04:42,740 --> 00:04:43,560
 funcionando

188
00:04:43,560 --> 00:04:45,500
 y disfrutando de sus episodios,

189
00:04:45,500 --> 00:04:46,700
 que ahora son episodios más

190
00:04:46,700 --> 00:04:47,280
 cortitos.

191
00:04:47,280 --> 00:04:48,520
 Otra opción es que

192
00:04:48,520 --> 00:04:50,720
 directamente borréis el feed

193
00:04:50,720 --> 00:04:52,560
 antiguo y pongáis el nuevo,

194
00:04:52,560 --> 00:04:53,940
 pero como yo soy un fan de las

195
00:04:53,940 --> 00:04:54,800
 estadísticas,

196
00:04:54,800 --> 00:04:56,180
 no quería perder los que ya

197
00:04:56,180 --> 00:04:57,120
 tenía escuchado,

198
00:04:57,120 --> 00:04:58,110
 que creo que son todos, pero

199
00:04:58,110 --> 00:04:59,460
 bueno, así me cuenta para las

200
00:04:59,460 --> 00:05:00,280
 estadísticas

201
00:05:00,280 --> 00:05:02,060
 y me salen estos nuevos que ha

202
00:05:02,060 --> 00:05:04,480
 puesto ahora de Antennapod Echo,

203
00:05:04,480 --> 00:05:06,300
 que es un poco parecido al Grub

204
00:05:06,300 --> 00:05:07,640
 que te hacen algunas aplicaciones

205
00:05:07,640 --> 00:05:09,000
 y te hace Spotify.

206
00:05:09,000 --> 00:05:11,020
 Si vuestra aplicación lo hace,

207
00:05:11,020 --> 00:05:12,800
 buena manera de darle publicidad

208
00:05:12,800 --> 00:05:14,240
 si vuestros podcast que escucháis

209
00:05:14,240 --> 00:05:15,420
 son algo que se pueda dar

210
00:05:15,420 --> 00:05:15,920
 publicidad,

211
00:05:15,920 --> 00:05:17,280
 si son sobre cosas de software

212
00:05:17,280 --> 00:05:18,500
 libre y no son cosas raras que

213
00:05:18,500 --> 00:05:20,080
 no queráis que la gente sepa.

214
00:05:20,080 --> 00:05:21,360
 La última cosa que quería

215
00:05:21,360 --> 00:05:22,590
 comentaros es que aunque yo

216
00:05:22,590 --> 00:05:24,240
 estoy muy en contra del consumismo,

217
00:05:24,240 --> 00:05:26,600
 comprar por comprar, las navidades,

218
00:05:26,600 --> 00:05:27,560
 tiene que ser un periodo en el

219
00:05:27,560 --> 00:05:29,040
 que hay que comprar 5.000 cosas

220
00:05:29,040 --> 00:05:30,360
 y regalar 5.000 cosas,

221
00:05:30,360 --> 00:05:31,810
 cuando hacen falta cosas, está

222
00:05:31,810 --> 00:05:32,970
 bien comprarlas y está bien

223
00:05:32,970 --> 00:05:33,600
 investigarlas

224
00:05:33,600 --> 00:05:35,320
 y cosas que sean buenas que duran.

225
00:05:35,320 --> 00:05:37,120
 Pues cosas que son buenas y que

226
00:05:37,120 --> 00:05:37,520
 duren,

227
00:05:37,520 --> 00:05:40,350
 Slimbook acaba de actualizar su

228
00:05:40,350 --> 00:05:40,680
 web

229
00:05:40,680 --> 00:05:42,570
 y ahí tenéis disponible, como

230
00:05:42,570 --> 00:05:45,200
 sabréis, KDE Slimbook,

231
00:05:45,200 --> 00:05:46,640
 que es un portátil que está muy

232
00:05:46,640 --> 00:05:48,940
 bien y que una parte de la

233
00:05:48,940 --> 00:05:50,080
 compra

234
00:05:50,080 --> 00:05:51,760
 va directamente al proyecto KD

235
00:05:51,760 --> 00:05:53,450
 Internacional para poder pagar

236
00:05:53,450 --> 00:05:54,600
 a los desarrolladores.

237
00:05:54,600 --> 00:05:55,800
 Con lo cual, si estáis

238
00:05:55,800 --> 00:05:57,280
 pensando en renovar equipos,

239
00:05:57,280 --> 00:05:58,580
 un recordatorio que yo creo que

240
00:05:58,580 --> 00:05:59,760
 no lo hacemos suficiente es que

241
00:05:59,760 --> 00:06:00,920
 tenemos

242
00:06:00,920 --> 00:06:03,320
 empresas en España que podemos

243
00:06:03,320 --> 00:06:04,360
 comprar local,

244
00:06:04,360 --> 00:06:05,810
 productos que respetan el

245
00:06:05,810 --> 00:06:06,720
 software libre

246
00:06:06,720 --> 00:06:08,110
 y empresas que invierten en la

247
00:06:08,110 --> 00:06:09,720
 comunidad del software libre.

248
00:06:09,720 --> 00:06:11,400
 Si os hace falta, mirad la

249
00:06:11,400 --> 00:06:12,880
 nueva página de Slimbook.

250
00:06:12,880 --> 00:06:15,000
 Si no os hace falta, reutilizad

251
00:06:15,000 --> 00:06:16,920
 vuestro ordenador hasta que podáis

252
00:06:16,920 --> 00:06:18,530
 y cuando sea el momento, ya

253
00:06:18,530 --> 00:06:20,360
 investigáis qué se ajusta a

254
00:06:20,360 --> 00:06:21,640
 vuestras necesidades.

255
00:06:21,640 --> 00:06:23,080
 Por cierto, casi se me olvida,

256
00:06:23,080 --> 00:06:24,920
 el 28 de febrero, como si

257
00:06:24,920 --> 00:06:26,360
 escucháis este podcast seguro

258
00:06:26,360 --> 00:06:26,960
 que sabéis,

259
00:06:26,960 --> 00:06:30,210
 sale KDE 6 y se está empezando

260
00:06:30,210 --> 00:06:31,120
 a organizar

261
00:06:31,120 --> 00:06:33,250
 una especie de quedadas, no

262
00:06:33,250 --> 00:06:34,720
 llegan a ser install party,

263
00:06:34,720 --> 00:06:36,350
 pero se ve que antiguamente, a

264
00:06:36,350 --> 00:06:37,920
 mí no me pilló todavía enganchar

265
00:06:37,920 --> 00:06:38,640
 a la comunidad,

266
00:06:38,640 --> 00:06:40,890
 cuando salía una versión muy

267
00:06:40,890 --> 00:06:43,560
 grande del proyecto KDE o de Plasma,

268
00:06:43,560 --> 00:06:45,550
 se hacían como fiestas, reuniones,

269
00:06:45,550 --> 00:06:46,600
 para celebrarlo.

270
00:06:46,600 --> 00:06:48,520
 Pues se ha propuesto retomar

271
00:06:48,520 --> 00:06:49,200
 esta idea,

272
00:06:49,200 --> 00:06:50,540
 con lo cual para la semana del

273
00:06:50,540 --> 00:06:52,280
 28 de febrero o la primera de

274
00:06:52,280 --> 00:06:52,640
 marzo,

275
00:06:52,640 --> 00:06:53,730
 no tiene que ser el día en

276
00:06:53,730 --> 00:06:54,680
 concreto que sale,

277
00:06:54,680 --> 00:06:55,980
 podéis organizar una quedada

278
00:06:55,980 --> 00:06:57,880
 con amigos o actualizar juntos

279
00:06:57,880 --> 00:07:00,520
 o quedar a tomar cerveza, a ser

280
00:07:00,520 --> 00:07:01,760
 posible sin alcohol.

281
00:07:01,760 --> 00:07:03,860
 Diciendo que es lo que más os

282
00:07:03,860 --> 00:07:05,880
 gusta de esta nueva versión,

283
00:07:05,880 --> 00:07:07,230
 yo en concreto soy alguien en

284
00:07:07,230 --> 00:07:08,240
 la región de Murcia,

285
00:07:08,240 --> 00:07:10,330
 que quiera juntarse a ver cómo

286
00:07:10,330 --> 00:07:12,880
 actualizamos algún equipo,

287
00:07:12,880 --> 00:07:14,200
 cómo instalamos de cero,

288
00:07:14,200 --> 00:07:15,440
 o simplemente nos llevamos los

289
00:07:15,440 --> 00:07:16,520
 equipos ya actualizados

290
00:07:16,520 --> 00:07:17,830
 y jugamos un poco con las

291
00:07:17,830 --> 00:07:18,720
 nuevas cosas,

292
00:07:18,720 --> 00:07:20,320
 estoy dispuesto, podéis

293
00:07:20,320 --> 00:07:22,000
 contactarme con Mastodon.

294
00:07:22,000 --> 00:07:23,250
 Y ya con esto sí que cierro el

295
00:07:23,250 --> 00:07:24,000
 episodio de hoy,

296
00:07:24,000 --> 00:07:26,760
 si no nos escuchamos antes,

297
00:07:26,760 --> 00:07:28,690
 felices fiestas como quiera que

298
00:07:28,690 --> 00:07:30,200
 lo celebréis vosotros,

299
00:07:30,200 --> 00:07:32,370
 y os dejo con la promo de

300
00:07:32,370 --> 00:07:34,440
 Roberto, que espero que os guste.

301
00:07:34,440 --> 00:07:35,200
 Hasta luego.

302
00:07:35,200 --> 00:07:37,310
 Buenas, bienvenidos a Disperso,

303
00:07:37,310 --> 00:07:38,440
 Disperso Tiempo Escaso,

304
00:07:38,440 --> 00:07:39,930
 un podcast que grabo cuando

305
00:07:39,930 --> 00:07:41,080
 buenamente puedo.

306
00:07:41,080 --> 00:07:44,360
 Vamos a hablar un poco de Mastodon,

307
00:07:44,360 --> 00:07:45,950
 viendo una película y jugando

308
00:07:45,950 --> 00:07:46,920
 un videojuego.

309
00:07:46,920 --> 00:07:48,400
 Eso, eso es una redirección.

310
00:07:48,400 --> 00:07:49,590
 Les voy a explicar cómo me

311
00:07:49,590 --> 00:07:52,390
 planifico usando un simple archivo

312
00:07:52,390 --> 00:07:53,400
 de texto.

313
00:07:53,400 --> 00:07:54,180
 La red se está cambiando

314
00:07:54,180 --> 00:07:54,720
 muchísimo.

315
00:07:54,720 --> 00:07:56,480
 Clientes de Twitter.

316
00:07:56,480 --> 00:07:58,650
 Obsidian sobre inteligencia

317
00:07:58,650 --> 00:07:59,360
 artificial.

318
00:07:59,360 --> 00:08:00,560
 Tecnología Lofi.

319
00:08:00,560 --> 00:08:03,640
 Un par de cositas sobre el feriverso.

320
00:08:03,640 --> 00:08:05,090
 Que hasta ahora estaba en mi

321
00:08:05,090 --> 00:08:05,520
 cabeza,

322
00:08:05,520 --> 00:08:06,770
 y que ahora está en la de

323
00:08:06,770 --> 00:08:07,840
 todos vosotros.

324
00:08:07,840 --> 00:08:09,280
 tiemposcaso.com

325
00:08:09,280 --> 00:08:10,280
 Nos escuchamos pronto.

