1
00:00:00,980 --> 00:00:02,730
 Buenas, este es el podcast KDE

2
00:00:02,730 --> 00:00:04,640
 Express, en el que en audios de

3
00:00:04,640 --> 00:00:07,180
 máximo 15-30 minutos os hablaremos

4
00:00:07,180 --> 00:00:09,060
 de noticias, eventos, entrevistas,

5
00:00:09,060 --> 00:00:10,380
 novedades e información en

6
00:00:10,380 --> 00:00:11,940
 general sobre el software libre

7
00:00:11,940 --> 00:00:13,840
 y en particular sobre KDE Plasma,

8
00:00:13,840 --> 00:00:15,630
 Dolphin, Okular, Kdenlive,

9
00:00:15,630 --> 00:00:17,950
 Konsole, Neochat, Kate, Krita,

10
00:00:17,950 --> 00:00:19,240
 Akademy y la infinidad de

11
00:00:19,240 --> 00:00:20,740
 proyectos que tenemos en esta

12
00:00:20,740 --> 00:00:22,000
 familia de KDE.

