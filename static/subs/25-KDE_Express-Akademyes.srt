1
00:00:00,000 --> 00:00:02,390
Buenas, soy David Marzal, estoy

2
00:00:02,390 --> 00:00:04,290
grabando el 28 de marzo, y esto

3
00:00:04,290 --> 00:00:06,000
es KDE Express 25.

4
00:00:06,000 --> 00:00:07,470
Hoy voy a hablaros sobre los

5
00:00:07,470 --> 00:00:09,440
eventos que tenemos por delante,

6
00:00:09,440 --> 00:00:11,240
algo sobre Plasma 6,

7
00:00:11,240 --> 00:00:12,300
y ponernos al día de un

8
00:00:12,300 --> 00:00:13,650
montón de noticias que se me

9
00:00:13,650 --> 00:00:15,240
han ido acumulando este mes.

10
00:00:15,240 --> 00:00:16,940
Akademy-es está a la vuelta de

11
00:00:16,940 --> 00:00:18,770
la esquina, cuando escuchéis

12
00:00:18,770 --> 00:00:20,600
esto se habrá acabado el plazo

13
00:00:20,600 --> 00:00:21,920
para enviar propuestas,

14
00:00:21,920 --> 00:00:23,680
pero todavía está abierto el

15
00:00:23,680 --> 00:00:25,600
plazo para ser patrocinador.

16
00:00:25,600 --> 00:00:27,250
En esLibre, que es donde se va a

17
00:00:27,250 --> 00:00:29,060
realizar Akademy, ahora mismo

18
00:00:29,060 --> 00:00:31,400
hay un montón de charlas aprobadas.

19
00:00:31,400 --> 00:00:33,610
Todavía se puede enviar alguna

20
00:00:33,610 --> 00:00:35,360
ponencia, pero la verdad es que

21
00:00:35,360 --> 00:00:36,720
hay un montón, un montón,

22
00:00:36,720 --> 00:00:38,370
y si no os dais prisa, yo no

23
00:00:38,370 --> 00:00:40,360
sé si va a ser posible porque

24
00:00:40,360 --> 00:00:42,350
en las naves de Valencia no sé

25
00:00:42,350 --> 00:00:44,360
si van a caber tantos ponentes.

26
00:00:44,360 --> 00:00:45,720
Lo cual es muy buena señal,

27
00:00:45,720 --> 00:00:47,360
significa que hay buena salud

28
00:00:47,360 --> 00:00:49,040
en el mundo organizativo de

29
00:00:49,040 --> 00:00:51,350
eventos de GNU/Linux y Software

30
00:00:51,350 --> 00:00:52,800
Libre, y cultura en general.

31
00:00:52,800 --> 00:00:54,580
Os voy a poner en las notas del

32
00:00:54,580 --> 00:00:56,410
programa un enlace a Mancomún

33
00:00:56,410 --> 00:00:57,040
Podcast,

34
00:00:57,040 --> 00:00:58,930
donde hemos grabado un episodio

35
00:00:58,930 --> 00:01:00,910
especial hablando del evento, y

36
00:01:00,910 --> 00:01:02,340
así no tengo que repetirlo y

37
00:01:02,340 --> 00:01:03,680
enrollarme por aquí.

38
00:01:03,680 --> 00:01:05,330
También quiero poneros al día,

39
00:01:05,330 --> 00:01:06,350
porque como hace bastante que

40
00:01:06,350 --> 00:01:07,640
no grabo porque me han estado

41
00:01:07,640 --> 00:01:09,720
liando en otros programas,

42
00:01:09,720 --> 00:01:14,160
que estamos con KDE Gears 24.02.1,

43
00:01:14,160 --> 00:01:15,870
que si ya todo el mundo sabréis

44
00:01:15,870 --> 00:01:17,650
que salió Plasma 6 con todo el

45
00:01:17,650 --> 00:01:19,010
mega lanzamiento de los

46
00:01:19,010 --> 00:01:21,090
frameworks y los Gears portados

47
00:01:21,090 --> 00:01:21,720
a Qt 6,

48
00:01:21,720 --> 00:01:22,910
y ya empezamos a tener las

49
00:01:22,910 --> 00:01:24,410
versiones de arreglo, que es

50
00:01:24,410 --> 00:01:25,860
donde podéis estar más seguros

51
00:01:25,860 --> 00:01:27,110
de que el software es todavía

52
00:01:27,110 --> 00:01:27,920
más estable.

53
00:01:27,920 --> 00:01:31,740
En la 24.02.1 por ejemplo, Dolphin

54
00:01:31,740 --> 00:01:33,250
recupera el cambio de color en

55
00:01:33,250 --> 00:01:34,800
su icono, según el escritorio,

56
00:01:34,800 --> 00:01:36,520
esto que tiene KDE, de que si

57
00:01:36,520 --> 00:01:38,600
configuras que tú quieres que

58
00:01:38,600 --> 00:01:41,060
los colores, el tema del color,

59
00:01:41,060 --> 00:01:43,100
se ajuste al fondo de pantalla

60
00:01:43,100 --> 00:01:43,840
que tienes,

61
00:01:43,840 --> 00:01:45,720
pues te va cambiando los colorinchis

62
00:01:45,720 --> 00:01:47,100
que hay en la barra de tarea o

63
00:01:47,100 --> 00:01:48,520
en las barras de menú.

64
00:01:48,520 --> 00:01:49,740
A mí la verdad es que me gusta

65
00:01:49,740 --> 00:01:51,160
mucho. Eso parece ser que dejó

66
00:01:51,160 --> 00:01:52,510
de funcionar y ya rápidamente

67
00:01:52,510 --> 00:01:53,440
lo han arreglado.

68
00:01:53,440 --> 00:01:54,640
Por supuesto hay muchísimas

69
00:01:54,640 --> 00:01:56,440
más arreglas, pero yo dejo en

70
00:01:56,440 --> 00:01:59,000
las notas del programa el enlace.

71
00:01:59,000 --> 00:01:59,870
Yo la verdad es que no he

72
00:01:59,870 --> 00:02:01,000
encontrado prácticamente

73
00:02:01,000 --> 00:02:02,500
ningún bug, pero según por lo

74
00:02:02,500 --> 00:02:03,920
que han escrito, ha habido unos

75
00:02:03,920 --> 00:02:04,400
cuantos.

76
00:02:04,400 --> 00:02:06,430
Y hablando de bugs, pues KDE Plasma

77
00:02:06,430 --> 00:02:08,600
después del lanzamiento grandioso

78
00:02:08,600 --> 00:02:09,320
que tuvimos,

79
00:02:09,320 --> 00:02:11,130
que fue bastante bien, quitando

80
00:02:11,130 --> 00:02:12,740
algún errorcillo en KDE Neon

81
00:02:12,740 --> 00:02:14,280
que se solucionó pronto,

82
00:02:14,280 --> 00:02:16,730
está ya por la 6.0.3, que eso

83
00:02:16,730 --> 00:02:18,380
significa que ya tienen dos

84
00:02:18,380 --> 00:02:20,280
semanas de arreglo de toda la

85
00:02:20,280 --> 00:02:21,920
gente que rápidamente instaló

86
00:02:21,920 --> 00:02:23,280
con prisa, como yo.

87
00:02:23,280 --> 00:02:24,670
Ya han ido reportando, los

88
00:02:24,670 --> 00:02:26,380
desarrolladores han puesto mano

89
00:02:26,380 --> 00:02:28,960
a la obra, y en la 6.0.3 yo lo

90
00:02:28,960 --> 00:02:31,120
defino como la versión que le

91
00:02:31,120 --> 00:02:32,440
podría poner ya a mi madre.

92
00:02:32,440 --> 00:02:33,820
Es evidente que después de un

93
00:02:33,820 --> 00:02:35,470
cambio tan gordo, algunos bugs

94
00:02:35,470 --> 00:02:37,450
tenían que salir que no lo habíamos

95
00:02:37,450 --> 00:02:39,120
podido sacar en la fase de beta,

96
00:02:39,120 --> 00:02:40,720
pero ahora, como no hemos tirado

97
00:02:40,720 --> 00:02:42,240
dos semanas, venga a reportar y

98
00:02:42,240 --> 00:02:43,900
venga a arreglar, yo creo que

99
00:02:43,900 --> 00:02:45,200
esto ya es como una versión

100
00:02:45,200 --> 00:02:46,320
normal y corriente,

101
00:02:46,320 --> 00:02:47,990
y hemos pasado el pico de que

102
00:02:47,990 --> 00:02:50,130
acabamos de cambiar prácticamente

103
00:02:50,130 --> 00:02:51,930
el motor entero del coche, y

104
00:02:51,930 --> 00:02:53,720
podemos decir que esto está

105
00:02:53,720 --> 00:02:54,600
marchando.

106
00:02:54,600 --> 00:02:56,180
Luego sobre Plasma 6, no quiero

107
00:02:56,180 --> 00:02:57,600
entrar mucho en detalle porque

108
00:02:57,600 --> 00:02:58,900
ahora mismo hay un montón de

109
00:02:58,900 --> 00:03:00,380
gente haciendo vídeos, la

110
00:03:00,380 --> 00:03:01,680
gente lo está probando,

111
00:03:01,680 --> 00:03:03,340
pero como yo siempre digo,

112
00:03:03,340 --> 00:03:05,600
aquí estamos los Early Adopters,

113
00:03:05,600 --> 00:03:06,770
los que nos gusta mucho mucho

114
00:03:06,770 --> 00:03:07,850
la tecnología y estar a la

115
00:03:07,850 --> 00:03:08,400
última.

116
00:03:08,400 --> 00:03:09,490
Pero hay un montón de gente

117
00:03:09,490 --> 00:03:10,700
que Plasma 6 no lo verá hasta

118
00:03:10,700 --> 00:03:12,210
dentro de un año, entonces yo

119
00:03:12,210 --> 00:03:13,400
prefiero ir poco a poco

120
00:03:13,400 --> 00:03:15,680
hablando algo de las novedades,

121
00:03:15,680 --> 00:03:17,430
pero no hacer un pedazo así de

122
00:03:17,430 --> 00:03:19,130
episodio exclusivo en el que

123
00:03:19,130 --> 00:03:20,810
hago uno a uno cuáles son las

124
00:03:20,810 --> 00:03:22,520
cosas que han cambiado.

125
00:03:22,520 --> 00:03:24,040
Sí que quiero reseñar que os

126
00:03:24,040 --> 00:03:25,690
dejo el enlace a la publicación

127
00:03:25,690 --> 00:03:27,370
oficial donde vienen un montón

128
00:03:27,370 --> 00:03:29,080
de cambios que se han hecho.

129
00:03:29,080 --> 00:03:31,120
He grabado hace poco en PeerTube

130
00:03:31,120 --> 00:03:32,810
un vídeo enseñándoles a

131
00:03:32,810 --> 00:03:35,300
José Jiménez de 24H24L un

132
00:03:35,300 --> 00:03:36,720
poco alguna novedad y alguna

133
00:03:36,720 --> 00:03:37,560
cosa que se ha hecho,

134
00:03:37,560 --> 00:03:38,810
y hablando de cómo ha ido la

135
00:03:38,810 --> 00:03:40,920
migración. Os dejo también el

136
00:03:40,920 --> 00:03:42,320
enlace en las notas.

137
00:03:42,320 --> 00:03:44,190
Y en las notas van a ver un

138
00:03:44,190 --> 00:03:46,990
montón de URLs en las que tenéis

139
00:03:46,990 --> 00:03:49,050
"User Facing Changes", que esto

140
00:03:49,050 --> 00:03:51,000
es la página oficial de Plasma,

141
00:03:51,000 --> 00:03:53,340
la oficial hacia adentro, no es

142
00:03:53,340 --> 00:03:55,850
la que promoción publicita, en

143
00:03:55,850 --> 00:03:57,700
la que vienen todos los cambios

144
00:03:57,700 --> 00:03:59,260
uno a uno con enlace a por qué

145
00:03:59,260 --> 00:04:00,400
se ha hecho y cómo,

146
00:04:00,400 --> 00:04:02,260
por si tenéis realmente curiosidad

147
00:04:02,260 --> 00:04:03,820
por saber hasta la última miga

148
00:04:03,820 --> 00:04:05,440
de cambios que hemos tenido.

149
00:04:05,440 --> 00:04:06,650
Yo querría destacar, por

150
00:04:06,650 --> 00:04:07,980
ejemplo, aparte de que todo el

151
00:04:07,980 --> 00:04:09,240
mundo está muy contento con

152
00:04:09,240 --> 00:04:10,280
que ha vuelto el cubo,

153
00:04:10,280 --> 00:04:12,010
que ahora hay un efecto nuevo

154
00:04:12,010 --> 00:04:13,730
que se llama como agitar para

155
00:04:13,730 --> 00:04:15,770
encontrar el ratón, que yo lo

156
00:04:15,770 --> 00:04:16,480
he activado,

157
00:04:16,480 --> 00:04:17,580
que es que si una vez que lo

158
00:04:17,580 --> 00:04:18,800
tienes activado mueves muy

159
00:04:18,800 --> 00:04:20,480
rápido el ratón, el ratón

160
00:04:20,480 --> 00:04:21,960
empieza a crecer, a crecer, a

161
00:04:21,960 --> 00:04:22,000
crecer...

162
00:04:22,000 --> 00:04:23,060
Bueno, el ratón no, el icono

163
00:04:23,060 --> 00:04:24,700
del ratón. Y entonces lo ves

164
00:04:24,700 --> 00:04:25,640
muy fácil.

165
00:04:25,640 --> 00:04:26,740
Si no sabes por dónde está

166
00:04:26,740 --> 00:04:27,940
por la pantalla, tú empiezas a

167
00:04:27,940 --> 00:04:29,280
menear, a menear, a menear y se

168
00:04:29,280 --> 00:04:30,480
hace gigante y dices "aquí

169
00:04:30,480 --> 00:04:31,240
está, sin duda".

170
00:04:31,240 --> 00:04:32,360
Y luego desaparece en un

171
00:04:32,360 --> 00:04:33,770
segundo, vuelve a su tamaño

172
00:04:33,770 --> 00:04:34,480
original.

173
00:04:34,480 --> 00:04:36,120
También os dejo un artículo

174
00:04:36,120 --> 00:04:37,770
sobre cómo fue la migración

175
00:04:37,770 --> 00:04:39,200
de KDE Neon a Plasma 6.

176
00:04:39,200 --> 00:04:41,090
El 90% de la gente que está

177
00:04:41,090 --> 00:04:43,130
usando Plasma 6 está contento

178
00:04:43,130 --> 00:04:44,880
con él y no tiene demasiados

179
00:04:44,880 --> 00:04:45,880
problemas.

180
00:04:45,880 --> 00:04:48,070
Siempre están los que si tengo

181
00:04:48,070 --> 00:04:50,180
envidia y tengo esta versión

182
00:04:50,180 --> 00:04:52,160
de los drivers que es muy antigua

183
00:04:52,160 --> 00:04:54,360
porque mi placa ya no lo soportan...

184
00:04:54,360 --> 00:04:55,680
Y entonces ahí con Wyland

185
00:04:55,680 --> 00:04:57,670
puede ser... Siempre hay casos,

186
00:04:57,670 --> 00:04:59,130
pero vamos, para la mayoría de

187
00:04:59,130 --> 00:05:00,560
la gente tendría que funcionar.

188
00:05:00,560 --> 00:05:03,020
OpenSUSE, openSUSE, Tumbleweed

189
00:05:03,020 --> 00:05:05,000
y Kalpa también están ya en

190
00:05:05,000 --> 00:05:05,880
Plasma 6.

191
00:05:05,880 --> 00:05:08,250
También ha llegado a PostMarket

192
00:05:08,250 --> 00:05:10,280
OS, que es una versión para

193
00:05:10,280 --> 00:05:11,000
móviles.

194
00:05:11,000 --> 00:05:13,000
Y luego también os dejo un enlace

195
00:05:13,000 --> 00:05:14,790
a una charla que tuve con José

196
00:05:14,790 --> 00:05:16,490
Picón y su equipo, que tiene

197
00:05:16,490 --> 00:05:18,800
un nuevo podcast que se llama "WeHacks"

198
00:05:18,800 --> 00:05:19,680
sobre privacidad.

199
00:05:19,680 --> 00:05:21,490
Y me invitaron para hablar un

200
00:05:21,490 --> 00:05:23,740
poco sobre KDE, sobre Plasma 6

201
00:05:23,740 --> 00:05:25,190
y en ese momento también hablamos

202
00:05:25,190 --> 00:05:26,670
sobre el drama que había con

203
00:05:26,670 --> 00:05:27,940
Telegram, que ya lo tenemos

204
00:05:27,940 --> 00:05:28,600
superado.

205
00:05:28,600 --> 00:05:30,500
Eso era el montón de enlaces y

206
00:05:30,500 --> 00:05:32,570
aquí os lo quería mencionar,

207
00:05:32,570 --> 00:05:34,320
que os he dejado en las notas

208
00:05:34,320 --> 00:05:35,320
del programa.

209
00:05:35,320 --> 00:05:36,530
Y luego quería entrar en una

210
00:05:36,530 --> 00:05:37,720
serie de noticias que no van

211
00:05:37,720 --> 00:05:39,090
directamente sobre Plasma 6,

212
00:05:39,090 --> 00:05:40,880
pero que sí van sobre el ecosistema.

213
00:05:40,880 --> 00:05:42,280
Por ejemplo, "Muy Luino" hizo

214
00:05:42,280 --> 00:05:43,760
un artículo que a mí me gustó

215
00:05:43,760 --> 00:05:44,990
mucho sobre cómo cambiar el

216
00:05:44,990 --> 00:05:46,720
tamaño de los archivos en Dolphin.

217
00:05:46,720 --> 00:05:48,790
De aparte entraba en cómo Dolphin

218
00:05:48,790 --> 00:05:50,650
es una aplicación super útil

219
00:05:50,650 --> 00:05:52,550
de productividad y que en el

220
00:05:52,550 --> 00:05:54,640
momento que la usas te enamoras

221
00:05:54,640 --> 00:05:56,220
de ella, si eres de los que te

222
00:05:56,220 --> 00:05:58,970
gusta personalizar e intentar

223
00:05:58,970 --> 00:06:00,120
conseguir ser más productivo

224
00:06:00,120 --> 00:06:01,330
cuando estás haciendo trabajo

225
00:06:01,330 --> 00:06:01,920
ofimático.

226
00:06:01,920 --> 00:06:04,300
Luego también la NLNET, que es

227
00:06:04,300 --> 00:06:06,460
una pata, una sección que hay

228
00:06:06,460 --> 00:06:08,840
de la Unión Europea que se dedica

229
00:06:08,840 --> 00:06:11,220
a fundar proyectos de soberanía

230
00:06:11,220 --> 00:06:12,400
tecnológica.

231
00:06:12,400 --> 00:06:14,590
Ha aceptado una propuesta para

232
00:06:14,590 --> 00:06:17,300
mejorar la accesibilidad de KDE Plasma

233
00:06:17,300 --> 00:06:18,880
con Wayland, que es una cosa

234
00:06:18,880 --> 00:06:19,890
que a mí me alegra mucho

235
00:06:19,890 --> 00:06:22,110
porque KDE Plasma es maravilloso,

236
00:06:22,110 --> 00:06:23,540
la versión 6 es la mejor que

237
00:06:23,540 --> 00:06:24,800
hemos tenido, pero tenemos

238
00:06:24,800 --> 00:06:25,950
mucho trabajo que hacer en

239
00:06:25,950 --> 00:06:26,720
accesibilidad.

240
00:06:26,720 --> 00:06:28,100
Y esto nos va a ayudar mucho

241
00:06:28,100 --> 00:06:30,000
porque el trabajo de accesibilidad

242
00:06:30,000 --> 00:06:31,500
es duro, requiere gente que

243
00:06:31,500 --> 00:06:33,630
reporte, gente que pueda probarlo

244
00:06:33,630 --> 00:06:35,520
y desarrolladores que puedan

245
00:06:35,520 --> 00:06:37,360
tener el tiempo de hacerlo

246
00:06:37,360 --> 00:06:38,530
cuando no es de los casos más

247
00:06:38,530 --> 00:06:39,520
fáciles de programar.

248
00:06:39,520 --> 00:06:41,010
Con lo cual el dinero siempre

249
00:06:41,010 --> 00:06:42,440
es bien recibido y la ayuda de

250
00:06:42,440 --> 00:06:44,320
voluntarios también, claro.

251
00:06:44,320 --> 00:06:46,110
Yo tengo previsto hacer un

252
00:06:46,110 --> 00:06:48,220
vídeo, no sé cuándo, pero

253
00:06:48,220 --> 00:06:50,170
quiero hacer uno con todas las

254
00:06:50,170 --> 00:06:52,350
posibilidades de KDE Plasma que

255
00:06:52,350 --> 00:06:54,640
tiene en materia de accesibilidad.

256
00:06:54,640 --> 00:06:55,840
Yo estoy probando ya algunas

257
00:06:55,840 --> 00:06:57,090
aquí en mi casa, algunas son

258
00:06:57,090 --> 00:06:58,490
combinaciones de teclas, otras

259
00:06:58,490 --> 00:06:59,850
son configuraciones que dejas

260
00:06:59,850 --> 00:07:00,160
puestas.

261
00:07:00,160 --> 00:07:01,910
Esto mismo que comentaba antes

262
00:07:01,910 --> 00:07:03,570
de mover el ratón, la verdad

263
00:07:03,570 --> 00:07:05,370
es que puede ayudar, pero haré

264
00:07:05,370 --> 00:07:07,430
un listado y haré una demostración

265
00:07:07,430 --> 00:07:09,020
una a una de cómo se activan y

266
00:07:09,020 --> 00:07:10,400
cómo funcionan.

267
00:07:10,400 --> 00:07:12,190
También tenemos que nuestro

268
00:07:12,190 --> 00:07:14,750
querido Slimbook, que no es patrocinador

269
00:07:14,750 --> 00:07:16,530
del programa, pero sí que es

270
00:07:16,530 --> 00:07:18,450
una empresa comprometida con el

271
00:07:18,450 --> 00:07:20,020
software libre y con KDE en

272
00:07:20,020 --> 00:07:22,090
particular, ha sacado su

273
00:07:22,090 --> 00:07:24,580
versión de portátil del Slimbook

274
00:07:24,580 --> 00:07:26,160
que siempre hemos tenido, una

275
00:07:26,160 --> 00:07:27,200
versión renovada.

276
00:07:27,200 --> 00:07:28,920
Es muy lindo hacer un reportaje

277
00:07:28,920 --> 00:07:30,700
sobre él, el resumen es porque

278
00:07:30,700 --> 00:07:32,700
es la actualización, mejor que

279
00:07:32,700 --> 00:07:34,980
el que había antes, igual de

280
00:07:34,980 --> 00:07:37,040
bueno, con cosas más modernas.

281
00:07:37,040 --> 00:07:38,810
También quería contaros que

282
00:07:38,810 --> 00:07:40,200
es algo que yo uso de vez en

283
00:07:40,200 --> 00:07:41,930
cuando con mi hija, de hecho es

284
00:07:41,930 --> 00:07:43,680
lo único que usa del ordenador

285
00:07:43,680 --> 00:07:45,760
ella, es GCompris.

286
00:07:45,760 --> 00:07:47,470
La aplicación o programa de

287
00:07:47,470 --> 00:07:49,390
educación infantil han sacado

288
00:07:49,390 --> 00:07:51,270
ocho nuevas actividades, la

289
00:07:51,270 --> 00:07:53,050
verdad es que tienen más de

290
00:07:53,050 --> 00:07:54,660
las que yo he sido capaz de

291
00:07:54,660 --> 00:07:55,200
probar.

292
00:07:55,200 --> 00:07:56,680
Está bien porque viene por

293
00:07:56,680 --> 00:07:58,350
categorías, por edades o por

294
00:07:58,350 --> 00:08:00,200
dificultad, y luego por sección

295
00:08:00,200 --> 00:08:02,200
de si es gramática, si son puzles,

296
00:08:02,200 --> 00:08:03,360
si es escritura.

297
00:08:03,360 --> 00:08:05,640
Si tenéis críos o personas

298
00:08:05,640 --> 00:08:07,930
mayores, que también es viable,

299
00:08:07,930 --> 00:08:10,120
os recomiendo que le echéis un

300
00:08:10,120 --> 00:08:12,240
vistazo, aparte GCompris

301
00:08:12,240 --> 00:08:14,850
está en Android, Mac, Windows,

302
00:08:14,850 --> 00:08:17,050
Flatpak, vamos, lo podéis

303
00:08:17,050 --> 00:08:18,390
instalar prácticamente en

304
00:08:18,390 --> 00:08:19,660
cualquier sitio, no vais a

305
00:08:19,660 --> 00:08:20,640
tener problemas.

306
00:08:20,640 --> 00:08:23,070
Luego digiKam 8.3 incorpora un

307
00:08:23,070 --> 00:08:25,790
descodificador RAW interno. digiKam

308
00:08:25,790 --> 00:08:28,350
es la aplicación de organización

309
00:08:28,350 --> 00:08:30,400
fotográfica, organización

310
00:08:30,400 --> 00:08:31,760
plus plus diría yo, porque

311
00:08:31,760 --> 00:08:32,800
tienes que hacer muchas cosas.

312
00:08:32,800 --> 00:08:34,420
Yo tengo ahí organizado mis

313
00:08:34,420 --> 00:08:36,030
fotos y tengo etiquetadas en

314
00:08:36,030 --> 00:08:37,640
las caras para saber en todas

315
00:08:37,640 --> 00:08:39,170
las fotos que salgo yo con mi

316
00:08:39,170 --> 00:08:40,850
mujer, en todas las fotos que

317
00:08:40,850 --> 00:08:42,640
sale mi hija con no sé qué.

318
00:08:42,640 --> 00:08:44,390
Y como está en local, pues no

319
00:08:44,390 --> 00:08:46,070
es esto de Google Fotos de que

320
00:08:46,070 --> 00:08:47,760
sabe hasta dónde has pasado

321
00:08:47,760 --> 00:08:49,460
las navidades. Funciona muy

322
00:08:49,460 --> 00:08:50,870
bien, se van actualizando cada

323
00:08:50,870 --> 00:08:51,760
cierto tiempo.

324
00:08:51,760 --> 00:08:53,670
Esta característica en concreto

325
00:08:53,670 --> 00:08:55,370
es para gente más aficionada a

326
00:08:55,370 --> 00:08:57,150
la fotografía, porque es raro

327
00:08:57,150 --> 00:08:58,850
que lo tengáis si hacéis foto

328
00:08:58,850 --> 00:09:00,440
con el móvil, aunque ya hay

329
00:09:00,440 --> 00:09:02,560
móviles que también lo hacen.

330
00:09:02,560 --> 00:09:04,110
Pero bueno, está bien que

331
00:09:04,110 --> 00:09:05,730
sigue mejorando en todos los

332
00:09:05,730 --> 00:09:06,560
aspectos.

333
00:09:06,560 --> 00:09:08,170
Luego tengo una noticia que

334
00:09:08,170 --> 00:09:09,920
para quien no lo sepa puede ser

335
00:09:09,920 --> 00:09:11,740
un poco así chocante, estaba

336
00:09:11,740 --> 00:09:13,710
dedicada José Jiménez, que es

337
00:09:13,710 --> 00:09:15,480
que Fedora 40 vendrá pronto

338
00:09:15,480 --> 00:09:17,400
con Plasma 6, pero sin opción

339
00:09:17,400 --> 00:09:18,240
de X11.

340
00:09:18,240 --> 00:09:19,490
Y entonces aquí, en algunos

341
00:09:19,490 --> 00:09:21,240
otros sitios ya he hecho la advertencia,

342
00:09:21,240 --> 00:09:22,480
pero quería recordarlo.

343
00:09:22,480 --> 00:09:24,710
Plasma 6 soporta X11 de manera

344
00:09:24,710 --> 00:09:27,110
nativa, o sea, no es que en Wayland

345
00:09:27,110 --> 00:09:29,720
puedas abrir aplicaciones de X11,

346
00:09:29,720 --> 00:09:31,410
que eso lo tienen todos los

347
00:09:31,410 --> 00:09:33,280
escritorios con Wayland.

348
00:09:33,280 --> 00:09:35,140
Plasma 6 está para X11 y

349
00:09:35,140 --> 00:09:37,230
funciona con X11, de hecho yo

350
00:09:37,230 --> 00:09:38,740
tengo un portátil en el que

351
00:09:38,740 --> 00:09:40,580
arranco sesión de Plasma 6 con

352
00:09:40,580 --> 00:09:42,920
X11 porque me pasa algo con las

353
00:09:42,920 --> 00:09:44,410
gráficas que son duales de

354
00:09:44,410 --> 00:09:45,840
interna y externa.

355
00:09:45,840 --> 00:09:47,840
La cosa es que Fedora, la

356
00:09:47,840 --> 00:09:49,860
distribución, ha decidido que

357
00:09:49,860 --> 00:09:52,430
X11 ya es demasiado antiguo y

358
00:09:52,430 --> 00:09:54,150
que ellos no van a empaquetar

359
00:09:54,150 --> 00:09:56,080
el soporte de X11.

360
00:09:56,080 --> 00:09:57,560
No significa que Plasma 6 no

361
00:09:57,560 --> 00:09:59,030
vaya a funcionar en cualquier

362
00:09:59,030 --> 00:10:01,160
otra distribución con X11. Sí,

363
00:10:01,160 --> 00:10:02,520
seguramente lo van a hacer en

364
00:10:02,520 --> 00:10:04,350
muchas, pero este es el pistoletazo

365
00:10:04,350 --> 00:10:06,560
de salida para tener en cuenta

366
00:10:06,560 --> 00:10:09,020
que X11, más pronto que tarde,

367
00:10:09,020 --> 00:10:10,160
va a ir desapareciendo de

368
00:10:10,160 --> 00:10:11,520
algunas distribuciones.

369
00:10:11,520 --> 00:10:13,290
En algunas, por ejemplo, Debian,

370
00:10:13,290 --> 00:10:14,900
yo creo que dentro de 10 años

371
00:10:14,900 --> 00:10:16,940
lo mismo todavía podemos arrancar

372
00:10:16,940 --> 00:10:18,190
porque Plasma 6 tiene soporte

373
00:10:18,190 --> 00:10:19,760
para mucho, mucho tiempo.

374
00:10:19,760 --> 00:10:21,640
Pero sí es verdad que ya vamos

375
00:10:21,640 --> 00:10:23,170
a tener que ir teniendo en

376
00:10:23,170 --> 00:10:24,660
cuenta que si tenemos un

377
00:10:24,660 --> 00:10:26,750
hardware específico, como una

378
00:10:26,750 --> 00:10:28,530
Nvidia muy antigua, que los

379
00:10:28,530 --> 00:10:30,300
drivers propietarios no lo soportan

380
00:10:30,300 --> 00:10:31,920
porque dejaron de dar soporte,

381
00:10:31,920 --> 00:10:33,760
valga la redundancia.

382
00:10:33,760 --> 00:10:36,100
El driver Open Source sí que

383
00:10:36,100 --> 00:10:38,990
soporta que vaya bien con Wayland,

384
00:10:38,990 --> 00:10:42,080
pero tengo 3 monitores...

385
00:10:42,080 --> 00:10:43,580
La cosa es que tendréis que ir

386
00:10:43,580 --> 00:10:45,260
probando, yo espero que Wayland

387
00:10:45,260 --> 00:10:46,990
haya mejorado suficientemente y

388
00:10:46,990 --> 00:10:49,220
los drivers hayan mejorado suficientemente

389
00:10:49,220 --> 00:10:50,660
para que le funcione a todo el

390
00:10:50,660 --> 00:10:52,290
mundo, pero habrá algún caso

391
00:10:52,290 --> 00:10:54,210
en el que lo mismo hay que

392
00:10:54,210 --> 00:10:56,240
cambiar de distribución.

393
00:10:56,240 --> 00:10:57,670
O lo mismo, como esto es

394
00:10:57,670 --> 00:10:59,670
software libre, alguien saca un

395
00:10:59,670 --> 00:11:01,640
repositorio, creo que en Fedora

396
00:11:01,640 --> 00:11:04,080
es Kopr o algo así, y le da soporte.

397
00:11:04,080 --> 00:11:06,130
Sé que en Fedora 40 es posible

398
00:11:06,130 --> 00:11:08,110
que Gnome todavía lo tenga,

399
00:11:08,110 --> 00:11:09,790
pero Gnome también por lo que

400
00:11:09,790 --> 00:11:11,550
he leído está pensando en la

401
00:11:11,550 --> 00:11:13,920
41 cargarse el soporte X11.

402
00:11:13,920 --> 00:11:16,450
Así que probar y ir teniendo

403
00:11:16,450 --> 00:11:19,220
una distribución de cabecera

404
00:11:19,220 --> 00:11:21,530
como plan B por si queréis

405
00:11:21,530 --> 00:11:24,490
seguir usando Plasma y X11 ya

406
00:11:24,490 --> 00:11:27,120
dejar de ser una opción.

407
00:11:27,120 --> 00:11:29,350
Yo, aunque tengo pendiente de

408
00:11:29,350 --> 00:11:32,270
probarla, ya directamente recomiendo

409
00:11:32,270 --> 00:11:34,960
que probéis OpenSUSE SlowRoll,

410
00:11:34,960 --> 00:11:36,540
porque sobre el papel tiene

411
00:11:36,540 --> 00:11:38,110
todo lo que yo le pido a una

412
00:11:38,110 --> 00:11:39,920
distribución para poner en

413
00:11:39,920 --> 00:11:40,320
masa.

414
00:11:40,320 --> 00:11:42,140
Tiene muy buen soporte, viene

415
00:11:42,140 --> 00:11:43,610
de una compañía y de una

416
00:11:43,610 --> 00:11:45,710
comunidad muy grande, como es SUSE

417
00:11:45,710 --> 00:11:47,970
y como es la comunidad de OpenSUSE,

418
00:11:47,970 --> 00:11:49,530
es una distribución de las

419
00:11:49,530 --> 00:11:51,470
primeras, de las grandes, con

420
00:11:51,470 --> 00:11:52,830
lo cual tiene un montón de

421
00:11:52,830 --> 00:11:55,120
desarrollo detrás, son punteros.

422
00:11:55,120 --> 00:11:57,110
No es tan tan tan tan tan moderna

423
00:11:57,110 --> 00:11:59,520
y rolling release como Tumbleweed,

424
00:11:59,520 --> 00:12:00,990
con lo cual va a ser más

425
00:12:00,990 --> 00:12:03,040
estable y va a ser más sencillo

426
00:12:03,040 --> 00:12:05,060
de manejar, no vais a tener que

427
00:12:05,060 --> 00:12:07,120
estar tan pendiente de actualizaciones.

428
00:12:07,120 --> 00:12:08,580
Pero no va a ser tan tan tan

429
00:12:08,580 --> 00:12:09,980
lentas como otras que vas a

430
00:12:09,980 --> 00:12:11,470
tener que estar viendo si

431
00:12:11,470 --> 00:12:13,230
instalas esto en Flatpak, que si

432
00:12:13,230 --> 00:12:15,120
instalas esto en no sé qué,

433
00:12:15,120 --> 00:12:16,740
aunque yo la verdad es que instalarlo

434
00:12:16,740 --> 00:12:18,120
en Flatpak me va bastante bien.

435
00:12:18,120 --> 00:12:19,850
Pero no sé, creo que tiene una

436
00:12:19,850 --> 00:12:21,780
cosa ahí, un intermedio que

437
00:12:21,780 --> 00:12:23,120
merece la pena probarlo.

438
00:12:23,120 --> 00:12:24,860
Y luego, aunque yo no soy muy

439
00:12:24,860 --> 00:12:27,000
fan, estoy a favor de la libertad,

440
00:12:27,000 --> 00:12:28,870
el software libre, no se puede

441
00:12:28,870 --> 00:12:31,340
guardar que haya nuevas distribuciones

442
00:12:31,340 --> 00:12:32,960
porque si no esto no sería

443
00:12:32,960 --> 00:12:34,120
software libre.

444
00:12:34,120 --> 00:12:35,530
Pero si es verdad que dentro de

445
00:12:35,530 --> 00:12:36,950
que tiene que haber libertad y

446
00:12:36,950 --> 00:12:38,470
si no hubiera habido libertad,

447
00:12:38,470 --> 00:12:40,180
una de las que hoy son grandes,

448
00:12:40,180 --> 00:12:41,580
ayer no se hubieran podido

449
00:12:41,580 --> 00:12:43,250
crear, no estoy por ir probando

450
00:12:43,250 --> 00:12:44,610
todas las nuevas distros que

451
00:12:44,610 --> 00:12:45,120
aparecen.

452
00:12:45,120 --> 00:12:47,020
Pero aún así, yo las menciono

453
00:12:47,020 --> 00:12:48,710
y cada uno es libre de probar y

454
00:12:48,710 --> 00:12:50,120
ver si les va mejor.

455
00:12:50,120 --> 00:12:52,200
Y lo mismo el día de mañana,

456
00:12:52,200 --> 00:12:54,320
una de estas pega el petardazo

457
00:12:54,320 --> 00:12:56,050
y se convierte en una de las

458
00:12:56,050 --> 00:12:57,120
principales.

459
00:12:57,120 --> 00:12:59,240
Y todo este rollo venía porque

460
00:12:59,240 --> 00:13:01,070
hay una nueva distro que se

461
00:13:01,070 --> 00:13:02,590
llama X-Ray OS, que por lo que

462
00:13:02,590 --> 00:13:03,850
he podido leer, y por supuesto

463
00:13:03,850 --> 00:13:05,050
tenéis en las notas del

464
00:13:05,050 --> 00:13:06,120
programa el enlace,

465
00:13:06,120 --> 00:13:07,780
es una especie de Arch Linux

466
00:13:07,780 --> 00:13:09,960
Vanilla, que son los repositorios

467
00:13:09,960 --> 00:13:12,430
de Arch sin meterle muchos repositorios

468
00:13:12,430 --> 00:13:14,120
propios y hacer grandes cambios,

469
00:13:14,120 --> 00:13:15,640
que está enfocada a juegos y

470
00:13:15,640 --> 00:13:17,030
que por supuesto viene con

471
00:13:17,030 --> 00:13:18,120
Plasma por defecto.

472
00:13:18,120 --> 00:13:19,680
Y al final, que como en Devor,

473
00:13:19,680 --> 00:13:21,180
lo que hace es que puedas tener

474
00:13:21,180 --> 00:13:22,700
un Plasma sin tener que hacer

475
00:13:22,700 --> 00:13:24,120
el proceso de instalación

476
00:13:24,120 --> 00:13:26,120
desde cero por línea de comando

477
00:13:26,120 --> 00:13:28,450
o con el menú TUI por el que

478
00:13:28,450 --> 00:13:30,120
se puede hacer.

479
00:13:30,120 --> 00:13:31,960
O sea, es un Arch Linux para

480
00:13:31,960 --> 00:13:34,440
humanos de a pie, que algo debe

481
00:13:34,440 --> 00:13:36,290
tener especial para juegos, por

482
00:13:36,290 --> 00:13:38,120
si lo queréis probar.

483
00:13:38,120 --> 00:13:40,580
Pues todo este popurrí de noticias

484
00:13:40,580 --> 00:13:42,450
es lo que me he ido apuntando

485
00:13:42,450 --> 00:13:43,120
este mes.

486
00:13:43,120 --> 00:13:45,230
La verdad es que esto viene

487
00:13:45,230 --> 00:13:47,960
siendo una lectura del artículo

488
00:13:47,960 --> 00:13:49,120
que voy a tener,

489
00:13:49,120 --> 00:13:50,150
porque la verdad es que el

490
00:13:50,150 --> 00:13:51,260
episodio de hoy tiene mucha

491
00:13:51,260 --> 00:13:52,530
más gracia si tenéis delante

492
00:13:52,530 --> 00:13:54,180
los enlaces, porque son una parrada

493
00:13:54,180 --> 00:13:55,310
de enlaces por si queréis

494
00:13:55,310 --> 00:13:56,120
profundizar.

495
00:13:56,120 --> 00:13:58,330
Pero no quería dejar de comentarlos

496
00:13:58,330 --> 00:14:00,360
para que tuvierais ahí un resumen

497
00:14:00,360 --> 00:14:01,700
por encima, por si alguno os

498
00:14:01,700 --> 00:14:02,660
interesa, ya podréis vosotros

499
00:14:02,660 --> 00:14:04,120
investigar más.

500
00:14:04,120 --> 00:14:05,640
Y así ponernos al día y una

501
00:14:05,640 --> 00:14:07,510
vez que haya terminado el periplo

502
00:14:07,510 --> 00:14:10,120
de pasar por Mancomún, por Wihax

503
00:14:10,120 --> 00:14:12,600
y por accesibilidad con tecnologías

504
00:14:12,600 --> 00:14:13,120
libres,

505
00:14:13,120 --> 00:14:15,250
recupero un poco de tiempo y el

506
00:14:15,250 --> 00:14:17,210
siguiente lo puedo hacer antes

507
00:14:17,210 --> 00:14:19,180
y os traigo las novedades un

508
00:14:19,180 --> 00:14:20,120
poco antes.

509
00:14:20,120 --> 00:14:21,740
Recordar que esLibre está a

510
00:14:21,740 --> 00:14:23,480
la vuelta de la esquina, que yo

511
00:14:23,480 --> 00:14:25,210
iría reservando ya un hotel si

512
00:14:25,210 --> 00:14:26,120
queréis ir,

513
00:14:26,120 --> 00:14:27,960
porque Valencia por esas fechas,

514
00:14:27,960 --> 00:14:29,950
si lo hacéis con muy poca antelación,

515
00:14:29,950 --> 00:14:31,400
va a ser caro y va a haber pocas

516
00:14:31,400 --> 00:14:32,120
opciones.

517
00:14:32,120 --> 00:14:33,680
Que Akademy-es estará allí por

518
00:14:33,680 --> 00:14:35,240
si nos queréis ver en una mesa

519
00:14:35,240 --> 00:14:36,680
que pondremos muy chula con un

520
00:14:36,680 --> 00:14:38,120
montón de cosas de probar.

521
00:14:38,120 --> 00:14:40,120
Y que nos vemos en las redes.

522
00:14:40,120 --> 00:14:42,120
Un saludo, hasta luego.

