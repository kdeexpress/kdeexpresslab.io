1
00:00:00,580 --> 00:00:02,030
 Buenas, esto es KDE Express,

2
00:00:02,030 --> 00:00:04,070
 episodio número 33 y estoy

3
00:00:04,070 --> 00:00:05,820
 grabando el 4 de septiembre de

4
00:00:05,820 --> 00:00:06,420
 2024.

5
00:00:06,420 --> 00:00:07,880
 Si escucháis algo raro de

6
00:00:07,880 --> 00:00:09,540
 fondo es que está cayendo una

7
00:00:09,540 --> 00:00:11,780
 tromba de agua aquí por Cartagena.

8
00:00:11,780 --> 00:00:13,600
 Volvemos de vacaciones y tengo

9
00:00:13,600 --> 00:00:15,290
 un montón de noticias que se

10
00:00:15,290 --> 00:00:16,360
 me han acumulado

11
00:00:16,360 --> 00:00:17,360
 y yo creo que no ha pasado

12
00:00:17,360 --> 00:00:18,620
 tanto tiempo desde el último,

13
00:00:18,620 --> 00:00:21,160
 así que quería que os tomarais

14
00:00:21,160 --> 00:00:23,830
 un momento para meditar la de

15
00:00:23,830 --> 00:00:24,780
 aplicaciones,

16
00:00:24,780 --> 00:00:26,440
 la de actualizaciones, la de

17
00:00:26,440 --> 00:00:28,260
 trabajo que está haciendo esta

18
00:00:28,260 --> 00:00:29,840
 gente en horario de verano.

19
00:00:29,840 --> 00:00:31,870
 ¡Ah! Increíble la comunidad

20
00:00:31,870 --> 00:00:34,540
 de KDE, como no paran ni en julio,

21
00:00:34,540 --> 00:00:36,300
 ni en agosto, ni en ningún mes

22
00:00:36,300 --> 00:00:36,720
 del año.

23
00:00:36,720 --> 00:00:38,170
 Y luego entrando ya en temas

24
00:00:38,170 --> 00:00:39,990
 noticias, pues tenemos una de

25
00:00:39,990 --> 00:00:41,690
 nuestros blogs de referencia de

26
00:00:41,690 --> 00:00:42,940
 KDE Blogs de Baltasar

27
00:00:42,940 --> 00:00:45,060
 que es un mapa de usuarios KDE.

28
00:00:45,060 --> 00:00:46,800
 Parece ser que existía ya un

29
00:00:46,800 --> 00:00:48,820
 mapa de usuarios y simpatizantes

30
00:00:48,820 --> 00:00:50,160
 que yo no conocía,

31
00:00:50,160 --> 00:00:52,220
 pero gracias a Baltasar ahora

32
00:00:52,220 --> 00:00:54,360
 sabemos que tenemos uno nuevo

33
00:00:54,360 --> 00:00:57,840
 de una persona que ha decidido

34
00:00:57,840 --> 00:00:59,560
 renovar el que había abandonado.

35
00:00:59,560 --> 00:01:02,560
 Y lo ha hecho con Umap, que es

36
00:01:02,560 --> 00:01:04,940
 una plataforma o software libre

37
00:01:04,940 --> 00:01:05,850
 en el que tú puedes ir

38
00:01:05,850 --> 00:01:07,130
 haciendo diferentes capas,

39
00:01:07,130 --> 00:01:08,470
 diferentes mapas y compartirlo

40
00:01:08,470 --> 00:01:09,060
 con la gente.

41
00:01:09,060 --> 00:01:11,040
 Ya hay bastantes personas y vosotros

42
00:01:11,040 --> 00:01:12,300
 os podéis añadir.

43
00:01:12,300 --> 00:01:13,710
 Por supuesto es voluntario y no

44
00:01:13,710 --> 00:01:14,600
 hace falta que pongas

45
00:01:14,600 --> 00:01:15,680
 exactamente tu casa,

46
00:01:15,680 --> 00:01:16,900
 vale con que sea la ciudad.

47
00:01:16,900 --> 00:01:18,580
 Yo ya he visto que Cartagena

48
00:01:18,580 --> 00:01:20,160
 está cubierta, así que no he

49
00:01:20,160 --> 00:01:21,440
 puesto nada más.

50
00:01:21,440 --> 00:01:23,360
 Pero está curioso porque ves a

51
00:01:23,360 --> 00:01:24,950
 lo largo de España donde hay

52
00:01:24,950 --> 00:01:25,380
 gente

53
00:01:25,380 --> 00:01:26,940
 y luego siempre puedes ir a Cañas

54
00:01:26,940 --> 00:01:27,740
 y Bravas y decir

55
00:01:27,740 --> 00:01:29,010
 oye, he visto que hay gente por

56
00:01:29,010 --> 00:01:29,380
 Madrid.

57
00:01:29,380 --> 00:01:30,860
 Por mi ciudad, a ver si hacemos

58
00:01:30,860 --> 00:01:32,220
 una quedada KDE era.

59
00:01:32,220 --> 00:01:33,830
 Como siguiente noticia os traigo

60
00:01:33,830 --> 00:01:35,540
 las típicas de actualizaciones

61
00:01:35,540 --> 00:01:37,000
 de software y aplicaciones.

62
00:01:37,000 --> 00:01:39,310
 Empezando con KDE Frameworks,

63
00:01:39,310 --> 00:01:41,780
 ya tenemos la versión 6.5

64
00:01:41,780 --> 00:01:43,400
 que por supuesto trae muchas

65
00:01:43,400 --> 00:01:44,800
 cosas que les sirven a los

66
00:01:44,800 --> 00:01:45,920
 desarrolladores

67
00:01:45,920 --> 00:01:47,200
 para mejorar las aplicaciones.

68
00:01:47,200 --> 00:01:49,130
 Y yo lo que quiero destacar es

69
00:01:49,130 --> 00:01:51,070
 que mejora la accesibilidad de

70
00:01:51,070 --> 00:01:52,420
 la pantalla de configuración

71
00:01:52,420 --> 00:01:53,640
 de los atajos de teclado.

72
00:01:53,640 --> 00:01:55,090
 Ya sabéis que yo últimamente

73
00:01:55,090 --> 00:01:56,560
 estoy ahí metiéndole mucha caña

74
00:01:56,560 --> 00:01:57,380
 a todas las mejoras

75
00:01:57,380 --> 00:01:58,620
 que tengan que ver con accesibilidad.

76
00:01:59,260 --> 00:02:00,160
 Así que os quería destacar

77
00:02:00,160 --> 00:02:00,480
 esta.

78
00:02:00,480 --> 00:02:01,660
 Y luego tengo un artículo que

79
00:02:01,660 --> 00:02:02,480
 me ha encantado.

80
00:02:02,480 --> 00:02:03,770
 De hecho tengo dos de esta

81
00:02:03,770 --> 00:02:06,860
 página web que son profundos,

82
00:02:06,860 --> 00:02:08,700
 pero van al grano y me parece

83
00:02:08,700 --> 00:02:10,900
 que son súper interesantes.

84
00:02:10,900 --> 00:02:13,390
 El resumen es mejora tu

85
00:02:13,390 --> 00:02:15,010
 experiencia en KDE Plasma con

86
00:02:15,010 --> 00:02:17,320
 estos 15 widgets o plasmoides

87
00:02:17,320 --> 00:02:18,460
 como le llamamos nosotros, ¿no?

88
00:02:18,460 --> 00:02:20,460
 Y entonces te viene un listado

89
00:02:20,460 --> 00:02:22,000
 en el que te va diciendo

90
00:02:22,000 --> 00:02:22,620
 algunos widgets

91
00:02:22,620 --> 00:02:23,780
 que vosotros ya conoceréis

92
00:02:23,780 --> 00:02:24,720
 como el botón de inicio

93
00:02:24,720 --> 00:02:26,710
 o donde se ponen las aplicaciones

94
00:02:26,710 --> 00:02:28,200
 que están abiertas.

95
00:02:28,200 --> 00:02:29,140
 La cosa es que...

96
00:02:29,140 --> 00:02:30,750
 Te las explica de una manera

97
00:02:30,750 --> 00:02:31,540
 que es visual

98
00:02:31,540 --> 00:02:32,760
 y te da algunos detalles que

99
00:02:32,760 --> 00:02:33,900
 tú a lo mejor no conocías.

100
00:02:33,900 --> 00:02:35,540
 Entonces pues te dice, este sirve

101
00:02:35,540 --> 00:02:36,000
 para esto.

102
00:02:36,000 --> 00:02:37,860
 Pero tiene esta configuración

103
00:02:37,860 --> 00:02:38,720
 que a lo mejor no sabías

104
00:02:38,720 --> 00:02:39,990
 que podías cambiarle el botón

105
00:02:39,990 --> 00:02:40,400
 de inicio

106
00:02:40,400 --> 00:02:42,140
 o que puedes cambiar el texto.

107
00:02:42,140 --> 00:02:44,040
 Y en cada una te explica muy

108
00:02:44,040 --> 00:02:45,940
 bien con texto y con imágenes

109
00:02:45,940 --> 00:02:47,990
 las peculiaridades que tiene y

110
00:02:47,990 --> 00:02:48,920
 algunas cosillas que podéis

111
00:02:48,920 --> 00:02:49,340
 cambiar.

112
00:02:49,340 --> 00:02:51,310
 Yo os lo recomiendo que pinchéis

113
00:02:51,310 --> 00:02:52,700
 en el enlace a verla.

114
00:02:52,700 --> 00:02:55,340
 Luego tenemos una mini noticia

115
00:02:55,340 --> 00:02:56,920
 que es que KDE Neon está casi

116
00:02:56,920 --> 00:02:58,660
 listo para actualizarse con

117
00:02:58,660 --> 00:02:59,020
 base...

118
00:02:59,020 --> 00:03:00,700
 En Ubuntu 24.04.

119
00:03:00,700 --> 00:03:02,820
 Sabéis que KDE Neon se actualiza

120
00:03:02,820 --> 00:03:04,880
 en plan casi rolling release

121
00:03:04,880 --> 00:03:06,650
 en lo que son las aplicaciones

122
00:03:06,650 --> 00:03:08,300
 y los frameworks de Plasma.

123
00:03:08,300 --> 00:03:10,200
 Pero lo que es el sistema, el

124
00:03:10,200 --> 00:03:11,870
 archivo, el repositorio que

125
00:03:11,870 --> 00:03:12,200
 tiene,

126
00:03:12,200 --> 00:03:13,640
 la mayoría de aplicaciones son

127
00:03:13,640 --> 00:03:15,340
 las que te da Ubuntu por defecto.

128
00:03:15,340 --> 00:03:17,300
 Entonces pues esto lleva su

129
00:03:17,300 --> 00:03:18,180
 trabajo.

130
00:03:18,180 --> 00:03:20,100
 Tienen que hacer varios ajustes

131
00:03:20,100 --> 00:03:21,400
 pero están a punto de terminar

132
00:03:21,400 --> 00:03:22,960
 la migración a 24.04.

133
00:03:22,960 --> 00:03:24,380
 Y así tendréis el resto de

134
00:03:24,380 --> 00:03:25,500
 aplicaciones también

135
00:03:25,500 --> 00:03:26,600
 con una base y una versión

136
00:03:26,600 --> 00:03:27,140
 moderna.

137
00:03:27,140 --> 00:03:28,900
 De paso, os dejo una guía

138
00:03:28,900 --> 00:03:30,990
 y un vídeo sobre la instalación,

139
00:03:30,990 --> 00:03:32,360
 configuración y personalización

140
00:03:32,360 --> 00:03:33,140
 de Stadistro.

141
00:03:33,140 --> 00:03:34,440
 Pues la que te dicen pues no es

142
00:03:34,440 --> 00:03:34,860
 muy larga

143
00:03:34,860 --> 00:03:36,750
 y son pues 4 o 5 cosillas que

144
00:03:36,750 --> 00:03:38,300
 hacen los de Soplos Linux

145
00:03:38,300 --> 00:03:40,100
 cuando instalan KDE Neon.

146
00:03:40,100 --> 00:03:41,500
 Ahora os voy a hablar de aplicaciones

147
00:03:41,500 --> 00:03:43,580
 a nivel individual

148
00:03:43,580 --> 00:03:45,580
 que no están dentro de KDE Gear.

149
00:03:45,580 --> 00:03:47,580
 Una es Caligra

150
00:03:47,580 --> 00:03:49,020
 que según nos cuenta MuyLinux

151
00:03:49,020 --> 00:03:50,770
 resucita tras un largo letargo

152
00:03:50,770 --> 00:03:52,340
 con su versión 4.0.

153
00:03:52,340 --> 00:03:53,680
 Yo la verdad es que soy más de

154
00:03:53,680 --> 00:03:54,340
 LibreOffice

155
00:03:54,340 --> 00:03:56,340
 pero oye, que no sea por alternativas.

156
00:03:56,340 --> 00:03:57,600
 Y la han publicado como

157
00:03:57,600 --> 00:03:58,780
 versión Flatpak.

158
00:03:58,780 --> 00:03:59,980
 Con lo cual es muy sencillo

159
00:03:59,980 --> 00:04:00,780
 probarla

160
00:04:00,780 --> 00:04:02,780
 a la última y ver si

161
00:04:02,780 --> 00:04:04,330
 es más ligera o tiene algo que

162
00:04:04,330 --> 00:04:04,780
 os convenza.

163
00:04:04,780 --> 00:04:06,780
 Y también tenemos Haruna

164
00:04:06,780 --> 00:04:08,780
 o Aruna, no sé cómo se pronuncia

165
00:04:08,780 --> 00:04:10,220
 que es el reproductor del que

166
00:04:10,220 --> 00:04:10,780
 nos habló Iván

167
00:04:10,780 --> 00:04:12,780
 hace poco en el otro episodio

168
00:04:12,780 --> 00:04:14,780
 que se ha actualizado a la 1.02.

169
00:04:14,780 --> 00:04:16,780
 Y he encontrado un artículo

170
00:04:16,780 --> 00:04:20,780
 de omgubuntu.co.uk

171
00:04:20,780 --> 00:04:22,780
 que lo tengo por aquí

172
00:04:22,780 --> 00:04:24,780
 en el que nos dice que

173
00:04:24,780 --> 00:04:26,780
 les gusta mucho este reproductor

174
00:04:26,780 --> 00:04:26,780
 porque

175
00:04:26,780 --> 00:04:28,660
 visualmente es muy

176
00:04:28,660 --> 00:04:30,660
 atractivo, que tiene una

177
00:04:30,660 --> 00:04:32,660
 barra inferior que

178
00:04:32,660 --> 00:04:34,660
 aparece y desaparece según la

179
00:04:34,660 --> 00:04:34,660
 configura

180
00:04:34,660 --> 00:04:36,030
 si pasa el ratón por la

181
00:04:36,030 --> 00:04:36,660
 pantalla

182
00:04:36,660 --> 00:04:38,660
 o si pasa el ratón solo por la

183
00:04:38,660 --> 00:04:38,660
 parte

184
00:04:38,660 --> 00:04:40,660
 baja de la pantalla. Que tiene

185
00:04:40,660 --> 00:04:42,660
 unas buenas opciones de subtítulos.

186
00:04:42,660 --> 00:04:44,660
 Vamos, que a los de

187
00:04:44,660 --> 00:04:46,660
 esta página les gusta el

188
00:04:46,660 --> 00:04:46,660
 reproductor.

189
00:04:46,660 --> 00:04:48,660
 Yo estoy acostumbrado

190
00:04:48,660 --> 00:04:50,660
 a VLC porque hago un uso muy

191
00:04:50,660 --> 00:04:50,660
 muy muy

192
00:04:50,660 --> 00:04:52,660
 corto de ver vídeos.

193
00:04:52,660 --> 00:04:54,370
 Pero si a Iván y a esta gente

194
00:04:54,370 --> 00:04:54,660
 les gusta

195
00:04:54,660 --> 00:04:56,660
 yo recomendaría que lo probarais.

196
00:04:56,660 --> 00:04:57,970
 Y por supuesto también está

197
00:04:57,970 --> 00:04:58,540
 en versión Flatpak.

198
00:04:58,540 --> 00:05:00,540
 Con lo cual, si no está

199
00:05:00,540 --> 00:05:02,540
 la última versión en vuestro

200
00:05:02,540 --> 00:05:02,540
 repositorio

201
00:05:02,540 --> 00:05:04,540
 la tenéis aquí. Ellos

202
00:05:04,540 --> 00:05:04,540
 también en el mismo

203
00:05:04,540 --> 00:05:06,540
 artículo te hablan de Caligra.

204
00:05:06,540 --> 00:05:08,540
 Pero yo este ya os lo he

205
00:05:08,540 --> 00:05:10,540
 comentado. Y entonces entramos

206
00:05:10,540 --> 00:05:10,540
 ya

207
00:05:10,540 --> 00:05:12,540
 en lo que viene siendo KDE

208
00:05:12,540 --> 00:05:14,540
 JIR, que es el

209
00:05:14,540 --> 00:05:16,540
 paraguas, el paquete en la que

210
00:05:16,540 --> 00:05:16,540
 se meten

211
00:05:16,540 --> 00:05:18,540
 casi todas las aplicaciones y

212
00:05:18,540 --> 00:05:18,540
 utilidades

213
00:05:18,540 --> 00:05:20,540
 que se publican a la vez.

214
00:05:20,540 --> 00:05:22,540
 Porque van coordinadas

215
00:05:22,540 --> 00:05:24,540
 en la release.

216
00:05:24,540 --> 00:05:26,540
 En cómo llevan la agenda de

217
00:05:26,540 --> 00:05:26,540
 publicaciones.

218
00:05:26,540 --> 00:05:28,420
 Entonces,

219
00:05:28,420 --> 00:05:30,420
 os dejo el artículo principal

220
00:05:30,420 --> 00:05:32,420
 con todos los cambios, las

221
00:05:32,420 --> 00:05:32,420
 fotos

222
00:05:32,420 --> 00:05:34,420
 y os hago un resumen de las

223
00:05:34,420 --> 00:05:34,420
 cosas que yo he ido

224
00:05:34,420 --> 00:05:36,420
 encontrando que me parecen más

225
00:05:36,420 --> 00:05:36,420
 destacables

226
00:05:36,420 --> 00:05:38,420
 siempre desde el seco

227
00:05:38,420 --> 00:05:40,420
 de lo que yo uso. A lo mejor a

228
00:05:40,420 --> 00:05:40,420
 vosotros

229
00:05:40,420 --> 00:05:42,420
 os parece mucho más importante

230
00:05:42,420 --> 00:05:42,420
 o estabais esperando

231
00:05:42,420 --> 00:05:44,420
 otra cosa que ya tiene.

232
00:05:44,420 --> 00:05:46,420
 Yo voy poniendo las cosas que a

233
00:05:46,420 --> 00:05:46,420
 mi más

234
00:05:46,420 --> 00:05:48,420
 me llaman la atención. Y es

235
00:05:48,420 --> 00:05:48,420
 por ejemplo:

236
00:05:48,420 --> 00:05:50,420
 Dolphin permite

237
00:05:50,420 --> 00:05:52,420
 mover a una nueva carpeta los

238
00:05:52,420 --> 00:05:52,420
 archivos

239
00:05:52,420 --> 00:05:54,420
 seleccionados o seleccionar

240
00:05:54,420 --> 00:05:54,420
 todo

241
00:05:54,420 --> 00:05:56,420
 con doble clic en un hueco vacío.

242
00:05:56,420 --> 00:05:58,300
 Esto es que si tu seleccionas

243
00:05:58,300 --> 00:06:00,300
 todos los ficheros y le das

244
00:06:00,300 --> 00:06:02,300
 al botón derecho, pues te va a

245
00:06:02,300 --> 00:06:02,300
 dejar meterlos en una carpeta.

246
00:06:02,300 --> 00:06:04,300
 O si haces doble clic en un hueco

247
00:06:04,300 --> 00:06:06,300
 que no tenga ningún fichero,

248
00:06:06,300 --> 00:06:06,300
 pues te va a

249
00:06:06,300 --> 00:06:07,720
 seleccionar todo lo que haya en

250
00:06:07,720 --> 00:06:08,300
 esa carpeta.

251
00:06:08,300 --> 00:06:10,300
 Filelight, que es un

252
00:06:10,300 --> 00:06:11,820
 analizador de espacio, por así

253
00:06:11,820 --> 00:06:12,300
 decirlo,

254
00:06:12,300 --> 00:06:14,300
 ahora se puede instalar pulsando

255
00:06:14,300 --> 00:06:14,300
 sobre

256
00:06:14,300 --> 00:06:16,300
 el texto de espacio libre

257
00:06:16,300 --> 00:06:17,820
 que hay en la esquina inferior

258
00:06:17,820 --> 00:06:18,300
 derecha de Dolphin.

259
00:06:18,300 --> 00:06:20,300
 Y le han renovado la pantalla.

260
00:06:20,300 --> 00:06:22,300
 Esto si vais a Dolphin, si vais

261
00:06:22,300 --> 00:06:22,300
 abajo

262
00:06:22,300 --> 00:06:24,300
 a la derecha, ahí os suele

263
00:06:24,300 --> 00:06:24,300
 decir

264
00:06:24,300 --> 00:06:26,300
 las unidades cuánto os queda

265
00:06:26,300 --> 00:06:26,300
 libre.

266
00:06:26,300 --> 00:06:28,180
 Pues ahora, aparte de darle

267
00:06:28,180 --> 00:06:28,180
 información

268
00:06:28,180 --> 00:06:29,420
 tiene un enlace a instalar la

269
00:06:29,420 --> 00:06:30,060
 aplicación

270
00:06:30,060 --> 00:06:32,060
 si no la tenéis. Konsole, la

271
00:06:32,060 --> 00:06:33,740
 aplicación terminal por defecto

272
00:06:33,740 --> 00:06:34,060
 de KDE

273
00:06:34,060 --> 00:06:36,060
 y que es la que yo uso, ahora

274
00:06:36,060 --> 00:06:36,060
 permite

275
00:06:36,060 --> 00:06:38,060
 marcadores en la barra de desplazamiento.

276
00:06:38,060 --> 00:06:39,700
 Esto significa que cuando tú

277
00:06:39,700 --> 00:06:40,060
 haces scroll

278
00:06:40,060 --> 00:06:42,060
 para arriba y para abajo, pues

279
00:06:42,060 --> 00:06:42,060
 puedes

280
00:06:42,060 --> 00:06:44,060
 marcar algunas líneas y

281
00:06:44,060 --> 00:06:44,060
 entonces

282
00:06:44,060 --> 00:06:46,060
 siempre puedes volver a esos

283
00:06:46,060 --> 00:06:46,060
 huecos

284
00:06:46,060 --> 00:06:47,440
 bueno, a esos huecos, a esos

285
00:06:47,440 --> 00:06:48,060
 marcadores

286
00:06:48,060 --> 00:06:49,620
 por si querías, te lo has dejado

287
00:06:49,620 --> 00:06:50,060
 marcado.

288
00:06:50,060 --> 00:06:51,810
 Yo quiero ver esta línea del

289
00:06:51,810 --> 00:06:52,060
 log y esta viene del log

290
00:06:52,060 --> 00:06:53,430
 luego sigues bajando y quieres

291
00:06:53,430 --> 00:06:54,060
 volver a ella.

292
00:06:54,060 --> 00:06:56,060
 Es una cosa interesante. Luego

293
00:06:56,060 --> 00:06:58,060
 Kdenlive, ahora puede usar el

294
00:06:58,060 --> 00:06:58,060
 nuevo

295
00:06:58,060 --> 00:06:59,940
 editor de curvas de fotogramas

296
00:06:59,940 --> 00:07:01,940
 clave para personalizar efectos

297
00:07:01,940 --> 00:07:03,940
 y métodos de atenuación.

298
00:07:03,940 --> 00:07:05,940
 Entrada, salida cúbica y

299
00:07:05,940 --> 00:07:05,940
 entrada

300
00:07:05,940 --> 00:07:07,940
 salida exponencial para fundida.

301
00:07:07,940 --> 00:07:09,610
 Bueno, esto lo que viene a

302
00:07:09,610 --> 00:07:09,940
 decir es que

303
00:07:09,940 --> 00:07:11,940
 han mejorado los efectos,

304
00:07:11,940 --> 00:07:11,940
 porque yo la verdad

305
00:07:11,940 --> 00:07:13,940
 es que no tengo ni idea

306
00:07:13,940 --> 00:07:15,940
 que es un

307
00:07:15,940 --> 00:07:17,940
 Keyframe Curve Editor

308
00:07:17,940 --> 00:07:19,940
 ni de qué va esto.

309
00:07:19,940 --> 00:07:21,940
 Pero en el artículo anterior

310
00:07:21,940 --> 00:07:23,940
 que he dicho de omegum, tú

311
00:07:23,940 --> 00:07:23,940
 también lo haces en

312
00:07:23,940 --> 00:07:25,940
 referencia y os dejo también

313
00:07:25,940 --> 00:07:27,940
 un artículo en concreto sobre

314
00:07:27,940 --> 00:07:27,940
 Kdenlive.

315
00:07:27,940 --> 00:07:29,940
 Si sois usuarios, a lo mejor os

316
00:07:29,940 --> 00:07:29,940
 suena

317
00:07:29,940 --> 00:07:31,940
 de qué va la cosa. La cosa es

318
00:07:31,940 --> 00:07:31,940
 que

319
00:07:31,940 --> 00:07:33,940
 Kdenlive es

320
00:07:33,940 --> 00:07:35,940
 no un contendiente, sino

321
00:07:35,940 --> 00:07:37,940
 una gran alternativa a los

322
00:07:37,940 --> 00:07:37,940
 profesionales

323
00:07:37,940 --> 00:07:39,380
 que hay y que no paran de

324
00:07:39,380 --> 00:07:39,940
 mejorar

325
00:07:39,940 --> 00:07:41,070
 y se ven que hay cosas

326
00:07:41,070 --> 00:07:41,940
 específicas

327
00:07:41,940 --> 00:07:43,940
 que los usuarios demandan y las

328
00:07:43,940 --> 00:07:43,940
 van aplicando.

329
00:07:43,940 --> 00:07:45,940
 Luego, Tokodon, la

330
00:07:45,940 --> 00:07:47,940
 aplicación de mensajería

331
00:07:47,940 --> 00:07:49,940
 y redes sociales. Pues ahora se

332
00:07:49,940 --> 00:07:49,940
 puede

333
00:07:49,940 --> 00:07:51,620
 avisar de los registros llevados

334
00:07:51,620 --> 00:07:51,940
 a cabo en tu servidor

335
00:07:51,940 --> 00:07:53,940
 para una mejor gestión de

336
00:07:53,940 --> 00:07:55,940
 usuarios, entre otras mejores.

337
00:07:55,940 --> 00:07:55,940
 Esto es que ahora

338
00:07:55,940 --> 00:07:57,140
 te avisa cuando alguien te

339
00:07:57,140 --> 00:07:57,820
 quiere seguir.

340
00:07:57,820 --> 00:07:59,820
 O si alguien se ha intentado

341
00:07:59,820 --> 00:07:59,820
 registrar

342
00:07:59,820 --> 00:08:01,820
 en tu instancia. Luego,

343
00:08:01,820 --> 00:08:03,820
 Kate, en 2408,

344
00:08:03,820 --> 00:08:05,820
 mejora el complemento de formateo

345
00:08:05,820 --> 00:08:07,820
 de documentos con una mayor

346
00:08:07,820 --> 00:08:07,820
 compatibilidad

347
00:08:07,820 --> 00:08:09,820
 con archivos BASH, D, FISH,

348
00:08:09,820 --> 00:08:11,820
 configuraciones de NINX,

349
00:08:11,820 --> 00:08:13,820
 OPSI

350
00:08:13,820 --> 00:08:15,820
 SCRIPT, QML

351
00:08:15,820 --> 00:08:17,820
 y YAML. Vamos, que ahora

352
00:08:17,820 --> 00:08:19,820
 entiende mucho mejor los ficheros

353
00:08:19,820 --> 00:08:19,820
 que

354
00:08:19,820 --> 00:08:21,820
 vayan en este formato. Yo,

355
00:08:21,820 --> 00:08:23,490
 de todos los que ha puesto

356
00:08:23,490 --> 00:08:23,820
 aquí, yo los

357
00:08:23,820 --> 00:08:25,820
 YAML y los BASH son los que más

358
00:08:25,820 --> 00:08:25,820
 abro.

359
00:08:25,820 --> 00:08:27,700
 Y luego, como novedad relacionada,

360
00:08:27,700 --> 00:08:29,700
 la funcionalidad del protocolo

361
00:08:29,700 --> 00:08:31,700
 del servidor de lenguajes LSP,

362
00:08:31,700 --> 00:08:33,700
 una cosa muy importante cuando

363
00:08:33,700 --> 00:08:35,700
 quieres un editor potente,

364
00:08:35,700 --> 00:08:37,700
 añade compatibilidad con lenguajes

365
00:08:37,700 --> 00:08:39,700
 GLEAM,

366
00:08:39,700 --> 00:08:41,700
 GLEAM, PURESCRIPT y TYPES.

367
00:08:41,700 --> 00:08:43,700
 Que si escucháis

368
00:08:43,700 --> 00:08:45,700
 a Atareao sabréis

369
00:08:45,700 --> 00:08:47,700
 que es como una alternativa,

370
00:08:47,700 --> 00:08:49,700
 una versión renovada

371
00:08:49,700 --> 00:08:51,700
 de LATEX, y

372
00:08:51,700 --> 00:08:53,350
 Atareao le está metiendo mucha

373
00:08:53,350 --> 00:08:53,700
 caña

374
00:08:53,700 --> 00:08:55,700
 en este tema. Y luego, Okular,

375
00:08:55,700 --> 00:08:57,580
 el lector de documentos

376
00:08:57,580 --> 00:08:59,580
 que tiene el código certificado

377
00:08:59,580 --> 00:09:01,580
 de KDE, pues mejora la compatibilidad

378
00:09:01,580 --> 00:09:03,580
 con los formularios que se

379
00:09:03,580 --> 00:09:03,580
 pueden

380
00:09:03,580 --> 00:09:05,580
 rellenar en los documentos PDF.

381
00:09:05,580 --> 00:09:05,580
 Ya sabéis que

382
00:09:05,580 --> 00:09:06,930
 los documentos PDF, según

383
00:09:06,930 --> 00:09:07,580
 quien lo haga,

384
00:09:07,580 --> 00:09:09,580
 pueden ser un infierno o no,

385
00:09:09,580 --> 00:09:11,320
 y que alguien te da un documento

386
00:09:11,320 --> 00:09:11,580
 PDF en el que

387
00:09:11,580 --> 00:09:12,630
 no puedes hacer nada y lo

388
00:09:12,630 --> 00:09:13,580
 tienes que imprimir, y hay

389
00:09:13,580 --> 00:09:13,580
 quien

390
00:09:13,580 --> 00:09:15,340
 se lo curra, y los documentos

391
00:09:15,340 --> 00:09:15,580
 PDF

392
00:09:15,580 --> 00:09:16,950
 puedes rellenar directamente

393
00:09:16,950 --> 00:09:17,580
 los datos.

394
00:09:17,580 --> 00:09:19,580
 Pues han mejorado las opciones

395
00:09:19,580 --> 00:09:19,580
 para

396
00:09:19,580 --> 00:09:21,580
 que, según con qué aplicación

397
00:09:21,580 --> 00:09:22,970
 o con qué opciones se cree ese

398
00:09:22,970 --> 00:09:23,580
 formulario,

399
00:09:23,580 --> 00:09:25,580
 en Okular sea más fácil

400
00:09:25,580 --> 00:09:27,460
 o funcione mejor el rellenarlo.

401
00:09:27,460 --> 00:09:29,460
 Luego, os he hablado un poco de

402
00:09:29,460 --> 00:09:29,460
 Konsole,

403
00:09:29,460 --> 00:09:31,460
 pero también tengo

404
00:09:31,460 --> 00:09:33,460
 un interesantísimo artículo,

405
00:09:33,460 --> 00:09:35,460
 que lleva además un vídeo asociado,

406
00:09:35,460 --> 00:09:37,460
 sobre 15 trucos

407
00:09:37,460 --> 00:09:39,460
 para sacarle partido a Konsole.

408
00:09:39,460 --> 00:09:41,030
 La mitad de las opciones yo la

409
00:09:41,030 --> 00:09:41,460
 verdad es que no las conocía,

410
00:09:41,460 --> 00:09:42,860
 y alguna creo que la voy a

411
00:09:42,860 --> 00:09:43,460
 utilizar bastante.

412
00:09:43,460 --> 00:09:45,460
 Yo os dejo el enlace para que

413
00:09:45,460 --> 00:09:47,460
 vosotros investiguéis todas, a

414
00:09:47,460 --> 00:09:47,460
 ver cuáles son

415
00:09:47,460 --> 00:09:49,100
 las que no conocíais y las que

416
00:09:49,100 --> 00:09:49,460
 sí, porque ya sabéis que

417
00:09:49,460 --> 00:09:51,460
 Dolphin es una herramienta

418
00:09:51,460 --> 00:09:53,460
 de productividad de la leche, y

419
00:09:53,460 --> 00:09:55,140
 Konsole no se le queda atrás,

420
00:09:55,140 --> 00:09:55,460
 ¿no?

421
00:09:55,460 --> 00:09:57,340
 Entonces, una opción que puede

422
00:09:57,340 --> 00:09:57,340
 ser

423
00:09:57,340 --> 00:09:59,340
 hacer en Konsole,

424
00:09:59,340 --> 00:10:01,340
 a parte el artículo te dice

425
00:10:01,340 --> 00:10:01,340
 dónde activarlo

426
00:10:01,340 --> 00:10:03,340
 y cómo funciona, es que ahora

427
00:10:03,340 --> 00:10:05,340
 puedes abrir en Dolphin

428
00:10:05,340 --> 00:10:07,340
 carpetas o ficheros.

429
00:10:07,340 --> 00:10:09,340
 Esto significa que tú activas

430
00:10:09,340 --> 00:10:11,340
 una opción y haces un

431
00:10:11,340 --> 00:10:13,340
 ls -l y las carpetas,

432
00:10:13,340 --> 00:10:15,340
 bueno, supongo un ls menos...

433
00:10:15,340 --> 00:10:17,340
 un ls funcionaría,

434
00:10:17,340 --> 00:10:19,340
 -l para que te salga en columna,

435
00:10:19,340 --> 00:10:19,340
 es la costumbre.

436
00:10:19,340 --> 00:10:21,340
 La cosa es que cuando sale pintado

437
00:10:21,340 --> 00:10:22,740
 el nombre del fichero en

438
00:10:22,740 --> 00:10:23,340
 pantalla,

439
00:10:23,340 --> 00:10:25,340
 tú le das al botón derecho

440
00:10:25,340 --> 00:10:27,220
 y te da la opción de abrirlo

441
00:10:27,220 --> 00:10:29,220
 directamente para ver ese fichero,

442
00:10:29,220 --> 00:10:31,220
 como si estuvierais en Dolphin.

443
00:10:31,220 --> 00:10:32,710
 Y si es una carpeta, pues lo

444
00:10:32,710 --> 00:10:33,220
 que hace es abrirte

445
00:10:33,220 --> 00:10:35,220
 la en Dolphin. Aparte, cuando

446
00:10:35,220 --> 00:10:35,220
 activas esto,

447
00:10:35,220 --> 00:10:37,220
 también te activa que con una

448
00:10:37,220 --> 00:10:37,220
 combinación

449
00:10:37,220 --> 00:10:39,220
 de teclas, que creo que es con

450
00:10:39,220 --> 00:10:39,220
 control,

451
00:10:39,220 --> 00:10:41,220
 si pinchas el fichero, o

452
00:10:41,220 --> 00:10:43,220
 no, si haces mouse over, si pasas

453
00:10:43,220 --> 00:10:44,680
 el ratón por encima, teniendo

454
00:10:44,680 --> 00:10:45,220
 el control pulsado,

455
00:10:45,220 --> 00:10:47,220
 te hace una preview si es

456
00:10:47,220 --> 00:10:48,480
 una imagen, que la verdad es

457
00:10:48,480 --> 00:10:49,220
 que está chulo.

458
00:10:49,220 --> 00:10:50,530
 Eso ya, por ejemplo, lo hacía

459
00:10:50,530 --> 00:10:51,220
 con los colores,

460
00:10:51,220 --> 00:10:52,980
 si en esa decimal ponía un

461
00:10:52,980 --> 00:10:53,220
 color, al pasar

462
00:10:53,220 --> 00:10:55,220
 el ratón te lo decía. Y luego

463
00:10:55,220 --> 00:10:55,220
 tiene unos

464
00:10:55,220 --> 00:10:57,100
 comandos rápidos,

465
00:10:57,100 --> 00:10:59,100
 que es que tú puedes configurar

466
00:10:59,100 --> 00:11:01,100
 que haga cosas directamente.

467
00:11:01,100 --> 00:11:03,100
 En vez de usar un alias, o si

468
00:11:03,100 --> 00:11:03,100
 es

469
00:11:03,100 --> 00:11:05,100
 una cosa más complicada, pues

470
00:11:05,100 --> 00:11:05,100
 te deja

471
00:11:05,100 --> 00:11:06,600
 configurarlo. Y aquí, en este

472
00:11:06,600 --> 00:11:07,100
 artículo, pues te va

473
00:11:07,100 --> 00:11:09,100
 diciendo, uno a uno,

474
00:11:09,100 --> 00:11:11,100
 los casos de uso

475
00:11:11,100 --> 00:11:13,100
 que puedes darle a todas estas

476
00:11:13,100 --> 00:11:15,100
 opciones, que no está nada mal.

477
00:11:15,100 --> 00:11:17,100
 Este es el otro artículo que

478
00:11:17,100 --> 00:11:17,100
 te decía

479
00:11:17,100 --> 00:11:19,100
 de itsfos. Y luego,

480
00:11:19,100 --> 00:11:21,100
 para terminar, tengo una

481
00:11:21,100 --> 00:11:21,100
 retrospectiva

482
00:11:21,100 --> 00:11:23,100
 de mi tocayo Edmundsons,

483
00:11:23,100 --> 00:11:25,100
 sobre la métrica

484
00:11:25,100 --> 00:11:26,980
 de Plasma. Ya sabéis

485
00:11:26,980 --> 00:11:28,980
 que en Plasma y

486
00:11:28,980 --> 00:11:30,980
 en Dolphin, por separado, KDE

487
00:11:30,980 --> 00:11:32,980
 os deja voluntariamente, porque

488
00:11:32,980 --> 00:11:32,980
 por defecto

489
00:11:32,980 --> 00:11:34,980
 está apagado, el que vosotros

490
00:11:34,980 --> 00:11:36,980
 activéis las métricas, que es

491
00:11:36,980 --> 00:11:36,980
 simplemente

492
00:11:36,980 --> 00:11:38,980
 pues qué tipo de ordenador tenéis,

493
00:11:38,980 --> 00:11:40,980
 si la pantalla es doble monitor,

494
00:11:40,980 --> 00:11:42,980
 la resolución, si estáis en Wayland,

495
00:11:42,980 --> 00:11:44,980
 si la utilidad que utilizáis

496
00:11:44,980 --> 00:11:46,980
 de entorno de escritorio estáis

497
00:11:46,980 --> 00:11:46,980
 en Wayland

498
00:11:46,980 --> 00:11:48,980
 o estáis en X11.

499
00:11:48,980 --> 00:11:50,980
 Información que no es privada,

500
00:11:50,980 --> 00:11:50,980
 o sea, no dice

501
00:11:50,980 --> 00:11:52,980
 cuántas veces haces click en

502
00:11:52,980 --> 00:11:54,460
 LibreOSI, ni qué palabras

503
00:11:54,460 --> 00:11:54,980
 tiene.

504
00:11:54,980 --> 00:11:56,180
 Es algo que yo creo que todo el

505
00:11:56,180 --> 00:11:56,860
 mundo debería activar,

506
00:11:56,860 --> 00:11:58,860
 mira, yo soy un fiel defensor

507
00:11:58,860 --> 00:11:58,860
 de la privacidad,

508
00:11:58,860 --> 00:12:00,860
 pero este tipo de cosas

509
00:12:00,860 --> 00:12:02,860
 no afecta la privacidad, y le

510
00:12:02,860 --> 00:12:02,860
 ayuda

511
00:12:02,860 --> 00:12:04,860
 mucho a los desarrolladores a

512
00:12:04,860 --> 00:12:04,860
 saber

513
00:12:04,860 --> 00:12:06,860
 dónde tienen que enfocarse.

514
00:12:06,860 --> 00:12:06,860
 Entonces,

515
00:12:06,860 --> 00:12:08,860
 la retrospectiva es: el resumen

516
00:12:08,860 --> 00:12:10,860
 es que son útiles, pero necesitan

517
00:12:10,860 --> 00:12:10,860
 mejorar

518
00:12:10,860 --> 00:12:12,860
 la infraestructura ellos mismos,

519
00:12:12,860 --> 00:12:14,860
 y los datos que recogen. Yo

520
00:12:14,860 --> 00:12:14,860
 creo que

521
00:12:14,860 --> 00:12:16,860
 si queréis ayudar,

522
00:12:16,860 --> 00:12:18,860
 y no es monetariamente, y no es

523
00:12:18,860 --> 00:12:18,860
 programando,

524
00:12:18,860 --> 00:12:20,860
 y no es dando difusión, esto

525
00:12:20,860 --> 00:12:20,860
 es

526
00:12:20,860 --> 00:12:22,860
 lo primero que deberíais hacer.

527
00:12:22,860 --> 00:12:24,110
 Y luego, ampliando un poco el

528
00:12:24,110 --> 00:12:24,860
 resumen que hace,

529
00:12:24,860 --> 00:12:26,180
 pues dice que hay cosas que han

530
00:12:26,180 --> 00:12:26,740
 conseguido

531
00:12:26,740 --> 00:12:28,740
 decidir, gracias a esta

532
00:12:28,740 --> 00:12:28,740
 información,

533
00:12:28,740 --> 00:12:30,740
 en plan, deberíamos

534
00:12:30,740 --> 00:12:32,740
 deshabilitar el OpenGL

535
00:12:32,740 --> 00:12:34,740
 2.2, porque es muy viejo.

536
00:12:34,740 --> 00:12:36,740
 Entonces, iban a las estadísticas

537
00:12:36,740 --> 00:12:36,740
 y veían

538
00:12:36,740 --> 00:12:38,740
 que tenían un 5 o un 10%

539
00:12:38,740 --> 00:12:40,740
 de usuarios que todavía lo usaban.

540
00:12:40,740 --> 00:12:42,240
 Entonces, pues todavía no se

541
00:12:42,240 --> 00:12:42,740
 lo cargan, aunque sea muy viejo.

542
00:12:42,740 --> 00:12:44,740
 O ven que

543
00:12:44,740 --> 00:12:46,740
 hay todavía mucha gente

544
00:12:46,740 --> 00:12:48,740
 que está usando X11,

545
00:12:48,740 --> 00:12:50,740
 pero ven que se nota mucho todo

546
00:12:50,740 --> 00:12:50,740
 el mundo

547
00:12:50,740 --> 00:12:52,740
 que está usando Plasma 6, de

548
00:12:52,740 --> 00:12:52,740
 esos

549
00:12:52,740 --> 00:12:54,740
 sólo un 20% han vuelto

550
00:12:54,740 --> 00:12:56,240
 a X11, porque sí que viene por

551
00:12:56,240 --> 00:12:56,620
 defecto

552
00:12:56,620 --> 00:12:58,620
 en WhiteLight las nuevas instalaciones.

553
00:12:58,620 --> 00:13:00,620
 O se nota mucho que

554
00:13:00,620 --> 00:13:02,620
 el de X11 se usa mucho más

555
00:13:02,620 --> 00:13:04,620
 en Nvidia, porque hasta hace

556
00:13:04,620 --> 00:13:04,620
 poco

557
00:13:04,620 --> 00:13:06,620
 era un infierno,

558
00:13:06,620 --> 00:13:08,620
 y con los drivers viejos

559
00:13:08,620 --> 00:13:10,620
 tampoco irá muy bien, es sólo

560
00:13:10,620 --> 00:13:12,620
 para tarjetas gráficas modernas.

561
00:13:12,620 --> 00:13:12,620
 La cosa es que

562
00:13:12,620 --> 00:13:14,620
 esa información les es útil,

563
00:13:14,620 --> 00:13:16,620
 necesitan que más gente

564
00:13:16,620 --> 00:13:18,620
 la active, y también

565
00:13:18,620 --> 00:13:19,840
 necesitan regalar más

566
00:13:19,840 --> 00:13:20,620
 información,

567
00:13:20,620 --> 00:13:22,240
 porque hay preguntas que se

568
00:13:22,240 --> 00:13:22,620
 hacen,

569
00:13:22,620 --> 00:13:24,620
 que como fueron muy prudentes,

570
00:13:24,620 --> 00:13:26,500
 y tú puedes ir poniendo

571
00:13:26,500 --> 00:13:28,500
 según una barra cuánta

572
00:13:28,500 --> 00:13:28,500
 información

573
00:13:28,500 --> 00:13:30,500
 quieres dar, ahora no es fácil

574
00:13:30,500 --> 00:13:32,500
 decir: "Anda, pues nos vendría

575
00:13:32,500 --> 00:13:32,500
 muy bien

576
00:13:32,500 --> 00:13:34,500
 que también nos mandara esta

577
00:13:34,500 --> 00:13:34,500
 información".

578
00:13:34,500 --> 00:13:36,500
 Pero claro, yo a la gente

579
00:13:36,500 --> 00:13:37,980
 le he pedido permiso sólo para

580
00:13:37,980 --> 00:13:38,500
 que me dé esto,

581
00:13:38,500 --> 00:13:40,500
 y no hay una opción en plan:

582
00:13:40,500 --> 00:13:42,500
 "Dame todo lo que

583
00:13:42,500 --> 00:13:44,500
 me pides",

584
00:13:44,500 --> 00:13:46,500
 o en realidad sería: "Quiero

585
00:13:46,500 --> 00:13:46,500
 activar,

586
00:13:46,500 --> 00:13:48,070
 darte toda la información que

587
00:13:48,070 --> 00:13:48,500
 me pides,

588
00:13:48,500 --> 00:13:49,760
 y la futura de este estilo

589
00:13:49,760 --> 00:13:50,500
 también".

590
00:13:50,500 --> 00:13:52,030
 Entonces están ahí pensando

591
00:13:52,030 --> 00:13:52,500
 qué hacer, cuál es

592
00:13:52,500 --> 00:13:54,500
 la mejor manera para,

593
00:13:54,500 --> 00:13:56,380
 en un compromiso entre

594
00:13:56,380 --> 00:13:58,380
 privacidad y utilidad,

595
00:13:58,380 --> 00:14:00,380
 que podamos volver a darle

596
00:14:00,380 --> 00:14:02,380
 algo más, algún dato más,

597
00:14:02,380 --> 00:14:04,380
 no se me ocurre, la escala de

598
00:14:04,380 --> 00:14:04,380
 colores de la pantalla,

599
00:14:04,380 --> 00:14:06,380
 algo que no estén pidiendo,

600
00:14:06,380 --> 00:14:08,380
 sin que sea resetear a cero

601
00:14:08,380 --> 00:14:08,380
 toda la información,

602
00:14:08,380 --> 00:14:10,380
 y tener que volver a pedirle

603
00:14:10,380 --> 00:14:12,380
 al escaso tanto porciento

604
00:14:12,380 --> 00:14:14,380
 de gente que se ha molestado en

605
00:14:14,380 --> 00:14:14,380
 activar esto,

606
00:14:14,380 --> 00:14:16,380
 en: "Bueno, y ahora

607
00:14:16,380 --> 00:14:17,920
 tengo que volver a darle al

608
00:14:17,920 --> 00:14:18,380
 scroll

609
00:14:18,380 --> 00:14:20,380
 en la configuración de Plasma,

610
00:14:20,380 --> 00:14:21,330
 y en la de Dolphin, que

611
00:14:21,330 --> 00:14:22,380
 también tiene una".

612
00:14:22,380 --> 00:14:24,380
 Y con este alegato sobre

613
00:14:24,380 --> 00:14:26,260
 cómo ayudar a KDE Plasma,

614
00:14:26,260 --> 00:14:28,260
 pues me despido

615
00:14:28,260 --> 00:14:30,260
 de vosotros, familia KDE-era,

616
00:14:30,260 --> 00:14:32,260
 espero vernos pronto, ya sabéis

617
00:14:32,260 --> 00:14:34,260
 que tenéis las notas del

618
00:14:34,260 --> 00:14:34,260
 programa

619
00:14:34,260 --> 00:14:36,260
 tanto en la página web como

620
00:14:36,260 --> 00:14:38,260
 en vuestro postcatcher, que tenéis

621
00:14:38,260 --> 00:14:38,260
 los datos

622
00:14:38,260 --> 00:14:40,260
 de contacto en la web, y que

623
00:14:40,260 --> 00:14:40,260
 estamos

624
00:14:40,260 --> 00:14:42,260
 en las redes sociales libres

625
00:14:42,260 --> 00:14:43,370
 por si nos queréis dejar

626
00:14:43,370 --> 00:14:44,260
 algún mensaje.

627
00:14:44,260 --> 00:14:46,260
 Un saludo, y de nuevo

628
00:14:46,260 --> 00:14:48,260
 gracias a Jorge por ayudarnos

629
00:14:48,260 --> 00:14:50,260
 con estos audios, hasta luego.

630
00:14:50,260 --> 00:14:50,500
 Gracias por ver el video.

631
00:14:50,500 --> 00:14:51,500
 Gracias.

