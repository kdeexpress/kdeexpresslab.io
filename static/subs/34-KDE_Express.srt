1
00:00:00,580 --> 00:00:01,850
 Buenas, episodio 34 de KDE

2
00:00:01,850 --> 00:00:04,430
 Express, grabado el 17 de septiembre

3
00:00:04,430 --> 00:00:05,760
 de 2024.

4
00:00:05,760 --> 00:00:07,380
 En el anterior íbamos cargados

5
00:00:07,380 --> 00:00:08,800
 de novedades y en este no nos

6
00:00:08,800 --> 00:00:09,860
 quedamos cortos.

7
00:00:09,860 --> 00:00:11,600
 Es más, he tenido que cortar

8
00:00:11,600 --> 00:00:13,430
 algunas de las cosas que tenía

9
00:00:13,430 --> 00:00:14,200
 que apuntar

10
00:00:14,200 --> 00:00:15,320
 porque si no iba a ser

11
00:00:15,320 --> 00:00:17,040
 demasiado condensado y me iba a

12
00:00:17,040 --> 00:00:18,080
 hacer un lío.

13
00:00:18,080 --> 00:00:19,650
 Hoy os traigo que se ha actualizado

14
00:00:19,650 --> 00:00:21,180
 la certificación de Blue Angel

15
00:00:21,180 --> 00:00:22,870
 con más categorías y opciones

16
00:00:22,870 --> 00:00:24,360
 más flexibles para medir la

17
00:00:24,360 --> 00:00:25,960
 eficiencia energética.

18
00:00:25,960 --> 00:00:27,510
 La Blue Angel, si os acordáis,

19
00:00:27,510 --> 00:00:28,920
 es una colaboración entre el

20
00:00:28,920 --> 00:00:31,120
 gobierno de Alemania y KDE Eco

21
00:00:31,120 --> 00:00:32,750
 para certificar varias cosas,

22
00:00:32,750 --> 00:00:34,800
 entre ellas software informático.

23
00:00:34,800 --> 00:00:36,140
 Okular fue el primer programa

24
00:00:36,140 --> 00:00:37,440
 que consiguió la etiqueta

25
00:00:37,440 --> 00:00:39,250
 y en KDE Eco siguen trabajando

26
00:00:39,250 --> 00:00:41,070
 para medir y certificar más

27
00:00:41,070 --> 00:00:42,040
 programas.

28
00:00:42,040 --> 00:00:43,630
 Es un proyecto que está muy

29
00:00:43,630 --> 00:00:44,140
 bien.

30
00:00:44,140 --> 00:00:45,730
 Sirve tanto a nivel monetario,

31
00:00:45,730 --> 00:00:47,500
 la eficiencia energética, como

32
00:00:47,500 --> 00:00:49,500
 de medio ambiente y de rendimiento.

33
00:00:49,500 --> 00:00:51,180
 O sea que es un win-win-win.

34
00:00:51,180 --> 00:00:53,050
 Y yo no puedo estar más orgulloso

35
00:00:53,050 --> 00:00:54,660
 de estar en un proyecto que

36
00:00:54,660 --> 00:00:55,940
 pone en valor estas cosas.

37
00:00:56,100 --> 00:00:58,200
 Luego tengo que Tellico, o Tellico,

38
00:00:58,200 --> 00:01:00,000
 no sé cómo se pronuncia,

39
00:01:00,000 --> 00:01:02,170
 pues tiene la versión 4.0

40
00:01:02,170 --> 00:01:03,890
 disponible en la que lo han

41
00:01:03,890 --> 00:01:05,180
 portado a Qt 6,

42
00:01:05,180 --> 00:01:06,860
 como hicimos nosotros hace poco

43
00:01:06,860 --> 00:01:08,150
 con Plasma y un montón de

44
00:01:08,150 --> 00:01:09,160
 aplicaciones.

45
00:01:09,160 --> 00:01:10,840
 Para el que no lo conozca, como

46
00:01:10,840 --> 00:01:12,020
 yo hasta hace poco,

47
00:01:12,020 --> 00:01:13,840
 es una aplicación para gestionar

48
00:01:13,840 --> 00:01:15,300
 colecciones que es capaz de

49
00:01:15,300 --> 00:01:17,020
 obtener un montón de metadata

50
00:01:17,020 --> 00:01:18,430
 sobre muchas categorías a

51
00:01:18,430 --> 00:01:19,200
 organizar.

52
00:01:19,200 --> 00:01:20,630
 O sea, tú le dices como el

53
00:01:20,630 --> 00:01:22,210
 nombre del libro y él te dice

54
00:01:22,210 --> 00:01:23,720
 un montón de datos de él

55
00:01:23,720 --> 00:01:24,980
 y tiene muchas cosas para

56
00:01:24,980 --> 00:01:25,720
 organizar.

57
00:01:25,720 --> 00:01:27,100
 Aparte de libros.

58
00:01:27,100 --> 00:01:28,900
 Luego, cuando estaba a punto de

59
00:01:28,900 --> 00:01:30,160
 grabar este episodio,

60
00:01:30,160 --> 00:01:31,960
 pues hace poco ha salido la noticia

61
00:01:31,960 --> 00:01:34,750
 de que se ha presentado el portátil

62
00:01:34,750 --> 00:01:36,660
 Slimbook KDE 6,

63
00:01:36,660 --> 00:01:38,160
 que es más, es mejor y sigue

64
00:01:38,160 --> 00:01:39,750
 teniendo el mismo compromiso

65
00:01:39,750 --> 00:01:41,060
 con el proyecto KDE.

66
00:01:41,060 --> 00:01:42,470
 Y además, unos pocos días

67
00:01:42,470 --> 00:01:43,100
 después,

68
00:01:43,100 --> 00:01:45,070
 Slimbook ha anunciado que

69
00:01:45,070 --> 00:01:47,350
 aunque el KDE Slimbook suele

70
00:01:47,350 --> 00:01:48,280
 venir con Neon,

71
00:01:48,280 --> 00:01:49,820
 aunque tú siempre puedes elegir

72
00:01:49,820 --> 00:01:51,260
 en su página la distribución

73
00:01:51,260 --> 00:01:52,000
 que quieres,

74
00:01:52,000 --> 00:01:53,780
 la que ellos tienen, por si

75
00:01:53,780 --> 00:01:55,020
 alguien no se aclara cuál

76
00:01:55,020 --> 00:01:55,660
 quiere,

77
00:01:55,660 --> 00:01:57,660
 y quiere los programas preinstalados

78
00:01:57,660 --> 00:01:58,440
 de Slimbook,

79
00:01:58,440 --> 00:02:00,240
 como el reconocimiento facial,

80
00:02:00,240 --> 00:02:02,700
 los perfiles de la CPU y esas

81
00:02:02,700 --> 00:02:03,520
 cosas,

82
00:02:03,520 --> 00:02:05,660
 pues ellos tienen Slimbook OS,

83
00:02:05,660 --> 00:02:06,860
 que en el fondo es un Ubuntu al

84
00:02:06,860 --> 00:02:08,340
 que no se le puede llamar Ubuntu,

85
00:02:08,340 --> 00:02:09,600
 al que alistaban los programas

86
00:02:09,600 --> 00:02:10,940
 y lo maqueaban un poquitín.

87
00:02:10,940 --> 00:02:12,840
 Hasta ahora venía solo en Gnome

88
00:02:12,840 --> 00:02:15,140
 y ahora, gracias al compromiso

89
00:02:15,140 --> 00:02:16,340
 que tiene Slimbook con el

90
00:02:16,340 --> 00:02:17,040
 proyecto KDE,

91
00:02:17,040 --> 00:02:18,570
 pues también te da la opción

92
00:02:18,570 --> 00:02:20,300
 de que Slimbook OS te venga con

93
00:02:20,300 --> 00:02:20,800
 Plasma.

94
00:02:20,800 --> 00:02:23,020
 Lo cual, miel sobre hojuelas.

95
00:02:23,020 --> 00:02:24,480
 Luego, para los que recordéis

96
00:02:24,480 --> 00:02:25,440
 el efecto de accesibilidad,

97
00:02:25,440 --> 00:02:27,560
 que comenté episodios atrás,

98
00:02:27,560 --> 00:02:29,060
 ese de que si mueves el ratón

99
00:02:29,060 --> 00:02:30,180
 muy rápido, va creciendo, va

100
00:02:30,180 --> 00:02:31,620
 creciendo y así es más fácil

101
00:02:31,620 --> 00:02:32,140
 de encontrar,

102
00:02:32,140 --> 00:02:34,850
 pues había gente que le gustaba,

103
00:02:34,850 --> 00:02:35,840
 pero no encontraba dónde

104
00:02:35,840 --> 00:02:36,120
 estaba

105
00:02:36,120 --> 00:02:37,740
 y hay gente que quería quitarlo.

106
00:02:37,740 --> 00:02:39,080
 ¿Cómo quitarlo? Ya lo dije en

107
00:02:39,080 --> 00:02:39,740
 el anterior.

108
00:02:39,740 --> 00:02:40,670
 Si no lo encontráis por

109
00:02:40,670 --> 00:02:41,740
 ninguna parte,

110
00:02:41,740 --> 00:02:42,750
 donde está especificado en las

111
00:02:42,750 --> 00:02:43,740
 notas del programa anterior

112
00:02:43,740 --> 00:02:44,820
 cómo se pone,

113
00:02:44,820 --> 00:02:45,970
 el problema es que sólo

114
00:02:45,970 --> 00:02:46,820
 funciona en Wayland.

115
00:02:46,820 --> 00:02:47,990
 Así que mirad si es que no

116
00:02:47,990 --> 00:02:48,820
 estáis en Wayland.

117
00:02:48,820 --> 00:02:51,520
 Tenemos algunas mejoras que se

118
00:02:51,520 --> 00:02:53,640
 vienen en Plasma 6.2.

119
00:02:53,640 --> 00:02:54,870
 Estas son cosas que todavía no

120
00:02:54,870 --> 00:02:55,220
 están,

121
00:02:55,220 --> 00:02:57,220
 que van a salir en octubre,

122
00:02:57,220 --> 00:02:59,220
 pero ya queda poco,

123
00:02:59,220 --> 00:03:01,220
 y como tiene tantas novedades,

124
00:03:01,220 --> 00:03:03,220
 o empiezo a contar algunas o se

125
00:03:03,220 --> 00:03:03,220
 me van a acumular

126
00:03:03,220 --> 00:03:05,220
 y al final las voy a perder.

127
00:03:05,220 --> 00:03:07,220
 Así que os voy a hacer un resumen

128
00:03:07,220 --> 00:03:07,810
 de lo más destacado que yo he

129
00:03:07,810 --> 00:03:08,220
 ido encontrando.

130
00:03:08,220 --> 00:03:09,220
 Y es, por ejemplo,

131
00:03:09,220 --> 00:03:10,770
 se permite renombrar los

132
00:03:10,770 --> 00:03:12,220
 dispositivos de audio

133
00:03:12,220 --> 00:03:13,550
 y elegir qué fuente de datos

134
00:03:13,550 --> 00:03:14,220
 le da el nombre.

135
00:03:14,220 --> 00:03:16,220
 Esto es que cuando tú le das

136
00:03:16,220 --> 00:03:17,220
 abajo a la izquierda a seleccionar

137
00:03:17,220 --> 00:03:18,220
 el dispositivo de audio,

138
00:03:18,220 --> 00:03:19,590
 pues te puede salir un chorraco

139
00:03:19,590 --> 00:03:21,640
 súper largo del nombre de la

140
00:03:21,640 --> 00:03:23,220
 tarjeta de sonido,

141
00:03:23,220 --> 00:03:24,320
 o lo mismo te sale en tres

142
00:03:24,320 --> 00:03:25,000
 palabras.

143
00:03:25,000 --> 00:03:26,370
 Había veces que eso, para la

144
00:03:26,370 --> 00:03:27,680
 gente que tiene una consola de

145
00:03:27,680 --> 00:03:29,000
 audio o una mesa de mezcla,

146
00:03:29,000 --> 00:03:30,880
 eso no es útil, porque de

147
00:03:30,880 --> 00:03:33,000
 donde coges la información,

148
00:03:33,000 --> 00:03:34,160
 pues no te dice exactamente con

149
00:03:34,160 --> 00:03:35,000
 qué corresponde.

150
00:03:35,000 --> 00:03:37,000
 Pues esto, por ejemplo, a Yoyo

151
00:03:37,000 --> 00:03:38,280
 le va a venir estupendo para

152
00:03:38,280 --> 00:03:39,000
 poder decir:

153
00:03:39,000 --> 00:03:41,000
 "Esta es la Scarlett boca 1.

154
00:03:41,000 --> 00:03:42,000
 Esto es la no sé qué".

155
00:03:42,000 --> 00:03:43,420
 Es un poco de nicho, pero es

156
00:03:43,420 --> 00:03:45,290
 muy útil para los que trasteamos

157
00:03:45,290 --> 00:03:46,000
 con audio.

158
00:03:46,000 --> 00:03:48,070
 Luego, más mundano, el plasmoide

159
00:03:48,070 --> 00:03:49,000
 del tiempo.

160
00:03:49,000 --> 00:03:50,050
 Ahora va a mostrar la

161
00:03:50,050 --> 00:03:52,000
 temperatura de "se siente cómodo".

162
00:03:52,000 --> 00:03:53,260
 Ya sabéis que en las medidas

163
00:03:53,260 --> 00:03:54,780
 de temperatura siempre te dicen:

164
00:03:54,780 --> 00:03:56,910
 "La máxima va a ser 35 o la

165
00:03:56,910 --> 00:03:58,780
 media va a ser 30".

166
00:03:58,780 --> 00:04:00,210
 Pero luego, según la humedad,

167
00:04:00,210 --> 00:04:01,150
 el viento, las otras

168
00:04:01,150 --> 00:04:02,780
 condiciones, otras variables,

169
00:04:02,780 --> 00:04:04,720
 eso lo tienen en cuenta muchos

170
00:04:04,720 --> 00:04:06,780
 proveedores meteorológicos,

171
00:04:06,780 --> 00:04:08,210
 y ahora el plasmoide te va a

172
00:04:08,210 --> 00:04:09,730
 dar eso que es lo único en lo

173
00:04:09,730 --> 00:04:10,780
 que yo me fijo.

174
00:04:10,780 --> 00:04:13,060
 Me da igual si pone 34, si lo

175
00:04:13,060 --> 00:04:14,780
 que pone "se siente cómodo"

176
00:04:14,780 --> 00:04:16,780
 son 25 o son 40.

177
00:04:16,780 --> 00:04:18,360
 También tenemos un rediseño

178
00:04:18,360 --> 00:04:19,990
 de la pantalla del teclado en

179
00:04:19,990 --> 00:04:20,780
 las configuraciones.

180
00:04:20,780 --> 00:04:22,620
 El plasmoide del brillo

181
00:04:22,620 --> 00:04:24,560
 permitirá configurar interiores

182
00:04:24,560 --> 00:04:26,560
 individualmente cada monitor.

183
00:04:26,560 --> 00:04:28,140
 Esto es que si tú tienes dos

184
00:04:28,140 --> 00:04:29,560
 monitores, el plasmoide no te

185
00:04:29,560 --> 00:04:30,560
 dejaba hasta ahora

186
00:04:30,560 --> 00:04:32,900
 seleccionar que es que tengo un

187
00:04:32,900 --> 00:04:34,560
 monitor que necesita más brillo

188
00:04:34,560 --> 00:04:34,560
 y otro menos.

189
00:04:34,560 --> 00:04:36,560
 Y esto ahora lo han solucionado

190
00:04:36,560 --> 00:04:37,500
 y va a estar muy bien cuando

191
00:04:37,500 --> 00:04:38,560
 tengamos Plasma 6.2.

192
00:04:38,560 --> 00:04:40,560
 También tenemos un nuevo icono

193
00:04:40,560 --> 00:04:42,560
 sobre el perfil de energía

194
00:04:42,560 --> 00:04:44,210
 que ahora podrá mostrar el

195
00:04:44,210 --> 00:04:46,560
 icono del perfil y de la batería.

196
00:04:46,560 --> 00:04:48,190
 Esto es que antes tú podías

197
00:04:48,190 --> 00:04:50,560
 poner: "Quiero perfil de rendimiento

198
00:04:50,560 --> 00:04:52,560
 medio o económico".

199
00:04:52,560 --> 00:04:54,340
 Y luego también tenía el ver,

200
00:04:54,340 --> 00:04:55,590
 ¿cuánta batería le quedaba a

201
00:04:55,590 --> 00:04:56,340
 tu portátil?

202
00:04:56,340 --> 00:04:57,650
 Ahora esto lo van a unificar

203
00:04:57,650 --> 00:04:58,690
 todo en un icono y va a mostrar

204
00:04:58,690 --> 00:04:59,340
 las dos informaciones.

205
00:04:59,340 --> 00:05:01,430
 Discover te va a mostrar las

206
00:05:01,430 --> 00:05:02,340
 diferencias entre las licencias

207
00:05:02,340 --> 00:05:03,340
 propietarias y no libres.

208
00:05:03,340 --> 00:05:04,230
 ¿Sabes que Discover cuando

209
00:05:04,230 --> 00:05:05,340
 estás instalando aplicaciones

210
00:05:05,340 --> 00:05:08,960
 te deja decir si este programa

211
00:05:08,960 --> 00:05:10,340
 tiene GPL o en lo que ahora?

212
00:05:10,340 --> 00:05:11,720
 Pues ahora han mejorado para

213
00:05:11,720 --> 00:05:13,340
 distinguir mejor cuando no son

214
00:05:13,340 --> 00:05:14,340
 completamente libres

215
00:05:14,340 --> 00:05:16,340
 otro tipo de licencias que hay.

216
00:05:16,340 --> 00:05:17,790
 También tenemos que los nuevos

217
00:05:17,790 --> 00:05:18,340
 plasmoides,

218
00:05:18,340 --> 00:05:20,310
 cuando se instale alguno nuevo

219
00:05:20,310 --> 00:05:21,340
 en el ordenador,

220
00:05:21,340 --> 00:05:23,230
 van a aparecer arriba en el menú

221
00:05:23,230 --> 00:05:24,120
 de selección.

222
00:05:24,120 --> 00:05:25,740
 Por ejemplo, tú tienes tus 20

223
00:05:25,740 --> 00:05:26,120
 plasmoides de siempre,

224
00:05:26,120 --> 00:05:28,550
 instalas uno desde Auro, desde

225
00:05:28,550 --> 00:05:30,120
 la página de Store de KDE,

226
00:05:30,120 --> 00:05:31,360
 para no tener que ir buscando

227
00:05:31,360 --> 00:05:32,120
 entre los 20

228
00:05:32,120 --> 00:05:33,220
 cuál es el nombre que

229
00:05:33,220 --> 00:05:34,120
 realmente tiene,

230
00:05:34,120 --> 00:05:36,120
 te va a aparecer arriba destacado.

231
00:05:36,120 --> 00:05:38,120
 También tenemos que las opciones

232
00:05:38,120 --> 00:05:38,120
 de accesibilidad

233
00:05:38,120 --> 00:05:40,120
 para problemas visuales de

234
00:05:40,120 --> 00:05:40,120
 color,

235
00:05:40,120 --> 00:05:42,120
 por ejemplo el daltonismo,

236
00:05:42,120 --> 00:05:44,120
 estarán ahora en accesibilidad,

237
00:05:44,120 --> 00:05:46,110
 en vez de con el resto de efectos

238
00:05:46,110 --> 00:05:47,120
 de escritorio.

239
00:05:47,120 --> 00:05:48,120
 Y esta página de configuración

240
00:05:48,120 --> 00:05:49,480
 ha sido mejorada en varios

241
00:05:49,480 --> 00:05:50,520
 aspectos para hacerla más

242
00:05:50,520 --> 00:05:51,120
 accesible.

243
00:05:51,120 --> 00:05:53,120
 Tiene sentido que si estas opciones

244
00:05:53,120 --> 00:05:53,900
 se usan

245
00:05:53,900 --> 00:05:55,900
 para cosas de accesibilidad,

246
00:05:55,900 --> 00:05:57,770
 pues lo junten todo en el apartado

247
00:05:57,770 --> 00:05:58,900
 de accesibilidad.

248
00:05:58,900 --> 00:06:00,900
 Y luego, a partir de Plasma 6.2,

249
00:06:00,900 --> 00:06:02,890
 se va a poner un recordatorio

250
00:06:02,890 --> 00:06:04,900
 de que puedes donar el proyecto.

251
00:06:04,900 --> 00:06:06,900
 Por supuesto, se podrá quitar,

252
00:06:06,900 --> 00:06:06,900
 es desinstalable,

253
00:06:06,900 --> 00:06:08,900
 y solo se hará una vez al año.

254
00:06:08,900 --> 00:06:09,960
 Pero está comprobado

255
00:06:09,960 --> 00:06:11,900
 científicamente que,

256
00:06:11,900 --> 00:06:13,420
 si no recuerdas de vez en

257
00:06:13,420 --> 00:06:13,900
 cuando,

258
00:06:13,900 --> 00:06:16,900
 a la mayoría de gente se le olvida

259
00:06:16,900 --> 00:06:18,170
 que los proyectos necesitan

260
00:06:18,170 --> 00:06:18,900
 financiación,

261
00:06:18,900 --> 00:06:21,260
 y un recordatorio al año creo

262
00:06:21,260 --> 00:06:23,310
 que no es mucho pedirle a la

263
00:06:23,310 --> 00:06:23,680
 gente,

264
00:06:23,680 --> 00:06:25,680
 teniendo en cuenta que la aplicación

265
00:06:25,680 --> 00:06:26,470
 y el software que se le está

266
00:06:26,470 --> 00:06:26,680
 dando

267
00:06:26,680 --> 00:06:27,800
 es de mucha calidad y es

268
00:06:27,800 --> 00:06:28,680
 completamente gratis.

269
00:06:28,680 --> 00:06:30,680
 Luego, también tengo que Gea2412,

270
00:06:30,680 --> 00:06:32,680
 que eso sería en diciembre,

271
00:06:32,680 --> 00:06:35,170
 Okular permitirá hablar el

272
00:06:35,170 --> 00:06:36,680
 texto de la página mostrada,

273
00:06:36,680 --> 00:06:38,680
 lo cual no es nada, eh.

274
00:06:38,680 --> 00:06:41,030
 Esto ahí es sin cosas raras

275
00:06:41,030 --> 00:06:41,680
 online,

276
00:06:41,680 --> 00:06:42,950
 y vas a poder decirle que te lea

277
00:06:42,950 --> 00:06:43,680
 una página.

278
00:06:43,680 --> 00:06:44,680
 Y luego, aunque lo tengo aquí,

279
00:06:44,680 --> 00:06:46,320
 aunque yo creo que tengo tantas

280
00:06:46,320 --> 00:06:47,390
 cosas que no sé si se me han

281
00:06:47,390 --> 00:06:47,680
 movido,

282
00:06:47,680 --> 00:06:49,190
 es que las aplicaciones podrán

283
00:06:49,190 --> 00:06:49,680
 bloquear

284
00:06:49,680 --> 00:06:51,240
 que el equipo se ponga en modo

285
00:06:51,240 --> 00:06:52,680
 reposo o se bloquee.

286
00:06:52,680 --> 00:06:53,460
 Esto es que, a veces,

287
00:06:53,460 --> 00:06:54,790
 si tú tienes, por ejemplo, una

288
00:06:54,790 --> 00:06:56,460
 película puesta,

289
00:06:56,460 --> 00:06:58,260
 eso puede inhibir perfectamente

290
00:06:58,260 --> 00:06:59,460
 el salvapantalla.

291
00:06:59,460 --> 00:07:01,460
 Hay otro tipo de aplicaciones

292
00:07:01,460 --> 00:07:02,460
 que a lo mejor te convence,

293
00:07:02,460 --> 00:07:04,130
 bueno, no, te conviene, por

294
00:07:04,130 --> 00:07:04,460
 ejemplo,

295
00:07:04,460 --> 00:07:06,280
 que Ktorrent, cuando estás bajando

296
00:07:06,280 --> 00:07:06,460
 algo,

297
00:07:06,460 --> 00:07:08,020
 si tú quieres que se te termine

298
00:07:08,020 --> 00:07:08,460
 de bajar,

299
00:07:08,460 --> 00:07:09,650
 no quieres que el equipo entre

300
00:07:09,650 --> 00:07:10,460
 en suspensión.

301
00:07:10,460 --> 00:07:11,900
 Ahora las aplicaciones van a

302
00:07:11,900 --> 00:07:13,760
 tener la posibilidad de bloquear

303
00:07:13,760 --> 00:07:14,460
 este tipo de cosas.

304
00:07:14,460 --> 00:07:18,650
 Para Plasma 6.2 ya hemos cerrado

305
00:07:18,650 --> 00:07:19,660
 todas las cosas nuevas que

306
00:07:19,660 --> 00:07:20,460
 pueden entrar,

307
00:07:20,460 --> 00:07:22,460
 de hecho ya está la beta lanzada,

308
00:07:22,460 --> 00:07:23,240
 ya se puede probar,

309
00:07:23,240 --> 00:07:25,510
 y lo que se ha visto es que el

310
00:07:25,510 --> 00:07:26,960
 número de bugs está a niveles

311
00:07:26,960 --> 00:07:28,240
 mínimos de 2015.

312
00:07:28,240 --> 00:07:29,800
 Esto quiere decir que desde que

313
00:07:29,800 --> 00:07:33,370
 hicieron el énfasis en el QA,

314
00:07:33,370 --> 00:07:34,240
 en el testing,

315
00:07:34,240 --> 00:07:36,270
 pues Plasma 6 ha sido mucho

316
00:07:36,270 --> 00:07:38,380
 más estable que algunas otras

317
00:07:38,380 --> 00:07:39,240
 migraciones

318
00:07:39,240 --> 00:07:41,830
 cuando pasamos de 3 a 4 o de 4

319
00:07:41,830 --> 00:07:42,960
 a 5, se ha estabilizado muy

320
00:07:42,960 --> 00:07:43,240
 rápido.

321
00:07:43,240 --> 00:07:44,790
 Y que ahora que estamos metiendo

322
00:07:44,790 --> 00:07:46,240
 mejoras, siempre hay bugs,

323
00:07:46,240 --> 00:07:47,810
 porque eso es imposible que no

324
00:07:47,810 --> 00:07:48,240
 lo haya,

325
00:07:48,240 --> 00:07:49,240
 pero no están saliendo tantos

326
00:07:49,240 --> 00:07:51,090
 y se están pudiendo arreglar

327
00:07:51,090 --> 00:07:52,240
 rápidamente.

328
00:07:52,240 --> 00:07:53,020
 Luego,

329
00:07:53,020 --> 00:07:56,200
 en el mundo KDE, os traigo una

330
00:07:56,200 --> 00:07:57,700
 noticia que es sobre software

331
00:07:57,700 --> 00:07:58,020
 libre en general,

332
00:07:58,020 --> 00:08:00,090
 que es que hay una campaña

333
00:08:00,090 --> 00:08:01,120
 contra el recorte de fondos a

334
00:08:01,120 --> 00:08:02,820
 proyectos de software libre por

335
00:08:02,820 --> 00:08:03,020
 la UE.

336
00:08:03,020 --> 00:08:06,160
 La UE tenía como unas subsidiarias

337
00:08:06,160 --> 00:08:07,020
 que daban dinero a proyectos

338
00:08:07,020 --> 00:08:07,020
 pequeños

339
00:08:07,020 --> 00:08:09,210
 que pensaban que eran estratégicos,

340
00:08:09,210 --> 00:08:11,020
 y eso sigue existiendo,

341
00:08:11,020 --> 00:08:12,640
 pero si ya es difícil

342
00:08:12,640 --> 00:08:14,460
 conseguir dinero, y el dinero

343
00:08:14,460 --> 00:08:15,020
 estaba muy bien,

344
00:08:15,020 --> 00:08:16,550
 pero tampoco era la panacea

345
00:08:16,550 --> 00:08:18,020
 para todos los proyectos,

346
00:08:18,020 --> 00:08:18,900
 porque hay miles y miles de

347
00:08:18,900 --> 00:08:20,020
 proyectos de software libre,

348
00:08:20,020 --> 00:08:21,940
 el problema es que en el

349
00:08:21,940 --> 00:08:22,800
 próximo...

350
00:08:22,800 --> 00:08:24,800
 presupuesto económico de la UE,

351
00:08:24,800 --> 00:08:26,540
 o sea, dentro de un par de

352
00:08:26,540 --> 00:08:26,800
 años creo que es,

353
00:08:26,800 --> 00:08:28,200
 se va a rebajar mucho esa

354
00:08:28,200 --> 00:08:28,800
 cantidad,

355
00:08:28,800 --> 00:08:30,140
 con lo cual podrían ser

356
00:08:30,140 --> 00:08:32,800
 problemas para muchas aplicaciones

357
00:08:32,800 --> 00:08:34,800
 que dependen de esos proyectos

358
00:08:34,800 --> 00:08:34,800
 para que esos desarrolladores

359
00:08:34,800 --> 00:08:36,800
 tengan tiempo para seguir manteniéndolos.

360
00:08:36,800 --> 00:08:38,640
 Todavía en KDE, que me ha saltado

361
00:08:38,640 --> 00:08:38,800
 uno,

362
00:08:38,800 --> 00:08:41,360
 os enlazo un artículo sobre si

363
00:08:41,360 --> 00:08:42,800
 KDE abarca demasiado,

364
00:08:42,800 --> 00:08:43,990
 para que veáis que no son

365
00:08:43,990 --> 00:08:44,800
 sólo noticias,

366
00:08:44,800 --> 00:08:46,300
 y que no sólo todo es bonito

367
00:08:46,300 --> 00:08:46,800
 para KDE.

368
00:08:46,800 --> 00:08:49,300
 Es un artículo que es de Linus

369
00:08:49,300 --> 00:08:49,800
 Addictor,

370
00:08:49,800 --> 00:08:51,640
 y viene a decir que a él le

371
00:08:51,640 --> 00:08:52,580
 gusta mucho KDE,

372
00:08:52,580 --> 00:08:53,580
 está encantado,

373
00:08:53,580 --> 00:08:55,580
 pero que de vez en cuando ve

374
00:08:55,580 --> 00:08:56,580
 que el proyecto KDE

375
00:08:56,580 --> 00:08:58,670
 empieza a programar una aplicación

376
00:08:58,670 --> 00:09:00,580
 para la que ya existe otra,

377
00:09:00,580 --> 00:09:01,790
 y no tiene claro si eso le

378
00:09:01,790 --> 00:09:03,580
 está quitando tiempo

379
00:09:03,580 --> 00:09:05,210
 a las mejoras de lo gordo y lo

380
00:09:05,210 --> 00:09:06,580
 realmente necesario,

381
00:09:06,580 --> 00:09:08,580
 o incluso que si funciona bien

382
00:09:08,580 --> 00:09:09,580
 la aplicación de KDE,

383
00:09:09,580 --> 00:09:10,710
 le está haciendo la puñeta al

384
00:09:10,710 --> 00:09:12,580
 desarrollador que ya existía,

385
00:09:12,580 --> 00:09:14,040
 y entonces la otra no va a ir

386
00:09:14,040 --> 00:09:14,580
 bien.

387
00:09:14,580 --> 00:09:16,580
 A ver, yo siempre pienso que

388
00:09:16,580 --> 00:09:17,580
 los desarrolladores son libres,

389
00:09:17,580 --> 00:09:19,300
 y no porque alguien se ponga a

390
00:09:19,300 --> 00:09:20,580
 programar otra cosa,

391
00:09:20,580 --> 00:09:22,360
 realmente significará que,

392
00:09:22,360 --> 00:09:23,300
 fuera a estar programando lo

393
00:09:23,300 --> 00:09:24,360
 que nosotros quisiéramos.

394
00:09:24,360 --> 00:09:25,860
 Con lo cual, quien lo hace por

395
00:09:25,860 --> 00:09:26,360
 software libre,

396
00:09:26,360 --> 00:09:27,750
 tiene todo el derecho a hacer

397
00:09:27,750 --> 00:09:29,360
 sus proyectos particulares.

398
00:09:29,360 --> 00:09:30,740
 Yo soy de los que estaría

399
00:09:30,740 --> 00:09:31,360
 encantado,

400
00:09:31,360 --> 00:09:32,360
 y si hubiera una varita mágica,

401
00:09:32,360 --> 00:09:34,240
 pues pondría a la mayoría de

402
00:09:34,240 --> 00:09:35,360
 gente de acuerdo

403
00:09:35,360 --> 00:09:37,910
 en unificar esfuerzos en uno,

404
00:09:37,910 --> 00:09:39,360
 dos o tres alternativas,

405
00:09:39,360 --> 00:09:40,980
 pero la gracia del software

406
00:09:40,980 --> 00:09:41,360
 libre es que,

407
00:09:41,360 --> 00:09:43,360
 si estoy aburrido y sé programar,

408
00:09:43,360 --> 00:09:44,610
 en un fin de semana me pongo a

409
00:09:44,610 --> 00:09:45,360
 hacer lo que me dé la gana,

410
00:09:45,360 --> 00:09:47,360
 y lo mismo me hubiera ido de caña

411
00:09:47,360 --> 00:09:48,360
 o me hubiera ido al cine,

412
00:09:48,360 --> 00:09:49,860
 no estaría programando en el

413
00:09:49,860 --> 00:09:51,360
 proyecto que a ti te gusta.

414
00:09:51,360 --> 00:09:52,140
 Con lo cual, yo creo,

415
00:09:52,140 --> 00:09:54,140
 que es lo que tenemos,

416
00:09:54,140 --> 00:09:56,140
 tiene sus pros y sus contras,

417
00:09:56,140 --> 00:09:58,140
 pero al final los pros,

418
00:09:58,140 --> 00:10:00,140
 creo que, como podéis imaginar,

419
00:10:00,140 --> 00:10:02,140
 están por encima de los contras.

420
00:10:02,140 --> 00:10:03,140
 Y ahora sí ya,

421
00:10:03,140 --> 00:10:05,220
 la última noticia que está

422
00:10:05,220 --> 00:10:06,140
 fuera del proyecto KDE,

423
00:10:06,140 --> 00:10:08,450
 es que me he encontrado un artículo

424
00:10:08,450 --> 00:10:09,140
 muy chulo,

425
00:10:09,140 --> 00:10:11,260
 que habla de hardware libre,

426
00:10:11,260 --> 00:10:13,140
 software libre y agricultura,

427
00:10:13,140 --> 00:10:14,820
 y cómo se puede usar uno con

428
00:10:14,820 --> 00:10:15,140
 otro,

429
00:10:15,140 --> 00:10:17,140
 y cómo hay lazos de unión,

430
00:10:17,140 --> 00:10:18,440
 que no va mucho con el tema de

431
00:10:18,440 --> 00:10:19,140
 este podcast,

432
00:10:19,140 --> 00:10:20,730
 pero os lo dejo al final de las

433
00:10:20,730 --> 00:10:21,920
 notas por si queréis.

434
00:10:21,920 --> 00:10:24,210
 Y luego no quería irme sin

435
00:10:24,210 --> 00:10:25,920
 recordaros de que

436
00:10:25,920 --> 00:10:28,240
 ya tenemos los vídeos de la

437
00:10:28,240 --> 00:10:29,920
 Akademy en bruto,

438
00:10:29,920 --> 00:10:31,480
 pero ahí hay mucho trabajo por

439
00:10:31,480 --> 00:10:31,920
 delante,

440
00:10:31,920 --> 00:10:32,920
 así que nos vendría muy bien

441
00:10:32,920 --> 00:10:33,920
 si hay algún voluntario

442
00:10:33,920 --> 00:10:35,580
 que quiera cogerse algún

443
00:10:35,580 --> 00:10:35,920
 vídeo

444
00:10:35,920 --> 00:10:38,510
 y ver dónde acaba la primera

445
00:10:38,510 --> 00:10:38,920
 charla,

446
00:10:38,920 --> 00:10:40,920
 dónde acaba la segunda,

447
00:10:40,920 --> 00:10:42,590
 y cortarlo para que tengamos

448
00:10:42,590 --> 00:10:42,920
 vídeos exactamente

449
00:10:42,920 --> 00:10:44,920
 de la duración de cada charla

450
00:10:44,920 --> 00:10:45,920
 para poder publicarlo.

451
00:10:45,920 --> 00:10:47,220
 Tenéis los métodos de contacto

452
00:10:47,220 --> 00:10:47,920
 en la página web,

453
00:10:47,920 --> 00:10:49,910
 sabéis que estamos en Mastodon,

454
00:10:49,910 --> 00:10:50,920
 en Correo, en Matrix,

455
00:10:50,920 --> 00:10:52,920
 y en Telegram.

456
00:10:52,920 --> 00:10:54,920
 Ya con esto me despido,

457
00:10:54,920 --> 00:10:56,270
 espero que os haya resultado

458
00:10:56,270 --> 00:10:56,920
 interesante,

459
00:10:56,920 --> 00:10:58,920
 gracias por vuestro tiempo,

460
00:10:58,920 --> 00:11:00,920
 y si sois socios, atentos al

461
00:11:00,920 --> 00:11:00,920
 correo electrónico,

462
00:11:00,920 --> 00:11:02,920
 que hay novedades pronto.

463
00:11:02,920 --> 00:11:04,920
 Un saludo.

