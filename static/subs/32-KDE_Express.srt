1
00:00:00,000 --> 00:00:02,030
 Buenas, esto es KDE Express y

2
00:00:02,030 --> 00:00:03,670
 estoy grabando el 14 de agosto

3
00:00:03,670 --> 00:00:04,660
 de 2024.

4
00:00:04,660 --> 00:00:06,910
 Hoy me uno a los podcasters que

5
00:00:06,910 --> 00:00:09,080
 aunque saben que en agosto la

6
00:00:09,080 --> 00:00:10,020
 mayoría de la gente

7
00:00:10,020 --> 00:00:11,320
 está haciendo otras cosas y no

8
00:00:11,320 --> 00:00:12,520
 escucháis tantos podcats,

9
00:00:12,520 --> 00:00:14,450
 que siguen queriendo contaros

10
00:00:14,450 --> 00:00:16,540
 las novedades, en mi caso, de

11
00:00:16,540 --> 00:00:17,720
 la comunidad KDE.

12
00:00:17,720 --> 00:00:20,310
 Hoy os traigo unas cuantas noticias,

13
00:00:20,310 --> 00:00:22,540
 la primera de accesibilidad,

14
00:00:22,540 --> 00:00:24,230
 que es que en la próxima

15
00:00:24,230 --> 00:00:26,920
 versión 6.2, que vendrá por

16
00:00:26,920 --> 00:00:27,740
 octubre,

17
00:00:27,740 --> 00:00:29,680
 en la página de configuración

18
00:00:29,680 --> 00:00:32,340
 del sistema, si os vais a accesibilidad,

19
00:00:32,340 --> 00:00:34,130
 ahí podéis cambiar el timbre

20
00:00:34,130 --> 00:00:35,730
 del sonido de las alertas que

21
00:00:35,730 --> 00:00:36,280
 salen.

22
00:00:36,280 --> 00:00:39,600
 Pues ahora el selector de ficheros

23
00:00:39,600 --> 00:00:43,080
 os va a dejar elegir ficheros.oga,

24
00:00:43,080 --> 00:00:45,840
 que suelen ser los ogg también,

25
00:00:45,840 --> 00:00:47,680
 pero con a de audio.

26
00:00:47,680 --> 00:00:49,930
 Y aparte os dirá en un listado

27
00:00:49,930 --> 00:00:52,160
 los formatos que soporta.

28
00:00:52,160 --> 00:00:53,740
 Eso es una pequeña mejora,

29
00:00:53,740 --> 00:00:55,340
 pero de estas del buen camino

30
00:00:55,340 --> 00:00:56,620
 que a mí me gustan.

31
00:00:56,620 --> 00:00:57,620
 Luego tenemos en...

32
00:00:57,620 --> 00:00:59,150
 En nivel de aplicaciones, que

33
00:00:59,150 --> 00:01:00,500
 digiKam se ha actualizado a la

34
00:01:00,500 --> 00:01:01,700
 versión 8.4,

35
00:01:01,700 --> 00:01:03,620
 que tiene muchos arreglos y

36
00:01:03,620 --> 00:01:05,120
 algunas novedades,

37
00:01:05,120 --> 00:01:06,840
 como que ahora tiene etiquetas

38
00:01:06,840 --> 00:01:09,040
 automáticas con análisis profundo

39
00:01:09,040 --> 00:01:10,190
 y traducción en línea de

40
00:01:10,190 --> 00:01:11,160
 estas etiquetas.

41
00:01:11,160 --> 00:01:13,120
 Esto quiere decir que tiene

42
00:01:13,120 --> 00:01:15,250
 como un analizador interno en

43
00:01:15,250 --> 00:01:15,740
 local

44
00:01:15,740 --> 00:01:17,400
 que es capaz de detectar cosas.

45
00:01:17,400 --> 00:01:18,600
 Y entonces te etiqueta porque

46
00:01:18,600 --> 00:01:19,780
 si hay un perro, que si hay una

47
00:01:19,780 --> 00:01:20,780
 nube, este tipo de cosas.

48
00:01:20,780 --> 00:01:22,800
 Pero como las etiquetas trabajan

49
00:01:22,800 --> 00:01:23,620
 en inglés,

50
00:01:23,620 --> 00:01:25,920
 ahora tiene un traductor automático,

51
00:01:26,500 --> 00:01:28,580
 que las puedes configurar tú,

52
00:01:28,580 --> 00:01:29,640
 voluntariamente,

53
00:01:29,640 --> 00:01:31,390
 si quieres que te lo analices y

54
00:01:31,390 --> 00:01:32,960
 si quieres que te lo traduzcas.

55
00:01:32,960 --> 00:01:34,330
 Luego también trae que tengo

56
00:01:34,330 --> 00:01:35,560
 por aquí apuntado que tiene

57
00:01:35,560 --> 00:01:36,820
 una nueva documentación,

58
00:01:36,820 --> 00:01:38,430
 tanto en la página web como en

59
00:01:38,430 --> 00:01:39,580
 formato de ogbog.

60
00:01:39,580 --> 00:01:43,320
 También añade mejoras de formato

61
00:01:43,320 --> 00:01:44,800
 de ficheros de las cámaras,

62
00:01:44,800 --> 00:01:46,490
 estilo de, pues, la Canon, no

63
00:01:46,490 --> 00:01:47,880
 sé qué, la no sé cuánto.

64
00:01:47,880 --> 00:01:49,970
 Cada una tiene sus peculiaridades

65
00:01:49,970 --> 00:01:51,560
 y van añadiendo cámaras que

66
00:01:51,560 --> 00:01:52,660
 leen mejor sus ficheros.

67
00:01:52,660 --> 00:01:54,320
 También tenemos que tiene una

68
00:01:54,320 --> 00:01:55,380
 nueva utilidad OCR,

69
00:01:55,380 --> 00:01:56,340
 de...

70
00:01:56,340 --> 00:01:58,340
 "Spell checking", de corrección

71
00:01:58,340 --> 00:02:00,970
 gramatical y de configuración

72
00:02:00,970 --> 00:02:02,140
 de idioma.

73
00:02:02,140 --> 00:02:03,400
 También ponen por aquí que

74
00:02:03,400 --> 00:02:05,460
 han mejorado la "metadata" y el

75
00:02:05,460 --> 00:02:06,700
 "lexiv tool",

76
00:02:06,700 --> 00:02:08,410
 que eso es como si fuera el ID3

77
00:02:08,410 --> 00:02:09,180
 de los audios.

78
00:02:09,180 --> 00:02:11,250
 Es como decir que si la cámara

79
00:02:11,250 --> 00:02:13,020
 ha grabado en vertical o en

80
00:02:13,020 --> 00:02:13,940
 horizontal,

81
00:02:13,940 --> 00:02:15,090
 que si ha grabado con no sé

82
00:02:15,090 --> 00:02:16,300
 cuánto de exposición,

83
00:02:16,300 --> 00:02:17,350
 todo ese tipo de cosas de la

84
00:02:17,350 --> 00:02:18,680
 gente que entiende de cámaras,

85
00:02:18,680 --> 00:02:19,580
 que no es mi caso.

86
00:02:19,580 --> 00:02:21,790
 También tienen una utilidad

87
00:02:21,790 --> 00:02:24,360
 que han mejorado para renombrar

88
00:02:24,360 --> 00:02:25,220
 ficheros.

89
00:02:25,220 --> 00:02:26,180
 Y qué más...

90
00:02:26,180 --> 00:02:28,340
 El buscador y la base de datos

91
00:02:28,340 --> 00:02:30,900
 también la han hecho mejorar.

92
00:02:30,900 --> 00:02:33,370
 La calidad del clasificador de

93
00:02:33,370 --> 00:02:35,630
 imágenes, que realmente no sé

94
00:02:35,630 --> 00:02:36,540
 lo que es.

95
00:02:36,540 --> 00:02:38,850
 También han mejorado la integración

96
00:02:38,850 --> 00:02:40,790
 con el Gmic, que es otra cosa

97
00:02:40,790 --> 00:02:41,800
 que se me escapa.

98
00:02:41,800 --> 00:02:43,970
 Y por supuesto, la migración a

99
00:02:43,970 --> 00:02:45,900
 Qt 6 que va llegando a todas

100
00:02:45,900 --> 00:02:48,700
 las aplicaciones del Paraguas KDE.

101
00:02:48,700 --> 00:02:51,920
 Y ya lo demás son arreglos típicos.

102
00:02:51,920 --> 00:02:53,440
 Eso era en cuanto a digiKam.

103
00:02:53,440 --> 00:02:56,020
 Luego tenemos que Plasma 6 se...

104
00:02:56,020 --> 00:02:57,840
 Sigue actualizando poco a poco,

105
00:02:57,840 --> 00:02:59,550
 una vez que ya hicimos la migración

106
00:02:59,550 --> 00:02:59,720
 gorda.

107
00:02:59,720 --> 00:03:01,890
 Y yo ahora mismo estoy en Plasma

108
00:03:01,890 --> 00:03:05,980
 6.1.4 y en el framework 6.4.0.

109
00:03:05,980 --> 00:03:09,120
 Pues en Plasma 6.1.2 han mejorado

110
00:03:09,120 --> 00:03:10,830
 el manejo de los Flatpaks en

111
00:03:10,830 --> 00:03:11,580
 Discover.

112
00:03:11,580 --> 00:03:12,750
 No sé si Flatpak es una cosa

113
00:03:12,750 --> 00:03:13,760
 que a mí me gusta mucho.

114
00:03:13,760 --> 00:03:15,180
 Te simplifica buscar cosas,

115
00:03:15,180 --> 00:03:17,460
 tiene el enjaulado para ser un

116
00:03:17,460 --> 00:03:18,680
 pelín más seguro.

117
00:03:18,680 --> 00:03:20,340
 Tampoco es la panacea.

118
00:03:20,340 --> 00:03:21,490
 Pero Flatpak tiene el

119
00:03:21,490 --> 00:03:22,860
 problema de que los runtime,

120
00:03:22,860 --> 00:03:24,100
 como si fueran las dependencias

121
00:03:24,100 --> 00:03:25,860
 gordas que todos usan,

122
00:03:25,860 --> 00:03:27,310
 a veces empiezan a ocupar y una

123
00:03:27,310 --> 00:03:28,750
 aplicación necesita una y otra

124
00:03:28,750 --> 00:03:30,200
 aplicación necesita otra.

125
00:03:30,200 --> 00:03:31,880
 Y entonces al final tienes tres

126
00:03:31,880 --> 00:03:33,300
 de los mismos en versiones

127
00:03:33,300 --> 00:03:34,200
 diferentes.

128
00:03:34,200 --> 00:03:35,200
 Ocupa bastante espacio.

129
00:03:35,200 --> 00:03:37,390
 Si tienes un disco duro grande,

130
00:03:37,390 --> 00:03:38,400
 te da igual.

131
00:03:38,400 --> 00:03:39,610
 Si no tienes un TOC como yo que

132
00:03:39,610 --> 00:03:40,860
 te gusta tenerlo limpio.

133
00:03:40,860 --> 00:03:42,740
 En línea de comando había maneras

134
00:03:42,740 --> 00:03:44,460
 de hacer limpieza, es decir, oye,

135
00:03:44,460 --> 00:03:45,160
 bórrame todas las

136
00:03:45,160 --> 00:03:47,220
 que ya no uses o actualízame

137
00:03:47,220 --> 00:03:47,860
 esta.

138
00:03:47,860 --> 00:03:49,710
 Ahora lo que han hecho es que

139
00:03:49,710 --> 00:03:51,940
 automáticamente Discover hace

140
00:03:51,940 --> 00:03:53,700
 rebases de los runtimes y

141
00:03:53,700 --> 00:03:54,240
 eliminan

142
00:03:54,240 --> 00:03:55,700
 los que están.

143
00:03:55,700 --> 00:03:58,810
 Que hacen mantenimiento más

144
00:03:58,810 --> 00:04:00,950
 automático la aplicación

145
00:04:00,950 --> 00:04:01,960
 gráfica que antes podías

146
00:04:01,960 --> 00:04:02,200
 hacer

147
00:04:02,200 --> 00:04:04,500
 tú por línea de consola.

148
00:04:04,500 --> 00:04:11,620
 En Plasma 6.1.3 y 6.2.0, porque

149
00:04:11,620 --> 00:04:12,120
 son arreglos y mejoras que van

150
00:04:12,120 --> 00:04:12,640
 a venir por parte, han metido

151
00:04:12,640 --> 00:04:15,110
 mucha caña en plan accesibilidad

152
00:04:15,110 --> 00:04:17,010
 con las sticky case, que es

153
00:04:17,010 --> 00:04:18,340
 algo que yo he aprendido lo que

154
00:04:18,340 --> 00:04:20,500
 es para este episodio y es que

155
00:04:20,500 --> 00:04:23,050
 hay personas que por discapacidad

156
00:04:23,050 --> 00:04:25,540
 o por comodidad o productividad,

157
00:04:25,540 --> 00:04:26,780
 no se les da bien hacer la

158
00:04:26,780 --> 00:04:28,160
 combinación de teclas de tener

159
00:04:28,160 --> 00:04:29,500
 una tecla pulsada y pulsar

160
00:04:29,500 --> 00:04:30,500
 otra.

161
00:04:30,500 --> 00:04:32,300
 Entonces las sticky case lo que

162
00:04:32,300 --> 00:04:33,950
 te dejan es dejar pulsada una

163
00:04:33,950 --> 00:04:35,380
 tecla como el control, el

164
00:04:35,380 --> 00:04:37,030
 shift o el alt, fija que tú le

165
00:04:37,030 --> 00:04:38,420
 das y ya se queda pulsada y

166
00:04:38,420 --> 00:04:40,300
 entonces pulsar la siguiente

167
00:04:40,300 --> 00:04:41,300
 tecla.

168
00:04:41,300 --> 00:04:43,340
 Esto se ve que estaba controlado

169
00:04:43,340 --> 00:04:45,160
 en X11, en Wayland se ha ido

170
00:04:45,160 --> 00:04:46,460
 por el camino y le han

171
00:04:46,460 --> 00:04:47,940
 metido muchísima caña y

172
00:04:47,940 --> 00:04:49,870
 parece ser que en la 6.2 ya lo

173
00:04:49,870 --> 00:04:51,680
 van a tener controlado y en la

174
00:04:51,680 --> 00:04:53,580
 6.1.3 hay algunas mejoras, pero

175
00:04:53,580 --> 00:04:54,790
 todavía no está a la par con

176
00:04:54,790 --> 00:04:55,380
 X11.

177
00:04:55,380 --> 00:04:57,470
 Luego también os traigo un

178
00:04:57,470 --> 00:04:59,730
 proyecto hecho por un padre que

179
00:04:59,730 --> 00:05:01,280
 me da un aire porque tiene

180
00:05:01,280 --> 00:05:03,690
 un crío de 3 años y pico y

181
00:05:03,690 --> 00:05:05,110
 entonces ha ido explicando qué

182
00:05:05,110 --> 00:05:06,880
 le ha hecho a su hijo para

183
00:05:06,880 --> 00:05:08,750
 que pueda jugar un poco con una

184
00:05:08,750 --> 00:05:10,390
 Raspberry Pi 5 o un par de

185
00:05:10,390 --> 00:05:12,280
 accesorios más, cuenta en

186
00:05:12,280 --> 00:05:14,010
 un post por las fotos y qué ha

187
00:05:14,010 --> 00:05:15,810
 ido haciendo, el cable que ha

188
00:05:15,810 --> 00:05:17,880
 elegido, una pantalla, todas

189
00:05:17,880 --> 00:05:18,200
 esas cosas.

190
00:05:18,200 --> 00:05:20,300
 Está gracioso, se gusta trastear

191
00:05:20,300 --> 00:05:22,120
 y lo gracioso para este podcast

192
00:05:22,120 --> 00:05:23,480
 es que lo que ha elegido

193
00:05:23,480 --> 00:05:25,220
 es como aplicación principal.

194
00:05:25,220 --> 00:05:27,100
 G-Comprise, es una maravilla

195
00:05:27,100 --> 00:05:29,660
 con 5.000 mini aplicaciones y

196
00:05:29,660 --> 00:05:31,520
 juegos dentro para todas las

197
00:05:31,520 --> 00:05:31,940
 edades.

198
00:05:31,940 --> 00:05:34,440
 K-Letters, que es específico

199
00:05:34,440 --> 00:05:36,750
 para el tema de lectura y luego

200
00:05:36,750 --> 00:05:39,320
 también Tuxpain para dibujar

201
00:05:39,320 --> 00:05:39,620
 un poco.

202
00:05:39,620 --> 00:05:41,330
 Os dejo el enlace por si os

203
00:05:41,330 --> 00:05:43,080
 parece interesante.

204
00:05:43,080 --> 00:05:45,160
 También tenemos un artículo

205
00:05:45,160 --> 00:05:46,920
 en inglés en el que te dice

206
00:05:46,920 --> 00:05:48,820
 que puedes contribuir a KDE

207
00:05:48,820 --> 00:05:49,880
 en muchos lenguajes.

208
00:05:49,880 --> 00:05:52,520
 Es famoso que KDE está hecho

209
00:05:52,520 --> 00:05:54,540
 en C++ y algo de QML.

210
00:05:55,060 --> 00:05:57,110
 Entonces, pues para mucha gente

211
00:05:57,110 --> 00:05:58,680
 no es amigable dedicarse a

212
00:05:58,680 --> 00:06:00,740
 intentar leer todo ese código

213
00:06:00,740 --> 00:06:02,420
 y hacer algún mal recuerdo y

214
00:06:02,420 --> 00:06:04,170
 aportar código porque ese no

215
00:06:04,170 --> 00:06:06,100
 es su lenguaje de programación

216
00:06:06,100 --> 00:06:06,740
 de cabecera.

217
00:06:06,740 --> 00:06:09,010
 Pero lo que yo no sabía y este

218
00:06:09,010 --> 00:06:11,560
 chico se ha dedicado a explicar

219
00:06:11,560 --> 00:06:13,900
 es que tú realmente puedes

220
00:06:13,900 --> 00:06:16,150
 usar código que no sea C++ en

221
00:06:16,150 --> 00:06:18,280
 muchas partes del ecosistema KDE.

222
00:06:18,280 --> 00:06:20,090
 Y entonces te hace un listado,

223
00:06:20,090 --> 00:06:21,710
 entonces te dice que puedes

224
00:06:21,710 --> 00:06:23,530
 usar Python, Ruby, Perl,

225
00:06:23,530 --> 00:06:24,340
 Containers,

226
00:06:24,900 --> 00:06:27,290
 tipo Docker y Pokemon, HTML, S,

227
00:06:27,290 --> 00:06:29,660
 CSS y Javascript, WebAssembly,

228
00:06:29,660 --> 00:06:32,040
 Flatpak y Snap, ZenMate, Java

229
00:06:32,040 --> 00:06:32,640
 y Rust.

230
00:06:32,640 --> 00:06:34,280
 En todos esos lenguajes hay

231
00:06:34,280 --> 00:06:36,110
 proyectos de KDE en los que tú

232
00:06:36,110 --> 00:06:37,540
 puedes colaborar.

233
00:06:37,540 --> 00:06:39,630
 Y entonces el tema uno por uno,

234
00:06:39,630 --> 00:06:41,470
 eso era el TOC, la guía del

235
00:06:41,470 --> 00:06:42,120
 post.

236
00:06:42,120 --> 00:06:43,590
 Si tú vas pinchando te va

237
00:06:43,590 --> 00:06:45,130
 diciendo cómo encontrar

238
00:06:45,130 --> 00:06:47,220
 proyectos que usan esos lenguajes

239
00:06:47,220 --> 00:06:48,400
 y qué tipo

240
00:06:48,400 --> 00:06:49,550
 de proyectos hay en cada uno de

241
00:06:49,550 --> 00:06:50,340
 esos lenguajes.

242
00:06:50,340 --> 00:06:51,910
 Pues mira, en Python hacemos

243
00:06:51,910 --> 00:06:53,540
 esto, esto y esto que tiene que

244
00:06:53,540 --> 00:06:54,740
 ver con el AT-SPI o con

245
00:06:54,740 --> 00:06:56,720
 el Kantai Tracker, el Localize.

246
00:06:56,720 --> 00:06:58,350
 Entonces te hace un resumen de

247
00:06:58,350 --> 00:07:00,070
 las cosas que puedes echarle un

248
00:07:00,070 --> 00:07:01,620
 vistazo por si te interesa

249
00:07:01,620 --> 00:07:03,530
 echar una mano en ese lenguaje

250
00:07:03,530 --> 00:07:05,460
 que tú sí que controles.

251
00:07:05,460 --> 00:07:07,320
 Lo cual siempre hemos dicho que

252
00:07:07,320 --> 00:07:08,890
 a KDE o al software libre en

253
00:07:08,890 --> 00:07:10,400
 general se puede ayudar

254
00:07:10,400 --> 00:07:12,840
 de miles de formas, con publicidad,

255
00:07:12,840 --> 00:07:14,830
 ayudando, moderando foros,

256
00:07:14,830 --> 00:07:17,820
 generando subtítulos o transcripciones

257
00:07:17,820 --> 00:07:19,450
 de los vídeos, de los audios,

258
00:07:19,450 --> 00:07:21,170
 con diseño gráfico, haciendo

259
00:07:21,170 --> 00:07:22,580
 publicidad, un montón

260
00:07:22,580 --> 00:07:23,020
 de maneras.

261
00:07:23,020 --> 00:07:24,580
 Pero también es cierto que...

262
00:07:24,580 --> 00:07:26,310
 Que se puede ayudar programando

263
00:07:26,310 --> 00:07:27,800
 y no tiene por qué ser C++.

264
00:07:27,800 --> 00:07:29,670
 Y luego también, no sé cuánta

265
00:07:29,670 --> 00:07:31,540
 gente podrá, aquí sé que Kiva

266
00:07:31,540 --> 00:07:33,020
 ha estado a punto, pero

267
00:07:33,020 --> 00:07:34,500
 no ha podido por motivos que

268
00:07:34,500 --> 00:07:35,880
 dentro de lo que hay no son malos,

269
00:07:35,880 --> 00:07:36,980
 que es que consigue un

270
00:07:36,980 --> 00:07:38,330
 trabajo nuevo, y es que tenemos

271
00:07:38,330 --> 00:07:40,220
 la Akademy Internacional 2024.

272
00:07:40,220 --> 00:07:42,320
 Va a ser en Würzburg, en sus

273
00:07:42,320 --> 00:07:44,900
 Alemanias, entre el 7 y el 12

274
00:07:44,900 --> 00:07:46,320
 de septiembre.

275
00:07:46,320 --> 00:07:47,820
 Por si hay alguien que se anima

276
00:07:47,820 --> 00:07:49,200
 y tiene la suerte de poder ir

277
00:07:49,200 --> 00:07:50,580
 para allá, ya sabéis que

278
00:07:50,580 --> 00:07:52,150
 la Akademy Internacional es en

279
00:07:52,150 --> 00:07:53,670
 inglés, pero ahí hay gente

280
00:07:53,670 --> 00:07:54,280
 muy pro.

281
00:07:54,420 --> 00:07:56,260
 Así como en la Akademy España

282
00:07:56,260 --> 00:07:58,570
 hay mucho más socialización,

283
00:07:58,570 --> 00:08:00,460
 aplicación, nivel usuario,

284
00:08:00,460 --> 00:08:02,240
 en la Akademy Internacional hay

285
00:08:02,240 --> 00:08:03,580
 mucho desarrollador.

286
00:08:03,580 --> 00:08:05,040
 Pero bueno, si sois desarrollador

287
00:08:05,040 --> 00:08:06,310
 y queréis ir, seguramente ya

288
00:08:06,310 --> 00:08:07,580
 lo sabríais, pero bueno,

289
00:08:07,580 --> 00:08:09,540
 está bien que en este podcast

290
00:08:09,540 --> 00:08:10,640
 lo recordemos.

291
00:08:10,640 --> 00:08:12,180
 También, los que no podáis ir

292
00:08:12,180 --> 00:08:13,740
 pero os interese, seguramente

293
00:08:13,740 --> 00:08:15,320
 se retransmitirá online.

294
00:08:15,320 --> 00:08:17,070
 Eso era en cuanto a noticias

295
00:08:17,070 --> 00:08:18,420
 del mundillo KDE.

296
00:08:18,420 --> 00:08:19,860
 También quería hablar un poco

297
00:08:19,860 --> 00:08:21,010
 de mi libro, o lo que viene

298
00:08:21,010 --> 00:08:22,660
 siendo mismo, de este podcast.

299
00:08:22,660 --> 00:08:24,260
 Y es que, como todavía...

300
00:08:24,260 --> 00:08:26,510
 Todavía las aplicaciones no

301
00:08:26,510 --> 00:08:29,190
 tienen muy integradas las etiquetas

302
00:08:29,190 --> 00:08:30,940
 del podcasting 2.0,

303
00:08:30,940 --> 00:08:31,870
 que seguro que he hablado

304
00:08:31,870 --> 00:08:33,290
 alguna vez, del Tag Person,

305
00:08:33,290 --> 00:08:34,730
 para decir quién habla, o

306
00:08:34,730 --> 00:08:35,320
 quién edita,

307
00:08:35,320 --> 00:08:37,470
 o quién participa, o los subtítulos,

308
00:08:37,470 --> 00:08:38,840
 o todo este tipo de cosas.

309
00:08:38,840 --> 00:08:39,780
 Lo que he hecho es que en la

310
00:08:39,780 --> 00:08:41,530
 página web de este podcast, y

311
00:08:41,530 --> 00:08:43,060
 en la de accesibilidad con

312
00:08:43,060 --> 00:08:44,640
 tecnologías libres, porque uso

313
00:08:44,640 --> 00:08:46,170
 el mismo framework, he añadido

314
00:08:46,170 --> 00:08:48,660
 la información que ya pongo en

315
00:08:48,660 --> 00:08:50,740
 el feed, en el XML, me la he

316
00:08:50,740 --> 00:08:52,760
 traído a la parte web.

317
00:08:52,760 --> 00:08:54,100
 Entonces, si abrís el

318
00:08:54,100 --> 00:08:55,810
 artículo de este episodio, por

319
00:08:55,810 --> 00:08:58,460
 ejemplo, ahora abajo os saldrá

320
00:08:58,460 --> 00:09:00,480
 subtítulos disponibles en

321
00:09:00,480 --> 00:09:01,180
 y un enlace.

322
00:09:01,180 --> 00:09:03,210
 Y luego, por si vuestra aplicación

323
00:09:03,210 --> 00:09:04,560
 no lo implementa, y un enlace a

324
00:09:04,560 --> 00:09:05,720
 un listado de aplicaciones

325
00:09:05,720 --> 00:09:06,820
 que sí que lo implementan.

326
00:09:06,820 --> 00:09:08,070
 Este en concreto ya lo había

327
00:09:08,070 --> 00:09:09,420
 puesto, pero hace poco se me ha

328
00:09:09,420 --> 00:09:10,740
 ocurrido que podría hacer

329
00:09:10,740 --> 00:09:12,070
 lo mismo con los comentarios y

330
00:09:12,070 --> 00:09:13,020
 con las personas.

331
00:09:13,020 --> 00:09:14,590
 Entonces, ahora nosotros, en

332
00:09:14,590 --> 00:09:16,350
 estos programas, siempre enlazamos

333
00:09:16,350 --> 00:09:17,700
 un pod de Mastodon, donde

334
00:09:17,700 --> 00:09:19,440
 si comentáis en las aplicaciones

335
00:09:19,440 --> 00:09:21,120
 como Podverse, TrueFans y todas

336
00:09:21,120 --> 00:09:22,260
 estas, ahí aparecen

337
00:09:22,260 --> 00:09:23,000
 directamente

338
00:09:23,000 --> 00:09:23,680
 los comentarios.

339
00:09:23,940 --> 00:09:25,140
 De hecho, en TrueFans, creo que

340
00:09:25,140 --> 00:09:26,260
 la semana que viene, se podrá

341
00:09:26,260 --> 00:09:27,260
 comentar directamente

342
00:09:27,260 --> 00:09:28,100
 desde la aplicación.

343
00:09:28,100 --> 00:09:29,360
 Pues como estos lo usan muy pocas

344
00:09:29,360 --> 00:09:30,590
 aplicaciones, lo que he hecho

345
00:09:30,590 --> 00:09:31,600
 es poner un enlace.

346
00:09:31,600 --> 00:09:33,790
 Enlace de comentarios yo llevará

347
00:09:33,790 --> 00:09:35,580
 al pod de Mastodon, donde en

348
00:09:35,580 --> 00:09:37,500
 KDE España, pues publicitamos

349
00:09:37,500 --> 00:09:38,500
 este episodio, por si queréis

350
00:09:38,500 --> 00:09:39,410
 decirnos algo directamente

351
00:09:39,410 --> 00:09:39,800
 allí.

352
00:09:39,800 --> 00:09:42,050
 También he puesto un click, en

353
00:09:42,050 --> 00:09:44,120
 que si le dais a y transcripción

354
00:09:44,120 --> 00:09:45,540
 completa aquí, os

355
00:09:45,540 --> 00:09:46,660
 sale toda la transcripción del

356
00:09:46,660 --> 00:09:47,160
 programa.

357
00:09:47,160 --> 00:09:48,520
 Esto sale tanto en la web, como

358
00:09:48,520 --> 00:09:49,830
 en las notas del programa, en

359
00:09:49,830 --> 00:09:51,060
 vuestra aplicación.

360
00:09:51,060 --> 00:09:52,170
 Así, si estáis en vuestra

361
00:09:52,170 --> 00:09:53,220
 aplicación antigua.

362
00:09:53,780 --> 00:09:54,950
 Y no podéis ver nada de esto,

363
00:09:54,950 --> 00:09:56,140
 pues simplemente en un enlace

364
00:09:56,140 --> 00:09:57,380
 en las notas del programa,

365
00:09:57,380 --> 00:09:57,880
 pincháis

366
00:09:57,880 --> 00:09:58,880
 y lo tenéis disponible.

367
00:09:58,880 --> 00:10:00,500
 Y luego, lo que he puesto, pero

368
00:10:00,500 --> 00:10:01,880
 que está solo en la web, es

369
00:10:01,880 --> 00:10:03,380
 que a la derecha, si estáis

370
00:10:03,380 --> 00:10:05,080
 en ordenador, y abajo, después

371
00:10:05,080 --> 00:10:06,550
 de las notas del programa, si

372
00:10:06,550 --> 00:10:08,120
 estáis en móvil, después

373
00:10:08,120 --> 00:10:09,820
 de todo el texto, con todos los

374
00:10:09,820 --> 00:10:11,760
 enlaces que yo dejo de las noticias,

375
00:10:11,760 --> 00:10:13,260
 tendréis un participante

376
00:10:13,260 --> 00:10:14,890
 dos puntos, y os saldrán unos

377
00:10:14,890 --> 00:10:16,740
 circulitos con las imágenes, y

378
00:10:16,740 --> 00:10:17,900
 el texto, y en calidad

379
00:10:17,900 --> 00:10:19,640
 de las personas que participan.

380
00:10:19,640 --> 00:10:21,420
 Normalmente, últimamente, pues

381
00:10:21,420 --> 00:10:22,720
 David Marzal, el que habla

382
00:10:22,720 --> 00:10:23,620
 principalmente,

383
00:10:23,620 --> 00:10:25,240
 Jorge Lama es el que edita.

384
00:10:25,240 --> 00:10:26,860
 Aunque en el anterior, si vais

385
00:10:26,860 --> 00:10:28,500
 al episodio anterior, pues tenéis

386
00:10:28,500 --> 00:10:29,700
 a Iván, que os dice

387
00:10:29,700 --> 00:10:31,320
 que, a ver que lo tengo aquí,

388
00:10:31,320 --> 00:10:33,130
 saldrá, participan, Iván como

389
00:10:33,130 --> 00:10:34,900
 invitado, aparte de Jorge Lama

390
00:10:34,900 --> 00:10:35,360
 como editor.

391
00:10:35,360 --> 00:10:37,370
 Pequeñas mejoras, algunas de

392
00:10:37,370 --> 00:10:39,680
 accesibilidad, algunas pijotadas

393
00:10:39,680 --> 00:10:41,260
 que nos gustan a la gente

394
00:10:41,260 --> 00:10:43,010
 del podcasting, y algunas que

395
00:10:43,010 --> 00:10:44,750
 puede que os resulte útil, si

396
00:10:44,750 --> 00:10:46,300
 os gusta comentar, o ver

397
00:10:46,300 --> 00:10:47,370
 los subtítulos, o leer la

398
00:10:47,370 --> 00:10:48,730
 transcripción, por si queréis

399
00:10:48,730 --> 00:10:49,610
 ver algún nombre de un

400
00:10:49,610 --> 00:10:50,100
 programa

401
00:10:50,100 --> 00:10:50,900
 de algo que hemos dicho.

402
00:10:50,900 --> 00:10:52,350
 Con esto termino por hoy,

403
00:10:52,350 --> 00:10:53,460
 espero que estéis

404
00:10:53,460 --> 00:10:55,870
 pasando buen verano, que tengáis

405
00:10:55,870 --> 00:10:57,340
 salud, y que tengáis mucho

406
00:10:57,340 --> 00:10:58,000
 software libre.

