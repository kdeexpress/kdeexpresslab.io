1
00:00:00,980 --> 00:00:02,850
 Buenas, episodio 30 de KDE

2
00:00:02,850 --> 00:00:04,770
 Express, se dice pronto 30

3
00:00:04,770 --> 00:00:07,140
 episodios ya, hoy estamos grabando

4
00:00:07,140 --> 00:00:09,380
 el 5 de julio de 2024

5
00:00:09,380 --> 00:00:11,590
 y mientras que grabo tengo en

6
00:00:11,590 --> 00:00:14,600
 el ordenador Plasma 6.1.2,

7
00:00:14,600 --> 00:00:19,500
 Framework 6.3.0 y KDE Gears 24.05.1

8
00:00:19,500 --> 00:00:20,770
 Hoy os voy a traer una selección

9
00:00:20,770 --> 00:00:21,840
 de noticias del mes de junio

10
00:00:21,840 --> 00:00:22,820
 que me han ido llamando la

11
00:00:22,820 --> 00:00:23,480
 atención

12
00:00:23,480 --> 00:00:25,760
 La primera es que Fedora propone

13
00:00:25,760 --> 00:00:28,210
 publicar una imagen con KDE Plasma

14
00:00:28,210 --> 00:00:30,360
 Mobile, seguramente enfocada a

15
00:00:30,360 --> 00:00:31,580
 Tabletas 2.1

16
00:00:31,580 --> 00:00:33,420
 Esto lo que quiere decir es que

17
00:00:33,420 --> 00:00:35,550
 en Fedora tienen una metodología

18
00:00:35,550 --> 00:00:37,500
 en la que tú haces propuestas

19
00:00:37,500 --> 00:00:38,540
 y lo tienen que votar y lo

20
00:00:38,540 --> 00:00:38,960
 aceptan

21
00:00:38,960 --> 00:00:40,880
 Ellos tienen varios spins, que

22
00:00:40,880 --> 00:00:42,600
 son las versiones que sacan de

23
00:00:42,600 --> 00:00:43,040
 Fedora

24
00:00:43,040 --> 00:00:44,720
 Igual que hay Fedoras con

25
00:00:44,720 --> 00:00:47,290
 diferentes entornos de escritorio,

26
00:00:47,290 --> 00:00:50,390
 hay Fedoras con diferentes tecnologías,

27
00:00:50,390 --> 00:00:52,090
 las inmutables o atómicas

28
00:00:52,090 --> 00:00:52,900
 mejor dicho

29
00:00:52,900 --> 00:00:53,100
 Y ahora, la primera es que Fedora

30
00:00:53,100 --> 00:00:53,250
 propone publicar una imagen con

31
00:00:53,250 --> 00:00:53,460
 KDE Plasma Mobile, seguramente

32
00:00:53,460 --> 00:00:53,460
 enfocada a Tabletas 2.1

33
00:00:53,460 --> 00:00:55,470
 Y ahora han propuesto sacar una

34
00:00:55,470 --> 00:00:57,420
 versión móvil que no solo lleve

35
00:00:57,420 --> 00:00:59,590
 FOSS, la parte de Gnome, sino

36
00:00:59,590 --> 00:01:00,620
 Plasma Mobile

37
00:01:00,620 --> 00:01:01,800
 Porque se ve que hay gente

38
00:01:01,800 --> 00:01:03,290
 interesada en usarla en algunas

39
00:01:03,290 --> 00:01:03,800
 tabletas

40
00:01:03,800 --> 00:01:05,570
 Así que es un buen signo de

41
00:01:05,570 --> 00:01:07,240
 que se avanza por la parte

42
00:01:07,240 --> 00:01:09,160
 mobile y que KDE Plasma tiene

43
00:01:09,160 --> 00:01:11,200
 una buena evolución por ahí

44
00:01:11,200 --> 00:01:12,880
 Hablando de cosas de movilidad

45
00:01:12,880 --> 00:01:15,630
 como las tablets, pues PostMarket

46
00:01:15,630 --> 00:01:17,710
 OS, que es una de las distros

47
00:01:17,710 --> 00:01:19,900
 más famosas para meter Linux

48
00:01:19,900 --> 00:01:21,060
 puro en tu móvil

49
00:01:21,060 --> 00:01:23,440
 Se ha actualizado y es la 24.06

50
00:01:23,440 --> 00:01:25,130
 Ya tiene Plasma 6, lo cual

51
00:01:25,130 --> 00:01:26,510
 está muy bien, que hayan hecho

52
00:01:26,510 --> 00:01:27,880
 un cambio bastante gordo en tan

53
00:01:27,880 --> 00:01:28,480
 poco tiempo

54
00:01:28,480 --> 00:01:30,850
 Y quien también tiene Plasma 6

55
00:01:30,850 --> 00:01:32,870
 ahora es OpenMandriva, que yo

56
00:01:32,870 --> 00:01:34,070
 es una distro que no tengo muy

57
00:01:34,070 --> 00:01:34,660
 controlada

58
00:01:34,660 --> 00:01:36,060
 Desde los tiempos de Mandrake

59
00:01:36,060 --> 00:01:37,200
 estoy un poco perdido

60
00:01:37,200 --> 00:01:39,360
 Pues ya están probando Plasma

61
00:01:39,360 --> 00:01:39,660
 6

62
00:01:39,660 --> 00:01:41,510
 Se van apuntando al carro poco

63
00:01:41,510 --> 00:01:43,340
 a poco casi todas las distros

64
00:01:43,340 --> 00:01:44,340
 Debían llegar en algún

65
00:01:44,340 --> 00:01:45,540
 momento, no os preocupéis

66
00:01:45,540 --> 00:01:47,810
 También he leído, porque no

67
00:01:47,810 --> 00:01:49,660
 es algo que yo suelo usar

68
00:01:49,660 --> 00:01:51,950
 Que Plasma Browser, la extensión

69
00:01:51,950 --> 00:01:53,420
 de Firefox que integra Plasma 6

70
00:01:53,420 --> 00:01:54,590
 Que integra el escritorio con

71
00:01:54,590 --> 00:01:55,160
 el navegador

72
00:01:55,160 --> 00:01:56,440
 Tiene una nueva versión

73
00:01:56,440 --> 00:01:58,190
 Por si no lo conocéis, lo que

74
00:01:58,190 --> 00:01:59,940
 suele hacer o para lo que sirve

75
00:01:59,940 --> 00:02:00,840
 Plasma Browser

76
00:02:00,840 --> 00:02:03,080
 Es, te deja compartir enlaces

77
00:02:03,080 --> 00:02:06,230
 Encontrar tabs que tengas en el

78
00:02:06,230 --> 00:02:06,780
 navegador

79
00:02:06,780 --> 00:02:09,820
 Te deja ver en el historial de

80
00:02:09,820 --> 00:02:11,710
 páginas que ha visitado con

81
00:02:11,710 --> 00:02:12,460
 KRunner

82
00:02:12,460 --> 00:02:14,130
 Te deja monitorizar el proceso

83
00:02:14,130 --> 00:02:15,060
 de las descargas

84
00:02:15,060 --> 00:02:17,220
 Te integra las notificaciones

85
00:02:17,220 --> 00:02:18,710
 Puedes controlar música y

86
00:02:18,710 --> 00:02:19,140
 vídeo

87
00:02:19,140 --> 00:02:21,150
 Está bastante bien, incluso lo

88
00:02:21,150 --> 00:02:22,660
 puedes usar con KDE Connect

89
00:02:22,660 --> 00:02:23,400
 O sea que no es un problema

90
00:02:23,400 --> 00:02:25,500
 Porque si os gusta estar hiperconectado

91
00:02:25,500 --> 00:02:27,200
 Tiene una nueva versión con

92
00:02:27,200 --> 00:02:27,740
 mejoras

93
00:02:27,740 --> 00:02:28,940
 Y os dejo el enlace, claro

94
00:02:28,940 --> 00:02:31,460
 Y luego, una de las cosas que

95
00:02:31,460 --> 00:02:32,880
 me ha llamado la atención últimamente

96
00:02:32,880 --> 00:02:34,410
 Que yo sabía que podías hacer

97
00:02:34,410 --> 00:02:35,980
 casi de todo con Plasma

98
00:02:35,980 --> 00:02:37,380
 Pero no sabía que también podías

99
00:02:37,380 --> 00:02:38,940
 poner un vídeo como fondo de

100
00:02:38,940 --> 00:02:39,540
 pantalla

101
00:02:39,540 --> 00:02:41,450
 Entonces, os dejo un proyecto

102
00:02:41,450 --> 00:02:42,480
 que está en GitHub

103
00:02:42,480 --> 00:02:45,000
 Que te dice como si lo instalas

104
00:02:45,000 --> 00:02:46,560
 Puedes poner un vídeo y tenerlo

105
00:02:46,560 --> 00:02:47,280
 ahí de fondo

106
00:02:47,280 --> 00:02:48,380
 Mientras que se está moviendo

107
00:02:48,380 --> 00:02:50,200
 Y dicen que es bastante estable

108
00:02:50,200 --> 00:02:51,980
 Tiene versión para Aure, si

109
00:02:51,980 --> 00:02:52,720
 estás en Arch

110
00:02:53,380 --> 00:02:54,680
 El propio proyecto en GitHub

111
00:02:54,680 --> 00:02:57,200
 Y luego está en la KDE Store

112
00:02:57,200 --> 00:02:58,520
 Yo siempre recomiendo

113
00:02:58,520 --> 00:02:59,960
 Leérselo primero bien

114
00:02:59,960 --> 00:03:01,480
 Antes de instalarlo por si acaso

115
00:03:01,480 --> 00:03:03,140
 Pero por lo que he leído, va

116
00:03:03,140 --> 00:03:03,880
 bastante bien

117
00:03:03,880 --> 00:03:05,400
 Y ya entramos en el menú

118
00:03:05,400 --> 00:03:06,140
 principal

119
00:03:06,140 --> 00:03:07,560
 De lo que va a ser este episodio

120
00:03:07,560 --> 00:03:09,420
 Que es un resumen de las cosas

121
00:03:09,420 --> 00:03:10,080
 que nos trae

122
00:03:10,080 --> 00:03:11,300
 Plasma 6.1

123
00:03:11,300 --> 00:03:12,660
 Que como ya os he comentado al

124
00:03:12,660 --> 00:03:13,260
 principio

125
00:03:13,260 --> 00:03:14,140
 Ya está entre nosotros

126
00:03:14,140 --> 00:03:15,420
 Y lo he podido toquetear un

127
00:03:15,420 --> 00:03:15,700
 poco

128
00:03:15,700 --> 00:03:16,980
 La primera de las cosas

129
00:03:16,980 --> 00:03:19,050
 Es que ya puedes activar el

130
00:03:19,050 --> 00:03:20,020
 escritorio remoto

131
00:03:20,020 --> 00:03:21,840
 Desde la propia configuración

132
00:03:21,840 --> 00:03:22,480
 del sistema

133
00:03:22,480 --> 00:03:23,360
 Y este es el primer paso

134
00:03:23,360 --> 00:03:24,060
 Esto bajo Wayland

135
00:03:24,060 --> 00:03:25,390
 Que no sabéis el dolor de

136
00:03:25,390 --> 00:03:25,820
 cabeza

137
00:03:25,820 --> 00:03:26,870
 Que a los administradores de

138
00:03:26,870 --> 00:03:27,260
 sistemas

139
00:03:27,260 --> 00:03:28,300
 Nos ha supuesto

140
00:03:28,300 --> 00:03:30,910
 El paso a Wayland con el escritorio

141
00:03:30,910 --> 00:03:31,280
 remoto

142
00:03:31,280 --> 00:03:34,040
 Esto al final lo que usa es KRDP

143
00:03:34,040 --> 00:03:37,200
 Y han automatizado la configuración

144
00:03:37,200 --> 00:03:38,560
 Para que no tengas que estar toqueteando

145
00:03:38,560 --> 00:03:39,300
 en varios sitios

146
00:03:39,300 --> 00:03:41,060
 Yo he probado que funciona

147
00:03:41,060 --> 00:03:42,280
 El funcionamiento de esto te va

148
00:03:42,280 --> 00:03:43,480
 a las preferencias del sistema

149
00:03:43,480 --> 00:03:44,480
 Escritorio remoto

150
00:03:44,480 --> 00:03:46,260
 Tienes que crear un usuario

151
00:03:46,260 --> 00:03:47,680
 Le pones una contraseña

152
00:03:47,680 --> 00:03:49,540
 Lo activas, te pide unos permisos

153
00:03:49,540 --> 00:03:51,160
 Y entonces ya te deja iniciarlo

154
00:03:51,160 --> 00:03:52,160
 automáticamente

155
00:03:53,340 --> 00:03:53,940
 Lees el puerto

156
00:03:53,940 --> 00:03:58,740
 Y en las páginas que tiene KDE Plasma

157
00:03:58,740 --> 00:04:00,170
 Os sale un vídeo de cómo

158
00:04:00,170 --> 00:04:00,620
 funciona

159
00:04:00,620 --> 00:04:02,160
 Una mejora que estábamos

160
00:04:02,160 --> 00:04:02,820
 esperando

161
00:04:02,820 --> 00:04:03,750
 Muchos administradores de

162
00:04:03,750 --> 00:04:04,100
 sistemas

163
00:04:04,100 --> 00:04:05,120
 Y ya está aquí

164
00:04:05,120 --> 00:04:07,640
 Poco a poco Wayland está cogiendo

165
00:04:07,640 --> 00:04:11,200
 La funcionalidad equivalente

166
00:04:11,200 --> 00:04:12,300
 A la que tenía X11

167
00:04:12,300 --> 00:04:14,130
 Aunque no está dentro de las

168
00:04:14,130 --> 00:04:14,760
 noticias

169
00:04:14,760 --> 00:04:15,640
 Pero sí que me he enterado

170
00:04:15,640 --> 00:04:17,280
 Que Wayland ahora mismo tiene

171
00:04:17,280 --> 00:04:18,120
 un fallo

172
00:04:18,120 --> 00:04:19,620
 Bastante gordo con Orca

173
00:04:19,620 --> 00:04:22,120
 Que es el reproductor o el lector

174
00:04:22,120 --> 00:04:22,660
 de pantalla

175
00:04:22,660 --> 00:04:23,320
 Que es el reproductor o el lector

176
00:04:23,320 --> 00:04:23,320
 de pantalla

177
00:04:23,320 --> 00:04:24,180
 Todavía no está fino

178
00:04:24,180 --> 00:04:26,000
 Y yo creo que es donde deberían

179
00:04:26,000 --> 00:04:27,800
 enfocarse mayoritariamente

180
00:04:27,800 --> 00:04:29,140
 Para que la accesibilidad

181
00:04:29,140 --> 00:04:30,640
 Pueda estar a la par tanto en

182
00:04:30,640 --> 00:04:31,700
 Wayland como en X11

183
00:04:31,700 --> 00:04:32,880
 Sé que están trabajando en

184
00:04:32,880 --> 00:04:33,100
 ello

185
00:04:33,100 --> 00:04:35,000
 Pero vamos, ahora mismo hay

186
00:04:35,000 --> 00:04:35,860
 bastantes quejas

187
00:04:35,860 --> 00:04:37,100
 Y yo creo que

188
00:04:37,100 --> 00:04:38,670
 La mayoría de gente que necesite

189
00:04:38,670 --> 00:04:38,940
 Orca

190
00:04:38,940 --> 00:04:40,280
 Estará usando X11

191
00:04:40,280 --> 00:04:42,080
 Volviendo a Plasma 6.1

192
00:04:42,080 --> 00:04:43,510
 Tenemos el modo de edición de

193
00:04:43,510 --> 00:04:44,640
 escritorio mejorado

194
00:04:44,640 --> 00:04:46,010
 Que os lo comenté en el episodio

195
00:04:46,010 --> 00:04:46,460
 anterior

196
00:04:46,460 --> 00:04:47,620
 Lo he probado y funciona muy

197
00:04:47,620 --> 00:04:47,860
 bien

198
00:04:47,860 --> 00:04:49,390
 La verdad es que es una mejora

199
00:04:49,390 --> 00:04:50,260
 que se agradece

200
00:04:50,260 --> 00:04:51,600
 También tenemos que ahora

201
00:04:51,600 --> 00:04:53,300
 podemos recordar aplicaciones

202
00:04:53,300 --> 00:04:54,450
 Abiertas en la sesión bajo

203
00:04:54,450 --> 00:04:54,900
 Wayland

204
00:04:54,900 --> 00:04:56,380
 Esto es algo que teníamos

205
00:04:56,380 --> 00:04:57,440
 antes en X11

206
00:04:57,440 --> 00:04:59,080
 Y que perdimos con Wayland

207
00:04:59,080 --> 00:05:00,320
 Que es que si tú cierras una

208
00:05:00,320 --> 00:05:00,760
 sesión

209
00:05:00,760 --> 00:05:01,790
 Se te vuelven a abrir todas las

210
00:05:01,790 --> 00:05:02,780
 aplicaciones que tenías

211
00:05:02,780 --> 00:05:04,120
 Y en el mismo sitio

212
00:05:04,120 --> 00:05:05,220
 Ahora mismo creo que hemos

213
00:05:05,220 --> 00:05:06,040
 conseguido solo

214
00:05:06,040 --> 00:05:07,840
 Que recuerde las aplicaciones

215
00:05:07,840 --> 00:05:09,490
 No todas se van a quedar en el

216
00:05:09,490 --> 00:05:10,280
 mismo sitio

217
00:05:10,280 --> 00:05:12,360
 Pero bueno, es un paso y está

218
00:05:12,360 --> 00:05:13,160
 bastante bien

219
00:05:13,160 --> 00:05:14,700
 Que sigamos avanzando

220
00:05:14,700 --> 00:05:16,130
 Otra cosa que tiene, aunque no

221
00:05:16,130 --> 00:05:17,150
 sé cuánto lo voy a poder

222
00:05:17,150 --> 00:05:17,600
 disfrutar

223
00:05:17,600 --> 00:05:18,100
 Es que

224
00:05:18,100 --> 00:05:20,150
 Sincronizar colores LED de tu

225
00:05:20,150 --> 00:05:21,340
 teclado es posible

226
00:05:21,340 --> 00:05:22,740
 Pero para eso tienes que tener

227
00:05:22,740 --> 00:05:23,280
 un teclado

228
00:05:23,280 --> 00:05:25,140
 Que tenga luces LED

229
00:05:25,140 --> 00:05:26,430
 Que sean configurables y que

230
00:05:26,430 --> 00:05:27,180
 sea compatible

231
00:05:27,180 --> 00:05:28,000
 Así que bueno

232
00:05:28,000 --> 00:05:30,040
 Una característica de nicho

233
00:05:30,040 --> 00:05:31,080
 para la gente de modding

234
00:05:31,080 --> 00:05:32,520
 Pero bueno, seguro que hay

235
00:05:32,520 --> 00:05:33,460
 gente que lo disfruta

236
00:05:33,460 --> 00:05:34,300
 Y está gracioso

237
00:05:34,300 --> 00:05:35,430
 Y luego una cosa que también

238
00:05:35,430 --> 00:05:36,420
 os comenté hace tiempo

239
00:05:36,420 --> 00:05:38,760
 Es que había un plugin

240
00:05:38,760 --> 00:05:40,080
 Un efecto de escritorio

241
00:05:40,080 --> 00:05:41,660
 En el que tú podías menear el

242
00:05:41,660 --> 00:05:42,100
 ratón

243
00:05:42,100 --> 00:05:43,580
 Y se hacía grande para que lo

244
00:05:43,580 --> 00:05:44,260
 encontraras

245
00:05:44,260 --> 00:05:45,560
 Y fuera más sencillo de ver

246
00:05:45,560 --> 00:05:47,090
 Pues esto lo han puesto por

247
00:05:47,090 --> 00:05:47,600
 defecto

248
00:05:47,600 --> 00:05:49,880
 Y aparte lo han mejorado

249
00:05:49,880 --> 00:05:52,160
 Para que cuando el icono

250
00:05:52,160 --> 00:05:53,260
 El puntero se vea

251
00:05:53,260 --> 00:05:53,920
 O se haga grande

252
00:05:53,920 --> 00:05:55,220
 Se vea nítido

253
00:05:55,220 --> 00:05:57,380
 No sea un icono horrible ahí

254
00:05:57,380 --> 00:05:58,520
 con un zoom feísimo

255
00:05:58,520 --> 00:06:00,260
 Por ahora solo va a funcionar

256
00:06:00,260 --> 00:06:01,900
 con los que vienen por defecto

257
00:06:01,900 --> 00:06:03,260
 Pero bueno, están trabajando

258
00:06:03,260 --> 00:06:04,880
 Para que cualquiera pueda poner

259
00:06:04,880 --> 00:06:06,320
 un SVG

260
00:06:06,320 --> 00:06:07,380
 Que es escalable

261
00:06:07,380 --> 00:06:08,980
 Y entonces se vean bien

262
00:06:08,980 --> 00:06:10,180
 Esto está muy bien que venga

263
00:06:10,180 --> 00:06:10,680
 por defecto

264
00:06:10,680 --> 00:06:12,280
 Porque para el tema de accesibilidad

265
00:06:12,280 --> 00:06:13,920
 Cuantas más cosas por defecto

266
00:06:13,920 --> 00:06:14,340
 vengan

267
00:06:14,340 --> 00:06:15,780
 Menos difícil será el primer

268
00:06:15,780 --> 00:06:16,920
 uso que tengan que darle

269
00:06:16,920 --> 00:06:18,060
 Las personas que tengan estos

270
00:06:18,060 --> 00:06:18,480
 problemas

271
00:06:18,480 --> 00:06:20,330
 Y luego os voy a hablar de la

272
00:06:20,330 --> 00:06:21,260
 primera característica

273
00:06:21,260 --> 00:06:23,240
 Que ha activado

274
00:06:23,240 --> 00:06:24,060
 Plasma

275
00:06:24,060 --> 00:06:25,430
 Que a mí no me ha gustado

276
00:06:25,430 --> 00:06:26,320
 personalmente

277
00:06:26,320 --> 00:06:28,580
 Aunque como podéis entender

278
00:06:28,580 --> 00:06:29,680
 Es configurable

279
00:06:29,680 --> 00:06:31,120
 Y ahora os cuento cómo se hace

280
00:06:31,120 --> 00:06:32,620
 Se llama barrera

281
00:06:32,620 --> 00:06:33,980
 Y sirve para no cambiar de

282
00:06:33,980 --> 00:06:35,400
 monitor sin querer

283
00:06:35,400 --> 00:06:37,280
 Cuando tú tienes dos monitores

284
00:06:37,280 --> 00:06:38,670
 Y te estás moviendo entre

285
00:06:38,670 --> 00:06:39,020
 ellos

286
00:06:39,020 --> 00:06:41,120
 A lo mejor quieres tocar justo

287
00:06:41,120 --> 00:06:42,160
 el borde de un monitor

288
00:06:42,160 --> 00:06:43,000
 Y no querías pasar al

289
00:06:43,000 --> 00:06:43,460
 siguiente

290
00:06:43,460 --> 00:06:44,640
 Entonces lo que han hecho es

291
00:06:44,640 --> 00:06:45,220
 como que tenga

292
00:06:45,220 --> 00:06:47,300
 Un imán, una atracción, ese borde

293
00:06:47,300 --> 00:06:48,830
 Y tienes que hacer un esfuerzo

294
00:06:48,830 --> 00:06:49,840
 extra con el ratón

295
00:06:49,840 --> 00:06:52,180
 Para pasar al siguiente monitor

296
00:06:52,180 --> 00:06:53,220
 Si mueves el ratón

297
00:06:53,220 --> 00:06:53,620
 De golpe

298
00:06:53,620 --> 00:06:54,940
 No hay ningún problema

299
00:06:54,940 --> 00:06:56,000
 Pero si vas despacito

300
00:06:56,000 --> 00:06:57,720
 Te cuesta pasar de uno a otro

301
00:06:57,720 --> 00:06:58,760
 Para mi gusto

302
00:06:58,760 --> 00:07:00,430
 La configuración es demasiado

303
00:07:00,430 --> 00:07:00,840
 fuerte

304
00:07:00,840 --> 00:07:03,300
 Me veía muchas veces atascado

305
00:07:03,300 --> 00:07:04,460
 Pasando de izquierda a derecha

306
00:07:04,460 --> 00:07:05,300
 Entre dos monitores

307
00:07:05,300 --> 00:07:06,880
 Entonces he estado buscando

308
00:07:06,880 --> 00:07:09,080
 Y si escribí en la configuración

309
00:07:09,080 --> 00:07:10,600
 En las preferencias del sistema

310
00:07:10,600 --> 00:07:11,880
 Bordes de la pantalla

311
00:07:11,880 --> 00:07:14,440
 Os saldrá en la misma configuración

312
00:07:14,440 --> 00:07:15,260
 Donde podéis decir

313
00:07:15,260 --> 00:07:16,440
 Que si vais a una esquina

314
00:07:16,440 --> 00:07:17,440
 Se abre una aplicación

315
00:07:17,440 --> 00:07:18,700
 O que si vais a otra esquina

316
00:07:18,700 --> 00:07:20,410
 Se abre el kickoff, el menú de

317
00:07:20,410 --> 00:07:20,720
 inicio

318
00:07:20,720 --> 00:07:22,060
 Pues ahí mismo, por ahí abajo

319
00:07:22,060 --> 00:07:23,100
 Tenéis una opción que se

320
00:07:23,100 --> 00:07:23,200
 llama

321
00:07:23,200 --> 00:07:24,440
 Barrera de bordes

322
00:07:24,440 --> 00:07:26,580
 Que estará en 100 píxeles

323
00:07:26,580 --> 00:07:27,100
 O algo así

324
00:07:27,100 --> 00:07:28,700
 Esa es cuánta fuerza

325
00:07:28,700 --> 00:07:29,820
 Cuánto movimiento tienes que

326
00:07:29,820 --> 00:07:30,060
 hacer

327
00:07:30,060 --> 00:07:31,220
 Para realmente pasar

328
00:07:31,220 --> 00:07:32,780
 Yo lo que he hecho es dejarlo a

329
00:07:32,780 --> 00:07:33,000
 cero

330
00:07:33,000 --> 00:07:34,940
 Que lo transforma en ninguna

331
00:07:34,940 --> 00:07:37,000
 Y con eso me he quedado cómodo

332
00:07:37,000 --> 00:07:38,220
 Vosotros si veis que os molesta

333
00:07:38,220 --> 00:07:39,380
 Podéis ir bajando el número

334
00:07:39,380 --> 00:07:42,060
 Y ver si la funcionalidad

335
00:07:42,060 --> 00:07:43,400
 Se ajusta más a vuestros gustos

336
00:07:43,400 --> 00:07:44,360
 Como siempre

337
00:07:44,360 --> 00:07:45,740
 Suelen hacer cosas

338
00:07:45,740 --> 00:07:47,080
 La mayoría de cosas nos gustan

339
00:07:47,080 --> 00:07:48,240
 Pero cuando no nos gustan

340
00:07:48,240 --> 00:07:49,140
 En Plasma

341
00:07:49,140 --> 00:07:50,200
 Las podemos cambiar

342
00:07:50,200 --> 00:07:51,320
 Y una mejora que seguro

343
00:07:51,320 --> 00:07:52,180
 Que le gustarán a todos

344
00:07:52,180 --> 00:07:53,180
 Los que tengan un Intel

345
00:07:53,180 --> 00:07:55,600
 Es que han hecho una activación

346
00:07:55,600 --> 00:07:58,040
 Parcial del triple buffering

347
00:07:58,040 --> 00:07:58,740
 Que lo que hace es

348
00:07:58,740 --> 00:07:59,760
 Cuando detecta casos

349
00:07:59,760 --> 00:08:01,160
 Que sería recomendable

350
00:08:01,160 --> 00:08:02,540
 Lo activa

351
00:08:02,540 --> 00:08:04,920
 Y se ve que en alguna gráfica

352
00:08:04,920 --> 00:08:05,180
 Intel

353
00:08:05,180 --> 00:08:05,900
 Muy antigua

354
00:08:05,900 --> 00:08:07,000
 Esto es muy necesario

355
00:08:07,000 --> 00:08:08,920
 Y han conseguido que el rendimiento

356
00:08:08,920 --> 00:08:10,400
 Sea muchísimo mejor

357
00:08:10,400 --> 00:08:11,540
 Al activar esto

358
00:08:11,540 --> 00:08:13,020
 En Wayland por supuesto

359
00:08:13,020 --> 00:08:14,480
 Porque en X11 ya estaba

360
00:08:14,480 --> 00:08:15,860
 Estas serían las

361
00:08:15,860 --> 00:08:17,540
 características principales

362
00:08:17,540 --> 00:08:19,160
 Creo que no me dejo ninguna

363
00:08:19,160 --> 00:08:20,380
 A ver...

364
00:08:20,380 --> 00:08:23,160
 Bueno que en...

365
00:08:23,160 --> 00:08:23,920
 En NVIDIA

366
00:08:23,920 --> 00:08:24,800
 Como ahora tiene

367
00:08:24,800 --> 00:08:26,120
 La sincronización explícita

368
00:08:26,120 --> 00:08:27,000
 Volverá a ir mucho mejor

369
00:08:27,000 --> 00:08:28,920
 Pero como yo no soy muy fan

370
00:08:28,920 --> 00:08:29,520
 De NVIDIA

371
00:08:29,520 --> 00:08:30,720
 No le he hecho mucho caso

372
00:08:30,720 --> 00:08:31,820
 Y con esto

373
00:08:31,820 --> 00:08:32,560
 Voy a pasar

374
00:08:32,560 --> 00:08:34,310
 A la sección despedida del

375
00:08:34,310 --> 00:08:35,000
 programa

376
00:08:35,000 --> 00:08:36,740
 Que es el atajo recomendado

377
00:08:36,740 --> 00:08:38,580
 El atajo recomendado de hoy

378
00:08:38,580 --> 00:08:39,320
 Es

379
00:08:39,320 --> 00:08:40,440
 META R

380
00:08:40,440 --> 00:08:41,820
 La tecla con el Linux

381
00:08:41,820 --> 00:08:42,880
 O la tecla de Windows

382
00:08:42,880 --> 00:08:43,700
 Y R

383
00:08:43,700 --> 00:08:44,400
 Esto lo que te hace

384
00:08:44,400 --> 00:08:45,200
 Es que te va a abrir

385
00:08:45,200 --> 00:08:46,240
 Espectacle

386
00:08:46,240 --> 00:08:47,480
 En modo grabación

387
00:08:47,480 --> 00:08:48,620
 Te va a dejar seleccionar un

388
00:08:48,620 --> 00:08:49,640
 rectángulo

389
00:08:49,640 --> 00:08:50,320
 Tú le pinchas

390
00:08:50,320 --> 00:08:51,760
 Y ya está grabando automáticamente

391
00:08:51,760 --> 00:08:52,400
 un vídeo

392
00:08:52,400 --> 00:08:53,040
 Entonces saldrá

393
00:08:53,040 --> 00:08:53,720
 Un icono rojo

394
00:08:53,720 --> 00:08:54,520
 En la barra de tareas

395
00:08:54,520 --> 00:08:55,300
 Para poder pararlo

396
00:08:55,300 --> 00:08:56,980
 Tiene combinaciones de teclas

397
00:08:56,980 --> 00:08:57,620
 Según si hace

398
00:08:57,620 --> 00:08:59,180
 META + SHIFT

399
00:08:59,180 --> 00:08:59,740
 + R

400
00:08:59,740 --> 00:09:00,920
 O META + ALT

401
00:09:00,920 --> 00:09:01,440
 + R

402
00:09:01,440 --> 00:09:02,920
 Va a ser la pantalla completa

403
00:09:02,920 --> 00:09:03,420
 Va a ser

404
00:09:03,420 --> 00:09:05,440
 La ventana en concreto

405
00:09:05,440 --> 00:09:06,660
 O el modo recuadro

406
00:09:06,660 --> 00:09:08,060
 Pero está gracioso

407
00:09:08,060 --> 00:09:09,560
 Pues si soléis hacer grabaciones

408
00:09:09,560 --> 00:09:11,000
 O queréis hacer un videocast

409
00:09:11,000 --> 00:09:11,320
 de algo

410
00:09:11,320 --> 00:09:12,560
 Pues hay un...

411
00:09:12,560 --> 00:09:13,960
 Un atajo de teclado

412
00:09:13,960 --> 00:09:14,860
 Directamente

413
00:09:14,860 --> 00:09:16,200
 Y ahora voy a añadir

414
00:09:16,200 --> 00:09:17,960
 Un atajo recomendado

415
00:09:17,960 --> 00:09:18,780
 Pero de accesibilidad

416
00:09:18,780 --> 00:09:20,460
 Que es uno que me he encontrado

417
00:09:20,460 --> 00:09:20,960
 sin querer

418
00:09:20,960 --> 00:09:22,520
 Es que si le das a la tecla META

419
00:09:22,520 --> 00:09:22,920
 Y al lado de la tecla R

420
00:09:22,920 --> 00:09:23,400
 Y al Más

421
00:09:23,400 --> 00:09:25,260
 Se te va a hacer un zoom de la

422
00:09:25,260 --> 00:09:25,740
 pantalla

423
00:09:25,740 --> 00:09:27,660
 Entonces tú te puedes ir moviendo

424
00:09:27,660 --> 00:09:28,320
 con el ratón

425
00:09:28,320 --> 00:09:30,560
 Y el zoom va siguiendo al ratón

426
00:09:30,560 --> 00:09:32,760
 Para que no sea un cuadro fijo

427
00:09:32,760 --> 00:09:34,150
 Esto realmente es que yo

428
00:09:34,150 --> 00:09:34,800
 quería hacer

429
00:09:34,800 --> 00:09:36,680
 META Asterisco

430
00:09:36,680 --> 00:09:39,220
 Y el teclado numérico

431
00:09:39,220 --> 00:09:40,080
 Para esto no funciona

432
00:09:40,080 --> 00:09:41,560
 Tenéis que usar el teclado a

433
00:09:41,560 --> 00:09:42,640
 partir del Enter y el Red

434
00:09:42,640 --> 00:09:45,340
 Y entonces para hacer el META Asterisco

435
00:09:45,340 --> 00:09:46,540
 No funciona

436
00:09:46,540 --> 00:09:47,600
 Porque donde está el Asterisco

437
00:09:47,600 --> 00:09:48,140
 está el Más

438
00:09:48,140 --> 00:09:49,420
 Entonces tendrías que hacer META

439
00:09:49,420 --> 00:09:50,460
 SHIFT Asterisco

440
00:09:50,460 --> 00:09:52,460
 Y entonces al hacer META Más

441
00:09:52,460 --> 00:09:52,800
 Pues va a ser

442
00:09:52,800 --> 00:09:53,580
 Vas haciendo zoom

443
00:09:53,580 --> 00:09:54,780
 Puedes seguir haciendo zoom

444
00:09:54,780 --> 00:09:56,950
 También puedes darle al META

445
00:09:56,950 --> 00:09:57,440
 Menos

446
00:09:57,440 --> 00:09:59,480
 O al META Cero

447
00:09:59,480 --> 00:10:01,420
 Y entonces el zoom se resetea

448
00:10:01,420 --> 00:10:03,450
 Y vuelves de golpe a la

449
00:10:03,450 --> 00:10:04,720
 pantalla como solía estar

450
00:10:04,720 --> 00:10:06,040
 Cosas que son prácticas

451
00:10:06,040 --> 00:10:07,720
 Yo os recomiendo iros a las opciones

452
00:10:07,720 --> 00:10:09,540
 Ir a atajos de teclado

453
00:10:09,540 --> 00:10:10,740
 Y ahí la verdad es que hay un

454
00:10:10,740 --> 00:10:11,160
 montón

455
00:10:11,160 --> 00:10:13,560
 Y ahí dedicarse a estar un ratito

456
00:10:13,560 --> 00:10:14,840
 De vez en cuando mirar las

457
00:10:14,840 --> 00:10:16,160
 aplicaciones que más usáis

458
00:10:16,160 --> 00:10:17,580
 Seguro que encontráis funciones

459
00:10:17,580 --> 00:10:19,700
 Que os alegran la vida del

460
00:10:19,700 --> 00:10:20,800
 tiempo que recuperáis

461
00:10:20,800 --> 00:10:22,080
 Con esto os dejo

462
00:10:22,080 --> 00:10:22,680
 Espero que os haya gustado

463
00:10:22,680 --> 00:10:23,740
 Espero conseguir hacer un

464
00:10:23,740 --> 00:10:24,600
 episodio en julio

465
00:10:24,600 --> 00:10:26,560
 Y no entrar en modo vacaciones

466
00:10:26,560 --> 00:10:28,740
 Dado que sería el tercer aniversario

467
00:10:28,740 --> 00:10:29,560
 de KDE Express

468
00:10:29,560 --> 00:10:31,020
 Y si no ya sabéis que tenemos

469
00:10:31,020 --> 00:10:32,100
 Una sala de Matrix

470
00:10:32,100 --> 00:10:33,980
 Un grupo de Telegram

471
00:10:33,980 --> 00:10:35,700
 Cuenta también del canal de

472
00:10:35,700 --> 00:10:36,320
 Telegram

473
00:10:36,320 --> 00:10:37,580
 Correo electrónico

474
00:10:37,580 --> 00:10:38,900
 Estamos por el Fediverso

475
00:10:38,900 --> 00:10:40,930
 Ya sabéis que nos gusta que

476
00:10:40,930 --> 00:10:42,620
 nos escribáis por allí

477
00:10:42,620 --> 00:10:44,460
 Y si no en cañas y bravas

478
00:10:44,460 --> 00:10:45,860
 Estamos teniendo conversaciones

479
00:10:45,860 --> 00:10:46,820
 siempre interesantes

480
00:10:46,820 --> 00:10:47,860
 Un saludo

481
00:10:47,860 --> 00:10:50,120
 Y una especial dedicatoria

482
00:10:50,120 --> 00:10:52,560
 A nuestro editor en vacaciones

483
00:10:52,560 --> 00:10:53,500
 Jorge Lama

484
00:10:53,500 --> 00:10:54,860
 Que aunque estéis sufriendo

485
00:10:54,860 --> 00:10:55,820
 que estáis de vacaciones

486
00:10:55,820 --> 00:10:57,220
 Se lo merecía

487
00:10:57,220 --> 00:10:58,660
 Y ahora mismo estoy editando yo

488
00:10:58,660 --> 00:10:58,940
 esto

489
00:10:58,940 --> 00:11:00,520
 Imagino que se notará un poco

490
00:11:00,520 --> 00:11:02,980
 Un saludo Jorge y a todos vosotros

491
00:11:02,980 --> 00:11:03,800
 Y hasta la próxima

