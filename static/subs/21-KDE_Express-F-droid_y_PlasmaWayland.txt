KDE Express 21, si no me equivoco, voy en coche así que no lo 
tengo apuntado. Quería contarte un par de 
cosas cortas de las que se pueden utilizar ya y de las que 
vienen a futuro. De las que pueden utilizar ya 
es, en KDE tenemos unos repositorios para F-Droid que tienen 
aplicaciones extras de las que hay en el repositorio oficial 
de F-Droid. Yo lo uso mucho por ejemplo para NeoChat 
y para Itinerary creo que es y pues con eso consigue la aplicación 
en APK directamente, no te la tienes que estar bajando, pero 
eso hasta ahora tenía una pega que 
es que ese repositorio a pesar de ser un repositorio estable, 
porque hay un repositorio con todos los 
builds de la rama master, o sea el código que se está 
desarrollando, hay un repositorio que 
todas las noches se compila y tú tienes pues la versión de 
pruebas, pero el repositorio estable 
solo tiene las versiones que están marcadas con el punto 
algo, pero tenía una pega que a mí me 
volvía loco, que es que también se compilaba todas las 
noches. Todos los días tú tenías dos 
actualizaciones, bueno yo porque tengo esas dos aplicaciones 
instaladas fuera del repositorio oficial, pero toda aplicación 
que venga de ese repositorio todas las noches te aparecía 
en F-Droid como que había una nueva 
versión y si no querías tener la notificación pues tenías 
que decirle que esa aplicación no te avisara y 
tú de vez en cuando meterte y actualizar a mano o estabas 
instalando todos los días la misma aplicación 
porque era exactamente la misma que se había compilado de 
nuevo hasta el día que cambiaba algo el código y entonces esa 
sí era nueva, pero tú no veías la 
diferencia y por suerte a partir de hoy que estáis 
escuchando esto, eso ya no pasa, porque han cambiado 
la infraestructura en la que se compilan, han pasado de Fabricator 
o no sé qué, cómo se llama, lo que 
compilaba antes y ahora lo hacen con GitLab, que es donde 
se está centralizando la infraestructura de KDE Internacional. Entonces 
ahora este segundo repositorio extra que no es el de F-Droid 
oficial y que tiene aplicaciones que no 
están en el oficial, ya sólo te va a dar actualizaciones 
cuando realmente se haya hecho una 
versión nueva de la aplicación y eso a mí la verdad es que me 
viene muy bien porque machaco menos 
la memoria interna del teléfono, el TOC que tengo de que no me 
gusta ver notificaciones me lo va a 
arreglar y de paso voy a saber realmente cuándo se ha actualizado 
la aplicación y cuándo hay 
alguna novedad, así que esa es una de las cosas que ya podéis 
disfrutar y de paso por si no lo sabíais para 
que tengáis en cuenta que hay un repositorio de KDE que pondré 
las notas del programa cómo llegar para poder tener instaladas 
aplicaciones de KDE fuera del repositorio oficial. Eso era lo 
que es para hoy, luego antes de lo que es para el futuro 
quería desde aquí mandar un abrazo 
muy fuerte a Juan Febles que si no estáis en Mastodon lo mismo 
no había enterado de que súbitamente 
nos ha anunciado de que tiene que parar el proyecto Linux 
Express y Podcast Linux por motivos 
personales, sean los que sean esperamos todos que le vaya muy 
bien a Juan porque se merece lo mejor, 
porque en calidad humana no sólo era de los mejores Podcasts 
de Linux de habla hispana sino que 
era de las mejores personas en la comunidad de Linux de habla 
hispana, así que sea lo que sea esperamos 
que se solucione, estoy seguro que todos tus oyentes Juan 
esperan que en el futuro dado que la 
vida es muy larga pueda retomarlo. Y ya volviendo a KDE quería 
contaros que últimamente llevo viendo varias noticias sobre 
que Plasma 6 se pasa a Wayland por defecto y según el artículo 
la verdad es que he visto por lo menos 12, todo no me ha dado 
tiempo a leérmelo entero porque ya ha 
habido un momento que me he saturado, había algunos que 
estaban bien y otros que estaban regular, 
entonces os quiero hacer un resumen por si habéis caído en alguno 
de los que está regular. Plasma 6 ha decidido ser Wayland por 
defecto pero eso es lo que no significa 
es que si tú tienes un ordenador instalado con Plasma al actualizar 
a Plasma 6 no te va a desaparecer el X11 ni se te va 
a cambiar por defecto a Wayland, lo que significa es que para 
nuevas instalaciones de nuevas distribuciones que usen la compilación 
por defecto de Plasma esa compilación va a ir con Wayland, 
se va a poder seguir instalando la sesión X11, 
simplemente la tendrás que instalar dos paquetes y elegir 
a la hora de inicio y muchas distribuciones no compilan 
Wayland por defecto, ellos harán lo que les dé la gana, 
o sea, igual que no se ve igual Plasma en openSUSE, en Fedora, 
en Arch o en Debian pues cada una de esas distribuciones 
eligen qué flags, qué etiquetas activar y desactivar cuando 
compilan, para eso están las distribuciones, 
para hacer elecciones y configurar las cosas a su gusto, 
quitando Gentoo que ahí te lo haces todo tú completamente. 
Entonces habrá distribuciones que porque ellos lo consideran 
que no está maduro o porque les gusta, compilarán Plasma 
con X11 y por defecto tú arrancarás X11 y tendrás que activar Wayland 
tú a mano, que es lo que yo recomiendo, 
porque está bastante maduro, pruébalo y si hay algo en 
concreto que no te va siempre puede volver X11, pero hay 
muchos artículos que te dicen que Plasma 
ha dejado obsoleto X11 y ya no va a poder usarlo, si es verdad 
que yo creo que como Fedora y Gnome 
están hablando de eso, están proyectando a futuro cargarse X11 
y hacerse solo Wayland, pues yo no sé si alguno se ha 
confundido y ha pensado que lo de Plasma va a igual pero no, 
Plasma va a seguir funcionando con X11 durante mucho tiempo y 
la 527 pues todo lo que dure la 527 y la Plasma 6 como va a 
tener soporte de X11 también cuando salga, también va a 
haber una chorra de años con X11 en Plasma 
6, o sea que si os quedaba alguna duda de qué pasa con Plasma 
y Wayland simplemente es el por 
defecto en las compilaciones por defecto, ¿eso qué es? 
pues KDE Neon por ejemplo, KDE Neon 
si que compila como viene configurado el código en los 
repositorios, pues KDE Neon si hace una 
instalación nueva vendrá con Plasma Wayland y lo dicho estará 
en la wiki cuáles son los dos o 
tres paquetes que tienes que instalar para tener el X11 
disponible si hay algo en concreto que no 
te funcione. Y con estas dos cosas terminamos por hoy la 
versión coche, nos escuchamos pronto, un saludo. 
[MÚSICA] [Música de cierre] 
