1
00:00:00,460 --> 00:00:01,870
 Buenas, KDE Express está de

2
00:00:01,870 --> 00:00:04,000
 vuelta con el episodio 35 y

3
00:00:04,000 --> 00:00:06,780
 estoy grabando el 23 de octubre.

4
00:00:06,780 --> 00:00:08,910
 Lo primero comentar que Archive.org

5
00:00:08,910 --> 00:00:10,510
 vuelve a estar funcionando,

6
00:00:10,510 --> 00:00:12,280
 aunque solo en modo lectura.

7
00:00:12,280 --> 00:00:14,790
 Durante una semana ha dejado a

8
00:00:14,790 --> 00:00:16,710
 un montón de podcast y

9
00:00:16,710 --> 00:00:19,880
 servicios y gente y organizaciones

10
00:00:19,880 --> 00:00:21,420
 sin un montón de recursos

11
00:00:21,420 --> 00:00:23,030
 súper importantes para la

12
00:00:23,030 --> 00:00:25,010
 comunidad de software libre y

13
00:00:25,010 --> 00:00:26,660
 de la cultura en general,

14
00:00:26,660 --> 00:00:28,270
 con lo cual es un buen momento

15
00:00:28,270 --> 00:00:29,960
 para donarle porque se están

16
00:00:29,960 --> 00:00:31,520
 recuperando, pero se ha visto

17
00:00:31,520 --> 00:00:32,640
 con qué facilidad

18
00:00:32,640 --> 00:00:34,060
 podría borrarse de un plumazo

19
00:00:34,060 --> 00:00:35,350
 un montón de información

20
00:00:35,350 --> 00:00:36,520
 súper interesante.

21
00:00:36,520 --> 00:00:38,400
 Nosotros en KDE lo sublimos

22
00:00:38,400 --> 00:00:40,570
 porque tenemos servidores propios

23
00:00:40,570 --> 00:00:42,420
 y estamos en camino de pasar

24
00:00:42,420 --> 00:00:43,420
 todos los audios

25
00:00:43,420 --> 00:00:45,380
 ahí para aligerar un poco el

26
00:00:45,380 --> 00:00:48,430
 trabajo que le damos a Archive.org,

27
00:00:48,430 --> 00:00:50,620
 pero no está de mal recordar

28
00:00:50,620 --> 00:00:51,460
 que a veces

29
00:00:51,460 --> 00:00:53,600
 damos por sentado servicios que

30
00:00:53,600 --> 00:00:55,410
 si desaparecieran nos harían

31
00:00:55,410 --> 00:00:56,700
 un gran agujero.

32
00:00:56,700 --> 00:00:57,970
 Entrando ya en lo que es el

33
00:00:57,970 --> 00:00:59,310
 episodio en sí, estoy en

34
00:00:59,310 --> 00:01:02,330
 Plasma 6.2.1, ya fui hablando

35
00:01:02,330 --> 00:01:03,220
 de las novedades

36
00:01:03,220 --> 00:01:04,780
 que iban a llegar y han llegado.

37
00:01:04,780 --> 00:01:06,090
 Ya no solo estamos en 6.2 sino

38
00:01:06,090 --> 00:01:08,080
 que estamos en 6.2.1 y por lo

39
00:01:08,080 --> 00:01:08,780
 que oigo

40
00:01:08,780 --> 00:01:12,020
 la 6.2.1 está muy estabilizada,

41
00:01:12,020 --> 00:01:15,170
 la 6.2.2 estará perfecta y por

42
00:01:15,170 --> 00:01:17,340
 supuesto están trabajando ya

43
00:01:17,340 --> 00:01:18,540
 en la 6.3.

44
00:01:18,540 --> 00:01:21,770
 también tenemos el year 24.08.2

45
00:01:21,770 --> 00:01:24,470
 con sus mejoras correspondientes,

46
00:01:24,470 --> 00:01:25,940
 pero ahora voy a entrar en

47
00:01:25,940 --> 00:01:27,940
 materia y os voy a ir contando

48
00:01:27,940 --> 00:01:29,260
 las novedades que han pasado en

49
00:01:29,260 --> 00:01:30,620
 este tiempo desde que grabé.

50
00:01:30,620 --> 00:01:32,410
 Lo primero es que ya tenemos

51
00:01:32,410 --> 00:01:34,150
 todas las charlas de la Akademy

52
00:01:34,150 --> 00:01:36,460
 E 2024 de Valencia en nuestra

53
00:01:36,460 --> 00:01:37,700
 instancia de virtud.

54
00:01:38,040 --> 00:01:39,670
 De nuevo queremos agradecer a

55
00:01:39,670 --> 00:01:41,390
 John por su colaboración, que

56
00:01:41,390 --> 00:01:43,280
 hemos ido partiendo, separando,

57
00:01:43,280 --> 00:01:44,280
 mejorando el audio.

58
00:01:44,280 --> 00:01:46,320
 Luego con Whisper, igual que

59
00:01:46,320 --> 00:01:48,310
 con este podcast, pues le he

60
00:01:48,310 --> 00:01:50,400
 ido pasando los subtítulos y

61
00:01:50,400 --> 00:01:52,180
 ya están todas en una playlist

62
00:01:52,180 --> 00:01:54,120
 para que podáis disfrutarlas

63
00:01:54,120 --> 00:01:56,040
 los que no tuvierais la inmensa

64
00:01:56,040 --> 00:01:58,220
 fortuna de estar presencialmente

65
00:01:58,220 --> 00:01:59,420
 allí como yo.

66
00:01:59,980 --> 00:02:01,520
 También comentar que el 14 de

67
00:02:01,520 --> 00:02:03,400
 octubre celebramos el 28 cumpleaños

68
00:02:03,400 --> 00:02:05,470
 de KDE, se nos pasó entre

69
00:02:05,470 --> 00:02:07,410
 episodios, pero sigue siendo

70
00:02:07,410 --> 00:02:09,480
 ahí una efeméride

71
00:02:09,480 --> 00:02:11,320
 que está bien saber que este

72
00:02:11,320 --> 00:02:13,460
 proyecto tiene la miaja de 28

73
00:02:13,460 --> 00:02:15,450
 años ya, no está nada mal y

74
00:02:15,450 --> 00:02:17,000
 aparte de vez en cuando ver

75
00:02:17,000 --> 00:02:18,260
 algunas fotos antiguas

76
00:02:18,260 --> 00:02:20,280
 da así un gustirrinín de

77
00:02:20,280 --> 00:02:22,500
 cómo ha avanzado la cosa y lo

78
00:02:22,500 --> 00:02:24,920
 que disfrutábamos entonces y

79
00:02:24,920 --> 00:02:26,970
 lo fácil que son las cosas

80
00:02:26,970 --> 00:02:27,860
 ahora.

81
00:02:28,260 --> 00:02:30,210
 También quería deciros que a

82
00:02:30,210 --> 00:02:32,000
 las ya conocidas entradas de

83
00:02:32,000 --> 00:02:33,960
 esta semana en KDE, que es un

84
00:02:33,960 --> 00:02:36,020
 blog del que este podcast se

85
00:02:36,020 --> 00:02:36,500
 nutre

86
00:02:36,500 --> 00:02:37,850
 igual que todos los demás

87
00:02:37,850 --> 00:02:39,970
 blogs del mundo que hablan de KDE,

88
00:02:39,970 --> 00:02:41,420
 pues ahora como este Yasuo se

89
00:02:41,420 --> 00:02:42,900
 centra en Plasma en sí

90
00:02:42,900 --> 00:02:45,020
 tenemos la contrapartida para

91
00:02:45,020 --> 00:02:47,670
 las aplicaciones del Blogs KDE,

92
00:02:47,670 --> 00:02:48,890
 donde nos irán informando de

93
00:02:48,890 --> 00:02:50,170
 que está por venir de las

94
00:02:50,170 --> 00:02:51,700
 nuevas versiones de Gears.

95
00:02:52,200 --> 00:02:54,720
 Siguiendo, tenemos que Ubuntu KDE

96
00:02:54,720 --> 00:02:56,960
 Neon Core es una nueva distribución

97
00:02:56,960 --> 00:02:58,580
 que está preparando Ubuntu,

98
00:02:58,580 --> 00:03:00,440
 que sería una versión inmutable

99
00:03:00,440 --> 00:03:03,550
 atómica de KDE con Plasma. Están

100
00:03:03,550 --> 00:03:05,850
 de moda ahora mismo y está

101
00:03:05,850 --> 00:03:07,640
 bien que tengamos versiones con

102
00:03:07,640 --> 00:03:08,680
 KDE, porque hay algunas

103
00:03:08,680 --> 00:03:10,080
 por ejemplo en otros sistemas

104
00:03:10,080 --> 00:03:12,240
 operativos, pero siempre las de

105
00:03:12,240 --> 00:03:15,000
 Gnome suelen estar más avanzadas.

106
00:03:15,000 --> 00:03:16,920
 Con lo cual, esto me parece una

107
00:03:16,920 --> 00:03:16,920
 buena iniciativa

108
00:03:16,920 --> 00:03:18,170
 para los casos de uso que se vea

109
00:03:18,170 --> 00:03:20,360
 que la inmutable o la atómica

110
00:03:20,360 --> 00:03:23,260
 viene mejor. Hablando de KDE Neon,

111
00:03:23,260 --> 00:03:25,150
 esta vez la versión normal y

112
00:03:25,150 --> 00:03:25,160
 corriente

113
00:03:25,160 --> 00:03:27,160
 pues tras salir Ubuntu 24-04

114
00:03:27,160 --> 00:03:31,400
 estuvieron trabajando como locos

115
00:03:31,400 --> 00:03:33,400
 y ya la tenemos disponible.

116
00:03:33,400 --> 00:03:35,160
 Ya sabéis que son los repos

117
00:03:35,160 --> 00:03:38,060
 oficiales de Ubuntu 24-04, pero

118
00:03:38,060 --> 00:03:39,400
 con la paquetería por encima

119
00:03:39,400 --> 00:03:41,080
 de todo lo necesario para tener

120
00:03:41,080 --> 00:03:43,160
 Plasma a la última.

121
00:03:43,160 --> 00:03:44,420
 Otro otro que también se han

122
00:03:44,420 --> 00:03:46,410
 subido al carro de Plasma 6 es

123
00:03:46,410 --> 00:03:49,160
 Ubuntu Studio, que le ha costado

124
00:03:49,160 --> 00:03:51,600
 llegar a la 24-10, pero bueno

125
00:03:51,600 --> 00:03:53,600
 ya sabemos que Ubuntu a partir

126
00:03:53,600 --> 00:03:55,460
 de la 24-10 por fin tendrá

127
00:03:55,460 --> 00:03:57,570
 nuestro querido Plasma 6, que

128
00:03:57,570 --> 00:04:00,440
 es un salto sustancial y que yo

129
00:04:00,440 --> 00:04:02,190
 aunque valoro mucho las distribuciones

130
00:04:02,190 --> 00:04:06,360
 estables, me parece un viaje en

131
00:04:06,360 --> 00:04:08,120
 el tiempo eso de tener que

132
00:04:08,120 --> 00:04:09,560
 estar esperando tanto tanto

133
00:04:09,560 --> 00:04:11,160
 tiempo para tener las mejoras

134
00:04:11,160 --> 00:04:12,510
 que ya están publicadas hoy en

135
00:04:12,510 --> 00:04:13,020
 día.

136
00:04:13,020 --> 00:04:15,130
 Yo lo llamo que tengo versionitis

137
00:04:15,130 --> 00:04:17,340
 y para eso tenemos distribuciones

138
00:04:17,340 --> 00:04:19,420
 adecuadas y luego está quien

139
00:04:19,420 --> 00:04:21,320
 no quiere tanta filigrana y

140
00:04:21,320 --> 00:04:24,100
 quiere que le funcione tranquilamente

141
00:04:24,100 --> 00:04:26,050
 y se queda con versiones más

142
00:04:26,050 --> 00:04:26,880
 estables.

143
00:04:26,880 --> 00:04:28,510
 Aunque yo tengo que decir que

144
00:04:28,510 --> 00:04:30,310
 yo casi nunca tengo problemas y

145
00:04:30,310 --> 00:04:31,870
 estoy a la última, así que

146
00:04:31,870 --> 00:04:33,490
 ahí hay un buen debate sobre

147
00:04:33,490 --> 00:04:35,100
 si que es más estable, algo

148
00:04:35,100 --> 00:04:36,570
 muy antiguo que tienen que

149
00:04:36,570 --> 00:04:38,300
 mantener los voluntarios o lo

150
00:04:38,300 --> 00:04:39,990
 que va sacando el Upstream, el

151
00:04:39,990 --> 00:04:41,500
 proyecto madre.

152
00:04:41,820 --> 00:04:43,160
 Eso lo dejamos para otra ocasión.

153
00:04:43,160 --> 00:04:44,600
 Una de las novedades que se me

154
00:04:44,600 --> 00:04:46,700
 olvidó adelantar de Plasma 6.2,

155
00:04:46,700 --> 00:04:48,140
 yo os fui comentando que iba a

156
00:04:48,140 --> 00:04:49,880
 venir en Plasma 6.2 antes de

157
00:04:49,880 --> 00:04:51,630
 que viniera, porque normalmente

158
00:04:51,630 --> 00:04:52,890
 los desarrolladores van

159
00:04:52,890 --> 00:04:54,450
 diciendo ya hemos hecho esto,

160
00:04:54,450 --> 00:04:56,350
 ya hemos hecho lo otro, y hay

161
00:04:56,350 --> 00:04:58,180
 una que se me olvidó y gracias

162
00:04:58,180 --> 00:05:00,180
 a cada blog la tenemos siempre

163
00:05:00,180 --> 00:05:02,700
 todas en mente, que es que han

164
00:05:02,700 --> 00:05:04,620
 mejorado el manejo del brillo

165
00:05:04,620 --> 00:05:06,740
 para los perfiles HDR e ICC,

166
00:05:06,740 --> 00:05:09,280
 así como el rendimiento HDR.

167
00:05:09,280 --> 00:05:11,070
 Lo cual a mí me viene de Perla

168
00:05:11,070 --> 00:05:12,870
 porque tenía dos monitores,

169
00:05:12,870 --> 00:05:14,290
 uno se lo he regalado a mi

170
00:05:14,290 --> 00:05:15,640
 madre y estoy a punto de

171
00:05:15,640 --> 00:05:17,520
 comprarme otro, y el que me voy

172
00:05:17,520 --> 00:05:19,180
 a comprar creo que va a tener

173
00:05:19,180 --> 00:05:20,990
 soporte HDR y estoy deseando

174
00:05:20,990 --> 00:05:22,340
 empezar a jugar.

175
00:05:22,700 --> 00:05:24,130
 Con suerte para cuando me lo

176
00:05:24,130 --> 00:05:25,950
 compre estará Plasma 6.3 y

177
00:05:25,950 --> 00:05:27,510
 seguro que estará todavía

178
00:05:27,510 --> 00:05:29,370
 mejor implementado, pero bueno,

179
00:05:29,370 --> 00:05:30,790
 la cosa es que ya en Linux hay

180
00:05:30,790 --> 00:05:32,350
 soporte para HDR, que es algo

181
00:05:32,350 --> 00:05:34,180
 que nos ha costado llegar.

182
00:05:34,580 --> 00:05:36,390
 Por supuesto, esto será en Wayland,

183
00:05:36,390 --> 00:05:37,800
 en X11 no creo que eso vaya a

184
00:05:37,800 --> 00:05:38,400
 llegar.

185
00:05:38,400 --> 00:05:39,980
 Una de las cosas de futuro, y

186
00:05:39,980 --> 00:05:42,180
 ya dejamos Plasma 6.2, que esto

187
00:05:42,180 --> 00:05:43,550
 ya lo tenemos aquí en las

188
00:05:43,550 --> 00:05:45,420
 manos, es que Dolphin mejorará

189
00:05:45,420 --> 00:05:47,360
 la búsqueda dentro de los ficheros

190
00:05:47,360 --> 00:05:49,010
 en la próxima versión, que

191
00:05:49,010 --> 00:05:50,870
 sea seguramente por diciembre,

192
00:05:50,870 --> 00:05:52,880
 en la 2412 de Gears.

193
00:05:53,260 --> 00:05:55,740
 Y la cosa es que quien conozcáis

194
00:05:55,740 --> 00:05:58,400
 RipGrep, que no es una variante,

195
00:05:58,400 --> 00:06:00,690
 es una alternativa a Grep, lo

196
00:06:00,690 --> 00:06:02,540
 que pasa es que es súper

197
00:06:02,540 --> 00:06:04,840
 rápido y es capaz de buscar en

198
00:06:04,840 --> 00:06:06,160
 proyectos.

199
00:06:07,080 --> 00:06:08,360
 Tiene mucha más funcionalidad

200
00:06:08,360 --> 00:06:09,560
 y mucho más rendimiento.

201
00:06:09,560 --> 00:06:10,860
 Bueno, pues Dolphin va a ser

202
00:06:10,860 --> 00:06:12,280
 capaz de usarlo, si lo tenéis

203
00:06:12,280 --> 00:06:13,710
 instalado en el sistema, para

204
00:06:13,710 --> 00:06:15,060
 que las búsquedas sean mucho

205
00:06:15,060 --> 00:06:16,040
 más rápidas.

206
00:06:16,040 --> 00:06:17,720
 Esto creo que solo va a funcionar

207
00:06:17,720 --> 00:06:19,340
 dentro de los ficheros, que es

208
00:06:19,340 --> 00:06:20,780
 lo que hace RipGrep, busca

209
00:06:20,780 --> 00:06:22,210
 contenido, y si lo tenéis

210
00:06:22,210 --> 00:06:23,100
 activado.

211
00:06:23,100 --> 00:06:24,930
 Lo que es el nombre de fichero

212
00:06:24,930 --> 00:06:27,010
 va a seguir funcionando igual,

213
00:06:27,010 --> 00:06:29,190
 y también vais a seguir teniendo

214
00:06:29,190 --> 00:06:31,180
 la funcionalidad de Akonadi,

215
00:06:31,180 --> 00:06:32,540
 eso no cambia.

216
00:06:33,100 --> 00:06:35,190
 Una pequeña noticia de accesibilidad

217
00:06:35,190 --> 00:06:36,380
 tangencialmente.

218
00:06:36,380 --> 00:06:38,970
 Ya es posible crear y usar cursores

219
00:06:38,970 --> 00:06:40,060
 SVG en Plasma.

220
00:06:40,060 --> 00:06:41,750
 Esto, por ejemplo, si os acordáis,

221
00:06:41,750 --> 00:06:42,980
 el efecto que ahora viene por

222
00:06:42,980 --> 00:06:44,240
 defecto, de que sin meneer el

223
00:06:44,240 --> 00:06:45,800
 ratón se va haciendo grande.

224
00:06:45,800 --> 00:06:47,690
 La primera implementación, el

225
00:06:47,690 --> 00:06:49,490
 ratón se hacía, el puntero se

226
00:06:49,490 --> 00:06:51,050
 hacía grande, pero se hacía

227
00:06:51,050 --> 00:06:52,660
 borroso y era un poco feo.

228
00:06:52,660 --> 00:06:53,860
 Eso es porque estaba escalando

229
00:06:53,860 --> 00:06:54,460
 una imagen.

230
00:06:54,460 --> 00:06:57,010
 Ahora que tenemos iconos SVG

231
00:06:57,010 --> 00:06:59,970
 que se escalan perfectamente,

232
00:06:59,970 --> 00:07:02,530
 pues el ratón va a poder tener

233
00:07:02,530 --> 00:07:04,260
 un puntero SVG.

234
00:07:04,260 --> 00:07:05,720
 Ahora mismo no están todos

235
00:07:05,720 --> 00:07:07,100
 disponibles, pero ya hay

236
00:07:07,100 --> 00:07:08,680
 algunos dentro del Plasma, y

237
00:07:08,680 --> 00:07:10,320
 luego se podrán ir añadiendo

238
00:07:10,320 --> 00:07:11,800
 más, que escalen bien.

239
00:07:11,800 --> 00:07:12,800
 Y esto viene muy bien a la

240
00:07:12,800 --> 00:07:14,300
 accesibilidad, porque hay gente

241
00:07:14,300 --> 00:07:15,740
 que necesita el ratón muy gordo,

242
00:07:15,740 --> 00:07:17,880
 y está bien que sea fino y con

243
00:07:17,880 --> 00:07:20,100
 líneas y no emborronado.

244
00:07:20,300 --> 00:07:22,310
 Luego, dentro de noticias de la

245
00:07:22,310 --> 00:07:24,370
 Asociación de KDE, pues esto

246
00:07:24,370 --> 00:07:25,970
 ha pasado volando desde el

247
00:07:25,970 --> 00:07:27,910
 episodio anterior, pero hemos

248
00:07:27,910 --> 00:07:29,560
 hecho unas elecciones que las

249
00:07:29,560 --> 00:07:32,440
 teníamos pendientes de la Akademy,

250
00:07:32,440 --> 00:07:33,720
 porque a nuestro querido

251
00:07:33,720 --> 00:07:35,420
 presidente Adrián se le acababa

252
00:07:35,420 --> 00:07:36,930
 el plazo de mandato, son los

253
00:07:36,930 --> 00:07:39,140
 estatutos, y entonces decidimos

254
00:07:39,140 --> 00:07:41,240
 que para hacer unas elecciones

255
00:07:41,240 --> 00:07:42,520
 solo a presidente,

256
00:07:42,860 --> 00:07:44,210
 y al poco tener que hacer otras

257
00:07:44,210 --> 00:07:45,510
 para la Junta, dijimos, mira,

258
00:07:45,510 --> 00:07:47,110
 lo hacemos todo de golpe, dimitimos

259
00:07:47,110 --> 00:07:48,600
 y renovamos Junta y Presidente.

260
00:07:48,600 --> 00:07:50,040
 Y hemos cambiado un poco la

261
00:07:50,040 --> 00:07:51,720
 distribución, aunque no tanto,

262
00:07:51,720 --> 00:07:53,100
 porque lo que hemos hecho es

263
00:07:53,100 --> 00:07:55,040
 que Adrián, que era presidente,

264
00:07:55,040 --> 00:07:56,500
 desde aquí gracias por tu

265
00:07:56,500 --> 00:07:58,720
 trabajo, pasa a vicepresidente.

266
00:07:58,720 --> 00:08:00,740
 Rubén, que era vicepresidente,

267
00:08:00,740 --> 00:08:02,380
 ahora pasa a presidente.

268
00:08:02,380 --> 00:08:03,820
 Mucho ánimo, presidente, con

269
00:08:03,820 --> 00:08:05,520
 el trabajo que tienes por delante.

270
00:08:06,020 --> 00:08:07,890
 Repite el tesorero, José Millán,

271
00:08:07,890 --> 00:08:09,830
 repite nuestro inigualable

272
00:08:09,830 --> 00:08:12,140
 secretario, Baltasar Ortega,

273
00:08:12,140 --> 00:08:13,640
 repetimos de vocales,

274
00:08:13,640 --> 00:08:16,240
 Rosy Matthews y yo, y tenemos

275
00:08:16,240 --> 00:08:18,410
 una nueva incorporación, que

276
00:08:18,410 --> 00:08:20,660
 es Iván G.J., ya lo conocéis

277
00:08:20,660 --> 00:08:21,620
 porque ha estado por aquí

278
00:08:21,620 --> 00:08:23,650
 y también está divulgando por

279
00:08:23,650 --> 00:08:25,350
 PeerTube y por YouTube, que

280
00:08:25,350 --> 00:08:27,310
 entra en sustitución de José

281
00:08:27,310 --> 00:08:30,140
 Picón, que fue el padre espiritual

282
00:08:30,140 --> 00:08:30,960
 de este podcast,

283
00:08:30,960 --> 00:08:33,810
 al que le debemos muchas iniciativas

284
00:08:33,810 --> 00:08:36,110
 y mucho tiempo que ha echado de

285
00:08:36,110 --> 00:08:37,940
 su vida personal en este

286
00:08:37,940 --> 00:08:39,460
 proyecto y que ahora mismo

287
00:08:39,460 --> 00:08:40,970
 necesita tomarse un pequeño

288
00:08:40,970 --> 00:08:41,640
 descanso,

289
00:08:41,640 --> 00:08:43,010
 pero que sigue con nosotros en

290
00:08:43,010 --> 00:08:44,490
 la comunidad y al que le deseamos

291
00:08:44,490 --> 00:08:45,120
 lo mejor.

292
00:08:45,120 --> 00:08:47,140
 Y luego, una noticia fuera ya

293
00:08:47,140 --> 00:08:48,900
 del entorno KDE, pero que

294
00:08:48,900 --> 00:08:51,120
 nosotros lo usamos, es que hay

295
00:08:51,120 --> 00:08:52,560
 una nueva versión de PeerTube,

296
00:08:52,560 --> 00:08:54,270
 en la que tenemos ya audio y

297
00:08:54,270 --> 00:08:55,360
 vídeo separado.

298
00:08:55,360 --> 00:08:56,830
 Esto es de que puedes tener,

299
00:08:56,830 --> 00:08:58,860
 aparte de que quiero ver a 1080

300
00:08:58,860 --> 00:09:01,000
 el vídeo, quiero verlo a 720,

301
00:09:01,000 --> 00:09:03,680
 pues ahora quiero ver solo una

302
00:09:03,680 --> 00:09:06,620
 pantalla negra y tener el audio,

303
00:09:06,620 --> 00:09:08,520
 porque a lo mejor tengo el móvil

304
00:09:08,520 --> 00:09:10,270
 apagado o no me interesa y así

305
00:09:10,270 --> 00:09:11,440
 ahorras datos.

306
00:09:11,680 --> 00:09:13,300
 Esto además viene bien porque

307
00:09:13,300 --> 00:09:14,620
 es una primera parte que hará

308
00:09:14,620 --> 00:09:16,310
 falta para conseguir implementar

309
00:09:16,310 --> 00:09:18,280
 que se puedan tener varios audios.

310
00:09:18,280 --> 00:09:19,910
 Ahora mismo no lo puedes tener,

311
00:09:19,910 --> 00:09:21,600
 pero es una petición que hay

312
00:09:21,600 --> 00:09:23,580
 para poder tener o varios idiomas

313
00:09:23,580 --> 00:09:25,100
 o audios con comentarios

314
00:09:25,100 --> 00:09:27,060
 descriptivos para personas con

315
00:09:27,060 --> 00:09:28,780
 deficiencias visuales.

316
00:09:28,780 --> 00:09:30,390
 Pues es una primera piedra en

317
00:09:30,390 --> 00:09:31,200
 el camino.

318
00:09:31,200 --> 00:09:33,530
 Luego también trae un buscador

319
00:09:33,530 --> 00:09:35,860
 en el widget de subtítulos.

320
00:09:35,860 --> 00:09:37,270
 Tú puedes tener subtítulos en

321
00:09:37,270 --> 00:09:38,200
 PeerTube, que los ves

322
00:09:38,200 --> 00:09:39,600
 directamente en el vídeo.

323
00:09:39,680 --> 00:09:41,430
 Ahora hay un widget que se pone

324
00:09:41,430 --> 00:09:43,040
 en el panel lateral derecho

325
00:09:43,040 --> 00:09:44,770
 abajo y entonces vas viendo

326
00:09:44,770 --> 00:09:45,640
 todo el texto

327
00:09:45,640 --> 00:09:46,780
 y ahora ahí es donde han

328
00:09:46,780 --> 00:09:48,000
 puesto que puedes buscar

329
00:09:48,000 --> 00:09:48,720
 palabras.

330
00:09:48,720 --> 00:09:50,190
 Es una funcionalidad que muchas

331
00:09:50,190 --> 00:09:51,700
 aplicaciones de podcast tienen

332
00:09:51,700 --> 00:09:52,940
 y que viene muy bien cuando

333
00:09:52,940 --> 00:09:54,220
 quieres saber algo en concreto

334
00:09:54,220 --> 00:09:55,000
 que se ha dicho.

335
00:09:55,000 --> 00:09:56,300
 Y luego hay una tercera novedad

336
00:09:56,300 --> 00:09:57,400
 que a mí me sorprendió un

337
00:09:57,400 --> 00:09:57,760
 poco,

338
00:09:57,760 --> 00:09:59,780
 que es que tienes la posibilidad

339
00:09:59,780 --> 00:10:01,660
 de configurar YouTube DL para

340
00:10:01,660 --> 00:10:02,960
 importaciones.

341
00:10:03,520 --> 00:10:04,800
 Yo me pensaba que YouTube DL

342
00:10:04,800 --> 00:10:06,380
 estaba en mantenimiento flojo y

343
00:10:06,380 --> 00:10:07,480
 que había uno nuevo,

344
00:10:07,480 --> 00:10:09,020
 pero si lo han implementado

345
00:10:09,020 --> 00:10:10,920
 ahora debe ser que les va bien.

346
00:10:10,920 --> 00:10:12,480
 PeerTube tiene la opción de que

347
00:10:12,480 --> 00:10:14,130
 tú puedes sincronizar un canal

348
00:10:14,130 --> 00:10:15,320
 de YouTube con PeerTube

349
00:10:15,320 --> 00:10:16,580
 y entonces te hace un espejo.

350
00:10:16,580 --> 00:10:18,700
 Si borras, te lo vuelve a poner.

351
00:10:18,700 --> 00:10:20,580
 Si lo que hay en YouTube se

352
00:10:20,580 --> 00:10:23,200
 asegura de que esté en PeerTube.

353
00:10:23,560 --> 00:10:25,300
 Pero como YouTube está siempre

354
00:10:25,300 --> 00:10:27,050
 así haciendo un poco la puñeta

355
00:10:27,050 --> 00:10:28,740
 en que esto no sea tan fácil,

356
00:10:28,740 --> 00:10:30,380
 se ve que no es suficiente con

357
00:10:30,380 --> 00:10:31,860
 tener una implementación

358
00:10:31,860 --> 00:10:33,940
 directamente en PeerTube ahí

359
00:10:33,940 --> 00:10:34,940
 escrita de golpe,

360
00:10:34,940 --> 00:10:36,620
 sino que han hecho como dependencia

361
00:10:36,620 --> 00:10:38,020
 un programa que se encargue de

362
00:10:38,020 --> 00:10:39,250
 actualizarse cada vez que

363
00:10:39,250 --> 00:10:39,620
 YouTube

364
00:10:39,620 --> 00:10:41,490
 haga alguna de sus triquiñuelas

365
00:10:41,490 --> 00:10:43,520
 para que esto deje de funcionar.

366
00:10:44,480 --> 00:10:46,280
 Y ya sí que fuera de lo que es

367
00:10:46,280 --> 00:10:48,070
 el Entorno KDE y casi el Show

368
00:10:48,070 --> 00:10:48,800
 Art Libre,

369
00:10:48,800 --> 00:10:50,740
 quería dar las gracias a los

370
00:10:50,740 --> 00:10:53,100
 compañeros de Cañas y Poscas,

371
00:10:53,100 --> 00:10:54,780
 que es un evento que hicieron

372
00:10:54,780 --> 00:10:56,440
 los últimos de Feedlipinas y

373
00:10:56,440 --> 00:10:58,680
 otros Poscas de Madrid,

374
00:10:58,680 --> 00:11:00,360
 por ejemplo Borrachuzo y Elena.

375
00:11:00,360 --> 00:11:01,930
 Es un evento de podcasting

376
00:11:01,930 --> 00:11:03,900
 amateur, de la antigua usanza,

377
00:11:03,900 --> 00:11:05,580
 de juntarse gente, escuchar

378
00:11:05,580 --> 00:11:07,280
 directos, hablar de las cosas

379
00:11:07,280 --> 00:11:07,900
 que les gustan.

380
00:11:07,900 --> 00:11:09,870
 Y tiene un poco de parecido con

381
00:11:09,870 --> 00:11:11,810
 el Show Art Libre porque esta

382
00:11:11,810 --> 00:11:13,780
 gente defiende el feed libre.

383
00:11:13,780 --> 00:11:15,920
 O esta gente y yo, que tengo

384
00:11:15,920 --> 00:11:18,480
 ahí también una sección en

385
00:11:18,480 --> 00:11:20,800
 los últimos de Feedlipinas

386
00:11:20,800 --> 00:11:22,700
 con Roberto Ruizán, al que le

387
00:11:22,700 --> 00:11:24,710
 mando un saludo, que fue un placer

388
00:11:24,710 --> 00:11:25,940
 de virtualizar.

389
00:11:25,940 --> 00:11:27,480
 Y la cosa es que tú puedes

390
00:11:27,480 --> 00:11:29,370
 tener un podcast en Spotify sin

391
00:11:29,370 --> 00:11:31,060
 RSS con aplicaciones propietarias

392
00:11:31,060 --> 00:11:33,710
 o tú puedes usar el estándar

393
00:11:33,710 --> 00:11:36,660
 RSS con un feed que esté colgado

394
00:11:36,660 --> 00:11:39,620
 en un servicio codificado

395
00:11:39,620 --> 00:11:40,900
 con aplicaciones libres, como

396
00:11:40,900 --> 00:11:41,920
 se hace este podcast.

397
00:11:41,920 --> 00:11:43,490
 Entonces son diferentes maneras

398
00:11:43,490 --> 00:11:44,890
 de entender cómo publicar el

399
00:11:44,890 --> 00:11:47,200
 contenido y cómo distribuirlo.

400
00:11:47,200 --> 00:11:49,320
 Pues este grupo de gente defiende

401
00:11:49,320 --> 00:11:51,280
 que no todo es el industrial,

402
00:11:51,280 --> 00:11:54,320
 el comercial y el monetizar

403
00:11:54,320 --> 00:11:57,720
 y el usar herramientas propietarias.

404
00:11:57,720 --> 00:11:59,210
 Con lo cual es un evento que la

405
00:11:59,210 --> 00:12:01,440
 verdad es que estuvo muy entretenido.

406
00:12:01,440 --> 00:12:03,540
 Aparte me sirvió para desvirtualizar,

407
00:12:03,540 --> 00:12:05,100
 que también le mando un saludo

408
00:12:05,100 --> 00:12:05,520
 a Iván.

409
00:12:05,520 --> 00:12:07,360
 Y ojalá pudiera ir a más.

410
00:12:07,360 --> 00:12:09,290
 El problema es que el tiempo y

411
00:12:09,290 --> 00:12:11,680
 la vida 1.0 no lo permite.

412
00:12:11,680 --> 00:12:13,700
 También le voy a mandar un saludo,

413
00:12:13,700 --> 00:12:14,990
 aunque en el evento lo vi muy

414
00:12:14,990 --> 00:12:15,460
 poco,

415
00:12:15,460 --> 00:12:17,530
 pero bueno, lo conseguí saludar

416
00:12:17,530 --> 00:12:19,960
 antes de tener que irme a Papafriquei.

417
00:12:20,280 --> 00:12:21,860
 Y de leer enhorabuena porque

418
00:12:21,860 --> 00:12:24,180
 por la caída de Archive.org se

419
00:12:24,180 --> 00:12:26,740
 ha cogido ahí los machos

420
00:12:26,740 --> 00:12:29,010
 y ha decidido pasarse a Castopod

421
00:12:29,010 --> 00:12:31,040
 y ha migrado todo su podcast

422
00:12:31,040 --> 00:12:32,640
 con mucho trabajo y esfuerzo

423
00:12:32,640 --> 00:12:34,130
 y trabajo manual a manopla de

424
00:12:34,130 --> 00:12:35,340
 ir arreglando el feed.

425
00:12:35,340 --> 00:12:37,280
 Y ahora autoaloja y lo hace en

426
00:12:37,280 --> 00:12:38,910
 una plataforma hecha en

427
00:12:38,910 --> 00:12:40,320
 software libre.

428
00:12:40,720 --> 00:12:42,460
 Así que enhorabuena.

429
00:12:42,460 --> 00:12:43,840
 Con esto me despido.

430
00:12:43,840 --> 00:12:45,830
 Por supuesto, gracias a vosotros

431
00:12:45,830 --> 00:12:47,480
 que estáis escuchando.

432
00:12:47,480 --> 00:12:50,190
 A Jorge, que suele editar esto

433
00:12:50,190 --> 00:12:52,320
 con mucha paciencia.

434
00:12:52,320 --> 00:12:54,080
 Y a todas las personas amantes

435
00:12:54,080 --> 00:12:56,190
 del software libre, si se lo hacéis

436
00:12:56,190 --> 00:12:56,820
 llegar.

437
00:12:56,820 --> 00:12:58,440
 Un saludo y nos vemos pronto.

438
00:12:58,440 --> 00:12:58,820
 entonces

