1
00:00:00,000 --> 00:00:06,250
Hola, buenas. Esto es KDE

2
00:00:06,250 --> 00:00:08,320
Express, un podcast donde vamos

3
00:00:08,320 --> 00:00:10,960
a hablar de noticias KDE, tanto

4
00:00:10,960 --> 00:00:14,440
de España como KDE Internacional.

5
00:00:14,440 --> 00:00:16,270
Yo soy José Picón, soy de

6
00:00:16,270 --> 00:00:17,920
GNU/Linux Valencia,

7
00:00:17,920 --> 00:00:20,880
local de KDE España.

8
00:00:20,880 --> 00:00:25,260
Somos tecnológicos y mi

9
00:00:25,260 --> 00:00:30,320
distribución que utilizo es Debian

10
00:00:30,320 --> 00:00:33,320
con KDE. Los podcasts de

11
00:00:33,320 --> 00:00:35,380
KDE que vamos a hacer, los

12
00:00:35,380 --> 00:00:38,030
Express, porque queremos hacer

13
00:00:38,030 --> 00:00:40,560
una durabilidad entre 15 y

14
00:00:40,560 --> 00:00:44,310
30 minutos. Entre ese intervalo

15
00:00:44,310 --> 00:00:46,700
queremos que quede un muy buen

16
00:00:46,700 --> 00:00:48,680
podcast de noticias. Y si

17
00:00:48,680 --> 00:00:50,840
vemos que nos pasamos, cortamos

18
00:00:50,840 --> 00:00:53,830
y para el siguiente, no continuaremos.

19
00:00:53,830 --> 00:00:55,080
A ello le doy

20
00:00:55,080 --> 00:00:59,960
paso a mi compañero Brais Arias.

21
00:00:59,960 --> 00:01:02,760
Hola buenas, pues yo soy Brais,

22
00:01:02,760 --> 00:01:04,860
también soy socio de KDE

23
00:01:04,860 --> 00:01:07,280
España, yo soy socio raso

24
00:01:07,280 --> 00:01:07,400
desde

25
00:01:07,400 --> 00:01:09,270
hace unos años y bueno estoy

26
00:01:09,270 --> 00:01:11,370
en algunas otras comunidades de

27
00:01:11,370 --> 00:01:13,600
software libre como por ejemplo

28
00:01:13,600 --> 00:01:16,550
la asociación GPUL de Coruña

29
00:01:16,550 --> 00:01:19,460
y además también trabajo como

30
00:01:19,460 --> 00:01:21,640
consultor de software

31
00:01:21,640 --> 00:01:24,260
libre en Icarto y hacemos

32
00:01:24,260 --> 00:01:27,510
también el Mancomun Podcast y

33
00:01:27,510 --> 00:01:30,520
pues soy sobre todo amante del

34
00:01:30,520 --> 00:01:30,880
software

35
00:01:30,880 --> 00:01:34,520
libre y todo lo que puedo hago

36
00:01:34,520 --> 00:01:37,000
difusión y etc.

37
00:01:37,000 --> 00:01:39,340
Y bueno, aparte también me

38
00:01:39,340 --> 00:01:42,170
gusta usar KDE, obviamente, KDE

39
00:01:42,170 --> 00:01:44,760
Plasma. Yo lo uso con Manjaro,

40
00:01:44,760 --> 00:01:46,760
con la distribución Manjaro y

41
00:01:46,760 --> 00:01:48,370
bueno la uso desde uso KDE

42
00:01:48,370 --> 00:01:50,620
desde hace mucho tiempo, salvo

43
00:01:50,620 --> 00:01:52,820
en una franja en la que KDE 4

44
00:01:52,820 --> 00:01:54,850
era muy muy inestable y

45
00:01:54,850 --> 00:01:57,360
entonces en ese momento pues no

46
00:01:57,360 --> 00:02:00,590
la usé demasiado, pero por lo

47
00:02:00,590 --> 00:02:03,360
general estoy súper contento

48
00:02:03,360 --> 00:02:04,520
de usar KDE.

49
00:02:04,520 --> 00:02:07,720
Hola David Marzal.

50
00:02:07,720 --> 00:02:09,510
Bueno pues yo para quien no me

51
00:02:09,510 --> 00:02:11,500
conozca soy administrador de

52
00:02:11,500 --> 00:02:13,600
sistemas, vivo en Cartagena

53
00:02:13,600 --> 00:02:15,730
y colaboro con el podcast de

54
00:02:15,730 --> 00:02:17,780
GNU/Linux Valencia, que

55
00:02:17,780 --> 00:02:20,220
también soy socio y como pues

56
00:02:20,220 --> 00:02:20,600
casi todos

57
00:02:20,600 --> 00:02:23,680
los que estamos aquí soy socio

58
00:02:23,680 --> 00:02:26,510
de KDE España y yo uso KDE en

59
00:02:26,510 --> 00:02:28,480
Arch Linux. A mí me gusta

60
00:02:28,480 --> 00:02:31,100
mucho trastear y meterme en fregar

61
00:02:31,100 --> 00:02:33,190
y uso KDE desde hace no tanto

62
00:02:33,190 --> 00:02:35,240
porque yo soy de los que

63
00:02:35,240 --> 00:02:37,780
estaba a gusto en Unity con el

64
00:02:37,780 --> 00:02:40,490
panel lateral, pero a base de

65
00:02:40,490 --> 00:02:43,360
ir escuchando el vídeo podcast

66
00:02:43,360 --> 00:02:45,730
de KDE España me fueron picando,

67
00:02:45,730 --> 00:02:48,050
me fueron picando y me conquistó

68
00:02:48,050 --> 00:02:49,840
la cantidad de opciones

69
00:02:49,840 --> 00:02:52,620
que tiene Plasma y la comunidad

70
00:02:52,620 --> 00:02:54,400
que tiene KDE.

71
00:02:54,400 --> 00:02:57,360
Genial. Nada, espero que os

72
00:02:57,360 --> 00:02:59,690
guste el podcast. Esto es una

73
00:02:59,690 --> 00:03:02,440
nueva iniciativa, como ya hemos

74
00:03:02,440 --> 00:03:04,050
dicho todos pertenecemos a la

75
00:03:04,050 --> 00:03:05,800
comunidad KDE España y esto es

76
00:03:05,800 --> 00:03:07,200
una iniciativa pues para

77
00:03:07,200 --> 00:03:11,360
dar conocimiento, noticias del

78
00:03:11,360 --> 00:03:14,790
mundo KDE y vamos a intentar

79
00:03:14,790 --> 00:03:18,560
hacerlo con la mayor prioridad,

80
00:03:18,560 --> 00:03:19,560
como se diría...

81
00:03:19,560 --> 00:03:20,560
Capacidad de síntesis.

82
00:03:20,560 --> 00:03:21,560
Sí, sí, sí.

83
00:03:21,560 --> 00:03:22,560
Sobrevedad.

84
00:03:22,560 --> 00:03:26,040
Y nada, intentaremos hacerlos

85
00:03:26,040 --> 00:03:30,400
semanal, cada dos semanas, mensual,

86
00:03:30,400 --> 00:03:32,320
no lo sabemos. Cada

87
00:03:32,320 --> 00:03:34,480
vez que tengamos noticias pues

88
00:03:34,480 --> 00:03:36,380
las iremos sacando y nada,

89
00:03:36,380 --> 00:03:38,640
gracias David, gracias Brais

90
00:03:38,640 --> 00:03:40,620
por este nuevo proyecto que

91
00:03:40,620 --> 00:03:42,600
hemos empezado y espero que

92
00:03:42,600 --> 00:03:44,490
entre los tres, entre los

93
00:03:44,490 --> 00:03:45,080
cuatro

94
00:03:45,080 --> 00:03:49,440
podcasts, hagamos un podcast

95
00:03:49,440 --> 00:03:53,340
que a todo el mundo les guste y

96
00:03:53,340 --> 00:03:57,760
nada, que no nos riña mucho

97
00:03:57,760 --> 00:03:59,360
Norbert Tassar porque luego

98
00:03:59,360 --> 00:04:00,760
escuchará y nos dará su

99
00:04:00,760 --> 00:04:02,290
feedback y ya veremos lo que

100
00:04:02,290 --> 00:04:02,680
nos

101
00:04:02,680 --> 00:04:04,810
dice. Muchas gracias a todos y

102
00:04:04,810 --> 00:04:06,400
hasta luego. Ciao.

103
00:04:06,400 --> 00:04:08,160
Hasta la próxima.

104
00:04:08,160 --> 00:04:09,760
Ciao, nos oímos.

105
00:04:09,760 --> 00:04:15,400
Se dice así en español, ¿verdad?

106
00:04:15,400 --> 00:04:15,880
Nos oímos.

