1
00:00:00,320 --> 00:00:01,950
 Buenas, bienvenido de vuelta a

2
00:00:01,950 --> 00:00:04,080
 KDE Express. Hoy 17 de mayo,

3
00:00:04,080 --> 00:00:05,780
 viernes, estoy grabando el

4
00:00:05,780 --> 00:00:06,620
 episodio 28

5
00:00:06,620 --> 00:00:08,230
 y hoy no te voy a hablar de

6
00:00:08,230 --> 00:00:10,350
 Akademy, creo que hemos hecho

7
00:00:10,350 --> 00:00:11,920
 una buena promoción

8
00:00:11,920 --> 00:00:13,660
 y está la información ahí,

9
00:00:13,660 --> 00:00:15,510
 así que si os interesa estaréis

10
00:00:15,510 --> 00:00:17,530
 ya informados y si no, la tenéis

11
00:00:17,530 --> 00:00:18,300
 disponible.

12
00:00:18,300 --> 00:00:20,310
 Hoy vengo a hacer otro resumen

13
00:00:20,310 --> 00:00:22,320
 de noticias del mundo de KDE en

14
00:00:22,320 --> 00:00:22,980
 general

15
00:00:22,980 --> 00:00:25,950
 y voy a hacer una primera sección

16
00:00:25,950 --> 00:00:29,880
 que es distros, empezando por KDE

17
00:00:29,880 --> 00:00:30,500
 Neon

18
00:00:30,500 --> 00:00:32,510
 que han hecho un postmortem,

19
00:00:32,510 --> 00:00:34,530
 que viene siendo un resumen,

20
00:00:34,530 --> 00:00:36,840
 una review, una retrospectiva

21
00:00:36,840 --> 00:00:39,730
 sobre qué falló en el cambio

22
00:00:39,730 --> 00:00:42,890
 de Plasma 5 a Plasma 6, con el

23
00:00:42,890 --> 00:00:44,760
 framework, los gears

24
00:00:44,760 --> 00:00:46,740
 y dicen que es que hubo un par

25
00:00:46,740 --> 00:00:48,910
 de cosas que no capturaron las

26
00:00:48,910 --> 00:00:51,160
 pruebas continuas que hacen

27
00:00:51,160 --> 00:00:52,960
 y que encima como son pocos...

28
00:00:52,960 --> 00:00:56,960
 y querían fomentar la promoción

29
00:00:56,960 --> 00:01:00,320
 del evento, ya que era un

30
00:01:00,320 --> 00:01:02,000
 cambio muy gordo para la

31
00:01:02,000 --> 00:01:02,620
 comunidad

32
00:01:02,620 --> 00:01:04,340
 pues uno de los principales

33
00:01:04,340 --> 00:01:05,990
 mantenedores de la distro

34
00:01:05,990 --> 00:01:08,080
 estaba por España, en Málaga

35
00:01:08,080 --> 00:01:10,650
 con Paul Brown haciendo el lanzamiento

36
00:01:10,650 --> 00:01:11,560
 de la noticia

37
00:01:11,560 --> 00:01:13,640
 y entonces todo ese tiempo de

38
00:01:13,640 --> 00:01:15,620
 coche o de viaje que tuvo que

39
00:01:15,620 --> 00:01:16,120
 tener

40
00:01:16,120 --> 00:01:18,100
 pues le quitó tiempo de testeo

41
00:01:18,100 --> 00:01:19,830
 y entonces vieron rápidamente

42
00:01:19,830 --> 00:01:21,420
 cuáles eran los problemas

43
00:01:21,420 --> 00:01:23,570
 lo arreglaron, luego siguieron

44
00:01:23,570 --> 00:01:25,180
 arreglando alguna cosa

45
00:01:25,180 --> 00:01:26,590
 pero vamos, tienen un blog en

46
00:01:26,590 --> 00:01:27,940
 el que explican más o menos

47
00:01:27,940 --> 00:01:29,060
 qué es lo que pasó

48
00:01:29,060 --> 00:01:30,470
 y qué han hecho para que no

49
00:01:30,470 --> 00:01:31,280
 vuelva a pasar

50
00:01:31,280 --> 00:01:33,250
 luego también hablando de distros,

51
00:01:33,250 --> 00:01:35,050
 pues tenemos que Garuda, que es

52
00:01:35,050 --> 00:01:36,560
 una distro basada en Ars Linus

53
00:01:36,560 --> 00:01:39,320
 ya ha cogido Plasma 6, también

54
00:01:39,320 --> 00:01:41,800
 tenemos que Tuxedo OS también

55
00:01:41,800 --> 00:01:42,880
 ha dado el salto

56
00:01:42,880 --> 00:01:45,760
 Asahi Remix 40 KDE también

57
00:01:45,760 --> 00:01:47,920
 está en Plasma 6 porque está

58
00:01:47,920 --> 00:01:49,300
 basado en Fedora 40

59
00:01:49,300 --> 00:01:51,300
 y entonces ellos pues han adquirido...

60
00:01:51,300 --> 00:01:52,490
 han adquirido Plasma 6 igual

61
00:01:52,490 --> 00:01:53,560
 que los usuarios de Fedora

62
00:01:53,560 --> 00:01:55,410
 entonces la noticia en sí es

63
00:01:55,410 --> 00:01:57,380
 que ya hay muchas distros que

64
00:01:57,380 --> 00:01:58,940
 han pegado el salto

65
00:01:58,940 --> 00:02:03,360
 no Ubuntu las LTS, no Debian,

66
00:02:03,360 --> 00:02:04,990
 pero sí hay bastantes distros

67
00:02:04,990 --> 00:02:05,480
 por ahí

68
00:02:05,480 --> 00:02:08,100
 como Tumbleweed, que también

69
00:02:08,100 --> 00:02:10,020
 lo tiene, de OpenSUSE

70
00:02:10,020 --> 00:02:12,210
 y os quiero decir una cosa que

71
00:02:12,210 --> 00:02:15,260
 a mí me encanta, que es espectáculo

72
00:02:15,260 --> 00:02:17,930
 que antes era un capturador de

73
00:02:17,930 --> 00:02:20,040
 pantalla, ahora es prácticamente

74
00:02:20,040 --> 00:02:21,060
 un editor de imágenes

75
00:02:21,060 --> 00:02:23,500
 porque tiene tanta funcionalidad

76
00:02:23,500 --> 00:02:25,110
 que yo a veces lo uso como si

77
00:02:25,110 --> 00:02:25,900
 tuviera el Paint

78
00:02:25,900 --> 00:02:27,150
 pues una de las funcionalidades

79
00:02:27,150 --> 00:02:29,030
 que tenía era desenfocar

80
00:02:29,030 --> 00:02:30,280
 algún texto

81
00:02:30,280 --> 00:02:31,550
 por si tú hacías una captura

82
00:02:31,550 --> 00:02:32,890
 de pantalla de una aplicación

83
00:02:32,890 --> 00:02:33,680
 de mensajería

84
00:02:33,680 --> 00:02:35,190
 y no querías que se viera

85
00:02:35,190 --> 00:02:36,040
 algún texto

86
00:02:36,040 --> 00:02:37,950
 pues tenía la opción de desenforcarlo

87
00:02:37,950 --> 00:02:39,550
 directamente para no tener que

88
00:02:39,550 --> 00:02:40,720
 ir tú tachando ahí cutre

89
00:02:40,720 --> 00:02:43,260
 y eso se perdió en el cambio

90
00:02:43,260 --> 00:02:45,680
 de Plasma 6 y ya se ha recuperado

91
00:02:45,680 --> 00:02:47,620
 en la versión 24.05

92
00:02:47,620 --> 00:02:50,230
 también tiene la opción de

93
00:02:50,230 --> 00:02:51,020
 pixelar

94
00:02:51,020 --> 00:02:52,910
 y también tiene opciones de ir

95
00:02:52,910 --> 00:02:54,060
 marcando numeritos

96
00:02:54,060 --> 00:02:55,420
 y te va poniendo el solo del círculo

97
00:02:55,420 --> 00:02:56,640
 con el número por si quieres

98
00:02:56,640 --> 00:02:57,420
 hacer un tutorial

99
00:02:57,420 --> 00:02:58,940
 la verdad es que es una aplicación

100
00:02:58,940 --> 00:02:59,920
 que yo lo uso mucho en el

101
00:02:59,920 --> 00:03:00,380
 trabajo

102
00:03:00,380 --> 00:03:01,860
 para explicar cosas o hacer

103
00:03:01,860 --> 00:03:03,520
 tutoriales y es una maravilla

104
00:03:03,520 --> 00:03:05,720
 fotos, vídeos, te captura lo

105
00:03:05,720 --> 00:03:07,920
 que quieras, te deja editarlo

106
00:03:07,920 --> 00:03:09,960
 completamente recomendable

107
00:03:09,960 --> 00:03:12,080
 luego, hablando de recomendar,

108
00:03:12,080 --> 00:03:13,820
 me he enterado de que digiKam

109
00:03:13,820 --> 00:03:17,060
 tiene un libro electrónico que

110
00:03:17,060 --> 00:03:19,540
 se llama Recipes, Recetas

111
00:03:19,540 --> 00:03:22,280
 que está en oferta y que

112
00:03:22,280 --> 00:03:23,800
 además no tiene DRM

113
00:03:23,800 --> 00:03:25,660
 así que si soy usuario de

114
00:03:25,660 --> 00:03:27,320
 hacer ritmos de digiKam

115
00:03:27,320 --> 00:03:28,920
 yo soy usuario básico todavía

116
00:03:28,920 --> 00:03:31,160
 pues puede ser una buena adquisición

117
00:03:31,160 --> 00:03:33,280
 ayudáis al proyecto y tenéis

118
00:03:33,280 --> 00:03:35,120
 ahí un montón de funcionalidad

119
00:03:35,120 --> 00:03:36,040
 bien explicada

120
00:03:36,040 --> 00:03:38,460
 luego también contaros que en

121
00:03:38,460 --> 00:03:41,420
 abril fue el KDE Goal Spring

122
00:03:41,420 --> 00:03:43,780
 que viene siendo como una quedada

123
00:03:43,780 --> 00:03:45,260
 de desarrolladores

124
00:03:45,260 --> 00:03:46,530
 en la que se centran en

125
00:03:46,530 --> 00:03:47,880
 trabajar algo juntos

126
00:03:47,880 --> 00:03:48,920
 para poder acelerar

127
00:03:48,920 --> 00:03:50,500
 acelerar el desarrollo

128
00:03:50,500 --> 00:03:52,540
 porque cuando estás en remoto

129
00:03:52,540 --> 00:03:53,580
 se pueden hacer muchas cosas

130
00:03:53,580 --> 00:03:54,880
 pero siempre cuando te juntas

131
00:03:54,880 --> 00:03:56,580
 se aceleran y las cosas van

132
00:03:56,580 --> 00:03:56,860
 mejor

133
00:03:56,860 --> 00:03:58,850
 y entonces ahí se enfocaron en

134
00:03:58,850 --> 00:04:00,870
 sostenibilidad, accesibilidad y

135
00:04:00,870 --> 00:04:01,960
 automatización

136
00:04:01,960 --> 00:04:04,090
 lo cual está muy bien porque

137
00:04:04,090 --> 00:04:07,180
 son tres de mis temas fetiches

138
00:04:07,180 --> 00:04:08,560
 luego una de las cosas que yo

139
00:04:08,560 --> 00:04:10,090
 estaba esperando desde hace

140
00:04:10,090 --> 00:04:10,840
 mucho tiempo

141
00:04:10,840 --> 00:04:13,100
 a nivel profesional o empresarial

142
00:04:13,100 --> 00:04:14,220
 por así decirlo

143
00:04:14,220 --> 00:04:15,880
 es el tema del escritorio remoto

144
00:04:15,880 --> 00:04:18,700
 y parece ser que en Plasma 6.1

145
00:04:18,700 --> 00:04:20,940
 se va a facilitar el configurar

146
00:04:20,940 --> 00:04:22,000
 esto directamente

147
00:04:22,000 --> 00:04:24,160
 desde las opciones del sistema

148
00:04:24,160 --> 00:04:26,890
 en Wayland siempre ha sido un

149
00:04:26,890 --> 00:04:28,980
 poco más complicado

150
00:04:28,980 --> 00:04:31,100
 un bastante más complicado

151
00:04:31,100 --> 00:04:33,620
 configurar el escritorio remoto

152
00:04:33,620 --> 00:04:35,440
 de hecho no es posible en

153
00:04:35,440 --> 00:04:36,660
 muchas distros

154
00:04:36,660 --> 00:04:37,780
 ni con muchas aplicaciones

155
00:04:37,780 --> 00:04:39,860
 pero parece ser que ya hay maneras

156
00:04:39,860 --> 00:04:40,600
 de hacerlo

157
00:04:40,600 --> 00:04:43,820
 y KDE lo va a poner al alcance

158
00:04:43,820 --> 00:04:44,880
 de dos o tres clics

159
00:04:44,880 --> 00:04:46,440
 estoy seguro que OpenSUSE en

160
00:04:46,440 --> 00:04:46,740
 Yast

161
00:04:46,740 --> 00:04:48,150
 tiene un par de botones para

162
00:04:48,150 --> 00:04:48,620
 hacerlo

163
00:04:48,620 --> 00:04:50,420
 pero en Arch yo no lo había

164
00:04:50,420 --> 00:04:51,400
 encontrado tan fácil

165
00:04:51,400 --> 00:04:53,670
 y en KDE Neon seguro que

166
00:04:53,670 --> 00:04:55,160
 también está sencillo

167
00:04:55,160 --> 00:04:57,700
 si lo están haciendo los de KDE

168
00:04:57,700 --> 00:04:59,780
 y luego también tenemos que en

169
00:04:59,780 --> 00:05:02,800
 la versión 24.08 de Dolphin

170
00:05:02,800 --> 00:05:04,390
 os va a dejar seleccionar todos

171
00:05:04,390 --> 00:05:05,100
 los ficheros

172
00:05:05,100 --> 00:05:07,450
 o otras opciones que estamos en

173
00:05:07,450 --> 00:05:07,760
 KDE

174
00:05:07,760 --> 00:05:09,520
 que tú podrás configurar en

175
00:05:09,520 --> 00:05:10,700
 la configuración

176
00:05:10,700 --> 00:05:12,870
 cuando hablas doble clic en un

177
00:05:12,870 --> 00:05:14,600
 hueco donde no haya ningún fichero

178
00:05:14,600 --> 00:05:16,240
 tú abres Dolphin, tendrás el

179
00:05:16,240 --> 00:05:17,140
 listado de ficheros

180
00:05:17,140 --> 00:05:18,190
 en alguno de los huecos haces

181
00:05:18,190 --> 00:05:18,600
 doble clic

182
00:05:18,600 --> 00:05:20,160
 y por defecto te va a seleccionar

183
00:05:20,160 --> 00:05:21,040
 todos los ficheros que hay en

184
00:05:21,040 --> 00:05:21,680
 esa carpeta

185
00:05:21,680 --> 00:05:23,140
 tú podrás cambiar que cuando

186
00:05:23,140 --> 00:05:23,880
 hagas doble clic

187
00:05:23,880 --> 00:05:25,060
 sean otras acciones las que se

188
00:05:25,060 --> 00:05:25,520
 realicen

189
00:05:25,520 --> 00:05:27,880
 lo cual está bastante bien

190
00:05:27,880 --> 00:05:29,280
 y luego yo había pensado en

191
00:05:29,280 --> 00:05:30,720
 hacer un episodio especial de

192
00:05:30,720 --> 00:05:31,960
 atajos de teclado

193
00:05:31,960 --> 00:05:33,180
 porque la verdad es que hay un

194
00:05:33,180 --> 00:05:34,840
 montón y es súper configurable

195
00:05:34,840 --> 00:05:36,050
 y es súper bueno para la

196
00:05:36,050 --> 00:05:36,880
 productividad

197
00:05:36,880 --> 00:05:38,020
 pero he pensado que lo mismo es

198
00:05:38,020 --> 00:05:39,160
 un poco rollo todo de golpe en

199
00:05:39,160 --> 00:05:40,160
 un podcast sin verlo

200
00:05:40,160 --> 00:05:41,920
 y puede quedar mejor si lo hago

201
00:05:41,920 --> 00:05:42,680
 de uno en uno

202
00:05:42,680 --> 00:05:45,170
 entonces voy a terminar el

203
00:05:45,170 --> 00:05:45,800
 episodio

204
00:05:45,800 --> 00:05:47,720
 diciendo que uno de los que

205
00:05:47,720 --> 00:05:48,560
 más uso yo

206
00:05:48,560 --> 00:05:49,560
 que es con la tecla meta

207
00:05:49,560 --> 00:05:51,720
 en mi teclado es un pingüino

208
00:05:51,720 --> 00:05:53,580
 con la palabra Slimbook

209
00:05:53,580 --> 00:05:54,880
 pero en muchos teclados será

210
00:05:54,880 --> 00:05:55,780
 la tecla de Windows

211
00:05:55,780 --> 00:05:58,560
 si haces meta y la E se te abre

212
00:05:58,560 --> 00:05:59,120
 Dolphin

213
00:05:59,120 --> 00:06:00,580
 en Windows yo creo que se abriría

214
00:06:00,580 --> 00:06:01,800
 el explorador de ficheros

215
00:06:01,800 --> 00:06:03,150
 pero la verdad es que a mí es

216
00:06:03,150 --> 00:06:04,870
 una de las que Dolphin lo usa

217
00:06:04,870 --> 00:06:05,460
 un montón

218
00:06:05,460 --> 00:06:06,880
 es una herramienta suiza

219
00:06:06,880 --> 00:06:09,850
 y casi siempre lo abro con meta

220
00:06:09,850 --> 00:06:09,960
 E

221
00:06:09,960 --> 00:06:11,180
 si vosotros tenéis alguno que

222
00:06:11,180 --> 00:06:12,500
 usáis un montón y sea súper

223
00:06:12,500 --> 00:06:12,760
 útil

224
00:06:12,760 --> 00:06:14,430
 ponerlo por Mastodon porque a

225
00:06:14,430 --> 00:06:16,140
 lo mejor o seguramente

226
00:06:16,140 --> 00:06:17,720
 habrá muchos que no conozcamos

227
00:06:17,720 --> 00:06:18,540
 pues con esto

228
00:06:18,540 --> 00:06:20,530
 y dándole de nuevo las gracias

229
00:06:20,530 --> 00:06:23,540
 a esLibre por ayudarnos a

230
00:06:23,540 --> 00:06:24,540
 organizar el Akademy

231
00:06:24,540 --> 00:06:26,700
 y por supuesto a Openshusa

232
00:06:26,700 --> 00:06:28,780
 que es como se pronuncia Openshusa

233
00:06:28,780 --> 00:06:32,350
 la distro con mejores vídeos

234
00:06:32,350 --> 00:06:34,200
 musicales de promoción

235
00:06:34,200 --> 00:06:36,960
 de todo internet

236
00:06:36,960 --> 00:06:38,560
 y la Universidad de La Laguna

237
00:06:38,560 --> 00:06:39,520
 en Canarias

238
00:06:39,520 --> 00:06:41,210
 pues con esos agradecimientos

239
00:06:41,210 --> 00:06:42,260
 termino el episodio

240
00:06:42,260 --> 00:06:44,350
 y espero veros dentro de poco

241
00:06:44,350 --> 00:06:46,160
 por Akademy en Valencia

242
00:06:46,160 --> 00:06:46,820
 hasta luego

