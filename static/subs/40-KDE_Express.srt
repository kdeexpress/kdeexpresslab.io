1
00:00:00,980 --> 00:00:01,880
 Buenas, bienvenidos a KD

2
00:00:01,880 --> 00:00:04,920
 Express, episodio 40. Buena cifra,

3
00:00:04,920 --> 00:00:06,280
 no me ha dado tiempo a hacer

4
00:00:06,280 --> 00:00:08,140
 nada especial de celebración

5
00:00:08,140 --> 00:00:09,340
 más que poder grabar un episodio

6
00:00:09,340 --> 00:00:10,520
 de los normales de noticias,

7
00:00:10,520 --> 00:00:11,820
 pero no es poco, entramos en la

8
00:00:11,820 --> 00:00:13,000
 cuarentena de la buena.

9
00:00:13,000 --> 00:00:14,520
 Hoy es 7 de febrero cuando

10
00:00:14,520 --> 00:00:16,220
 estoy grabando y os traigo, no

11
00:00:16,220 --> 00:00:18,000
 un episodio especial, pero sí

12
00:00:18,000 --> 00:00:19,840
 la verdad es que las tres primeras

13
00:00:19,840 --> 00:00:21,560
 noticias son dignas del señor

14
00:00:21,560 --> 00:00:23,850
 Miyaki, porque esto es cera pura,

15
00:00:23,850 --> 00:00:25,360
 la que nos están dando en

16
00:00:25,360 --> 00:00:27,260
 internet, todo para bien.

17
00:00:27,260 --> 00:00:29,360
 Así que comienzo con tres que

18
00:00:29,360 --> 00:00:31,780
 da mucho gusto de escuchar. La

19
00:00:31,780 --> 00:00:33,380
 primera es que el escritorio

20
00:00:33,380 --> 00:00:35,120
 más popular en WeLinux es KD

21
00:00:35,120 --> 00:00:37,680
 Plasma. Estamos justo por delante

22
00:00:37,680 --> 00:00:40,380
 de Gnome, de Cinnamon, de XFCE,

23
00:00:40,380 --> 00:00:42,340
 de Mate y ya una ristra.

24
00:00:42,340 --> 00:00:43,900
 Si es verdad que estamos un

25
00:00:43,900 --> 00:00:45,900
 poquito por delante, 31,7

26
00:00:45,900 --> 00:00:47,920
 contra 31,4, pero oye,

27
00:00:47,920 --> 00:00:49,310
 significa que las cosas se

28
00:00:49,310 --> 00:00:50,830
 están haciendo bien en la

29
00:00:50,830 --> 00:00:52,700
 comunidad y la gente lo ve.

30
00:00:52,700 --> 00:00:54,440
 Esto ya sabéis que es una encuesta

31
00:00:54,440 --> 00:00:55,940
 que hacen todos los años en la

32
00:00:55,940 --> 00:00:57,500
 que preguntan por tu navegador

33
00:00:57,500 --> 00:00:59,360
 preferido, por tu distro preferida

34
00:00:59,360 --> 00:01:00,660
 y este tipo de cosas.

35
00:01:00,660 --> 00:01:02,800
 No solo eso, sino que en Arch

36
00:01:02,800 --> 00:01:04,740
 Linux también han hecho una

37
00:01:04,740 --> 00:01:06,840
 encuesta parecida y en Arch

38
00:01:06,840 --> 00:01:09,130
 Linux todavía salimos mejor,

39
00:01:09,130 --> 00:01:11,460
 porque tenemos que KDE Plasma

40
00:01:11,460 --> 00:01:15,010
 sale elegido con el 43% de los

41
00:01:15,010 --> 00:01:15,720
 votos.

42
00:01:15,720 --> 00:01:17,710
 El siguiente sería Gnome con

43
00:01:17,710 --> 00:01:20,370
 el 21,5 y el siguiente es Hyperlan

44
00:01:20,370 --> 00:01:22,580
 con 13,5, que ese no me lo

45
00:01:22,580 --> 00:01:23,420
 esperaba.

46
00:01:23,420 --> 00:01:24,930
 Si es verdad que esto es una

47
00:01:24,930 --> 00:01:26,770
 encuesta de gente que ha decidido

48
00:01:26,770 --> 00:01:28,570
 rellenarla y que a mí me gustan

49
00:01:28,570 --> 00:01:30,230
 más las estadísticas que te

50
00:01:30,230 --> 00:01:31,670
 dan en Arch Linux que es del

51
00:01:31,670 --> 00:01:32,340
 uso.

52
00:01:32,860 --> 00:01:34,340
 Tú puedes, la página también

53
00:01:34,340 --> 00:01:35,850
 está asesgada porque es instalar

54
00:01:35,850 --> 00:01:37,100
 un paquete que reporta lo que

55
00:01:37,100 --> 00:01:38,360
 tú tienes instalado.

56
00:01:38,360 --> 00:01:39,900
 Entonces ellos tienen una

57
00:01:39,900 --> 00:01:41,650
 página web para saber si usan

58
00:01:41,650 --> 00:01:43,050
 Bing o Nano o Emacs, por

59
00:01:43,050 --> 00:01:43,900
 supuesto.

60
00:01:43,900 --> 00:01:45,210
 Total, que tú puedes ver los

61
00:01:45,210 --> 00:01:46,490
 paquetes más populares de la

62
00:01:46,490 --> 00:01:47,420
 distribución.

63
00:01:47,420 --> 00:01:49,420
 Allí también gana KDE Plasma,

64
00:01:49,420 --> 00:01:51,350
 pero según recuerdo no era por

65
00:01:51,350 --> 00:01:52,060
 tanto.

66
00:01:52,060 --> 00:01:53,950
 Pero bueno, en cualquier caso,

67
00:01:53,950 --> 00:01:55,640
 que tanto la comunidad de Arch

68
00:01:55,640 --> 00:01:57,250
 Linux como la de Muy Linux han

69
00:01:57,250 --> 00:01:58,690
 votado a KDE Plasma como a la

70
00:01:58,690 --> 00:02:00,160
 que más les gusta.

71
00:02:00,160 --> 00:02:01,270
 Yo, por cierto, no voté

72
00:02:01,270 --> 00:02:02,880
 ninguna de las dos, pero fíjate,

73
00:02:02,880 --> 00:02:03,660
 ahí estamos.

74
00:02:03,660 --> 00:02:06,690
 Y luego, para cerrar la triología

75
00:02:06,690 --> 00:02:09,310
 de noticias de Darnos Cera, es

76
00:02:09,310 --> 00:02:11,060
 una que dice...

77
00:02:11,060 --> 00:02:12,410
 ¿Por qué KDE Plasma sigue

78
00:02:12,410 --> 00:02:14,080
 liderando como la mejor noción

79
00:02:14,080 --> 00:02:15,120
 de escritorio?

80
00:02:15,120 --> 00:02:16,730
 Que ya solo el titular está

81
00:02:16,730 --> 00:02:17,800
 bastante bien.

82
00:02:17,800 --> 00:02:19,800
 Entonces, en Laboratorio Linux

83
00:02:19,800 --> 00:02:21,620
 hacen esto más que...

84
00:02:21,620 --> 00:02:22,790
 Esto ya no es una encuesta,

85
00:02:22,790 --> 00:02:24,010
 esto es un artículo en el que

86
00:02:24,010 --> 00:02:25,740
 detallan que ellos piensan que

87
00:02:25,740 --> 00:02:27,520
 gracias a la personalización

88
00:02:27,520 --> 00:02:28,320
 sin límites,

89
00:02:28,320 --> 00:02:30,190
 como los widgets y los paneles

90
00:02:30,190 --> 00:02:32,530
 personalizables, el rendimiento

91
00:02:32,530 --> 00:02:34,290
 y la eficacia, la optimización

92
00:02:34,290 --> 00:02:34,880
 de recursos,

93
00:02:34,880 --> 00:02:37,160
 la innovación constante, Wylan,

94
00:02:37,160 --> 00:02:39,160
 el High DPI, esto de cuando

95
00:02:39,160 --> 00:02:41,280
 tienes pantallas con mucha

96
00:02:41,280 --> 00:02:42,620
 resolución,

97
00:02:42,620 --> 00:02:44,140
 aplicaciones de calidad como

98
00:02:44,140 --> 00:02:45,950
 Dolphin, Krita, Oculus, Arcade

99
00:02:45,950 --> 00:02:47,790
 en Live, la comunidad activa y

100
00:02:47,790 --> 00:02:48,460
 su apoyo,

101
00:02:48,460 --> 00:02:50,130
 un proyecto comunitario y que

102
00:02:50,130 --> 00:02:52,020
 es ideal para usuarios avanzados

103
00:02:52,020 --> 00:02:53,690
 y principiantes, os he dado los

104
00:02:53,690 --> 00:02:54,300
 titulares.

105
00:02:54,300 --> 00:02:55,500
 Os dejo para que os podáis

106
00:02:55,500 --> 00:02:56,800
 leer el artículo entero y no

107
00:02:56,800 --> 00:02:57,700
 lapidárselo.

108
00:02:57,700 --> 00:02:59,480
 La cosa es que entre episodios

109
00:02:59,480 --> 00:03:01,210
 que yo he grabado han salido

110
00:03:01,210 --> 00:03:02,560
 estas tres noticias,

111
00:03:03,340 --> 00:03:04,920
 con lo cual el sentimiento en

112
00:03:04,920 --> 00:03:06,500
 la comunidad es que va la cosa

113
00:03:06,500 --> 00:03:08,260
 bien y que el proyecto se mueve

114
00:03:08,260 --> 00:03:09,040
 en buena dirección.

115
00:03:09,040 --> 00:03:10,860
 Eso era en cuanto a KDE Plasma

116
00:03:10,860 --> 00:03:12,760
 en general y el proyecto KDE de

117
00:03:12,760 --> 00:03:13,720
 la nocera.

118
00:03:13,720 --> 00:03:15,300
 Ahora ya vengo con las noticias

119
00:03:15,300 --> 00:03:16,800
 habituales de esto es lo que se

120
00:03:16,800 --> 00:03:18,200
 va a implementar o estas son

121
00:03:18,200 --> 00:03:19,240
 las novedades.

122
00:03:19,240 --> 00:03:21,880
 Entonces tenemos que Plasma 6.3,

123
00:03:21,880 --> 00:03:23,840
 que le queda poco para venir,

124
00:03:23,840 --> 00:03:25,460
 al llegar las notificaciones

125
00:03:25,460 --> 00:03:26,930
 y si tenemos el no molestar

126
00:03:26,930 --> 00:03:28,580
 activado, pues como yo ahora

127
00:03:28,580 --> 00:03:30,580
 mismo que estoy grabando esto,

128
00:03:30,580 --> 00:03:32,310
 cuando se vuelva al modo normal

129
00:03:32,310 --> 00:03:34,180
 habrá una única notificación

130
00:03:34,180 --> 00:03:36,140
 y su número, en vez de una cascada

131
00:03:36,140 --> 00:03:36,820
 de notificaciones.

132
00:03:36,820 --> 00:03:37,920
 Porque lo que pasa es que yo

133
00:03:37,920 --> 00:03:39,270
 ahora mismo cuando desactive el

134
00:03:39,270 --> 00:03:39,920
 no molestar,

135
00:03:39,920 --> 00:03:42,760
 si me han llamado 5 o 20 notificaciones

136
00:03:42,760 --> 00:03:44,620
 me van a salir 5 cuadraditos

137
00:03:44,620 --> 00:03:46,280
 ahí abajo a la derecha con

138
00:03:46,280 --> 00:03:47,720
 cada una de las notificaciones.

139
00:03:47,720 --> 00:03:48,820
 Y a veces la verdad es que era

140
00:03:48,820 --> 00:03:49,860
 una locura porque no te daba

141
00:03:49,860 --> 00:03:50,860
 tiempo a leer ninguna

142
00:03:50,860 --> 00:03:52,110
 y es más sencillo que te salga

143
00:03:52,110 --> 00:03:53,360
 una, tú pinches y puedas ver

144
00:03:53,360 --> 00:03:54,420
 las que te interesen.

145
00:03:54,420 --> 00:03:56,190
 Luego también tenemos que los

146
00:03:56,190 --> 00:03:58,500
 iconos que sean de enlaces simbólicos,

147
00:03:58,500 --> 00:03:59,920
 ahora tendrán una opción que

148
00:03:59,920 --> 00:04:01,290
 con el botón derecho en el menú

149
00:04:01,290 --> 00:04:03,300
 contextual podrás ver a dónde

150
00:04:03,300 --> 00:04:03,900
 apuntan

151
00:04:03,900 --> 00:04:06,700
 para saber exactamente ese fichero,

152
00:04:06,700 --> 00:04:08,310
 que en realidad es un enlace

153
00:04:08,310 --> 00:04:09,550
 simbólico que puede ser un

154
00:04:09,550 --> 00:04:10,520
 directorio o un fichero,

155
00:04:10,520 --> 00:04:12,040
 en Linux todos son ficheros,

156
00:04:12,040 --> 00:04:13,340
 vas a ver su destino.

157
00:04:13,340 --> 00:04:15,830
 Y luego también podrás elegir

158
00:04:15,830 --> 00:04:18,150
 en el menú inicio si quieres

159
00:04:18,150 --> 00:04:20,040
 iconos simbólicos o volver a

160
00:04:20,040 --> 00:04:20,900
 los iconos normales,

161
00:04:20,900 --> 00:04:22,610
 lo cual puedes seleccionar a

162
00:04:22,610 --> 00:04:24,480
 través del editor del menú.

163
00:04:24,480 --> 00:04:26,360
 Esto es que están haciendo un

164
00:04:26,360 --> 00:04:28,400
 cambio en diseño y entonces

165
00:04:28,400 --> 00:04:30,570
 van a ver si se puede ser más

166
00:04:30,570 --> 00:04:31,500
 accesible

167
00:04:31,500 --> 00:04:33,100
 o más directo a la hora de

168
00:04:33,100 --> 00:04:34,900
 decir qué es cada cosa.

169
00:04:34,900 --> 00:04:36,600
 Entonces, como siempre en KDE,

170
00:04:36,600 --> 00:04:38,130
 intentamos mejorar las cosas

171
00:04:38,130 --> 00:04:39,580
 pero damos opciones por si no

172
00:04:39,580 --> 00:04:40,360
 te gustan.

173
00:04:40,360 --> 00:04:42,820
 Luego, fuera de Plasma, tenemos

174
00:04:42,820 --> 00:04:45,780
 Gias 2504 y lo que he querido,

175
00:04:45,780 --> 00:04:46,910
 hay un montón de cambios, por

176
00:04:46,910 --> 00:04:47,340
 supuesto,

177
00:04:47,340 --> 00:04:49,370
 pero yo el que quiero destacar

178
00:04:49,370 --> 00:04:51,140
 es uno que a mí me va a venir

179
00:04:51,140 --> 00:04:52,840
 muy bien, que es Endorphin.

180
00:04:52,840 --> 00:04:54,380
 Si tú tienes un nombre súper,

181
00:04:54,380 --> 00:04:55,580
 súper largo y no cabe en la

182
00:04:55,580 --> 00:04:57,480
 pantalla, ahora mismo lo que

183
00:04:57,480 --> 00:04:59,220
 pasa es que se recorta

184
00:04:59,220 --> 00:05:00,710
 y aparecen tres puntitos. Pero

185
00:05:00,710 --> 00:05:02,550
 ¿qué pasa? Se recorta por un

186
00:05:02,550 --> 00:05:04,060
 lado y entonces a veces no

187
00:05:04,060 --> 00:05:05,020
 tienes ni idea

188
00:05:05,020 --> 00:05:06,490
 de qué va el fichero por cómo

189
00:05:06,490 --> 00:05:08,410
 está recortado. Ahora lo que

190
00:05:08,410 --> 00:05:09,660
 van a hacer es que se va a

191
00:05:09,660 --> 00:05:10,200
 mostrar

192
00:05:10,200 --> 00:05:11,470
 el principio del fichero y el

193
00:05:11,470 --> 00:05:12,680
 final del fichero y lo que se

194
00:05:12,680 --> 00:05:14,240
 va a recortar, los tres puntitos

195
00:05:14,240 --> 00:05:15,120
 van a ir en el medio.

196
00:05:15,120 --> 00:05:17,420
 Con lo cual, tanto la comunidad

197
00:05:17,420 --> 00:05:19,670
 de desarrollo de DKDE como yo

198
00:05:19,670 --> 00:05:22,200
 pensamos que va a ser más descifrable

199
00:05:22,200 --> 00:05:24,000
 qué es lo que va a salir en

200
00:05:24,000 --> 00:05:26,250
 ese fichero. Y luego Scrooge,

201
00:05:26,250 --> 00:05:28,580
 no sé si lo habré dicho bien,

202
00:05:28,580 --> 00:05:30,070
 es una aplicación que yo no

203
00:05:30,070 --> 00:05:31,570
 tenía fichada de KDE, que es

204
00:05:31,570 --> 00:05:33,140
 un administrador de finanzas

205
00:05:33,140 --> 00:05:33,980
 personal.

206
00:05:33,980 --> 00:05:35,760
 Pues la han portado a Qt6, que

207
00:05:35,760 --> 00:05:37,150
 es una de las pocas que van

208
00:05:37,150 --> 00:05:38,990
 quedando que todavía están en

209
00:05:38,990 --> 00:05:39,740
 Qt5.

210
00:05:40,200 --> 00:05:42,830
 Con lo cual, arreglamos fallos,

211
00:05:42,830 --> 00:05:45,560
 metemos mejoras y vamos terminando

212
00:05:45,560 --> 00:05:47,580
 de cimentar las que nos quedaban.

213
00:05:47,580 --> 00:05:49,370
 La de Scrooge, creo que eso ya

214
00:05:49,370 --> 00:05:51,830
 está, que fue en el 2501, que

215
00:05:51,830 --> 00:05:53,380
 fue fuera del ciclo.

216
00:05:53,380 --> 00:05:54,790
 La de Dolphin, sí que será

217
00:05:54,790 --> 00:05:56,900
 cuando salga el 2504, que será

218
00:05:56,900 --> 00:05:58,540
 en abrir como marca el nombre.

219
00:05:58,540 --> 00:06:00,400
 Dentro del entorno KDE, pero

220
00:06:00,400 --> 00:06:02,110
 que también lo han lanzado

221
00:06:02,110 --> 00:06:04,020
 fuera de lo que es el paraguas

222
00:06:04,020 --> 00:06:04,900
 de todos a la vez,

223
00:06:04,960 --> 00:06:07,040
 de Gears, tenemos que Jekon

224
00:06:07,040 --> 00:06:09,670
 Priest tiene el 25 aniversario

225
00:06:09,670 --> 00:06:11,580
 y la versión 25.

226
00:06:11,580 --> 00:06:13,410
 Han metido cinco nuevas actividades.

227
00:06:13,410 --> 00:06:15,800
 Una es de dibujo libre, otra es

228
00:06:15,800 --> 00:06:18,500
 calcular el complemento a 10,

229
00:06:18,500 --> 00:06:20,560
 suma vertical y dos variantes

230
00:06:20,560 --> 00:06:22,080
 de recta vertical.

231
00:06:22,080 --> 00:06:24,190
 O sea, si ya tenía tropecientas

232
00:06:24,190 --> 00:06:26,140
 utilidades y opciones para los

233
00:06:26,140 --> 00:06:27,990
 críos pequeños, pues ahora

234
00:06:27,990 --> 00:06:28,840
 hay cinco meses.

235
00:06:29,200 --> 00:06:31,210
 Y luego, una noticia que es de

236
00:06:31,210 --> 00:06:33,480
 software libre en general, pero

237
00:06:33,480 --> 00:06:35,460
 que aplica al entorno KDE,

238
00:06:35,460 --> 00:06:37,070
 es que Flatpak, lo que es la

239
00:06:37,070 --> 00:06:38,960
 aplicación en sí, ha lanzado

240
00:06:38,960 --> 00:06:40,510
 una mayor versión, una

241
00:06:40,510 --> 00:06:42,320
 versión gorda en español,

242
00:06:42,320 --> 00:06:44,460
 tras dos años. Tiene muchas

243
00:06:44,460 --> 00:06:46,760
 mejoras, como que hace la limpieza

244
00:06:46,760 --> 00:06:48,790
 automática de frameworks que

245
00:06:48,790 --> 00:06:49,480
 no te hacen falta,

246
00:06:49,480 --> 00:06:50,900
 los runtime que se instalan

247
00:06:50,900 --> 00:06:52,490
 porque son dependencias, que

248
00:06:52,490 --> 00:06:54,420
 mucha gente se queja de que...

249
00:06:54,420 --> 00:06:55,700
 Es que tenemos un montón de

250
00:06:55,700 --> 00:06:57,460
 dependencias. Necesitas las

251
00:06:57,460 --> 00:06:59,260
 dependencias para funcionar

252
00:06:59,260 --> 00:07:00,360
 y esas dependencias que se te

253
00:07:00,360 --> 00:07:01,550
 instalan son para que no estén

254
00:07:01,550 --> 00:07:02,780
 dentro de esa aplicación.

255
00:07:02,780 --> 00:07:04,820
 Si no, estarían siete veces en

256
00:07:04,820 --> 00:07:06,840
 cada aplicación. Cuando tienes

257
00:07:06,840 --> 00:07:08,110
 suficientes aplicaciones que

258
00:07:08,110 --> 00:07:08,940
 tiras de la misma,

259
00:07:08,940 --> 00:07:10,810
 ahí estás ahorrando. Lo bueno

260
00:07:10,810 --> 00:07:12,020
 es que los SSD y los NVMe

261
00:07:12,020 --> 00:07:13,120
 están barates.

262
00:07:13,120 --> 00:07:14,640
 La cosa es que lleva mucho

263
00:07:14,640 --> 00:07:16,700
 tiempo metiéndole mejoras, lo

264
00:07:16,700 --> 00:07:18,380
 hacen con eso de que las impares

265
00:07:18,380 --> 00:07:19,420
 son versiones de prueba

266
00:07:19,420 --> 00:07:20,890
 y las pares son versiones ya

267
00:07:20,890 --> 00:07:22,410
 estables. Entonces yo no me

268
00:07:22,410 --> 00:07:23,480
 había dado cuenta que hacía

269
00:07:23,480 --> 00:07:24,200
 tanto tiempo

270
00:07:24,200 --> 00:07:25,150
 que no habían sacado una nueva

271
00:07:25,150 --> 00:07:26,730
 versión. Entonces, os podéis

272
00:07:26,730 --> 00:07:28,060
 imaginar que hay un montón de

273
00:07:28,060 --> 00:07:28,800
 novedades.

274
00:07:28,800 --> 00:07:30,410
 A mí una de las que más me ha

275
00:07:30,410 --> 00:07:32,090
 gustado es esa y que ahora va a

276
00:07:32,090 --> 00:07:34,840
 tratar mejor los runtime que

277
00:07:34,840 --> 00:07:35,980
 están obsoletos,

278
00:07:35,980 --> 00:07:37,510
 los EOL, te lo va a indicar y

279
00:07:37,510 --> 00:07:39,200
 va a intentar actualizarte al

280
00:07:39,200 --> 00:07:39,880
 nuevo.

281
00:07:39,880 --> 00:07:41,800
 Mejoras por debajo del motor

282
00:07:41,800 --> 00:07:43,550
 para hacer una mejor

283
00:07:43,550 --> 00:07:45,550
 experiencia de este instalador

284
00:07:45,550 --> 00:07:47,060
 de contenedores.

285
00:07:47,480 --> 00:07:49,770
 También tenemos noticias de F-Droid.

286
00:07:49,770 --> 00:07:51,580
 Una es que PeerTube ya tiene su

287
00:07:51,580 --> 00:07:52,860
 aplicación disponible.

288
00:07:52,860 --> 00:07:54,310
 Con lo cual, a ver que me la

289
00:07:54,310 --> 00:07:56,160
 voy a poner por aquí...

290
00:07:56,160 --> 00:07:57,600
 Ahora mismo lo que te permite

291
00:07:57,600 --> 00:07:59,100
 es ver vídeos y suscribirte a

292
00:07:59,100 --> 00:07:59,920
 tus canales.

293
00:07:59,920 --> 00:08:02,370
 Puedes explorar la federación,

294
00:08:02,370 --> 00:08:04,480
 con lo cual puedes ver vídeos

295
00:08:04,480 --> 00:08:07,140
 o creadores de diferentes distancias

296
00:08:07,140 --> 00:08:08,680
 y por supuesto te va a dejar

297
00:08:08,680 --> 00:08:09,120
 buscar.

298
00:08:09,120 --> 00:08:10,560
 Con lo cual puedes buscar tu

299
00:08:10,560 --> 00:08:11,940
 vídeo sin tener que ir a la

300
00:08:11,940 --> 00:08:13,200
 aplicación de Sepia,

301
00:08:13,200 --> 00:08:14,330
 que era el buscador oficial que

302
00:08:14,330 --> 00:08:15,250
 más o menos te buscaba en

303
00:08:15,250 --> 00:08:15,680
 todas.

304
00:08:15,740 --> 00:08:17,140
 Y luego lo que están programando,

305
00:08:17,140 --> 00:08:18,040
 pero todavía no está

306
00:08:18,040 --> 00:08:18,780
 implementado,

307
00:08:18,780 --> 00:08:20,080
 son descargas offline para

308
00:08:20,080 --> 00:08:21,470
 poder descargarte un vídeo y

309
00:08:21,470 --> 00:08:23,240
 escucharlo en cualquier momento,

310
00:08:23,240 --> 00:08:25,020
 subir vídeos desde la misma

311
00:08:25,020 --> 00:08:27,140
 aplicación a tu instancia o

312
00:08:27,140 --> 00:08:28,580
 comentar e interactuar.

313
00:08:28,580 --> 00:08:30,270
 Esas son las cosas que están a

314
00:08:30,270 --> 00:08:30,640
 futuro.

315
00:08:30,640 --> 00:08:32,310
 Luego en PeerTube también

316
00:08:32,310 --> 00:08:34,140
 tenemos... En PeerTube no.

317
00:08:34,140 --> 00:08:36,030
 En F-Droid también tenemos que

318
00:08:36,030 --> 00:08:38,100
 cada y con él se ha actualizado.

319
00:08:38,100 --> 00:08:39,700
 Tenemos la 1.13.10.

320
00:08:39,700 --> 00:08:42,670
 Simplemente son parches de bugs

321
00:08:42,670 --> 00:08:45,380
 que se han ido arreglando.

322
00:08:45,380 --> 00:08:46,890
 Aunque estoy mirando ahora

323
00:08:46,890 --> 00:08:48,660
 mismo y está a punto...

324
00:08:48,660 --> 00:08:52,200
 No, ha salido ya la 1.32.11.

325
00:08:52,200 --> 00:08:54,320
 Con lo cual desde que yo me apunté

326
00:08:54,320 --> 00:08:56,460
 la noticia hasta que ha salido,

327
00:08:56,460 --> 00:08:57,560
 tiene una nueva.

328
00:08:57,560 --> 00:08:58,770
 Pero vamos, como es de punto 10

329
00:08:58,770 --> 00:09:01,540
 a punto 11, son todo seguro,

330
00:09:01,540 --> 00:09:03,820
 mejoras de arreglo y parches de

331
00:09:03,820 --> 00:09:04,300
 seguridad.

332
00:09:04,300 --> 00:09:05,560
 Luego, siguiendo con el

333
00:09:05,560 --> 00:09:07,060
 software libre en general,

334
00:09:07,060 --> 00:09:08,770
 tenemos una noticia de que Winnie

335
00:09:08,770 --> 00:09:10,460
 ha llegado a la versión 10.

336
00:09:10,540 --> 00:09:12,310
 Y entre todas las mejoras que

337
00:09:12,310 --> 00:09:14,480
 tiene para sobre todo jugar, yo

338
00:09:14,480 --> 00:09:16,440
 voy a destacar dos.

339
00:09:16,440 --> 00:09:18,290
 Que es que aunque Wayland se

340
00:09:18,290 --> 00:09:21,000
 introdujo en Winnie 9, se

341
00:09:21,000 --> 00:09:21,980
 introdujo.

342
00:09:21,980 --> 00:09:23,720
 Siempre que se introduce Wayland

343
00:09:23,720 --> 00:09:25,360
 sabemos que eso va a fuego lento,

344
00:09:25,360 --> 00:09:27,000
 que tiene trabajo por delante.

345
00:09:27,360 --> 00:09:28,620
 Con lo cual, ahora es cuando

346
00:09:28,620 --> 00:09:29,640
 está más maduro.

347
00:09:29,640 --> 00:09:31,770
 Ahora, por ejemplo, han mejorado

348
00:09:31,770 --> 00:09:33,720
 cosas como el soporte OpenGL,

349
00:09:33,720 --> 00:09:36,500
 el p-buffer, las posiciones de

350
00:09:36,500 --> 00:09:38,640
 las ventanas...

351
00:09:38,640 --> 00:09:39,800
 Vamos, que el driver de Wayland

352
00:09:39,800 --> 00:09:40,880
 ya está maduro e incluso está

353
00:09:40,880 --> 00:09:41,780
 activado por defecto en la

354
00:09:41,780 --> 00:09:42,640
 configuración.

355
00:09:43,640 --> 00:09:45,040
 Luego también, otra cosa que

356
00:09:45,040 --> 00:09:46,720
 han introducido en Winnie 10,

357
00:09:46,720 --> 00:09:48,540
 es el soporte experimental para

358
00:09:48,540 --> 00:09:50,850
 un driver de Bluetooth a varios

359
00:09:50,850 --> 00:09:53,080
 drivers del GID.

360
00:09:53,080 --> 00:09:54,990
 El GID suelen ser cosas de

361
00:09:54,990 --> 00:09:58,100
 movimiento de ratón, de tabletas.

362
00:09:58,100 --> 00:09:59,950
 También han metido soporte de

363
00:09:59,950 --> 00:10:01,950
 compilación de Direct 3D para

364
00:10:01,950 --> 00:10:03,520
 hacerlo de una manera más

365
00:10:03,520 --> 00:10:04,340
 moderna.

366
00:10:04,340 --> 00:10:05,800
 Un montón de mejoras por debajo

367
00:10:05,800 --> 00:10:06,970
 que va a hacer que se pueda

368
00:10:06,970 --> 00:10:08,400
 jugar a muchos más juegos.

369
00:10:08,400 --> 00:10:10,420
 Hay una cosa que no ha llegado

370
00:10:10,420 --> 00:10:12,360
 a entrar, que ya está el Mesh

371
00:10:12,360 --> 00:10:14,630
 Recuerd, la petición de mejora

372
00:10:14,630 --> 00:10:15,220
 hecha.

373
00:10:15,220 --> 00:10:17,040
 Y es que en el kernel han

374
00:10:17,040 --> 00:10:19,730
 puesto un protocolo NTSync, que

375
00:10:19,730 --> 00:10:22,020
 es algo que usa Windows mucho

376
00:10:22,020 --> 00:10:24,840
 para que los juegos vayan bien.

377
00:10:24,840 --> 00:10:27,300
 Y eso había que emularlo en

378
00:10:27,300 --> 00:10:28,240
 Linux.

379
00:10:28,240 --> 00:10:29,900
 Pues ahora está ese soporte,

380
00:10:29,900 --> 00:10:31,040
 por así decirlo.

381
00:10:31,040 --> 00:10:32,580
 Las llamadas que hacen los

382
00:10:32,580 --> 00:10:34,260
 juegos o las aplicaciones de

383
00:10:34,260 --> 00:10:36,590
 Windows, los S, que esperan que

384
00:10:36,590 --> 00:10:38,180
 funcione en el sistema.

385
00:10:39,100 --> 00:10:40,010
 Antes había que hacer cosas

386
00:10:40,010 --> 00:10:41,280
 para emularlas.

387
00:10:41,280 --> 00:10:42,750
 Ahora están dentro del kernel

388
00:10:42,750 --> 00:10:44,500
 de Linux, con lo cual el rendimiento

389
00:10:44,500 --> 00:10:45,740
 es mucho más grande.

390
00:10:45,740 --> 00:10:48,260
 Para hacer uso de esto, necesitamos

391
00:10:48,260 --> 00:10:50,280
 que Winnie lo implemente.

392
00:10:50,280 --> 00:10:52,030
 Ya está hecha la propuesta de

393
00:10:52,030 --> 00:10:53,740
 que se haya implementado.

394
00:10:53,740 --> 00:10:54,810
 El problema es que como se ha

395
00:10:54,810 --> 00:10:55,990
 hecho después de que saliera

396
00:10:55,990 --> 00:10:57,360
 Winnie 10, no va a estar hasta

397
00:10:57,360 --> 00:10:58,180
 Winnie 11.

398
00:10:58,860 --> 00:11:00,740
 O seguramente habrá forks de

399
00:11:00,740 --> 00:11:02,650
 Winnie o Proton seguramente lo

400
00:11:02,650 --> 00:11:04,360
 pongan mientras que llega a

401
00:11:04,360 --> 00:11:06,420
 Loopstring, que es Winnie.

402
00:11:06,420 --> 00:11:08,960
 La cosa es que ahora mismo Winnie

403
00:11:08,960 --> 00:11:11,420
 oficial está mucho mejor que

404
00:11:11,420 --> 00:11:13,470
 antes y que Proton va a tener

405
00:11:13,470 --> 00:11:17,520
 un boost, una subida de rendimiento

406
00:11:17,520 --> 00:11:18,840
 pronto.

407
00:11:19,080 --> 00:11:20,650
 Que no sabemos cómo de grande

408
00:11:20,650 --> 00:11:21,950
 va a ser porque la verdad es

409
00:11:21,950 --> 00:11:23,550
 que las expectativas son altas

410
00:11:23,550 --> 00:11:25,130
 porque han puesto unas pruebas

411
00:11:25,130 --> 00:11:26,780
 ahí que iban de un 100 a un

412
00:11:26,780 --> 00:11:28,720
 600% en algunos juegos.

413
00:11:28,720 --> 00:11:30,330
 Me parece exagerado, será un

414
00:11:30,330 --> 00:11:31,340
 caso concreto.

415
00:11:31,560 --> 00:11:33,070
 Pero en cualquier caso, si te

416
00:11:33,070 --> 00:11:35,180
 mejoran 10 puntos en vez de 600

417
00:11:35,180 --> 00:11:36,990
 o te mejoran 300, pues oye,

418
00:11:36,990 --> 00:11:38,000
 mejora, ¿eh?

419
00:11:38,000 --> 00:11:39,880
 Y tenemos también noticias de

420
00:11:39,880 --> 00:11:41,760
 software libre en general.

421
00:11:41,760 --> 00:11:44,630
 Thunderbird 134 ha sido publicado,

422
00:11:44,630 --> 00:11:46,640
 por supuesto, con muchos bugs

423
00:11:46,640 --> 00:11:47,740
 arreglados.

424
00:11:47,740 --> 00:11:49,560
 Y con una novedad que es que

425
00:11:49,560 --> 00:11:51,820
 tenemos notificaciones de escritorio

426
00:11:51,820 --> 00:11:53,020
 en tiempo real.

427
00:11:53,020 --> 00:11:54,550
 Eso significa que van a salir

428
00:11:54,550 --> 00:11:56,200
 las notificaciones más rápidas

429
00:11:56,200 --> 00:11:57,360
 cuando te lleguen.

430
00:11:57,360 --> 00:11:58,700
 Yo la verdad es que no tengo

431
00:11:58,700 --> 00:12:00,350
 quejas de las notificaciones de

432
00:12:00,350 --> 00:12:01,340
 Thunderbird.

433
00:12:01,340 --> 00:12:02,470
 Cuando me llega un correo, me

434
00:12:02,470 --> 00:12:03,160
 suele llegar.

435
00:12:03,160 --> 00:12:04,510
 No sé si es que me llegaba con

436
00:12:04,510 --> 00:12:05,940
 un poco de retardo, pero es lo

437
00:12:05,940 --> 00:12:06,980
 que pone las notas de la

438
00:12:06,980 --> 00:12:07,820
 versión.

439
00:12:07,820 --> 00:12:09,900
 Estas serían las noticias que

440
00:12:09,900 --> 00:12:11,980
 he ido recopilando más o menos

441
00:12:11,980 --> 00:12:12,860
 este mes.

442
00:12:12,860 --> 00:12:15,490
 Espero que os haya gustado la píldora

443
00:12:15,490 --> 00:12:17,500
 de las semanas pasadas, que

444
00:12:17,500 --> 00:12:18,860
 como la grabé ahí en modo

445
00:12:18,860 --> 00:12:19,280
 prueba,

446
00:12:19,280 --> 00:12:20,870
 recordando cómo se emitía en

447
00:12:20,870 --> 00:12:22,520
 Peer Tour, porque hacía mucho

448
00:12:22,520 --> 00:12:24,040
 que no usaba OBS para eso.

449
00:12:24,480 --> 00:12:25,500
 Vi que tenía ahí todos los

450
00:12:25,500 --> 00:12:26,440
 perfiles del tiempo de la

451
00:12:26,440 --> 00:12:27,600
 pandemia, de las pruebas que

452
00:12:27,600 --> 00:12:27,920
 hice.

453
00:12:27,920 --> 00:12:30,540
 No le di mucha introducción.

454
00:12:30,540 --> 00:12:31,880
 Lo publiqué tanto en vídeo

455
00:12:31,880 --> 00:12:32,720
 como en audio.

456
00:12:32,720 --> 00:12:34,190
 Por las descargas, parece que

457
00:12:34,190 --> 00:12:35,520
 lo habéis descargado más o

458
00:12:35,520 --> 00:12:36,810
 menos igual que un episodio

459
00:12:36,810 --> 00:12:37,400
 normal.

460
00:12:37,400 --> 00:12:38,650
 Puse en las notas del programa

461
00:12:38,650 --> 00:12:39,950
 por si alguien prefería verlo

462
00:12:39,950 --> 00:12:40,540
 en vídeo.

463
00:12:41,000 --> 00:12:42,890
 Si queréis decirme alguno, si

464
00:12:42,890 --> 00:12:44,650
 os parece que deberían estar

465
00:12:44,650 --> 00:12:46,600
 solo en vídeo o solo en audio,

466
00:12:46,600 --> 00:12:48,160
 un poco raro, porque en vídeo

467
00:12:48,160 --> 00:12:49,720
 mejora un poco la casa.

468
00:12:49,720 --> 00:12:51,290
 La cosa es que si queréis dar

469
00:12:51,290 --> 00:12:52,940
 feedback de si os parece útil

470
00:12:52,940 --> 00:12:54,710
 que aparte del vídeo lo cuelgue

471
00:12:54,710 --> 00:12:56,940
 aquí en el audio, se agradece.

472
00:12:56,940 --> 00:12:58,070
 Si no, lo seguiré haciendo

473
00:12:58,070 --> 00:12:59,110
 igual porque ya una vez que

474
00:12:59,110 --> 00:13:00,140
 hago el vídeo no me cuesta

475
00:13:00,140 --> 00:13:00,640
 nada.

476
00:13:01,120 --> 00:13:03,270
 Son 10 minutos más lanzarlo en

477
00:13:03,270 --> 00:13:04,180
 el podcast.

478
00:13:04,180 --> 00:13:05,370
 Con esto termino por hoy,

479
00:13:05,370 --> 00:13:06,690
 aunque tengo preparado ya el

480
00:13:06,690 --> 00:13:08,240
 siguiente episodio, así que

481
00:13:08,240 --> 00:13:09,780
 ese espero que sea rápido, si

482
00:13:09,780 --> 00:13:10,760
 el tiempo me lo permite.

483
00:13:10,760 --> 00:13:12,570
 Porque quiero dejarlo aparte

484
00:13:12,570 --> 00:13:14,000
 porque es un buen tema.

485
00:13:14,400 --> 00:13:16,140
 Un salido y nos vemos por las

486
00:13:16,140 --> 00:13:16,800
 redes.

487
00:13:16,800 --> 00:13:17,480
 Hasta luego.

488
00:13:17,480 --> 00:13:19,400
 Y se me olvidaba, hoy os voy a

489
00:13:19,400 --> 00:13:21,500
 dejar con una promo de una

490
00:13:21,500 --> 00:13:23,480
 compañera que conocí en el Cañas

491
00:13:23,480 --> 00:13:24,360
 y Poscas.

492
00:13:24,360 --> 00:13:26,250
 Su Poscas es Mujeres con Historia

493
00:13:26,250 --> 00:13:27,830
 y yo lo recomiendo mucho, que

494
00:13:27,830 --> 00:13:29,590
 lo estoy disfrutando y justo ha

495
00:13:29,590 --> 00:13:31,380
 publicado hace unos días.

496
00:13:31,380 --> 00:13:32,920
 Ahora sí, hasta luego.

497
00:13:36,920 --> 00:13:38,290
 ¿Quieres conocer a grandes

498
00:13:38,290 --> 00:13:38,940
 mujeres?

499
00:13:38,940 --> 00:13:41,640
 Mujeres inventoras, abogadas,

500
00:13:41,640 --> 00:13:44,830
 piratas, premios Nobel, cantantes,

501
00:13:44,830 --> 00:13:47,230
 rebeldes, mujeres escritoras,

502
00:13:47,230 --> 00:13:50,080
 astrónomas, mineras, actrices.

503
00:13:50,080 --> 00:13:51,940
 Mujeres con Historia.

504
00:13:51,940 --> 00:13:53,360
 El podcast donde Elena te

505
00:13:53,360 --> 00:13:55,070
 cuenta la historia de grandes

506
00:13:55,070 --> 00:13:56,870
 mujeres que han dejado su huella

507
00:13:56,870 --> 00:13:58,980
 en la historia de la humanidad.

508
00:13:58,980 --> 00:14:00,620
 Mujeres con Historia.

509
00:14:00,620 --> 00:14:02,360
 Escúchalo en cualquiera de tus

510
00:14:02,360 --> 00:14:03,880
 plataformas favoritas.
