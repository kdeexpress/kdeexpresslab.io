1
00:00:00,000 --> 00:00:02,000
 Buenas! KDE Express está de vuelta

2
00:00:02,000 --> 00:00:06,400
 con el episodio 24 grabado el 16 de febrero

3
00:00:06,400 --> 00:00:07,320
 de 2024.

4
00:00:07,320 --> 00:00:09,440
 Ha pasado un mes prácticamente desde

5
00:00:09,440 --> 00:00:11,680
 el último, pero entre tanto hemos

6
00:00:11,680 --> 00:00:13,480
 estado liados con varias

7
00:00:13,480 --> 00:00:16,590
 actividades como por ejemplo el 24H24L

8
00:00:16,590 --> 00:00:18,800
 que es un evento que este año se ha

9
00:00:18,800 --> 00:00:19,920
 reconvertido en

10
00:00:19,920 --> 00:00:22,600
 un podcast tradicional y en vez de

11
00:00:22,600 --> 00:00:25,990
 hacer 24 audios de golpe en un maratón

12
00:00:25,990 --> 00:00:27,120
 se están publicando

13
00:00:27,120 --> 00:00:29,540
 directamente al feed un episodio tras

14
00:00:29,540 --> 00:00:32,440
 otro conforme se van grabando y publicando.

15
00:00:32,440 --> 00:00:32,880
 Ahora

16
00:00:32,880 --> 00:00:35,510
 mismo tenéis ya cuatro episodios

17
00:00:35,510 --> 00:00:38,490
 nuevos que serían GNU/Linux en LATAM

18
00:00:38,490 --> 00:00:40,720
 2, Neovim vs EMAC,

19
00:00:40,720 --> 00:00:43,360
 Entornos de Escritorio Gnome y Plasma

20
00:00:43,360 --> 00:00:46,750
 y Redes Libres con Spectrum y Gabriel Biso.

21
00:00:46,750 --> 00:00:47,040
 Por eso

22
00:00:47,040 --> 00:00:49,070
 hemos estado un poco atareados y no

23
00:00:49,070 --> 00:00:51,380
 hemos podido grabar episodio de KDE

24
00:00:51,380 --> 00:00:52,520
 Express así que os

25
00:00:52,520 --> 00:00:55,250
 recomiendo que cojáis ese feed y lo añadáis

26
00:00:55,250 --> 00:00:57,670
 a vuestra aplicación de referencia.

27
00:00:57,670 --> 00:00:58,400
 También hemos

28
00:00:58,400 --> 00:01:00,470
 sacado episodios de accesibilidad con

29
00:01:00,470 --> 00:01:02,480
 tecnologías libres y también hay un

30
00:01:02,480 --> 00:01:04,520
 poco de KDE ahí. Pero

31
00:01:04,520 --> 00:01:06,640
 ya dejando las excusas de por qué

32
00:01:06,640 --> 00:01:09,360
 publicamos tan poco empiezo con el listado

33
00:01:09,360 --> 00:01:10,520
 de noticias que se han

34
00:01:10,520 --> 00:01:13,290
 ido acumulando y la primera es la que

35
00:01:13,290 --> 00:01:15,840
 va a dar título a este episodio que es

36
00:01:15,840 --> 00:01:17,040
 que se viene en

37
00:01:17,040 --> 00:01:19,920
 grandes eventos dentro de poco y es que

38
00:01:19,920 --> 00:01:22,700
 como todos sabréis pues Plasma 6 con

39
00:01:22,700 --> 00:01:24,360
 todos los frameworks y

40
00:01:24,360 --> 00:01:26,670
 todas las aplicaciones están a menos

41
00:01:26,670 --> 00:01:29,190
 de dos semanas para que salgan. Haremos

42
00:01:29,190 --> 00:01:30,440
 un especial una vez que

43
00:01:30,440 --> 00:01:32,630
 esté publicado de las novedades que

44
00:01:32,630 --> 00:01:34,840
 tiene porque como siempre digo estas

45
00:01:34,840 --> 00:01:36,320
 cosas están muy bien para

46
00:01:36,320 --> 00:01:38,750
 los prisas que queremos tenerlo todo

47
00:01:38,750 --> 00:01:40,430
 pronto pero hasta que llegue al gran

48
00:01:40,430 --> 00:01:42,000
 público pasarán meses

49
00:01:42,000 --> 00:01:44,410
 hasta que puedan ver Plasma. Lo que sí

50
00:01:44,410 --> 00:01:46,700
 que tenéis ya disponible y cosas para

51
00:01:46,700 --> 00:01:48,640
 inscribiros son los

52
00:01:48,640 --> 00:01:51,520
 eventos de esLibre que ya ha terminado

53
00:01:51,520 --> 00:01:54,330
 el primer Call for Papers con lo cual

54
00:01:54,330 --> 00:01:55,560
 ya sabéis un montón

55
00:01:55,560 --> 00:01:58,030
 de charlas que han sido aceptadas y

56
00:01:58,030 --> 00:02:00,600
 relacionado con KDE de las mesas o

57
00:02:00,600 --> 00:02:02,160
 DevRooms las salas que

58
00:02:02,160 --> 00:02:06,070
 van a ver es una de KDE, otra de Wikimedia

59
00:02:06,070 --> 00:02:09,440
 y otra de Universidades y Oficinal del

60
00:02:09,440 --> 00:02:10,400
 Software

61
00:02:10,400 --> 00:02:13,080
 Libre con lo cual podéis intuir que la

62
00:02:13,080 --> 00:02:15,250
 sala de KDE se va a dedicar a la

63
00:02:15,250 --> 00:02:17,800
 Akademy 2024 que como

64
00:02:17,800 --> 00:02:19,550
 quedamos muy satisfechos el año pasado

65
00:02:19,550 --> 00:02:21,260
 con OpenShouthCode hemos pensado este

66
00:02:21,260 --> 00:02:22,160
 año repetir la

67
00:02:22,160 --> 00:02:24,420
 experiencia dentro de un gran evento

68
00:02:24,420 --> 00:02:26,870
 esta vez con esLibre. Nosotros como ya

69
00:02:26,870 --> 00:02:27,920
 tenemos la sala

70
00:02:27,920 --> 00:02:30,530
 confirmada pues hemos abierto el Call

71
00:02:30,530 --> 00:02:33,260
 for Papers o la llamada a las charlas y

72
00:02:33,260 --> 00:02:34,360
 tenéis hasta el 1 de

73
00:02:34,360 --> 00:02:36,490
 abril para presentar vuestras propuestas.

74
00:02:36,490 --> 00:02:38,210
 Mi consejo es que no lo dejéis para el

75
00:02:38,210 --> 00:02:39,560
 último día y que os

76
00:02:39,560 --> 00:02:41,460
 animéis no hace falta ser programador

77
00:02:41,460 --> 00:02:43,410
 desarrollador o estar involucrado en un

78
00:02:43,410 --> 00:02:45,200
 proyecto a cualquiera que

79
00:02:45,200 --> 00:02:47,720
 tenga experiencia con la comunidad o

80
00:02:47,720 --> 00:02:50,160
 alguna aplicación a nivel usuario

81
00:02:50,160 --> 00:02:51,400
 también puede dar

82
00:02:51,400 --> 00:02:53,840
 una charla también tendremos aparte de

83
00:02:53,840 --> 00:02:55,750
 lo que es esa sala una mesa que nos

84
00:02:55,750 --> 00:02:57,280
 funcionó muy bien el año

85
00:02:57,280 --> 00:03:00,000
 pasado pues mostrando cosas del entorno

86
00:03:00,000 --> 00:03:02,500
 KDE por ejemplo yo el año pasado me

87
00:03:02,500 --> 00:03:03,880
 llevé la Steam Deck

88
00:03:03,880 --> 00:03:06,250
 y la gente le gustó estar jugueteando

89
00:03:06,250 --> 00:03:08,560
 teníamos una tableta concreta para

90
00:03:08,560 --> 00:03:10,720
 dibujar siempre hay

91
00:03:10,720 --> 00:03:12,540
 unos Konqis de peluches que están muy

92
00:03:12,540 --> 00:03:14,700
 chulos para comprar camisetas merchandising

93
00:03:14,700 --> 00:03:15,560
 de todo si

94
00:03:15,560 --> 00:03:17,890
 si os pasáis por Valencia pasar por la

95
00:03:17,890 --> 00:03:20,240
 mesa que seguro que no te frauda

96
00:03:20,240 --> 00:03:21,240
 después del evento en

97
00:03:21,240 --> 00:03:23,930
 sí ya vienen las noticias típicas de

98
00:03:23,930 --> 00:03:26,420
 KDE que se suelen dar y una que no me

99
00:03:26,420 --> 00:03:27,960
 esperaba es que la

100
00:03:27,960 --> 00:03:31,270
 distribución caos la versión 2024 01

101
00:03:31,270 --> 00:03:33,620
 ya lleva Plasma han debido de probarlo

102
00:03:33,620 --> 00:03:34,880
 y ver que iba también

103
00:03:34,880 --> 00:03:37,680
 que ya lo han lanzado en su distribución

104
00:03:37,680 --> 00:03:40,240
 a ver yo no voy a engañar estoy seguro

105
00:03:40,240 --> 00:03:42,000
 que hay books que

106
00:03:42,000 --> 00:03:43,510
 se van a ir arreglando de aquí en

107
00:03:43,510 --> 00:03:45,220
 estas dos últimas semanas y habrá

108
00:03:45,220 --> 00:03:46,760
 algún punto que se arreglará en

109
00:03:46,760 --> 00:03:49,300
 la siguiente pero debe estar en uno en

110
00:03:49,300 --> 00:03:51,790
 un estado muy usable si hay gente que

111
00:03:51,790 --> 00:03:53,120
 ya ha visto que eso

112
00:03:53,120 --> 00:03:55,600
 se puede lanzar sin temor a que sea una

113
00:03:55,600 --> 00:03:58,150
 catástrofe la otra cara de la moneda

114
00:03:58,150 --> 00:04:00,240
 es que el Kubuntu 2404

115
00:04:00,240 --> 00:04:02,780
 no va a llevar Plasma 6 porque dicen

116
00:04:02,780 --> 00:04:05,040
 que no les da tiempo a hacer el testeo

117
00:04:05,040 --> 00:04:06,440
 suficiente y que se

118
00:04:06,440 --> 00:04:09,000
 quedan en 527 que está muy bien y que

119
00:04:09,000 --> 00:04:11,200
 es muy estable hay que tener en cuenta

120
00:04:11,200 --> 00:04:12,120
 que Kubuntu y

121
00:04:12,120 --> 00:04:14,680
 la familia de Ubuntu está últimamente

122
00:04:14,680 --> 00:04:17,240
 muy enfocada al entorno empresarial y

123
00:04:17,240 --> 00:04:18,480
 no se la cogen con papel

124
00:04:18,480 --> 00:04:20,890
 de fumar esos llevan mucho cuidado y

125
00:04:20,890 --> 00:04:23,520
 están enfocados aunque yo la verdad es

126
00:04:23,520 --> 00:04:24,360
 que la uso en

127
00:04:24,360 --> 00:04:26,630
 el trabajo y algún buque encuentro no

128
00:04:26,630 --> 00:04:28,760
 quieren líos y la parte del aspecto

129
00:04:28,760 --> 00:04:30,000
 visual o las nuevas

130
00:04:30,000 --> 00:04:33,200
 funcionalidades no le importan tanto

131
00:04:33,200 --> 00:04:35,780
 como asegurarse el qb y el tener un

132
00:04:35,780 --> 00:04:38,040
 buen periodo de testeo pero

133
00:04:38,040 --> 00:04:40,190
 bueno Kubuntu siempre ha sido muy famosa

134
00:04:40,190 --> 00:04:42,050
 porque tú puedes pasar Kubuntu a la

135
00:04:42,050 --> 00:04:43,240
 última versión de

136
00:04:43,240 --> 00:04:45,180
 Plasma con un ppa bastante sencillo la

137
00:04:45,180 --> 00:04:47,110
 última vez tenías que poner dos para

138
00:04:47,110 --> 00:04:48,280
 tener lo último de lo

139
00:04:48,280 --> 00:04:49,860
 último pero vamos es cuestión de

140
00:04:49,860 --> 00:04:51,710
 poner 2 pp aunque mi recomendación es

141
00:04:51,710 --> 00:04:53,320
 que se vaya a poner esos 2 pp

142
00:04:53,320 --> 00:04:55,950
 y puesto a instalar de cero ya pondría

143
00:04:55,950 --> 00:04:58,270
 cadena que viene a ser lo mismo y te

144
00:04:58,270 --> 00:05:00,440
 ahorra los líos de las

145
00:05:00,440 --> 00:05:02,530
 dependencias y las actualizaciones

146
00:05:02,530 --> 00:05:04,660
 también tenemos que como cadena un capará

147
00:05:04,660 --> 00:05:06,600
 pues ha salido cada

148
00:05:06,600 --> 00:05:10,160
 framework 5 115 que supongo que

149
00:05:10,160 --> 00:05:12,460
 todavía habrá uno más aunque sea de

150
00:05:12,460 --> 00:05:14,520
 mantenimiento la verdad

151
00:05:14,520 --> 00:05:16,080
 es que eso no me he mirado si van a

152
00:05:16,080 --> 00:05:17,730
 seguir con LTS los frameworks o a

153
00:05:17,730 --> 00:05:20,060
 partir de que salga el 6 ya

154
00:05:20,060 --> 00:05:22,330
 no habrá ninguno más en la vez en la

155
00:05:22,330 --> 00:05:24,870
 rama 5 me lo miraré en otro episodio o

156
00:05:24,870 --> 00:05:26,880
 lo confirmo la cosa es

157
00:05:26,880 --> 00:05:28,630
 que que siguen actualizando lo que es

158
00:05:28,630 --> 00:05:30,270
 la base de Plasma y siguen haciendo

159
00:05:30,270 --> 00:05:31,920
 mejoras y correcciones en

160
00:05:31,920 --> 00:05:33,680
 las notas del programa por supuesto tenéis

161
00:05:33,680 --> 00:05:35,250
 un par de noticias para ver exactamente

162
00:05:35,250 --> 00:05:36,040
 qué hacen si os

163
00:05:36,040 --> 00:05:37,940
 gusta meteros en los detalles pero la

164
00:05:37,940 --> 00:05:39,680
 noticia en sí es que no paramos que

165
00:05:39,680 --> 00:05:42,000
 aunque haya Plasma 6 dentro

166
00:05:42,000 --> 00:05:45,220
 de muy poquito seguimos sacando al ritmo

167
00:05:45,220 --> 00:05:48,630
 habitual de cosas el resto del ecosistema

168
00:05:48,630 --> 00:05:49,920
 y una cosa curiosa

169
00:05:49,920 --> 00:05:52,430
 que Nate Graham ha estado comentando

170
00:05:52,430 --> 00:05:55,440
 últimamente es el estado de las actividades

171
00:05:55,440 --> 00:05:56,640
 de Plasma es una

172
00:05:56,640 --> 00:05:58,730
 cosa con mucho potencial que hay varios

173
00:05:58,730 --> 00:06:00,740
 vídeos de YouTube diciéndote de las

174
00:06:00,740 --> 00:06:01,800
 maravillas con las que

175
00:06:01,800 --> 00:06:04,160
 podéis hacer pero que realmente se usa

176
00:06:04,160 --> 00:06:06,280
 bastante poco lo bueno es que cuando

177
00:06:06,280 --> 00:06:07,440
 anunciaron que lo

178
00:06:07,440 --> 00:06:09,640
 mismo se lo cargaban en Plasma 6 porque

179
00:06:09,640 --> 00:06:11,770
 no estaba del todo bien mantenido ha

180
00:06:11,770 --> 00:06:12,900
 salido bastante gente

181
00:06:12,900 --> 00:06:14,790
 diciendo que por favor no lo maten que

182
00:06:14,790 --> 00:06:17,100
 a ellos les da la vida y que hacen virguerías

183
00:06:17,100 --> 00:06:17,840
 y entonces han

184
00:06:17,840 --> 00:06:20,430
 decidido darle un mínimo de mantenimiento

185
00:06:20,430 --> 00:06:22,610
 y mantenerlo en Plasma 6 pero lo que

186
00:06:22,610 --> 00:06:23,480
 les gustaría

187
00:06:23,480 --> 00:06:26,110
 hacer es mejorarlo y cambiar la funcionalidad

188
00:06:26,110 --> 00:06:28,430
 y adaptarlo a los tiempos modernos pero

189
00:06:28,430 --> 00:06:28,840
 que para

190
00:06:28,840 --> 00:06:31,960
 eso necesitan desarrolladores que arrimen

191
00:06:31,960 --> 00:06:34,610
 el hombro y se dediquen en esa parte

192
00:06:34,610 --> 00:06:35,280
 que si en el futuro

193
00:06:35,280 --> 00:06:37,710
 siguen sin tener nadie que quiera encargarse

194
00:06:37,710 --> 00:06:39,770
 de ese aspecto en concreto volverán a

195
00:06:39,770 --> 00:06:40,280
 tener que

196
00:06:40,280 --> 00:06:42,240
 plantearse cargárselo pero que para

197
00:06:42,240 --> 00:06:44,500
 empezar con Plasma 6 seguiremos teniendo

198
00:06:44,500 --> 00:06:45,480
 las actividades

199
00:06:45,480 --> 00:06:48,150
 disponibles por si más gente se anima

200
00:06:48,150 --> 00:06:50,470
 a usarla y sobre todo a mantener el

201
00:06:50,470 --> 00:06:51,920
 código que se ve que

202
00:06:51,920 --> 00:06:54,490
 es una parte que no muchos desarrolladores

203
00:06:54,490 --> 00:06:57,240
 de KDE están interesados en ir actualizando

204
00:06:57,240 --> 00:06:57,960
 luego

205
00:06:57,960 --> 00:07:00,670
 también quiero haceros referencia a un

206
00:07:00,670 --> 00:07:02,910
 artículo que ha salido muy Linux que

207
00:07:02,910 --> 00:07:04,240
 se titula comparar

208
00:07:04,240 --> 00:07:07,160
 Gnome y Plasma no tiene sentido y os

209
00:07:07,160 --> 00:07:10,310
 habla sobre que antiguamente Gnome y

210
00:07:10,310 --> 00:07:11,920
 Plasma se parecían mucho

211
00:07:11,920 --> 00:07:14,560
 porque Gnome 2 tenía la barra arriba

212
00:07:14,560 --> 00:07:16,480
 y abajo pero que era un entorno

213
00:07:16,480 --> 00:07:18,160
 tradicional del botón

214
00:07:18,160 --> 00:07:20,610
 en una esquina arriba o abajo del botón

215
00:07:20,610 --> 00:07:22,760
 de inicio y que ambos se parecían

216
00:07:22,760 --> 00:07:24,160
 bastante al funcionamiento

217
00:07:24,160 --> 00:07:25,630
 con Windows que había alguna

218
00:07:25,630 --> 00:07:27,610
 diferencia de licencia entre uno y otro

219
00:07:27,610 --> 00:07:28,560
 pero luego eso también

220
00:07:28,560 --> 00:07:31,420
 se soluciona pero que a partir de Gnome

221
00:07:31,420 --> 00:07:34,380
 3 pues ya la filosofía del escritorio

222
00:07:34,380 --> 00:07:36,040
 cambió mucho se va

223
00:07:36,040 --> 00:07:38,650
 pareciendo más a macops de Apple y

224
00:07:38,650 --> 00:07:41,350
 entonces pues ahora mismo es que son

225
00:07:41,350 --> 00:07:43,680
 dos entornos muy diferenciados

226
00:07:43,680 --> 00:07:46,860
 con dos filosofías completamente opuestas

227
00:07:46,860 --> 00:07:49,120
 aunque tu Plasma puedas adaptarlo y

228
00:07:49,120 --> 00:07:50,120
 hacer que se parezca

229
00:07:50,120 --> 00:07:53,080
 por completo a cualquier otro el diseño

230
00:07:53,080 --> 00:07:55,870
 en sí Gnome está hecho más para que

231
00:07:55,870 --> 00:07:57,480
 sea más táctil

232
00:07:57,480 --> 00:08:00,220
 más visual para quien le guste ese

233
00:08:00,220 --> 00:08:02,490
 estilo y Plasma sigue siendo

234
00:08:02,490 --> 00:08:04,920
 completamente configurable

235
00:08:04,920 --> 00:08:06,990
 y por defecto intenta que la curva de

236
00:08:06,990 --> 00:08:09,060
 aprendizaje para la gente que venga

237
00:08:09,060 --> 00:08:10,320
 nueva pues sea lo más

238
00:08:10,320 --> 00:08:12,980
 plana cuando la mayoría de gente viene

239
00:08:12,980 --> 00:08:15,760
 de Windows me ha gustado el artículo

240
00:08:15,760 --> 00:08:16,640
 aunque yo le habría

241
00:08:16,640 --> 00:08:18,480
 añadido que efectivamente no tiene

242
00:08:18,480 --> 00:08:20,420
 sentido seguir comparándolos son dos

243
00:08:20,420 --> 00:08:21,200
 productos muy

244
00:08:21,200 --> 00:08:23,710
 buenos cada uno que tira por un camino

245
00:08:23,710 --> 00:08:26,450
 aunque siempre se pueden hacer comparaciones

246
00:08:26,450 --> 00:08:27,000
 de esto

247
00:08:27,000 --> 00:08:28,570
 puede que para esto sea más productivo

248
00:08:28,570 --> 00:08:30,070
 para esto puede ser que uno tenga más

249
00:08:30,070 --> 00:08:31,520
 rendimiento que otro

250
00:08:31,520 --> 00:08:33,680
 puede ser que uno tenga más más

251
00:08:33,680 --> 00:08:36,260
 implementado la accesibilidad que otro

252
00:08:36,260 --> 00:08:38,040
 puede que uno implemente

253
00:08:38,040 --> 00:08:41,270
 antes que otro no se bailan la compatibilidad

254
00:08:41,270 --> 00:08:44,150
 con NVIDIA algunas cosas técnicas para

255
00:08:44,150 --> 00:08:45,320
 juegos yo creo

256
00:08:45,320 --> 00:08:47,770
 que ahí todavía está bien que siga viva

257
00:08:47,770 --> 00:08:50,080
 la comparación una comparación sana

258
00:08:50,080 --> 00:08:50,960
 sin pelea sin

259
00:08:50,960 --> 00:08:52,700
 decir que uno es una mierda y el otro

260
00:08:52,700 --> 00:08:54,750
 una maravilla ni viceversa simplemente

261
00:08:54,750 --> 00:08:56,400
 son dos proyectos de algo

262
00:08:56,400 --> 00:08:58,000
 parecido que un entorno de escritorio

263
00:08:58,000 --> 00:08:59,560
 con diferentes caminos y cada uno con

264
00:08:59,560 --> 00:09:00,720
 sus ventajas y sus

265
00:09:00,720 --> 00:09:03,310
 inconvenientes y luego hace poco ha

266
00:09:03,310 --> 00:09:06,080
 sido el día de san valentín aunque yo

267
00:09:06,080 --> 00:09:08,160
 personalmente no celebro

268
00:09:08,160 --> 00:09:10,120
 eso pero sí celebro el i love free

269
00:09:10,120 --> 00:09:12,540
 software de la Free Software Foundation

270
00:09:12,540 --> 00:09:13,800
 Europe y de hecho si

271
00:09:13,800 --> 00:09:16,890
 escucháis las charlas de 24 h 24 l en

272
00:09:16,890 --> 00:09:19,040
 las dos en las que estoy yo hacemos un

273
00:09:19,040 --> 00:09:20,880
 mención al proyecto y

274
00:09:20,880 --> 00:09:23,870
 felicitamos y agradecemos a una serie

275
00:09:23,870 --> 00:09:26,670
 de personas y proyectos por todo lo que

276
00:09:26,670 --> 00:09:28,240
 nos dan y yo quería

277
00:09:28,240 --> 00:09:29,930
 aprovechar este episodio aunque creo

278
00:09:29,930 --> 00:09:31,680
 que hice uno específico el año pasado

279
00:09:31,680 --> 00:09:32,880
 solo de felicitaciones

280
00:09:32,880 --> 00:09:35,830
 este lo voy a hacer más corto y quiero

281
00:09:35,830 --> 00:09:38,990
 agradecer mandar un abrazo muy grande a

282
00:09:38,990 --> 00:09:40,560
 Juan Febles que aunque

283
00:09:40,560 --> 00:09:43,000
 ya no siga publicando es un proyecto es

284
00:09:43,000 --> 00:09:45,620
 una persona que nos ha dado innumerables

285
00:09:45,620 --> 00:09:47,800
 horas y contenido de

286
00:09:47,800 --> 00:09:49,970
 calidad sobre software libre y sobre

287
00:09:49,970 --> 00:09:51,930
 todo un trato y una calidad humana

288
00:09:51,930 --> 00:09:53,440
 excelente entonces quería

289
00:09:53,440 --> 00:09:56,100
 acordarme de él antes de terminar este

290
00:09:56,100 --> 00:09:58,890
 episodio y espero que me esté escuchando

291
00:09:58,890 --> 00:10:00,160
 braille el compañero

292
00:10:00,160 --> 00:10:02,540
 porque tenemos planificado hacer una

293
00:10:02,540 --> 00:10:04,620
 especial k de express con algunos

294
00:10:04,620 --> 00:10:06,520
 compañeros que estuvieron

295
00:10:06,520 --> 00:10:09,040
 en la post en para que me puedan dar

296
00:10:09,040 --> 00:10:12,010
 envidia y nos cuenten la delegación de

297
00:10:12,010 --> 00:10:13,880
 KDE que experiencia tuvo

298
00:10:13,880 --> 00:10:17,150
 en ese otro gran evento que también es

299
00:10:17,150 --> 00:10:20,350
 en cierta manera musa espiritual de es

300
00:10:20,350 --> 00:10:21,600
 libre a nivel

301
00:10:21,600 --> 00:10:23,780
 internacional que pues yo creo es lo

302
00:10:23,780 --> 00:10:26,480
 más grande que hay en Europa de actividades

303
00:10:26,480 --> 00:10:27,320
 de software libre

304
00:10:27,320 --> 00:10:29,250
 y espero alguna vez poder ir pero

305
00:10:29,250 --> 00:10:31,370
 mientras tanto este año por lo menos

306
00:10:31,370 --> 00:10:32,920
 que tengo casi asegurada

307
00:10:32,920 --> 00:10:36,450
 la presencia a esLibre 2024

308
00:10:36,450 --> 00:10:39,720
 24-25 mayo en Valencia y ahí pues ya

309
00:10:39,720 --> 00:10:42,480
 iré cogiendo por fin un

310
00:10:42,480 --> 00:10:44,380
 poco de experiencia en grandes eventos

311
00:10:44,380 --> 00:10:46,220
 del software libre dado que

312
00:10:46,220 --> 00:10:47,460
 OpenSouthCode y Akademy-es me

313
00:10:47,460 --> 00:10:49,780
 encantó el año pasado con esto me

314
00:10:49,780 --> 00:10:52,340
 despido de vosotros un placer volver a

315
00:10:52,340 --> 00:10:53,880
 hablaros y espero

316
00:10:53,880 --> 00:10:55,560
 que nos escuchemos pronto hasta luego

