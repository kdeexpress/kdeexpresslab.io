El 24 y 25 de mayo la Asociación KDE España celebrará 
Akademy-es 2024 en Valencia, el evento anual más importante 
de la comunidad KDE que se enmarca dentro de otro de los grandes 
eventos libres como esLibre, coorganizado con la Asociación 
Anfitriona GNU Linux Valencia y gracias al patrocinio de la 
Universidad de La Laguna y OpenSUSE. El espacio elegido ha sido 
Las Naves, el centro de innovación social y urbana de la ciudad de 
Valencia y La Mutant, el espacio de artes vivas. Con 
esta será la segunda ocasión que los simpatizantes del 
proyecto KDE visiten la histórica capital de la comunidad Valenciana 
tras Akademy-es del 2018. Desde KDE España se alegran de 
trabajar junto con los organizadores de esLibre, 
uno de los eventos más importantes para la promoción 
y visualización de las tecnologías Estamos seguros disfrutaremos 
de unos días fantásticos, de ponencias, presentaciones, 
talleres, conversaciones, intercambio de ideas y de cultura libre en 
general. Durante el evento, como es costumbre, 
se realizarán charlas tanto para usuarios 
como para desarrolladores, además de talleres prácticos 
y otras actividades de carácter más social con las que se pretenden 
cumplir los siguientes objetivos. Poner en contacto a 
los desarrolladores de KDE de toda España para que puedan 
hablar de los proyectos, en que están trabajando, 
compartir código, experiencias y conocimiento. Dar a conocer 
los proyectos KDE como el entorno de escritorio a nuevos usuarios. 
Divulgar acerca de las tecnologías KDE tanto para 
nuevos desarrolladores como para usuarios que quieran 
conocer mejor KDE. El programa de ponencias, ya 
publicado, se desarrollará el viernes 24 de mayo a partir 
de las 12 del mediodía. Componentes de la talla de Lorenzo Carbonell, 
el Atareao o Alien. Además, de los ponentes tradicionales 
del evento, Albert Astals, Aleix Pol, Agustín Benito, 
José Millán o Baltasar Ortega. Para el sábado 25 de mayo, 
Akademy-es 2024 se celebrará en forma de evento social alrededor de 
su stand. En él se realizarán concursos, entrevistas y charlas 
distendidas entre los asistentes y podréis encontrar una buena 
muestra del software de la comunidad KDE, así como de 
hardware que los implementa por defecto como ordenadores, 
de la compañía Slimbook o la Steam Deck, la famosa consola 
portátil de Valve. Además de merchandising de freeware, camisetas, 
tazas, gorras, etc. Os esperamos en Akademy-es 2024 de 
Valencia, esLibre Edition. No puedes faltar. Más 
información en www.kde-Espana.org/Akademy-es-2024 ¡Hackers, you'll be free! 
