1
00:00:00,000 --> 00:00:02,120
 Buenas, feliz año, espero que

2
00:00:02,120 --> 00:00:03,690
 hayas tenido buenas fiestas o

3
00:00:03,690 --> 00:00:04,960
 por lo menos un periodo libre

4
00:00:04,960 --> 00:00:05,520
 de virus.

5
00:00:05,520 --> 00:00:08,010
 Yo una vez recuperado de lo que

6
00:00:08,010 --> 00:00:10,300
 son las relaciones familiares,

7
00:00:10,300 --> 00:00:12,660
 los tumultos y la organización

8
00:00:12,660 --> 00:00:15,040
 que estamos haciendo de esLibre

9
00:00:15,040 --> 00:00:17,480
y 24H24L que ahora os cuento un

10
00:00:17,480 --> 00:00:17,800
 poco.

11
00:00:17,800 --> 00:00:19,770
 Puedo dar por inaugurado el KDE

12
00:00:19,770 --> 00:00:22,050
 Express número 23 que se está

13
00:00:22,050 --> 00:00:24,100
 grabando el 11 de enero.

14
00:00:24,100 --> 00:00:25,960
 Lo primero es deciros que se

15
00:00:25,960 --> 00:00:28,710
 está haciendo un nuevo 24H24L,

16
00:00:28,710 --> 00:00:30,190
 que habrá algún audio con

17
00:00:30,190 --> 00:00:32,180
 temas relacionados con KDE,

18
00:00:32,180 --> 00:00:34,020
 estar atentos a la página web

19
00:00:34,020 --> 00:00:35,200
 y al feed.

20
00:00:35,200 --> 00:00:36,870
 Y estáis a tiempo todavía de

21
00:00:36,870 --> 00:00:39,690
 participar. Si queréis proponer

22
00:00:39,690 --> 00:00:42,430
 un audio o estar como moderador

23
00:00:42,430 --> 00:00:44,800
 o como participante, todavía

24
00:00:44,800 --> 00:00:46,050
 quedan un par de semanas en los

25
00:00:46,050 --> 00:00:47,300
 que se puede sacar algún tema

26
00:00:47,300 --> 00:00:47,760
 nuevo.

27
00:00:47,760 --> 00:00:49,530
 Y luego sobre esLibre, supongo

28
00:00:49,530 --> 00:00:51,110
 que sabréis que este año se

29
00:00:51,110 --> 00:00:53,310
 hace en Valencia, el 24 y 25 de

30
00:00:53,310 --> 00:00:54,970
 mayo, y que la asociación KDE

31
00:00:54,970 --> 00:00:56,930
 está haciendo unos trámites a

32
00:00:56,930 --> 00:00:58,430
 ver si conseguimos tener allí

33
00:00:58,430 --> 00:00:59,000
 presencia.

34
00:00:59,000 --> 00:01:00,570
 Ya os mantendremos informados

35
00:01:00,570 --> 00:01:02,000
 cuando haya algo seguro.

36
00:01:02,000 --> 00:01:03,700
 Hoy os traigo un popurrí de

37
00:01:03,700 --> 00:01:05,640
 noticias que he ido acumulando

38
00:01:05,640 --> 00:01:07,560
 en estas vacaciones, os voy a

39
00:01:07,560 --> 00:01:09,080
 hacer un breve resumen y os dejo

40
00:01:09,080 --> 00:01:10,430
 los enlaces por si queréis

41
00:01:10,430 --> 00:01:11,160
 profundizar.

42
00:01:11,160 --> 00:01:13,270
 La primera es que Novara, que

43
00:01:13,270 --> 00:01:15,390
 es una distribución de Linux

44
00:01:15,390 --> 00:01:17,730
 basada en Fedora y que está hecha

45
00:01:17,730 --> 00:01:20,340
 por el creador de ProtonGE, que

46
00:01:20,340 --> 00:01:23,230
 es una variante de Proton con

47
00:01:23,230 --> 00:01:25,400
 parches más recientes y que

48
00:01:25,400 --> 00:01:27,330
 está enfocado para el que le

49
00:01:27,330 --> 00:01:28,910
 gusta jugar aunque no esté

50
00:01:28,910 --> 00:01:29,740
 usando Steam.

51
00:01:29,740 --> 00:01:31,450
 Ese mismo creador tiene una

52
00:01:31,450 --> 00:01:33,480
 distribución entera porque le

53
00:01:33,480 --> 00:01:35,190
 gusta tener el ecosistema

54
00:01:35,190 --> 00:01:37,450
 completo para jugar y recientemente

55
00:01:37,450 --> 00:01:39,530
 se ha pasado de Gnome a KDE Plasma

56
00:01:39,530 --> 00:01:41,570
 como escritorio principal en su

57
00:01:41,570 --> 00:01:42,760
 distribución.

58
00:01:42,760 --> 00:01:45,190
 Dice que tiene unos requisitos

59
00:01:45,190 --> 00:01:47,290
 técnicos que cumple mejor KDE

60
00:01:47,290 --> 00:01:49,470
 Plasma, como por ejemplo que

61
00:01:49,470 --> 00:01:51,640
 tiene implementado la tasa de

62
00:01:51,640 --> 00:01:54,360
 refresco variable, el VRR, algo

63
00:01:54,360 --> 00:01:56,430
 del driver DRM que no termino

64
00:01:56,430 --> 00:01:58,680
 de entender muy bien pero que

65
00:01:58,680 --> 00:02:01,780
 dice que está mejor en KDE Plasma

66
00:02:01,780 --> 00:02:03,460
 para la red virtual con Wyland,

67
00:02:03,460 --> 00:02:05,280
 el escalado fraccional que es

68
00:02:05,280 --> 00:02:06,950
 algo de los DPI cuando tiene

69
00:02:06,950 --> 00:02:08,710
 una resolución muy grande y

70
00:02:08,710 --> 00:02:10,390
 aunque es una tontería que

71
00:02:10,390 --> 00:02:11,930
 tiene autodebox o sea por

72
00:02:11,930 --> 00:02:14,180
 defecto la posibilidad de arrastrar

73
00:02:14,180 --> 00:02:15,800
 y soltar archivos, iconos y

74
00:02:15,800 --> 00:02:17,120
 todas esas cosas que parece que

75
00:02:17,120 --> 00:02:19,060
 en Gnome no vienen por defecto

76
00:02:19,060 --> 00:02:20,220
 aunque imagino que con alguna

77
00:02:20,220 --> 00:02:21,520
 extensión se podrá arreglar.

78
00:02:21,520 --> 00:02:23,320
 Y como último pues que Valve

79
00:02:23,320 --> 00:02:25,190
 le da bastante soporte porque

80
00:02:25,190 --> 00:02:26,930
 como tiene el Steam Deck que

81
00:02:26,930 --> 00:02:28,780
 usa Plasma por debajo pues que

82
00:02:28,780 --> 00:02:30,720
 está bastante afinado y si hay

83
00:02:30,720 --> 00:02:32,670
 algún problema pues hay buena

84
00:02:32,670 --> 00:02:34,680
 comunicación entre el equipo

85
00:02:34,680 --> 00:02:36,640
 de KDE y Valve para que KDE Plasma

86
00:02:36,640 --> 00:02:38,580
 rinda bien en la aplicación de

87
00:02:38,580 --> 00:02:39,380
 Steam.

88
00:02:39,380 --> 00:02:41,500
 Os dejo un enlace a MuyLinux en

89
00:02:41,500 --> 00:02:43,800
 español y las notas originales

90
00:02:43,800 --> 00:02:46,480
 del proyecto Novara por si queréis

91
00:02:46,480 --> 00:02:47,620
 verlo vosotros.

92
00:02:47,620 --> 00:02:49,580
 Luego también os voy a dejar

93
00:02:49,580 --> 00:02:51,520
 una URL a un vídeo de YouTube

94
00:02:51,520 --> 00:02:53,650
 aunque creo que también está

95
00:02:53,650 --> 00:02:55,390
 en Odysee, la verdad es que

96
00:02:55,390 --> 00:02:57,630
 todavía no me lo he investigado,

97
00:02:57,630 --> 00:02:59,100
 sé que tiene una parte del

98
00:02:59,100 --> 00:03:00,850
 software libre pero hay gente

99
00:03:00,850 --> 00:03:02,620
 que dice que tiene algo raro,

100
00:03:02,620 --> 00:03:05,190
 no sé yo os dejo las dos opciones

101
00:03:05,190 --> 00:03:08,580
 y vosotros elegís con qué markedaros.

102
00:03:08,580 --> 00:03:10,610
 La cosa es que este vídeo que

103
00:03:10,610 --> 00:03:12,640
 se dice en inglés "Why isn't

104
00:03:12,640 --> 00:03:15,190
 every Linux distro shipping KDE"

105
00:03:15,190 --> 00:03:16,510
 o sea por qué no están todas

106
00:03:16,510 --> 00:03:18,070
 las distribuciones de Linux con

107
00:03:18,070 --> 00:03:19,930
 KDE por defecto porque hace un

108
00:03:19,930 --> 00:03:21,880
 repaso Brody Robertson, la

109
00:03:21,880 --> 00:03:23,300
 verdad es que está bastante

110
00:03:23,300 --> 00:03:25,480
 bien, es bastante dinámico y

111
00:03:25,480 --> 00:03:27,280
 te dice vamos a ver si KDE

112
00:03:27,280 --> 00:03:28,990
 está fenomenal por qué no

113
00:03:28,990 --> 00:03:30,600
 está más por defecto en el

114
00:03:30,600 --> 00:03:32,060
 resto de distribuciones porque

115
00:03:32,060 --> 00:03:33,960
 la mayoría lo que viene siendo

116
00:03:33,960 --> 00:03:36,090
 todo lo de Red Hat, Fedora, Red

117
00:03:36,090 --> 00:03:38,420
 Hat Enterprise, incluso OpenSUSE

118
00:03:38,420 --> 00:03:40,600
 aunque OpenSUSE si da soporte a

119
00:03:40,600 --> 00:03:42,880
 KDE Plasma, SUSE por defecto,

120
00:03:42,880 --> 00:03:44,660
 la versión empresarial

121
00:03:44,660 --> 00:03:46,630
 también usa Gnome, o sea hay

122
00:03:46,630 --> 00:03:47,710
 muchas distros que usan por

123
00:03:47,710 --> 00:03:49,460
 defecto Gnome quitando lo que

124
00:03:49,460 --> 00:03:51,460
 acabo de decir de Novara, KDE

125
00:03:51,460 --> 00:03:54,430
 Neon y otras cuantas pero mayoritariamente

126
00:03:54,430 --> 00:03:57,060
 son Gnome y entonces revisa

127
00:03:57,060 --> 00:03:58,180
 por los foros que hay algún

128
00:03:58,180 --> 00:04:00,740
 post de un creador de una

129
00:04:00,740 --> 00:04:03,060
 distribución y dice que la

130
00:04:03,060 --> 00:04:06,180
 manera en la que KDE libera sus

131
00:04:06,180 --> 00:04:07,180
 proyectos

132
00:04:07,180 --> 00:04:09,310
 que son KDE Gears, las aplicaciones,

133
00:04:09,310 --> 00:04:11,190
 Frameworks que son las librerías

134
00:04:11,190 --> 00:04:13,030
 y el escritorio en concreto que

135
00:04:13,030 --> 00:04:14,890
 tienen unos tiempos que no son

136
00:04:14,890 --> 00:04:16,950
 muy cómodos para una distribución

137
00:04:16,950 --> 00:04:18,700
 para ir haciendo las pruebas y

138
00:04:18,700 --> 00:04:20,520
 saber en qué fecha en concreto

139
00:04:20,520 --> 00:04:21,700
 va a poder lanzarse

140
00:04:21,700 --> 00:04:23,200
 entonces dice que eso es uno de

141
00:04:23,200 --> 00:04:24,820
 los principales motivos por los

142
00:04:24,820 --> 00:04:26,500
 que muchas distribuciones dicen

143
00:04:26,500 --> 00:04:28,260
 mira esto es muy complicado Gnome

144
00:04:28,260 --> 00:04:29,800
 lanza estas dos veces al año y

145
00:04:29,800 --> 00:04:31,400
 casi siempre en el mismo mes

146
00:04:31,400 --> 00:04:33,400
 con lo cual como es más predecible

147
00:04:33,400 --> 00:04:33,820
 uso esto

148
00:04:33,820 --> 00:04:35,590
 aunque dice que esto lo mismo

149
00:04:35,590 --> 00:04:37,570
 se arregla porque para KDE 6,

150
00:04:37,570 --> 00:04:39,570
 la mega release, esto que vamos

151
00:04:39,570 --> 00:04:41,360
 a hacer pasando a Qt 6 se ha

152
00:04:41,360 --> 00:04:42,870
 aprobado el cambio de que en

153
00:04:42,870 --> 00:04:44,300
 vez de lanzar tres veces al

154
00:04:44,300 --> 00:04:45,970
 año por ahora se va a empezar

155
00:04:45,970 --> 00:04:47,640
 a lanzar dos meses al año cada

156
00:04:47,640 --> 00:04:48,380
 seis meses

157
00:04:48,380 --> 00:04:49,320
 y se van a poner más de

158
00:04:49,320 --> 00:04:50,770
 acuerdo con las distribuciones

159
00:04:50,770 --> 00:04:52,280
 en qué mes en concreto le vendría

160
00:04:52,280 --> 00:04:53,610
 bien a las distribuciones que

161
00:04:53,610 --> 00:04:54,930
 salieran las versiones de KDE

162
00:04:54,930 --> 00:04:56,310
 Plasma para que ellos pudieran

163
00:04:56,310 --> 00:04:57,580
 empaquetarlas una versión

164
00:04:57,580 --> 00:04:58,220
 estable

165
00:04:58,220 --> 00:05:00,080
 así que si os interesa el tema

166
00:05:00,080 --> 00:05:01,790
 está en inglés pero está

167
00:05:01,790 --> 00:05:03,670
 curioso luego también quería

168
00:05:03,670 --> 00:05:05,080
 deciros que al respecto de KDE

169
00:05:05,080 --> 00:05:07,260
 Plasma 6 sigue el fundraising

170
00:05:07,260 --> 00:05:08,730
 la recogida de fondos que la

171
00:05:08,730 --> 00:05:10,090
 verdad es que va bastante bien

172
00:05:10,090 --> 00:05:11,790
 se están logrando los objetivos

173
00:05:11,790 --> 00:05:13,020
 pero que nunca sobra gente

174
00:05:13,020 --> 00:05:15,640
 si estáis dispuestos a donar

175
00:05:15,640 --> 00:05:18,370
 tiempo en algún algún tipo de

176
00:05:18,370 --> 00:05:20,900
 trabajo que podáis hacer o

177
00:05:20,900 --> 00:05:24,030
 dinero sigue la campaña de recogida

178
00:05:24,030 --> 00:05:27,180
 de colaboradores os dejo el enlace

179
00:05:27,180 --> 00:05:29,170
 también quería hacer un copy

180
00:05:29,170 --> 00:05:30,920
 paste de un artículo que ha

181
00:05:30,920 --> 00:05:32,660
 hecho Víctor HCK yo lo voy a

182
00:05:32,660 --> 00:05:34,900
 resumir aquí y os pongo el enlace

183
00:05:34,900 --> 00:05:35,960
 pero que está muy bien porque

184
00:05:35,960 --> 00:05:37,070
 son tips que a veces se te

185
00:05:37,070 --> 00:05:38,850
 olvidan o gente que ha llegado

186
00:05:38,850 --> 00:05:40,640
 nueva a la distribución no

187
00:05:40,640 --> 00:05:41,900
 sabe qué se puede hacer

188
00:05:41,900 --> 00:05:43,550
 y es tres maneras de abrir

189
00:05:43,550 --> 00:05:45,550
 programas en Plasma uno que es

190
00:05:45,550 --> 00:05:46,820
 muy común yo voy a poner un

191
00:05:46,820 --> 00:05:47,950
 cuarto que no lo ha dicho pero

192
00:05:47,950 --> 00:05:49,180
 que es el que usa todo el mundo

193
00:05:49,180 --> 00:05:52,200
 botón de inicio y ahí se abre

194
00:05:52,200 --> 00:05:52,780
 el menú

195
00:05:52,780 --> 00:05:54,460
 pues otras tres maneras para

196
00:05:54,460 --> 00:05:56,180
 hacerlo más rápido es tú

197
00:05:56,180 --> 00:05:58,240
 puedes pulsar la tecla super y

198
00:05:58,240 --> 00:05:59,710
 entonces se te abre ese menú

199
00:05:59,710 --> 00:06:01,260
 en inglés se llama kickoff

200
00:06:01,260 --> 00:06:02,780
 pero es el menú de inicio eso

201
00:06:02,780 --> 00:06:04,060
 en realidad lo que habla es un

202
00:06:04,060 --> 00:06:05,100
 carruner porque ahora mismo por

203
00:06:05,100 --> 00:06:06,550
 debajo es lo mismo pero vamos

204
00:06:06,550 --> 00:06:07,600
 es el menú de inicio y tú de

205
00:06:07,600 --> 00:06:08,780
 ahí puedes escribir la aplicación

206
00:06:08,780 --> 00:06:09,500
 que quieres

207
00:06:09,500 --> 00:06:11,200
 luego puedes lanzar carruner

208
00:06:11,200 --> 00:06:12,770
 que es lo que se te abre arriba

209
00:06:12,770 --> 00:06:14,320
 o en el centro de la pantalla

210
00:06:14,320 --> 00:06:16,400
 lo puedes configurar que por

211
00:06:16,400 --> 00:06:19,800
 defecto es al f2 o al barra espaciadora

212
00:06:19,800 --> 00:06:20,620
 que es lo que yo uso

213
00:06:20,620 --> 00:06:22,040
 eso por supuesto es configurarme

214
00:06:22,040 --> 00:06:23,560
 en Plasma la combinación de teclas

215
00:06:23,560 --> 00:06:24,710
 que tú quieras pero que por

216
00:06:24,710 --> 00:06:25,970
 defecto si haces eso se te abre

217
00:06:25,970 --> 00:06:27,190
 un desplegable y tú ahí

218
00:06:27,190 --> 00:06:28,670
 puedes escribir la aplicación

219
00:06:28,670 --> 00:06:30,280
 o 500 cosas más que puedes

220
00:06:30,280 --> 00:06:31,580
 activar o hacer ahí

221
00:06:31,580 --> 00:06:32,970
 y luego la tercera o cuarta

222
00:06:32,970 --> 00:06:34,510
 forma de hacerlo que no mucha

223
00:06:34,510 --> 00:06:36,090
 gente conoce es que si estás

224
00:06:36,090 --> 00:06:37,630
 en el escritorio vacío y no

225
00:06:37,630 --> 00:06:39,690
 tiene ninguna ventana con el foco

226
00:06:39,690 --> 00:06:40,930
 si tú empiezas a escribir se

227
00:06:40,930 --> 00:06:42,280
 te va a abrir un carruner y

228
00:06:42,280 --> 00:06:44,240
 ahí directamente puedes escribir

229
00:06:44,240 --> 00:06:45,380
 la aplicación que quieres

230
00:06:45,380 --> 00:06:45,660
 igual

231
00:06:45,660 --> 00:06:48,370
 como penúltima noticia os pongo

232
00:06:48,370 --> 00:06:51,340
 el enlace al artículo de MuyLinux

233
00:06:51,340 --> 00:06:52,840
 en la que han hecho una encuesta

234
00:06:52,840 --> 00:06:57,660
 de el uso de sus usuarios visitantes

235
00:06:57,660 --> 00:06:59,380
 no sé la gente que se mete en

236
00:06:59,380 --> 00:07:01,140
 MuyLinux todos los años hacen

237
00:07:01,140 --> 00:07:03,300
 una encuesta y en esta preguntaban

238
00:07:03,300 --> 00:07:04,940
 sobre qué entorno de escritorio

239
00:07:04,940 --> 00:07:05,500
 usaban

240
00:07:05,500 --> 00:07:08,410
 pues ha salido que gana Gnome

241
00:07:08,410 --> 00:07:12,250
 por un 2% tiene un 32% y un 30

242
00:07:12,250 --> 00:07:14,970
 KDE Plasma no está mal tiene

243
00:07:14,970 --> 00:07:17,320
 una alta tasa de uso y lo

244
00:07:17,320 --> 00:07:19,770
 cierto es que en KDE Plasma no

245
00:07:19,770 --> 00:07:22,340
 le dimos mucho volo o sea no

246
00:07:22,340 --> 00:07:23,660
 hicimos campaña para que la

247
00:07:23,660 --> 00:07:25,050
 gente votara en esta encuesta

248
00:07:25,050 --> 00:07:26,220
 porque es una encuesta de

249
00:07:26,220 --> 00:07:27,100
 google docs

250
00:07:27,100 --> 00:07:28,540
 entonces sabemos que

251
00:07:28,540 --> 00:07:30,560
 normalmente a la comunidad no

252
00:07:30,560 --> 00:07:32,560
 le hace mucha gracia hacer ese

253
00:07:32,560 --> 00:07:34,320
 tipo de encuestas donde al

254
00:07:34,320 --> 00:07:36,720
 final está gmail y google chupando

255
00:07:36,720 --> 00:07:38,560
 algo de datos con lo cual dejamos

256
00:07:38,560 --> 00:07:39,940
 que fueran los propios usuarios

257
00:07:39,940 --> 00:07:41,690
 que típicamente entran en MuyLinux

258
00:07:41,690 --> 00:07:43,690
 los que contestaran y no ha salido

259
00:07:43,690 --> 00:07:45,580
 mal del todo la encuesta

260
00:07:45,580 --> 00:07:47,510
 y para terminar si a Jorge Lama

261
00:07:47,510 --> 00:07:49,090
 le parece bien porque esto lo

262
00:07:49,090 --> 00:07:50,830
 tiene que hacer él os voy a

263
00:07:50,830 --> 00:07:52,250
 dejar en vez de una broma de

264
00:07:52,250 --> 00:07:54,040
 otro podcast aunque lo cierto

265
00:07:54,040 --> 00:07:56,080
 es que últimamente he conocido

266
00:07:56,080 --> 00:07:58,100
 no me da la vida si os gusta el

267
00:07:58,100 --> 00:08:00,640
 desarrollo web os lo recomiendo

268
00:08:00,640 --> 00:08:02,220
 porque son dos chicas una que

269
00:08:02,220 --> 00:08:03,820
 creo que es de aquí de cartagena

270
00:08:03,820 --> 00:08:05,060
 que yo empecé a escucharla

271
00:08:05,060 --> 00:08:06,650
 ahora pero creo que el podcast

272
00:08:06,650 --> 00:08:07,820
 ha subido como la espuma y

273
00:08:07,820 --> 00:08:09,420
 están super pro

274
00:08:09,420 --> 00:08:10,930
 yo que estoy por los primeros

275
00:08:10,930 --> 00:08:12,560
 episodios poniéndome al día

276
00:08:12,560 --> 00:08:14,210
 pero está muy muy interesante

277
00:08:14,210 --> 00:08:15,840
 muy dinámico no hablan solo de

278
00:08:15,840 --> 00:08:17,490
 desarrollo también hablan de

279
00:08:17,490 --> 00:08:19,150
 burnout de los problemas de las

280
00:08:19,150 --> 00:08:20,680
 mujeres en los entornos hiper

281
00:08:20,680 --> 00:08:22,430
 masculinizados no sé un

282
00:08:22,430 --> 00:08:23,900
 podcast que me ha gustado lo

283
00:08:23,900 --> 00:08:25,620
 recomiendo pero lo que os iba a

284
00:08:25,620 --> 00:08:27,020
 decir es que como final de

285
00:08:27,020 --> 00:08:29,840
 episodio si Jorge Lama le

286
00:08:29,840 --> 00:08:31,660
 parece una buena idea os voy a

287
00:08:31,660 --> 00:08:34,310
 dejar una cancioncilla unas

288
00:08:34,310 --> 00:08:36,390
 cosas de estas que hace Yoyo

289
00:08:36,390 --> 00:08:38,240
 que en esta ocasión es ama

290
00:08:38,240 --> 00:08:38,940
 Plasma

291
00:08:38,940 --> 00:08:40,030
 con esto os dejo y hasta el

292
00:08:40,030 --> 00:08:40,940
 próximo episodio

293
00:08:40,940 --> 00:08:42,940
 (música)

294
00:08:42,940 --> 00:08:44,940
 (música)

295
00:08:44,940 --> 00:08:46,940
 (música)

296
00:08:46,940 --> 00:08:48,940
 (música)

297
00:08:48,940 --> 00:08:50,940
 (música)

298
00:08:50,940 --> 00:08:52,940
 (música)

299
00:08:52,940 --> 00:08:54,940
 (música)

300
00:08:54,940 --> 00:08:56,940
 (música)

301
00:08:56,940 --> 00:08:58,940
 (música)

302
00:08:58,940 --> 00:09:00,940
 bajemos juntos

303
00:09:00,940 --> 00:09:02,940
 una distro Linux

304
00:09:02,940 --> 00:09:04,940
 (música)

305
00:09:04,940 --> 00:09:06,940
 y tiremos el Windows

306
00:09:06,940 --> 00:09:08,940
 (música)

307
00:09:08,940 --> 00:09:10,940
 por la ventana

308
00:09:10,940 --> 00:09:12,940
 (música)

309
00:09:12,940 --> 00:09:14,940
 (música)

310
00:09:14,940 --> 00:09:16,940
 quemar la ISO

311
00:09:16,940 --> 00:09:18,940
 me llena de alegría

312
00:09:18,940 --> 00:09:20,940
 (música)

313
00:09:20,940 --> 00:09:22,940
 (música)

314
00:09:22,940 --> 00:09:24,940
 dejemos todo lo demás

315
00:09:24,940 --> 00:09:26,940
 para otro día

316
00:09:26,940 --> 00:09:28,940
 (música)

317
00:09:28,940 --> 00:09:30,940
 (música)

318
00:09:30,940 --> 00:09:32,940
 ya voy a instalarte

319
00:09:32,940 --> 00:09:34,940
 pongo todo de mi parte

320
00:09:34,940 --> 00:09:36,940
 y uso gpartes

321
00:09:36,940 --> 00:09:38,940
 para espaciotitudarte

322
00:09:38,940 --> 00:09:40,940
 amar es poner

323
00:09:40,940 --> 00:09:42,940
 de la ars

324
00:09:42,940 --> 00:09:44,940
 con entorno Plasma

325
00:09:44,940 --> 00:09:46,940
 sin tener que esperar

326
00:09:46,940 --> 00:09:48,940
 (música)

327
00:09:48,940 --> 00:09:50,940
 amo a Plasma

328
00:09:50,940 --> 00:09:52,940
 (a Plasma)

329
00:09:52,940 --> 00:09:54,940
 tanto que le pido matrimonio

330
00:09:54,940 --> 00:09:56,940
 (música)

331
00:09:56,940 --> 00:09:58,940
 amo a Plasma

332
00:09:58,940 --> 00:10:00,940
 (a Plasma)

333
00:10:00,940 --> 00:10:02,940
 me libera de todos mis

334
00:10:02,940 --> 00:10:04,940
 demonios

335
00:10:04,940 --> 00:10:06,940
 (música)

336
00:10:06,940 --> 00:10:08,940
 no voy a arrancar ma Windows

337
00:10:08,940 --> 00:10:10,940
 que lo destruya

338
00:10:10,940 --> 00:10:12,940
 si seré yo

339
00:10:12,940 --> 00:10:14,940
 (música)

340
00:10:14,940 --> 00:10:16,940
 joven, recuerda que Linux

341
00:10:16,940 --> 00:10:18,940
 nace del respeto

342
00:10:18,940 --> 00:10:20,940
 que no hay nada más hermoso en

343
00:10:20,940 --> 00:10:20,940
 una pc

344
00:10:20,940 --> 00:10:22,940
 que ponerle software libre

345
00:10:22,940 --> 00:10:24,940
 ese momento maravilloso

346
00:10:24,940 --> 00:10:26,940
 que es la liberación de tu

347
00:10:26,940 --> 00:10:26,940
 hardware

348
00:10:26,940 --> 00:10:28,940
 tu paciencia tendrá recompensa

349
00:10:28,940 --> 00:10:30,940
 (música)

350
00:10:30,940 --> 00:10:32,940
 amo a Plasma

351
00:10:32,940 --> 00:10:34,940
 (a Plasma)

352
00:10:34,940 --> 00:10:36,940
 tanto que le pido matrimonio

353
00:10:36,940 --> 00:10:38,940
 (música)

354
00:10:38,940 --> 00:10:40,940
 amo a Plasma

355
00:10:40,940 --> 00:10:42,940
 (a Plasma)

356
00:10:42,940 --> 00:10:44,940
 me libera de todos mis demonios

357
00:10:44,940 --> 00:10:46,940
 (música)

358
00:10:46,940 --> 00:10:48,940
 amo a Plasma

359
00:10:48,940 --> 00:10:50,940
 (a Plasma)

360
00:10:50,940 --> 00:10:52,940
 tanto que le pido matrimonio

361
00:10:52,940 --> 00:10:54,940
 (música)

362
00:10:54,940 --> 00:10:56,940
 amo a Plasma

363
00:10:56,940 --> 00:10:58,940
 (a Plasma)

364
00:10:58,940 --> 00:11:00,940
 me libera de todos mis demonios

365
00:11:00,940 --> 00:11:02,940
 (música)

366
00:11:02,940 --> 00:11:04,940
 no voy a arrancar ma Windows

367
00:11:04,940 --> 00:11:06,940
 que lo destruya

368
00:11:06,940 --> 00:11:08,940
 si seré yo

369
00:11:08,940 --> 00:11:10,940
 ¡Seyo!
