1
00:00:00,000 --> 00:00:02,800
Buenas, soy David Marzal y

2
00:00:02,800 --> 00:00:05,760
tengo el placer de presentaros

3
00:00:05,760 --> 00:00:08,200
un nuevo subproyecto del área

4
00:00:08,200 --> 00:00:10,760
de comunicativa de KDE España.

5
00:00:10,760 --> 00:00:12,580
En esta ocasión vamos a crear

6
00:00:12,580 --> 00:00:15,040
unos canales en los que se pondrá

7
00:00:15,040 --> 00:00:18,240
vídeo, aquí por ejemplo en PeerTube,

8
00:00:18,240 --> 00:00:20,530
si entráis a la instancia en

9
00:00:20,530 --> 00:00:23,200
la que usamos en KDE, que es un

10
00:00:23,200 --> 00:00:25,050
poco rara de escribir, pero la

11
00:00:25,050 --> 00:00:26,730
tenéis en las notas del

12
00:00:26,730 --> 00:00:28,030
programa, y buscamos a la

13
00:00:28,030 --> 00:00:29,480
cuenta de KDE España,

14
00:00:29,480 --> 00:00:31,270
lo bueno que tiene PeerTube es

15
00:00:31,270 --> 00:00:33,160
que permite hacer canales para

16
00:00:33,160 --> 00:00:34,880
distinguir lo que tenemos

17
00:00:34,880 --> 00:00:35,480
puesto.

18
00:00:35,480 --> 00:00:37,690
Entonces en ver cuenta aquí

19
00:00:37,690 --> 00:00:39,290
tenemos los canales, el de KDE

20
00:00:39,290 --> 00:00:41,480
Mii donde están los vídeos de

21
00:00:41,480 --> 00:00:42,120
los eventos

22
00:00:42,120 --> 00:00:44,120
pasados, el de KDE España

23
00:00:44,120 --> 00:00:46,130
donde están los podcasts y

24
00:00:46,130 --> 00:00:48,640
hemos creado dos canales nuevos

25
00:00:48,640 --> 00:00:50,240
que son Pildoras

26
00:00:50,240 --> 00:00:52,530
y Books y sus apaños. Pues lo

27
00:00:52,530 --> 00:00:54,840
que vamos a poner aquí son en

28
00:00:54,840 --> 00:00:58,200
Books y sus apaños serán

29
00:00:58,200 --> 00:00:59,200
problemas

30
00:00:59,200 --> 00:01:02,000
que nos encontramos los usuarios

31
00:01:02,000 --> 00:01:04,100
de KDE y la solución para

32
00:01:04,100 --> 00:01:07,080
poder hacer un workaround o un

33
00:01:07,080 --> 00:01:07,720
apaño

34
00:01:07,720 --> 00:01:09,700
para poder funcionar mientras

35
00:01:09,700 --> 00:01:12,070
que los desarrolladores solucionan

36
00:01:12,070 --> 00:01:13,600
el problema. Una vez que esté

37
00:01:13,600 --> 00:01:14,200
solucionado

38
00:01:14,200 --> 00:01:16,190
ya en el software y no sea

39
00:01:16,190 --> 00:01:19,000
necesario pasaremos los vídeos

40
00:01:19,000 --> 00:01:21,240
de aquí a otro canal que se

41
00:01:21,240 --> 00:01:21,560
llamará

42
00:01:21,560 --> 00:01:24,050
solucionado o fixes y luego

43
00:01:24,050 --> 00:01:25,620
también vamos a crear Pildoras

44
00:01:25,620 --> 00:01:27,880
que serán vídeos explicando

45
00:01:27,880 --> 00:01:28,160
alguna

46
00:01:28,160 --> 00:01:30,920
funcionalidad en concreto.

47
00:01:30,920 --> 00:01:33,300
Entonces yo para empezar os

48
00:01:33,300 --> 00:01:35,960
quiero enseñar un bug que es

49
00:01:35,960 --> 00:01:36,160
de los

50
00:01:36,160 --> 00:01:37,920
pocos que yo tenía en Arch

51
00:01:37,920 --> 00:01:40,000
Linux que me volvían un poco

52
00:01:40,000 --> 00:01:41,690
loco y gracias al buscador y

53
00:01:41,690 --> 00:01:44,000
poner context

54
00:01:44,000 --> 00:01:46,250
menus y Wayland de los seis que

55
00:01:46,250 --> 00:01:48,320
entré este es el que más se

56
00:01:48,320 --> 00:01:50,710
parece. Os lo voy a enseñar,

57
00:01:50,710 --> 00:01:50,960
lo que

58
00:01:50,960 --> 00:01:52,920
dice es que los menús contextuales

59
00:01:52,920 --> 00:01:54,440
salen en un sitio que no es. Si

60
00:01:54,440 --> 00:01:55,760
yo por ejemplo abro Dolphin

61
00:01:57,120 --> 00:01:59,290
y le doy al botón derecho sale

62
00:01:59,290 --> 00:02:01,750
aquí perfectamente, pero el

63
00:02:01,750 --> 00:02:02,680
problema que yo tenía

64
00:02:02,680 --> 00:02:05,590
era que yo tengo un doble

65
00:02:05,590 --> 00:02:06,640
monitor

66
00:02:06,640 --> 00:02:09,270
y aquí como podéis ver yo ya

67
00:02:09,270 --> 00:02:11,540
tengo el parche aplicado. Yo el

68
00:02:11,540 --> 00:02:13,600
doble monitor lo tenía así

69
00:02:13,600 --> 00:02:15,540
centrado para que cuando paso

70
00:02:15,540 --> 00:02:17,300
el ratón por la pantalla al

71
00:02:17,300 --> 00:02:18,800
pasar a la derecha al otro

72
00:02:18,800 --> 00:02:19,120
monitor

73
00:02:19,120 --> 00:02:22,030
que vosotros no podéis verlo

74
00:02:22,030 --> 00:02:24,410
siguiera una línea recta

75
00:02:24,410 --> 00:02:27,130
entonces aquí seguía el ratón.

76
00:02:27,130 --> 00:02:27,760
¿Qué pasa

77
00:02:27,760 --> 00:02:29,180
cuando lo tengo aquí arriba el

78
00:02:29,180 --> 00:02:30,700
monitor? Que yo estoy haciendo

79
00:02:30,700 --> 00:02:32,440
aquí y de repente salta para

80
00:02:32,440 --> 00:02:32,680
arriba.

81
00:02:32,680 --> 00:02:35,510
Ahora el bug que yo tenía es

82
00:02:35,510 --> 00:02:38,860
este, si os fijáis ahora sale

83
00:02:38,860 --> 00:02:40,960
muy abajo el menú contextual,

84
00:02:40,960 --> 00:02:41,960
eso es

85
00:02:41,960 --> 00:02:44,970
como que desplaza todo este

86
00:02:44,970 --> 00:02:47,980
tamaño el menú contextual. Yo

87
00:02:47,980 --> 00:02:51,200
no sé si esto pasará en X11,

88
00:02:51,200 --> 00:02:54,050
yo os iré poniendo todo lo que

89
00:02:54,050 --> 00:02:56,560
me pase a mí en Wayland y en

90
00:02:56,560 --> 00:02:58,440
Arch Linux que viene siendo la

91
00:02:58,440 --> 00:02:58,760
última

92
00:02:58,760 --> 00:03:01,240
versión de Plasma. La solución

93
00:03:01,240 --> 00:03:03,310
que decían aquí es bastante

94
00:03:03,310 --> 00:03:05,760
sencilla, aquí alguien subió

95
00:03:05,760 --> 00:03:06,160
un vídeo

96
00:03:06,160 --> 00:03:08,270
explicando exactamente qué es

97
00:03:08,270 --> 00:03:10,180
lo que me pasaba a mí y en uno

98
00:03:10,180 --> 00:03:12,280
de los mensajes explicaba cómo

99
00:03:12,280 --> 00:03:13,480
solucionarlo. Este soy yo

100
00:03:13,480 --> 00:03:14,750
diciendo que me sigue pasando

101
00:03:14,750 --> 00:03:15,840
lo mismo por si le sirve a

102
00:03:15,840 --> 00:03:16,080
alguien.

103
00:03:16,080 --> 00:03:19,370
Entonces la solución es sencilla,

104
00:03:19,370 --> 00:03:22,100
coge los bornes en el 00 que

105
00:03:22,100 --> 00:03:25,000
viene siendo en la línea

106
00:03:25,000 --> 00:03:25,120
horizontal

107
00:03:25,120 --> 00:03:27,340
con el otro monitor, le das a

108
00:03:27,340 --> 00:03:31,430
aplicar, mantenemos y entonces

109
00:03:31,430 --> 00:03:34,310
ya todos los menús contextuales

110
00:03:34,310 --> 00:03:34,600
que

111
00:03:34,600 --> 00:03:36,530
no pasaba en todas las aplicaciones,

112
00:03:36,530 --> 00:03:38,010
porque aquí por ejemplo en

113
00:03:38,010 --> 00:03:40,000
Firefox no pasaba, pero en la

114
00:03:40,000 --> 00:03:41,730
mayoría de aplicaciones se nos

115
00:03:41,730 --> 00:03:43,380
iba el menú contextual. Con

116
00:03:43,380 --> 00:03:45,120
esto ya lo tenéis solucionado.

117
00:03:45,120 --> 00:03:48,000
Pues este sería el tipo de

118
00:03:48,000 --> 00:03:50,920
vídeo de Bugs y sus apaños.

119
00:03:50,920 --> 00:03:53,320
Una vez puestos en

120
00:03:53,320 --> 00:03:56,240
en este canal, en el de Bugs y

121
00:03:56,240 --> 00:03:58,800
sus apaños, lo pasaríamos

122
00:03:58,800 --> 00:04:01,930
también a la cuenta de YouTube.

123
00:04:01,930 --> 00:04:02,040
Como

124
00:04:02,040 --> 00:04:03,620
YouTube no permite hacer canales

125
00:04:03,620 --> 00:04:05,200
específicos, lo que haríamos

126
00:04:05,200 --> 00:04:06,560
sería ponerlo en una lista

127
00:04:06,560 --> 00:04:07,120
separada.

128
00:04:07,120 --> 00:04:08,400
Ya estáis vos que tenemos las

129
00:04:08,400 --> 00:04:09,800
listas de Akademy, os crearé

130
00:04:09,800 --> 00:04:12,360
una lista correspondiente. Para

131
00:04:12,360 --> 00:04:12,480
las

132
00:04:12,480 --> 00:04:14,620
cosas que sólo con sonido

133
00:04:14,620 --> 00:04:16,690
tengan sentido y se puedan

134
00:04:16,690 --> 00:04:19,680
entender, también los colgaré

135
00:04:19,680 --> 00:04:20,720
en KDE Express.

136
00:04:20,720 --> 00:04:22,490
Si son muy cortos, a

137
00:04:22,490 --> 00:04:23,970
lo mejor junto a varios y los

138
00:04:23,970 --> 00:04:26,920
cuelgo como un episodio. Si tenéis

139
00:04:26,920 --> 00:04:28,810
alguna cosa que os pase a vosotros

140
00:04:28,810 --> 00:04:30,300
y la tengáis controlada o

141
00:04:30,300 --> 00:04:32,080
alguna cosa que queráis que

142
00:04:32,080 --> 00:04:32,400
pongamos,

143
00:04:32,400 --> 00:04:34,860
podéis ponérnoslo en el mensaje.

144
00:04:34,860 --> 00:04:36,590
Ya sabéis que yo trato con

145
00:04:36,590 --> 00:04:39,160
Wayland y ahora mismo con 5.25.5.

146
00:04:39,160 --> 00:04:41,790
Bueno, espero que os guste este

147
00:04:41,790 --> 00:04:43,820
nuevo formato y nos vemos

148
00:04:43,820 --> 00:04:45,680
pronto. Hasta luego.

149
00:04:45,680 --> 00:04:55,680
[AUDIO_EN_BLANCO]

