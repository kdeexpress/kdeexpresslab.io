1
00:00:00,800 --> 00:00:04,150
 Buenas, 11 de diciembre de 2024,

2
00:00:04,150 --> 00:00:06,230
 KDE Express está de vuelta con

3
00:00:06,230 --> 00:00:07,720
 vuestra ración de noticias del

4
00:00:07,720 --> 00:00:09,640
 mundillo, KDE y software libre

5
00:00:09,640 --> 00:00:10,360
 en general.

6
00:00:10,360 --> 00:00:12,040
 Hoy os traigo una cosa que a

7
00:00:12,040 --> 00:00:13,570
 mí particularmente me gusta

8
00:00:13,570 --> 00:00:15,300
 mucho, que es que una utilidad

9
00:00:15,300 --> 00:00:16,720
 de línea de comandos que

10
00:00:16,720 --> 00:00:18,040
 estaba muy chula,

11
00:00:18,040 --> 00:00:20,560
 como OxyPNG, alguien de KDE, en

12
00:00:20,560 --> 00:00:22,630
 este caso Carl Schwann, ha

13
00:00:22,630 --> 00:00:25,060
 hecho una GUI, una aplicación

14
00:00:25,060 --> 00:00:27,760
 gráfica, para poder usarla.

15
00:00:27,900 --> 00:00:29,680
 No solo alrededor de esta aplicación,

16
00:00:29,680 --> 00:00:30,540
 sino de varias.

17
00:00:30,540 --> 00:00:33,240
 Y lo que ha hecho es OptiImage,

18
00:00:33,240 --> 00:00:35,770
 es una aplicación que te

19
00:00:35,770 --> 00:00:38,330
 permite comprimir imágenes de

20
00:00:38,330 --> 00:00:40,460
 PNG, JPG, WebP, NSVG,

21
00:00:40,460 --> 00:00:42,710
 y sin que tú visualmente prácticamente

22
00:00:42,710 --> 00:00:44,760
 veas nada, le reducen el tamaño.

23
00:00:44,760 --> 00:00:46,220
 Y aparte te dice cuánto ha

24
00:00:46,220 --> 00:00:47,560
 conseguido reducir.

25
00:00:47,560 --> 00:00:49,560
 Esto yo le he tirado bastantes

26
00:00:49,560 --> 00:00:51,650
 horas para conseguir que la web

27
00:00:51,650 --> 00:00:53,730
 y los logotipos que aparecen en

28
00:00:53,730 --> 00:00:55,180
 el feed de KDE Express

29
00:00:55,800 --> 00:00:57,270
 con tecnologías libres ocupen

30
00:00:57,270 --> 00:00:58,280
 lo menos posible.

31
00:00:58,280 --> 00:00:59,280
 Ya sabéis que esto hace que en

32
00:00:59,280 --> 00:01:00,240
 vuestros móviles vaya más

33
00:01:00,240 --> 00:01:01,660
 rápido, gasten menos batería

34
00:01:01,660 --> 00:01:03,410
 y contamine menos al medio

35
00:01:03,410 --> 00:01:04,220
 ambiente,

36
00:01:04,220 --> 00:01:05,720
 aunque no sean kilos y kilos.

37
00:01:05,720 --> 00:01:07,710
 Cualquier ayuda es buena, más

38
00:01:07,710 --> 00:01:09,920
 cuando hace que todo vaya mejor.

39
00:01:09,920 --> 00:01:11,340
 Pues que sepáis que ya están

40
00:01:11,340 --> 00:01:12,770
 empaquetando la aplicación

41
00:01:12,770 --> 00:01:14,370
 para que dentro de poco la podáis

42
00:01:14,370 --> 00:01:16,620
 disfrutar en cualquier entorno

43
00:01:16,620 --> 00:01:17,620
 de escritorio

44
00:01:17,620 --> 00:01:19,740
 o sistema operativo Linux estará

45
00:01:19,740 --> 00:01:20,600
 disponible.

46
00:01:20,600 --> 00:01:22,060
 OptiImage.

47
00:01:22,060 --> 00:01:24,490
 En la ronda habitual de actualizaciones,

48
00:01:24,490 --> 00:01:27,390
 pues tenemos que digiKam trae

49
00:01:27,390 --> 00:01:28,980
 mejoras de reconocimiento

50
00:01:28,980 --> 00:01:29,540
 facial

51
00:01:29,540 --> 00:01:31,210
 y han actualizado la librería

52
00:01:31,210 --> 00:01:33,220
 interna de decodificación RAW.

53
00:01:33,220 --> 00:01:34,720
 La que gusta la fotografía,

54
00:01:34,720 --> 00:01:36,160
 sabréis de qué va esto.

55
00:01:36,160 --> 00:01:37,590
 A mí lo que más me interesa

56
00:01:37,590 --> 00:01:39,210
 es la parte del reconocimiento

57
00:01:39,210 --> 00:01:39,660
 facial,

58
00:01:39,900 --> 00:01:42,390
 porque yo lo uso como biblioteca

59
00:01:42,390 --> 00:01:44,050
 local y como no me gusta que se

60
00:01:44,050 --> 00:01:45,360
 suban ni me identifiquen por

61
00:01:45,360 --> 00:01:46,800
 internet, Google y toda esta

62
00:01:46,800 --> 00:01:47,160
 gente,

63
00:01:47,160 --> 00:01:48,760
 digiKam es lo que mejor he

64
00:01:48,760 --> 00:01:50,440
 encontrado para saber,

65
00:01:50,440 --> 00:01:51,550
 encuéntrame todas las fotos de

66
00:01:51,550 --> 00:01:53,460
 mi hermano o las fotos en las

67
00:01:53,460 --> 00:01:56,260
 que sale mi hija con su prima.

68
00:01:56,340 --> 00:01:57,630
 La verdad es que hasta ahora

69
00:01:57,630 --> 00:01:59,360
 funcionaba bastante bien, pero

70
00:01:59,360 --> 00:02:01,560
 requería un trabajo manual,

71
00:02:01,560 --> 00:02:03,000
 evidentemente no es perfecto,

72
00:02:03,000 --> 00:02:04,470
 pues siguen mejorando este tipo

73
00:02:04,470 --> 00:02:05,040
 de cosas.

74
00:02:05,040 --> 00:02:07,000
 Luego os traigo un enlace, que

75
00:02:07,000 --> 00:02:08,320
 este sí que merece la pena que

76
00:02:08,320 --> 00:02:09,960
 entráis sí o sí, no vale con

77
00:02:09,960 --> 00:02:11,320
 mi simple comentario,

78
00:02:11,320 --> 00:02:13,570
 y es un resumen, un listado de

79
00:02:13,570 --> 00:02:15,580
 todas las cosas que puedes

80
00:02:15,580 --> 00:02:17,580
 buscar con KRunner.

81
00:02:18,080 --> 00:02:20,370
 Sabéis que KRunner es tanto

82
00:02:20,370 --> 00:02:22,410
 el botón de inicio como el al

83
00:02:22,410 --> 00:02:24,050
 espacio, lo tengo yo, que os

84
00:02:24,050 --> 00:02:25,260
 sale ahí arriba del todo,

85
00:02:25,260 --> 00:02:26,410
 aunque lo podéis configurar

86
00:02:26,410 --> 00:02:28,080
 para que salga en el medio, hacéis

87
00:02:28,080 --> 00:02:29,250
 combinación de teclas y empezáis

88
00:02:29,250 --> 00:02:29,860
 a escribir.

89
00:02:29,860 --> 00:02:32,160
 Aquí podéis abrir aplicaciones,

90
00:02:32,160 --> 00:02:34,280
 ficheros, hacer búsquedas por

91
00:02:34,280 --> 00:02:35,060
 internet,

92
00:02:35,060 --> 00:02:36,530
 podéis cambiar la prioridad de

93
00:02:36,530 --> 00:02:37,830
 cómo queréis que os salgan

94
00:02:37,830 --> 00:02:38,760
 las búsquedas.

95
00:02:38,760 --> 00:02:40,110
 Yo ahora que tengo un monitor

96
00:02:40,110 --> 00:02:41,390
 muy largo y ya no tengo doble

97
00:02:41,390 --> 00:02:41,980
 monitor,

98
00:02:41,980 --> 00:02:43,450
 lo que he hecho es que pueda

99
00:02:43,450 --> 00:02:44,820
 cambiar de ventana.

100
00:02:45,000 --> 00:02:46,740
 Entonces cuando pongo console,

101
00:02:46,740 --> 00:02:48,540
 no quiero que me abra una nueva

102
00:02:48,540 --> 00:02:49,620
 instancia de console,

103
00:02:49,620 --> 00:02:50,740
 lo que quiero es que me abra la

104
00:02:50,740 --> 00:02:52,080
 ventana que tengo de console,

105
00:02:52,080 --> 00:02:53,040
 pues tú ahí puedes cambiar

106
00:02:53,040 --> 00:02:53,680
 las preferencias.

107
00:02:53,680 --> 00:02:55,990
 Y en esta web os hacen un detalle

108
00:02:55,990 --> 00:02:58,490
 de las 500 cosas que podéis

109
00:02:58,490 --> 00:02:59,860
 buscar en plan,

110
00:02:59,860 --> 00:03:02,880
 pues si pongo wp2.kde que me

111
00:03:02,880 --> 00:03:04,880
 abra la Wikipedia,

112
00:03:04,880 --> 00:03:08,980
 si abro un traductor, si abro

113
00:03:08,980 --> 00:03:11,820
 cualquier cosa, está prácticamente

114
00:03:11,820 --> 00:03:13,320
 ahí ya predefinido,

115
00:03:13,500 --> 00:03:14,650
 o vosotros podéis poner las puestas

116
00:03:14,650 --> 00:03:15,100
 propias.

117
00:03:15,100 --> 00:03:17,000
 Y os hacen un listado muy concienzudo.

118
00:03:17,000 --> 00:03:19,130
 En el típico artículo que suelen

119
00:03:19,130 --> 00:03:20,760
 poner todas las semanas de

120
00:03:20,760 --> 00:03:21,580
 Point Stick,

121
00:03:21,580 --> 00:03:24,360
 hacen unas retrospectivas sobre

122
00:03:24,360 --> 00:03:27,380
 cómo ha funcionado la ventana

123
00:03:27,380 --> 00:03:28,680
 que se implementó hace poco en

124
00:03:28,680 --> 00:03:29,600
 Plasma 6.2,

125
00:03:29,600 --> 00:03:31,870
 de que una vez al año sale una

126
00:03:31,870 --> 00:03:33,280
 ventanita que dice

127
00:03:33,280 --> 00:03:34,890
 oye, el proyecto KDE está

128
00:03:34,890 --> 00:03:36,830
 hecho por personas, esto requiere

129
00:03:36,830 --> 00:03:38,120
 tiempo, estaría muy bien que

130
00:03:38,120 --> 00:03:38,580
 donaras,

131
00:03:38,580 --> 00:03:41,040
 si no lo puedes desactivar.

132
00:03:41,040 --> 00:03:43,180
 Pues esto ha hecho que en el

133
00:03:43,180 --> 00:03:45,350
 poco tiempo desde que salió la

134
00:03:45,350 --> 00:03:46,000
 ventana,

135
00:03:46,000 --> 00:03:47,860
 se han duplicado las donaciones

136
00:03:47,860 --> 00:03:49,680
 habituales al proyecto KDE.

137
00:03:49,680 --> 00:03:51,860
 Y esto teniendo en cuenta que

138
00:03:51,860 --> 00:03:54,200
 simplemente le ha salido a la

139
00:03:54,200 --> 00:03:55,520
 gente que usa Arch,

140
00:03:55,520 --> 00:03:58,290
 o usa KDE Neon, o a lo mejor

141
00:03:58,290 --> 00:04:00,900
 OpenSUSE Tumbleweed, o sea,

142
00:04:00,900 --> 00:04:02,020
 gente que está muy muy a la

143
00:04:02,020 --> 00:04:02,580
 última.

144
00:04:02,580 --> 00:04:04,560
 O sea, quien esté con Kubuntu,

145
00:04:04,560 --> 00:04:07,250
 no sé si Fedora, o desde luego

146
00:04:07,250 --> 00:04:07,840
 Debian,

147
00:04:07,840 --> 00:04:09,220
 vamos que la mayoría de gente

148
00:04:09,220 --> 00:04:10,700
 no está en Plasma 6.2,

149
00:04:10,700 --> 00:04:12,160
 o sea, desde la poca gente que

150
00:04:12,160 --> 00:04:13,880
 está a la última y le ha salido,

151
00:04:14,520 --> 00:04:15,840
 la recepción ha sido muy buena.

152
00:04:15,840 --> 00:04:16,920
 También han hecho una

153
00:04:16,920 --> 00:04:18,580
 investigación por internet, en

154
00:04:18,580 --> 00:04:20,900
 los foros, en Ready, en Lemmy,

155
00:04:20,900 --> 00:04:22,870
 y han visto que hay prácticamente

156
00:04:22,870 --> 00:04:24,590
 casi nadie quejándose de que

157
00:04:24,590 --> 00:04:25,820
 le haya salido una ventana.

158
00:04:25,820 --> 00:04:27,460
 En Mastodon tuvimos un debate

159
00:04:27,460 --> 00:04:29,260
 sobre el titular de MuyLinux,

160
00:04:29,260 --> 00:04:30,890
 que el artículo estaba bien,

161
00:04:30,890 --> 00:04:32,260
 pero el titular era un poco

162
00:04:32,260 --> 00:04:32,880
 clickbait,

163
00:04:32,880 --> 00:04:35,000
 y decía que si KDE tenía

164
00:04:35,000 --> 00:04:37,380
 publicidad o hacía spam,

165
00:04:37,380 --> 00:04:38,720
 aunque en el fondo decían que

166
00:04:38,720 --> 00:04:39,580
 estaba muy bien,

167
00:04:39,580 --> 00:04:41,170
 había gente que esto llamarlo

168
00:04:41,170 --> 00:04:43,240
 spam y publicidad le parece excesivo,

169
00:04:43,340 --> 00:04:44,810
 que un proyecto de software

170
00:04:44,810 --> 00:04:46,870
 libre que se da gratis te ponga

171
00:04:46,870 --> 00:04:48,920
 una ventana una vez al año

172
00:04:48,920 --> 00:04:50,480
 que tú puedes quitar, llamar

173
00:04:50,480 --> 00:04:51,800
 eso spam o publicidad.

174
00:04:51,800 --> 00:04:53,420
 En el sentido estricto, un poco

175
00:04:53,420 --> 00:04:54,980
 de publicidad podría ser que

176
00:04:54,980 --> 00:04:55,380
 fuera,

177
00:04:55,380 --> 00:04:57,020
 porque se le está dando publicidad

178
00:04:57,020 --> 00:04:58,420
 a las opciones de donación,

179
00:04:58,420 --> 00:05:00,040
 pero vamos, era simplemente por

180
00:05:00,040 --> 00:05:00,980
 la semántica.

181
00:05:00,980 --> 00:05:02,270
 En el fondo, la discusión,

182
00:05:02,270 --> 00:05:03,560
 todo el mundo estaba de acuerdo

183
00:05:03,560 --> 00:05:05,710
 de que ha sido una acción que

184
00:05:05,710 --> 00:05:06,800
 ha funcionado

185
00:05:06,800 --> 00:05:09,370
 y que puede marcar el camino de

186
00:05:09,370 --> 00:05:11,960
 que a la gente hay que animarla

187
00:05:11,960 --> 00:05:14,420
 y animarla fácil para que donen.

188
00:05:14,420 --> 00:05:15,840
 Que no es que estemos todos

189
00:05:15,840 --> 00:05:18,100
 ahí con el bolsillo agarrado

190
00:05:18,100 --> 00:05:19,480
 y nos soltemos un duro, sino

191
00:05:19,480 --> 00:05:21,100
 que a la gente hay que hacerle

192
00:05:21,100 --> 00:05:21,840
 recordatorio

193
00:05:21,840 --> 00:05:23,260
 y ponérselo fácil.

194
00:05:23,260 --> 00:05:26,000
 También os traigo que, si, por

195
00:05:26,000 --> 00:05:28,480
 una mala decisión o a sabiendas,

196
00:05:28,540 --> 00:05:30,860
 tenéis una NVIDIA, que si ya

197
00:05:30,860 --> 00:05:33,120
 os habéis descargado el driver

198
00:05:33,120 --> 00:05:34,680
 565711,

199
00:05:34,680 --> 00:05:37,780
 a partir de ahí, con Plasma 624

200
00:05:37,780 --> 00:05:40,860
 volvéis a tener el HDR disponible.

201
00:05:40,860 --> 00:05:42,480
 Ya sabéis que NVIDIA tiene

202
00:05:42,480 --> 00:05:44,140
 miles de problemas en el

203
00:05:44,140 --> 00:05:45,940
 software libre,

204
00:05:46,260 --> 00:05:48,170
 ya sea por Wayland, ya sea por

205
00:05:48,170 --> 00:05:50,040
 el driver propietario que no se

206
00:05:50,040 --> 00:05:51,140
 lleva bien con el kernel,

207
00:05:51,140 --> 00:05:53,390
 ya sea porque una actualización

208
00:05:53,390 --> 00:05:55,030
 rompe una cosa o porque de

209
00:05:55,030 --> 00:05:56,740
 repente le quitan el soporte,

210
00:05:56,740 --> 00:05:58,930
 o porque el driver de software

211
00:05:58,930 --> 00:06:01,110
 libre no ha tenido el apoyo que

212
00:06:01,110 --> 00:06:02,520
 se ha tenido el DMD

213
00:06:02,520 --> 00:06:04,090
 para que soporte todas las

214
00:06:04,090 --> 00:06:06,320
 características del hardware.

215
00:06:06,780 --> 00:06:08,620
 Bueno, la cosa es que últimamente

216
00:06:08,620 --> 00:06:10,220
 parece que están haciendo las

217
00:06:10,220 --> 00:06:11,300
 cosas algo mejor,

218
00:06:11,300 --> 00:06:12,960
 desde luego no al nivel que

219
00:06:12,960 --> 00:06:14,920
 otros fabricantes y no al nivel

220
00:06:14,920 --> 00:06:16,260
 que nos gustaría,

221
00:06:16,260 --> 00:06:18,290
 pero mejor, de estar lejos los

222
00:06:18,290 --> 00:06:20,670
 tiempos de Linux Torval enseñándoles

223
00:06:20,670 --> 00:06:21,340
 el dedo.

224
00:06:21,340 --> 00:06:23,110
 Con lo cual, ya tenemos una

225
00:06:23,110 --> 00:06:25,000
 cosa que se perdió y que hubiera

226
00:06:25,000 --> 00:06:25,400
 estado bien

227
00:06:25,400 --> 00:06:26,500
 que no hubiera desaparecido

228
00:06:26,500 --> 00:06:27,950
 durante un tiempo, que es el

229
00:06:27,950 --> 00:06:28,320
 HDR,

230
00:06:28,320 --> 00:06:29,720
 cosa que yo ahora estoy empezando

231
00:06:29,720 --> 00:06:30,720
 a probar porque mi nuevo

232
00:06:30,720 --> 00:06:31,380
 monitor tiene

233
00:06:31,380 --> 00:06:32,900
 y la verdad es que, al menos en

234
00:06:32,900 --> 00:06:34,420
 KDE, es súper sencillo.

235
00:06:34,420 --> 00:06:35,830
 Te vas a las opciones del

236
00:06:35,830 --> 00:06:37,520
 monitor y activas el HDR.

237
00:06:37,520 --> 00:06:38,480
 No tiene más.

238
00:06:38,480 --> 00:06:39,950
 Si es verdad que yo nunca he

239
00:06:39,950 --> 00:06:41,190
 sido de un ojo y un oído

240
00:06:41,190 --> 00:06:42,020
 súper fino

241
00:06:42,020 --> 00:06:43,860
 y todavía tengo pendiente

242
00:06:43,860 --> 00:06:45,860
 hacer pruebas de si realmente

243
00:06:45,860 --> 00:06:47,160
 lo estoy notando,

244
00:06:47,160 --> 00:06:48,960
 pero funcionar funciona.

245
00:06:48,960 --> 00:06:50,130
 Y dicen que en las próximas

246
00:06:50,130 --> 00:06:51,630
 versiones todavía va a funcionar

247
00:06:51,630 --> 00:06:52,080
 mejor.

248
00:06:52,080 --> 00:06:53,560
 También tenemos un artículo

249
00:06:53,560 --> 00:06:55,020
 interesante sobre cómo han

250
00:06:55,020 --> 00:06:57,100
 mejorado el plasmoide de impresión.

251
00:06:57,100 --> 00:06:58,430
 La cosa es que uno de los

252
00:06:58,430 --> 00:07:00,420
 desarrolladores de KDE tenía

253
00:07:00,420 --> 00:07:02,060
 un problema él mismo

254
00:07:02,060 --> 00:07:04,940
 con su impresión y entonces

255
00:07:04,940 --> 00:07:06,810
 decidió, digo, pues venga, le

256
00:07:06,810 --> 00:07:07,980
 voy a dedicar mi tiempo libre

257
00:07:07,980 --> 00:07:09,230
 a arreglar esto que me afecta a

258
00:07:09,230 --> 00:07:10,290
 mí y puede ayudar a mucha

259
00:07:10,290 --> 00:07:10,700
 gente.

260
00:07:10,700 --> 00:07:13,190
 Y es que por cómo están hechos

261
00:07:13,190 --> 00:07:15,480
 los plasmoides aquí en KDE,

262
00:07:15,480 --> 00:07:17,380
 no se puede poner todo lo que

263
00:07:17,380 --> 00:07:18,680
 tú quisieras.

264
00:07:18,680 --> 00:07:20,170
 En plan, no puedo poner cuatro

265
00:07:20,170 --> 00:07:21,490
 filas para poder meter esas

266
00:07:21,490 --> 00:07:22,020
 cosas.

267
00:07:22,020 --> 00:07:24,610
 La cosa es que había unos requerimientos,

268
00:07:24,610 --> 00:07:25,660
 unos requisitos

269
00:07:25,660 --> 00:07:26,910
 que no le permitía mostrar la

270
00:07:26,910 --> 00:07:28,120
 información como quería.

271
00:07:28,120 --> 00:07:30,240
 Y según ha ido avanzando la

272
00:07:30,240 --> 00:07:32,690
 tecnología y las ideas de diseño

273
00:07:32,690 --> 00:07:34,480
 que ha conseguido tener,

274
00:07:34,480 --> 00:07:36,500
 ahora en el mismo plasmoide el

275
00:07:36,500 --> 00:07:38,480
 resumen es que vaya a poder ver

276
00:07:38,480 --> 00:07:39,720
 la cola de impresión del

277
00:07:39,720 --> 00:07:40,120
 trabajo.

278
00:07:40,120 --> 00:07:41,440
 Que no tenéis que hacer un

279
00:07:41,440 --> 00:07:42,820
 clic más para meteros en la

280
00:07:42,820 --> 00:07:43,140
 cola

281
00:07:43,140 --> 00:07:44,330
 y entonces dentro de la cola

282
00:07:44,330 --> 00:07:45,300
 pinchar en el trabajo

283
00:07:45,300 --> 00:07:49,650
 y ver que pone error en el filtro,

284
00:07:49,650 --> 00:07:51,000
 que se ha parado o si lo queréis

285
00:07:51,000 --> 00:07:51,520
 cancelar.

286
00:07:51,520 --> 00:07:52,790
 O sea, el resumen muy a grosso

287
00:07:52,790 --> 00:07:54,130
 modo, pero el artículo está

288
00:07:54,130 --> 00:07:55,100
 muy interesante.

289
00:07:55,100 --> 00:07:56,280
 Está en inglés.

290
00:07:56,900 --> 00:07:58,110
 Hablando de accesibilidad, que

291
00:07:58,110 --> 00:08:00,020
 justo hace un momento acabo de

292
00:08:00,020 --> 00:08:01,060
 grabar con Jorge

293
00:08:01,060 --> 00:08:02,470
 accesibilidad con tecnologías

294
00:08:02,470 --> 00:08:04,210
 libres, pero como esto es muy

295
00:08:04,210 --> 00:08:04,980
 pequeñito

296
00:08:04,980 --> 00:08:06,100
 como para comentarlo allí en

297
00:08:06,100 --> 00:08:08,290
 general, es que Dolphin va a

298
00:08:08,290 --> 00:08:09,440
 mejorar la accesibilidad

299
00:08:09,440 --> 00:08:12,140
 con Orca 47 cuando salga Gears

300
00:08:12,140 --> 00:08:15,020
 2412, que ya le queda poco.

301
00:08:15,020 --> 00:08:16,630
 Han hecho varias mejoras a lo

302
00:08:16,630 --> 00:08:18,240
 largo del tiempo y esta es la

303
00:08:18,240 --> 00:08:18,940
 última.

304
00:08:18,940 --> 00:08:20,180
 Esto es una de las cosas que a

305
00:08:20,180 --> 00:08:21,540
 mí me gusta del proyecto KDE

306
00:08:21,540 --> 00:08:24,780
 que tiene muy interiorizado el

307
00:08:24,780 --> 00:08:27,460
 compromiso con, aunque saben

308
00:08:27,460 --> 00:08:28,840
 que KDE Plasma

309
00:08:28,840 --> 00:08:30,700
 ahora mismo no es súper accesible,

310
00:08:30,700 --> 00:08:32,140
 están intentando cada día

311
00:08:32,140 --> 00:08:32,940
 hacerlo mejor.

312
00:08:32,940 --> 00:08:34,600
 Y esto es una de las cosas que

313
00:08:34,600 --> 00:08:35,900
 han conseguido hacer.

314
00:08:35,900 --> 00:08:37,300
 Luego me he encontrado que hay

315
00:08:37,300 --> 00:08:38,880
 una aplicación nueva también,

316
00:08:38,880 --> 00:08:40,590
 esta todavía más nueva que la

317
00:08:40,590 --> 00:08:42,020
 de Optimage, que es Carp,

318
00:08:42,020 --> 00:08:44,030
 que es un editor de PDFs al

319
00:08:44,030 --> 00:08:46,000
 estilo PDF Arranger.

320
00:08:46,000 --> 00:08:47,960
 Antes PDF Arranger se llamaba

321
00:08:47,960 --> 00:08:49,570
 de otra manera, se ve que le

322
00:08:49,570 --> 00:08:50,480
 hicieron un Forge

323
00:08:50,480 --> 00:08:51,880
 porque quedó en desmantenimiento.

324
00:08:51,880 --> 00:08:53,820
 Y ahora dentro del ecosistema

325
00:08:53,820 --> 00:08:55,660
 de KDE, pues tenemos uno que es

326
00:08:55,660 --> 00:08:56,080
 lo típico

327
00:08:56,080 --> 00:08:58,090
 que te deja borrar una imagen o

328
00:08:58,090 --> 00:08:59,940
 una página dentro de un PDF en

329
00:08:59,940 --> 00:09:00,800
 concreto,

330
00:09:00,800 --> 00:09:02,270
 cambiar las páginas de orden,

331
00:09:02,270 --> 00:09:03,760
 hacer alguna cosita en concreto

332
00:09:03,760 --> 00:09:04,660
 a las páginas.

333
00:09:04,660 --> 00:09:06,380
 Está bien tener alternativas

334
00:09:06,380 --> 00:09:07,830
 que se integren bien con el

335
00:09:07,830 --> 00:09:08,540
 escritorio

336
00:09:08,540 --> 00:09:11,060
 y por si volviera a quedar sin

337
00:09:11,060 --> 00:09:13,400
 mantenimiento el PDF Arranger,

338
00:09:13,400 --> 00:09:14,180
 que es lo que yo estaba usando

339
00:09:14,180 --> 00:09:14,680
 hasta ahora.

340
00:09:14,680 --> 00:09:16,570
 Y luego también tengo que

341
00:09:16,570 --> 00:09:19,010
 dentro de todas las distros que

342
00:09:19,010 --> 00:09:20,000
 se han ido sumando

343
00:09:20,000 --> 00:09:22,980
 a poner Plasma 6.2 y dentro de

344
00:09:22,980 --> 00:09:24,740
 las distros, estas son menos,

345
00:09:24,740 --> 00:09:27,000
 que tienen Plasma por defecto,

346
00:09:27,000 --> 00:09:28,540
 pues tenemos que se ha publicado

347
00:09:28,540 --> 00:09:32,380
 openmandriba roma 2412, rome,

348
00:09:32,380 --> 00:09:34,340
 que viene así con la actualización

349
00:09:34,340 --> 00:09:35,790
 que venían de Plasma 5 y ya

350
00:09:35,790 --> 00:09:37,810
 están en Plasma 6 para los que

351
00:09:37,810 --> 00:09:38,900
 tengáis ahí

352
00:09:39,600 --> 00:09:41,660
 el recuerdo de mandrake mandriba

353
00:09:41,660 --> 00:09:42,900
 y openmandriba.

354
00:09:42,900 --> 00:09:44,620
 Luego estarán las noticias que

355
00:09:44,620 --> 00:09:46,770
 os quería contar hoy, pero

356
00:09:46,770 --> 00:09:48,580
 luego quería hacer un recordatorio,

357
00:09:48,580 --> 00:09:51,290
 por si acaso no estáis informados

358
00:09:51,290 --> 00:09:53,700
 vía correo o se os ha pasado,

359
00:09:53,700 --> 00:09:56,040
 que el sábado 21 de diciembre

360
00:09:56,040 --> 00:09:58,940
 a las 10 de la mañana tenemos

361
00:09:58,940 --> 00:10:01,680
 Asamblea General de KDE España.

362
00:10:02,220 --> 00:10:03,380
 Con lo cual aún está ya

363
00:10:03,380 --> 00:10:05,270
 tiempo de asociarse, creo que

364
00:10:05,270 --> 00:10:07,520
 son 20 euros, una cantidad simbólica,

365
00:10:07,520 --> 00:10:08,960
 y estaréis dentro de un

366
00:10:08,960 --> 00:10:10,750
 proyecto con gente muy maja en

367
00:10:10,750 --> 00:10:12,440
 el que tampoco se os pide que

368
00:10:12,440 --> 00:10:12,960
 trabajéis,

369
00:10:12,960 --> 00:10:14,490
 con el ser socio es suficiente

370
00:10:14,490 --> 00:10:16,220
 y simplemente pues tiene acceso

371
00:10:16,220 --> 00:10:18,340
 a la lista de distribución de

372
00:10:18,340 --> 00:10:18,780
 correo,

373
00:10:18,780 --> 00:10:20,620
 al grupo de Matrix y al grupo

374
00:10:20,620 --> 00:10:21,720
 de Telegram.

375
00:10:22,200 --> 00:10:24,020
 Es una manera de decir que apoyas

376
00:10:24,020 --> 00:10:25,730
 el proyecto, aunque no tengas

377
00:10:25,730 --> 00:10:26,660
 que trabajar,

378
00:10:26,660 --> 00:10:28,170
 aunque estamos muy agradecidos

379
00:10:28,170 --> 00:10:29,520
 a la gente como Iván, que

380
00:10:29,520 --> 00:10:31,920
 viene pisando fuerte y viene

381
00:10:31,920 --> 00:10:33,730
 también dedicándonos su

382
00:10:33,730 --> 00:10:34,040
 tiempo.

383
00:10:34,040 --> 00:10:35,660
 Y si no, pues ya os contaremos

384
00:10:35,660 --> 00:10:37,080
 qué es lo que hemos estado

385
00:10:37,080 --> 00:10:38,880
 hablando en la Asamblea General,

386
00:10:38,880 --> 00:10:40,390
 a parte de los típicos rollos

387
00:10:40,390 --> 00:10:42,080
 de cualquier asociación, de

388
00:10:42,080 --> 00:10:44,240
 los presupuestos, las cuentas

389
00:10:44,240 --> 00:10:45,140
 contables

390
00:10:45,140 --> 00:10:46,680
 y todas esas cosas que ni os

391
00:10:46,680 --> 00:10:48,580
 interesan a vosotros ni a mí.

392
00:10:48,580 --> 00:10:50,210
 Pero luego sí que se hacen

393
00:10:50,210 --> 00:10:52,280
 algunas cosas interesantes.

394
00:10:52,280 --> 00:10:55,140
 Por ejemplo, este año vamos a

395
00:10:55,140 --> 00:10:57,230
 proponer dedicar un tanto por

396
00:10:57,230 --> 00:10:59,560
 ciento de los fondos de la asociación

397
00:10:59,560 --> 00:11:00,910
 a algunos proyectos de software

398
00:11:00,910 --> 00:11:01,320
 libre.

399
00:11:01,320 --> 00:11:03,020
 Por ejemplo, ya hemos hecho una

400
00:11:03,020 --> 00:11:04,430
 donación a OP3, que es el

401
00:11:04,430 --> 00:11:05,730
 servicio de estadística que usamos

402
00:11:05,730 --> 00:11:06,440
 en este podcast,

403
00:11:07,380 --> 00:11:09,130
 y entonces de un listado que saquemos

404
00:11:09,130 --> 00:11:10,900
 de todos los socios, en plan,

405
00:11:10,900 --> 00:11:12,640
 pues, cosas que use sobre todo

406
00:11:12,640 --> 00:11:13,600
 la asociación,

407
00:11:13,600 --> 00:11:16,600
 ya sea el proyecto BBB o LibreOffice

408
00:11:16,600 --> 00:11:19,300
 o algunas aplicaciones que usen

409
00:11:19,300 --> 00:11:20,940
 mucho los socios,

410
00:11:20,940 --> 00:11:22,320
 entre todos los fondos que

411
00:11:22,320 --> 00:11:23,770
 tiene la Asociación Cada de

412
00:11:23,770 --> 00:11:25,860
 España, de la Akademy,

413
00:11:25,860 --> 00:11:27,180
 si conseguimos sacar algo con

414
00:11:27,180 --> 00:11:28,410
 las camisetas o de las cuotas

415
00:11:28,410 --> 00:11:29,200
 de los socios,

416
00:11:29,200 --> 00:11:30,570
 un tanto por ciento, para no

417
00:11:30,570 --> 00:11:31,880
 pillarnos los dedos con una

418
00:11:31,880 --> 00:11:32,940
 cantidad exacta,

419
00:11:32,940 --> 00:11:35,500
 lo dedicaremos al proyecto más

420
00:11:35,500 --> 00:11:38,200
 óptimo, que sea como dar ejemplos

421
00:11:38,200 --> 00:11:38,800
 en plan,

422
00:11:38,800 --> 00:11:40,990
 siempre predicamos que hay que

423
00:11:40,990 --> 00:11:44,180
 donar tiempo, conocimiento,

424
00:11:44,180 --> 00:11:46,920
 boca a boca o dinero.

425
00:11:46,920 --> 00:11:49,550
 Pues, KDE España dona mucho

426
00:11:49,550 --> 00:11:52,620
 tiempo, conocimiento y esfuerzo

427
00:11:52,620 --> 00:11:54,610
 de sus socios en divulgar el

428
00:11:54,610 --> 00:11:55,820
 software libre y KDE,

429
00:11:56,160 --> 00:11:58,180
 pero también vamos a donar un

430
00:11:58,180 --> 00:12:00,220
 poco a proyectos que nosotros

431
00:12:00,220 --> 00:12:02,090
 nos estamos nutriendo para

432
00:12:02,090 --> 00:12:03,380
 funcionar como asociación.

433
00:12:03,380 --> 00:12:05,200
 A ver qué os parece esta iniciativa,

434
00:12:05,200 --> 00:12:06,720
 a ver si tenéis propuestas.

435
00:12:06,720 --> 00:12:08,790
 Si no me da tiempo de volver a

436
00:12:08,790 --> 00:12:11,170
 grabar, feliz año, felices fiestas

437
00:12:11,170 --> 00:12:11,640
 a todos,

438
00:12:11,640 --> 00:12:13,350
 y mientras tanto, nos vemos por

439
00:12:13,350 --> 00:12:14,480
 el Fediverso.

440
00:12:14,480 --> 00:12:15,240
 Un saludo.

441
00:12:15,240 --> 00:12:17,280
 ¡Gracias!

