1
00:00:00,980 --> 00:00:03,120
 Buenas, estás de vuelta en KDE

2
00:00:03,120 --> 00:00:06,410
 Express Episodio 36. Este está

3
00:00:06,410 --> 00:00:08,220
 grabado el 24 de noviembre de

4
00:00:08,220 --> 00:00:08,880
 2024

5
00:00:08,880 --> 00:00:10,470
 y va a tener alguna noticia

6
00:00:10,470 --> 00:00:12,270
 menos de lo habitual porque me

7
00:00:12,270 --> 00:00:13,930
 he tirado unas semanas sin

8
00:00:13,930 --> 00:00:15,480
 poder entrar en el fe diverso

9
00:00:15,480 --> 00:00:18,430
 porque mi estancia de mastodon

10
00:00:18,430 --> 00:00:20,380
 estaba caída, tuvo problemas

11
00:00:20,380 --> 00:00:22,160
 con la dana y aunque se ha

12
00:00:22,160 --> 00:00:22,940
 recuperado

13
00:00:22,940 --> 00:00:24,540
 no han podido coger la copia de

14
00:00:24,540 --> 00:00:26,580
 seguridad y han empezado de cero,

15
00:00:26,580 --> 00:00:28,500
 con lo cual he perdido los marcadores

16
00:00:28,500 --> 00:00:30,570
 y los seguidores y la gente que

17
00:00:30,570 --> 00:00:32,540
 seguía y las listas y todo,

18
00:00:32,540 --> 00:00:34,720
 con lo cual unas cuantas cosas

19
00:00:34,720 --> 00:00:35,660
 que tenía apuntadas

20
00:00:35,660 --> 00:00:37,650
 se han quedado en el tintero.

21
00:00:37,650 --> 00:00:39,810
 Por suerte los administradores

22
00:00:39,810 --> 00:00:41,780
 de la instancia no han tenido

23
00:00:41,780 --> 00:00:42,780
 ningún problema personal

24
00:00:42,780 --> 00:00:45,190
 simplemente han tenido que

25
00:00:45,190 --> 00:00:47,190
 empezar una instancia de cero,

26
00:00:47,190 --> 00:00:48,810
 así que mucho ánimo para la

27
00:00:48,810 --> 00:00:50,520
 gente que no ha tenido tanta suerte

28
00:00:50,520 --> 00:00:52,150
 estas cosas en la tecnología

29
00:00:52,150 --> 00:00:53,970
 pasan. Sirva de recordatorio de

30
00:00:53,970 --> 00:00:55,630
 que es importante hacer copias

31
00:00:55,630 --> 00:00:57,000
 de seguridad y tenerlas

32
00:00:57,000 --> 00:00:59,150
 a salvo tanto cuando tenéis un

33
00:00:59,150 --> 00:01:00,700
 servidor como cuando tenéis un

34
00:01:00,700 --> 00:01:03,360
 cliente porque en Mastodon vosotros

35
00:01:03,360 --> 00:01:04,160
 podéis exportar

36
00:01:04,160 --> 00:01:08,070
 en csv las personas que seguís,

37
00:01:08,070 --> 00:01:11,320
 los listados, los marcadores,

38
00:01:11,320 --> 00:01:11,320
 todas esas cosas, no solo

39
00:01:11,320 --> 00:01:11,320
 dependen de vuestra distancia

40
00:01:11,320 --> 00:01:13,320
 sino que vosotros podéis

41
00:01:13,320 --> 00:01:15,400
 salvarla y tenerla a salvo. Yo

42
00:01:15,400 --> 00:01:16,570
 ya ahora que he recuperado mi

43
00:01:16,570 --> 00:01:19,000
 cuenta y estoy en una distancia

44
00:01:19,000 --> 00:01:20,340
 nueva y he seguido a todo el

45
00:01:20,340 --> 00:01:21,320
 que he encontrado que seguía

46
00:01:21,320 --> 00:01:23,900
 Por cierto, si os seguía y veis

47
00:01:23,900 --> 00:01:26,710
 que no he seguido en masto.es,

48
00:01:26,710 --> 00:01:28,480
 pegarme un toque porque seguía

49
00:01:28,480 --> 00:01:28,480
 mucha mucha gente y ha sido

50
00:01:28,480 --> 00:01:28,480
 difícil encontrar a todos

51
00:01:28,480 --> 00:01:32,800
 La cosa es que ahora estoy

52
00:01:32,800 --> 00:01:35,960
 haciendo copias de seguridad

53
00:01:35,960 --> 00:01:36,630
 regulares y nunca es suficiente

54
00:01:36,630 --> 00:01:38,840
 recordatorio de que hay que

55
00:01:38,840 --> 00:01:40,750
 hacer copias de seguridad de

56
00:01:40,750 --> 00:01:43,680
 todo, de todo y testearlas y

57
00:01:43,680 --> 00:01:44,640
 tenerlas a buen recaudo

58
00:01:44,640 --> 00:01:46,570
 Dicho lo cual, empiezo con las

59
00:01:46,570 --> 00:01:48,470
 noticias de KDE que seguramente

60
00:01:48,470 --> 00:01:49,840
 es a lo que habéis venido

61
00:01:49,840 --> 00:01:53,450
 Lo primero es, Fedora 42, noticia

62
00:01:53,450 --> 00:01:55,870
 bomba, va a tener una versión

63
00:01:55,870 --> 00:01:57,850
 oficial de KDE y va a dejar de

64
00:01:57,850 --> 00:01:59,200
 ser un simple spin

65
00:01:59,200 --> 00:02:00,720
 Esto lo que significa es que

66
00:02:00,720 --> 00:02:02,430
 tendrá su propia página web,

67
00:02:02,430 --> 00:02:03,950
 que se le tendrá que dar un trato

68
00:02:03,950 --> 00:02:05,470
 igualitario con la versión de

69
00:02:05,470 --> 00:02:05,840
 Gnome

70
00:02:05,840 --> 00:02:07,480
 que se llama Workstation, no se

71
00:02:07,480 --> 00:02:08,200
 llama Gnome

72
00:02:08,200 --> 00:02:10,600
 que la pondrán disponible en

73
00:02:10,600 --> 00:02:12,900
 la descarga en el mismo sitio

74
00:02:12,900 --> 00:02:14,840
 donde está la oficial

75
00:02:14,840 --> 00:02:16,520
 Vamos, que va a tener los

76
00:02:16,520 --> 00:02:19,180
 mismos recursos, la misma promoción

77
00:02:19,180 --> 00:02:20,960
 y esto viene gracias a un

78
00:02:20,960 --> 00:02:23,130
 trabajo muy grande de muchos

79
00:02:23,130 --> 00:02:25,660
 voluntarios del equipo de Fedora

80
00:02:25,660 --> 00:02:27,400
 que son los que llevaban el

81
00:02:27,400 --> 00:02:29,360
 spin de KDE, que se lo han

82
00:02:29,360 --> 00:02:30,200
 estado currando mucho

83
00:02:30,200 --> 00:02:31,830
 que han conseguido una versión

84
00:02:31,830 --> 00:02:33,700
 muy buena y han dicho que esa

85
00:02:33,700 --> 00:02:35,280
 versión se merecía un trato

86
00:02:35,280 --> 00:02:36,160
 igualitario

87
00:02:36,160 --> 00:02:39,930
 Propusieron el día de los inocentes

88
00:02:39,930 --> 00:02:41,540
 del año pasado

89
00:02:41,540 --> 00:02:43,880
 cambiar la versión oficial de

90
00:02:43,880 --> 00:02:44,800
 Gnome a KDE

91
00:02:44,800 --> 00:02:47,800
 Eso evidentemente, siendo Fedora

92
00:02:47,800 --> 00:02:50,530
 un proyecto paraguas de Red Hat,

93
00:02:50,530 --> 00:02:51,780
 estaba difícil que pasara

94
00:02:51,780 --> 00:02:53,410
 y entonces han llegado a un

95
00:02:53,410 --> 00:02:54,220
 compromiso

96
00:02:54,220 --> 00:02:55,840
 que bueno, en vez de hacer eso,

97
00:02:55,840 --> 00:02:57,220
 pues vamos a poner las dos al

98
00:02:57,220 --> 00:02:57,900
 mismo nivel

99
00:02:57,900 --> 00:03:00,120
 Hubo una votación en los

100
00:03:00,120 --> 00:03:03,020
 boards que tienen ahí en Fedora

101
00:03:03,020 --> 00:03:04,460
 y hoy ya salió para adelante y

102
00:03:04,460 --> 00:03:05,990
 parece ser que en la próxima

103
00:03:05,990 --> 00:03:06,540
 versión

104
00:03:06,540 --> 00:03:08,600
 pues ya deberíamos tener las

105
00:03:08,600 --> 00:03:09,800
 dos disponibles

106
00:03:09,800 --> 00:03:11,420
 Lo cual, oye, para todos los

107
00:03:11,420 --> 00:03:13,330
 que nos gusta KDE, nos parece

108
00:03:13,330 --> 00:03:15,840
 que es algo que se merecen

109
00:03:15,840 --> 00:03:17,900
 y que se han ganado

110
00:03:17,900 --> 00:03:19,790
 Luego aparte, en Fedora 42

111
00:03:19,790 --> 00:03:21,480
 parece ser que van a activar

112
00:03:21,480 --> 00:03:22,980
 una pantalla

113
00:03:22,980 --> 00:03:24,980
 justo en la bienvenida en la

114
00:03:24,980 --> 00:03:26,600
 que te va a dejar activar repositorios

115
00:03:26,600 --> 00:03:27,220
 de terceros

116
00:03:27,220 --> 00:03:28,670
 para directamente que no tengas

117
00:03:28,670 --> 00:03:29,360
 tú que ir a mano

118
00:03:29,360 --> 00:03:31,040
 y ir ahí por línea de comando

119
00:03:31,040 --> 00:03:32,580
 y activar cosas que se ve que

120
00:03:32,580 --> 00:03:34,440
 todo el mundo activa al inicio

121
00:03:34,440 --> 00:03:37,100
 Luego, la Steam Deck también

122
00:03:37,100 --> 00:03:37,820
 se ha actualizado

123
00:03:37,820 --> 00:03:40,300
 solo que se ha actualizado a Plasma

124
00:03:40,300 --> 00:03:41,760
 52710

125
00:03:41,760 --> 00:03:43,520
 que es un poco antiguo para

126
00:03:43,520 --> 00:03:44,220
 nosotros

127
00:03:44,220 --> 00:03:45,910
 pero ellos ya sabemos que van

128
00:03:45,910 --> 00:03:47,470
 con pies así de plomo, muy

129
00:03:47,470 --> 00:03:48,840
 despacio, asegurándose

130
00:03:48,840 --> 00:03:52,720
 Incluso la última 52711 es de

131
00:03:52,720 --> 00:03:54,360
 marzo de 2024

132
00:03:54,360 --> 00:03:55,330
 o sea, ni siquiera se han

133
00:03:55,330 --> 00:03:57,440
 puesto a la última última de

134
00:03:57,440 --> 00:04:00,880
 los parches de seguridad de la

135
00:04:00,880 --> 00:04:01,420
 27

136
00:04:01,420 --> 00:04:02,860
 pero ellos se ven que cogen un

137
00:04:02,860 --> 00:04:04,350
 Snapthost en algún momento y lo

138
00:04:04,350 --> 00:04:06,000
 testean, lo testean, lo testean

139
00:04:06,000 --> 00:04:06,880
 Yo la verdad es que no he

140
00:04:06,880 --> 00:04:08,080
 tenido ningún bug ni ningún

141
00:04:08,080 --> 00:04:08,580
 problema

142
00:04:08,580 --> 00:04:10,550
 De hecho, os voy a decir que

143
00:04:10,550 --> 00:04:11,820
 tengo por aquí apuntado, le he

144
00:04:11,820 --> 00:04:12,880
 hecho una captura de pantalla

145
00:04:12,880 --> 00:04:14,330
 cuáles son las versiones de

146
00:04:14,330 --> 00:04:15,600
 cada cosa que tenía y que

147
00:04:15,600 --> 00:04:16,960
 tiene ahora la Steam Deck

148
00:04:16,960 --> 00:04:19,900
 La Steam Deck tenía la 527.5

149
00:04:19,900 --> 00:04:20,640
 de Plasma

150
00:04:20,640 --> 00:04:22,450
 y hemos pasado, como he dicho,

151
00:04:22,450 --> 00:04:23,960
 a la 527.10

152
00:04:23,960 --> 00:04:27,880
 Tenía la 5.107.0 de los Frameworks

153
00:04:27,880 --> 00:04:29,780
 Ahora tiene la 5.115

154
00:04:29,780 --> 00:04:33,610
 Tenía la 5.15.9 de la versión

155
00:04:33,610 --> 00:04:34,080
 de Qt

156
00:04:34,080 --> 00:04:36,000
 y ahora tiene la 5.15.12

157
00:04:36,000 --> 00:04:37,950
 Y luego el Kernel han pasado de

158
00:04:37,950 --> 00:04:40,430
 la 6.1.52 con los parches de

159
00:04:40,430 --> 00:04:40,840
 Valve

160
00:04:40,840 --> 00:04:44,220
 a la 6.5 con los parches de

161
00:04:44,220 --> 00:04:44,660
 Valve

162
00:04:44,660 --> 00:04:46,280
 También hay un buen salto ahí

163
00:04:46,280 --> 00:04:48,570
 Bueno, la Steam Deck no es para

164
00:04:48,570 --> 00:04:49,700
 tener lo último

165
00:04:49,700 --> 00:04:51,240
 aunque esté basada en Arch

166
00:04:51,240 --> 00:04:53,420
 pero vemos que la siguen manteniendo

167
00:04:53,420 --> 00:04:54,570
 y es un proyecto que sigue y

168
00:04:54,570 --> 00:04:55,380
 que tiene futuro

169
00:04:55,380 --> 00:04:56,780
 y que incluso se plantean en

170
00:04:56,780 --> 00:04:57,620
 algún momento

171
00:04:57,620 --> 00:05:00,160
 que se hagan la Steam Deck 2

172
00:05:00,160 --> 00:05:01,440
 aunque dicen que todavía le

173
00:05:01,440 --> 00:05:02,680
 tiene bastante vida a esta

174
00:05:02,680 --> 00:05:03,960
 La cosa es que sigue el

175
00:05:03,960 --> 00:05:05,440
 proyecto para adelante

176
00:05:05,440 --> 00:05:07,280
 y poco a poco van llegando

177
00:05:07,280 --> 00:05:09,570
 Así que yo contento de que funcione

178
00:05:09,570 --> 00:05:09,940
 bien

179
00:05:09,940 --> 00:05:11,870
 y de que el proyecto no solo

180
00:05:11,870 --> 00:05:13,520
 está funcionando

181
00:05:13,520 --> 00:05:16,080
 sino que llegan acuerdos con la

182
00:05:16,080 --> 00:05:17,300
 comunidad de Arch

183
00:05:17,300 --> 00:05:19,250
 para que implementen con

184
00:05:19,250 --> 00:05:21,700
 personas que le están pagando

185
00:05:21,700 --> 00:05:24,060
 algunas cosas como firmar

186
00:05:24,060 --> 00:05:25,740
 actualizaciones digitalmente,

187
00:05:25,740 --> 00:05:26,440
 automáticamente

188
00:05:26,440 --> 00:05:28,810
 hacer más sencillo el proceso

189
00:05:28,810 --> 00:05:30,200
 de construir Arch

190
00:05:30,200 --> 00:05:32,360
 para que a la vez que sea más

191
00:05:32,360 --> 00:05:33,160
 sencillo

192
00:05:33,160 --> 00:05:35,220
 sea posible también hacerlo en

193
00:05:35,220 --> 00:05:36,120
 otras arquitecturas

194
00:05:36,120 --> 00:05:37,180
 porque se rumorea

195
00:05:37,180 --> 00:05:39,480
 de que a lo mejor sacan una

196
00:05:39,480 --> 00:05:41,240
 versión con ARM

197
00:05:41,240 --> 00:05:42,990
 y como Arch ahora mismo no lo

198
00:05:42,990 --> 00:05:44,500
 soporta oficialmente

199
00:05:44,500 --> 00:05:45,920
 sino que hay un proyecto paralelo

200
00:05:45,920 --> 00:05:46,900
 que sí que lo soporta

201
00:05:46,900 --> 00:05:49,060
 pues se ve que están facilitando

202
00:05:49,060 --> 00:05:50,220
 que sean los propios de Arch

203
00:05:50,220 --> 00:05:51,590
 la propia comunidad la que lo

204
00:05:51,590 --> 00:05:52,560
 haga oficialmente

205
00:05:52,560 --> 00:05:54,100
 Valve ve ahí echando una mano

206
00:05:54,100 --> 00:05:54,640
 como siempre

207
00:05:54,640 --> 00:05:56,500
 al mundo de GNU Linux

208
00:05:56,500 --> 00:05:57,880
 que aunque no sea una empresa

209
00:05:57,880 --> 00:05:58,820
 de software libre

210
00:05:58,820 --> 00:06:01,640
 mete pasta y mete soporte en la

211
00:06:01,640 --> 00:06:02,300
 comunidad

212
00:06:02,300 --> 00:06:03,380
 colabora

213
00:06:03,380 --> 00:06:06,340
 y que seríamos en el gaming

214
00:06:06,340 --> 00:06:07,300
 sin Proton

215
00:06:07,300 --> 00:06:09,360
 luego volviendo a Fedora

216
00:06:09,360 --> 00:06:10,810
 que la verdad es que es un buen

217
00:06:10,810 --> 00:06:11,600
 mes para ellos

218
00:06:11,600 --> 00:06:14,100
 no solo han anunciado lo del

219
00:06:14,100 --> 00:06:15,060
 spin de KDE

220
00:06:15,060 --> 00:06:16,360
 sino que han lanzado

221
00:06:16,360 --> 00:06:18,800
 unos spins que son Plasma

222
00:06:18,800 --> 00:06:19,220
 Mobile

223
00:06:19,220 --> 00:06:20,740
 y Kinoite Mobile

224
00:06:20,740 --> 00:06:23,500
 teníamos el spin de Kinoite

225
00:06:23,500 --> 00:06:25,780
 que es la inmutable o realmente

226
00:06:25,780 --> 00:06:26,500
 atómica

227
00:06:26,500 --> 00:06:27,700
 pues con esa base

228
00:06:27,700 --> 00:06:29,220
 lo que han hecho es decir

229
00:06:29,220 --> 00:06:30,460
 bueno si hay gente que usa

230
00:06:30,460 --> 00:06:31,280
 tabletas

231
00:06:31,280 --> 00:06:33,880
 con la versión de KDE

232
00:06:33,880 --> 00:06:34,970
 pues vamos a ponérselo más

233
00:06:34,970 --> 00:06:35,280
 fácil

234
00:06:35,280 --> 00:06:36,680
 y lo han hecho

235
00:06:36,680 --> 00:06:38,460
 no versión de ordenador

236
00:06:38,460 --> 00:06:39,620
 sino versión para móviles

237
00:06:39,620 --> 00:06:40,460
 que realmente

238
00:06:40,460 --> 00:06:41,400
 quien más usa esto

239
00:06:41,400 --> 00:06:42,340
 es gente que tiene tabletas

240
00:06:42,340 --> 00:06:42,660
 grandes

241
00:06:42,660 --> 00:06:43,240
 pero bueno

242
00:06:43,240 --> 00:06:45,220
 que ya son unos spins oficiales

243
00:06:45,220 --> 00:06:46,700
 y que se están construyendo

244
00:06:46,700 --> 00:06:47,760
 y que te los puedes descargar

245
00:06:47,760 --> 00:06:49,840
 luego dentro de las aplicaciones

246
00:06:49,840 --> 00:06:51,780
 tenemos que KDE Wave

247
00:06:51,780 --> 00:06:52,840
 WAVE

248
00:06:52,840 --> 00:06:53,640
 con V

249
00:06:53,640 --> 00:06:54,760
 en español

250
00:06:54,760 --> 00:06:56,440
 vuelve a estar en desarrollo

251
00:06:56,440 --> 00:06:58,540
 antes estaba en mantenimiento

252
00:06:58,540 --> 00:06:58,940
 básico

253
00:06:58,940 --> 00:07:00,600
 que era que se seguía compilando

254
00:07:00,600 --> 00:07:01,840
 para que saliera en el Gears

255
00:07:01,840 --> 00:07:02,560
 pero no había

256
00:07:02,560 --> 00:07:04,780
 ninguna mejora

257
00:07:04,780 --> 00:07:06,220
 y ahora mismo han conseguido

258
00:07:06,220 --> 00:07:06,560
 hacer

259
00:07:06,560 --> 00:07:08,180
 la parte más gorda y fea

260
00:07:08,180 --> 00:07:09,760
 que es que lo han portado entero

261
00:07:09,760 --> 00:07:11,220
 a Qt6

262
00:07:11,220 --> 00:07:11,960
 que es lo difícil

263
00:07:11,960 --> 00:07:13,360
 y alguien ha cogido

264
00:07:13,360 --> 00:07:14,560
 el mantenimiento del proyecto

265
00:07:14,560 --> 00:07:16,660
 con lo cual significa que

266
00:07:16,660 --> 00:07:17,980
 si por alguna cosa

267
00:07:17,980 --> 00:07:18,520
 Audacity

268
00:07:18,520 --> 00:07:20,160
 no te va bien

269
00:07:20,160 --> 00:07:20,760
 y no te gusta

270
00:07:20,760 --> 00:07:21,920
 pues que sepas

271
00:07:21,920 --> 00:07:22,680
 que esta alternativa

272
00:07:22,680 --> 00:07:23,600
 que teníamos en KDE

273
00:07:23,600 --> 00:07:25,060
 que estaba un poco abandonada

274
00:07:25,060 --> 00:07:26,480
 vuelve a tener vida

275
00:07:26,480 --> 00:07:27,660
 y que la van a seguir

276
00:07:27,660 --> 00:07:28,360
 metiendo caña

277
00:07:28,360 --> 00:07:29,620
 luego viene una noticia

278
00:07:29,620 --> 00:07:30,040
 de KDE

279
00:07:30,040 --> 00:07:31,780
 que está muy bien

280
00:07:31,780 --> 00:07:32,560
 que es

281
00:07:32,560 --> 00:07:33,540
 que en la

282
00:07:33,540 --> 00:07:34,420
 en la pasada

283
00:07:34,420 --> 00:07:34,840
 Akademy

284
00:07:34,840 --> 00:07:35,760
 en la internacional

285
00:07:35,760 --> 00:07:37,280
 dieron una charla

286
00:07:37,280 --> 00:07:38,180
 e hicieron la propuesta

287
00:07:38,180 --> 00:07:39,300
 de crear

288
00:07:39,300 --> 00:07:41,040
 una distribución

289
00:07:41,040 --> 00:07:41,880
 de KDE

290
00:07:41,880 --> 00:07:42,780
 inmutable

291
00:07:42,780 --> 00:07:43,600
 atómica

292
00:07:43,600 --> 00:07:44,440
 hecha

293
00:07:44,440 --> 00:07:45,800
 en Arch

294
00:07:45,800 --> 00:07:47,480
 y BRTFS

295
00:07:47,480 --> 00:07:49,020
 esto es que igual que esta

296
00:07:49,020 --> 00:07:49,880
 Kinoite

297
00:07:49,880 --> 00:07:50,780
 que estábamos hablando

298
00:07:50,780 --> 00:07:51,260
 en Fedora

299
00:07:51,260 --> 00:07:52,860
 pues la propia comunidad

300
00:07:52,860 --> 00:07:53,360
 de KDE

301
00:07:53,360 --> 00:07:54,400
 está pensando hacer

302
00:07:54,400 --> 00:07:55,580
 igual que tienen

303
00:07:55,580 --> 00:07:57,280
 la versión de Neon

304
00:07:57,280 --> 00:07:58,140
 que es coger

305
00:07:58,140 --> 00:07:58,780
 Ubuntu

306
00:07:58,780 --> 00:08:00,360
 y hacerle los cambios

307
00:08:00,360 --> 00:08:01,140
 para que tenga KDE

308
00:08:01,140 --> 00:08:01,640
 a la última

309
00:08:01,640 --> 00:08:03,380
 pues además

310
00:08:03,380 --> 00:08:04,640
 esto no sustituye a nada

311
00:08:04,640 --> 00:08:05,580
 simplemente un proyecto

312
00:08:05,580 --> 00:08:06,140
 de probar

313
00:08:06,140 --> 00:08:07,640
 y si cogemos un Arch

314
00:08:07,640 --> 00:08:08,780
 igual que hace Valve

315
00:08:08,780 --> 00:08:10,720
 la ponemos a nuestro gusto

316
00:08:10,720 --> 00:08:11,920
 le hacemos

317
00:08:11,920 --> 00:08:12,500
 inmutable

318
00:08:12,500 --> 00:08:14,720
 y hacemos varias versiones

319
00:08:14,720 --> 00:08:15,320
 igual que Neon

320
00:08:15,320 --> 00:08:16,380
 tiene la estable

321
00:08:16,380 --> 00:08:17,400
 la de desarrollador

322
00:08:17,400 --> 00:08:17,700
 no sé qué

323
00:08:17,700 --> 00:08:18,680
 pues van a hacer

324
00:08:18,680 --> 00:08:19,560
 como tres ramas

325
00:08:19,560 --> 00:08:20,840
 la de estar a la última

326
00:08:20,840 --> 00:08:21,320
 a la última

327
00:08:21,320 --> 00:08:22,480
 para que los programadores

328
00:08:22,480 --> 00:08:23,400
 puedan ahí

329
00:08:23,400 --> 00:08:24,640
 hacer sus cambios

330
00:08:24,640 --> 00:08:25,360
 y testear

331
00:08:25,360 --> 00:08:27,300
 la de los beta testers

332
00:08:27,300 --> 00:08:28,460
 y la del usuario final

333
00:08:28,460 --> 00:08:29,980
 entonces tendríamos

334
00:08:29,980 --> 00:08:31,380
 de la mano

335
00:08:31,380 --> 00:08:31,980
 de la comunidad

336
00:08:31,980 --> 00:08:32,500
 de KDE

337
00:08:32,500 --> 00:08:34,000
 una distro

338
00:08:34,000 --> 00:08:34,580
 de la

339
00:08:34,580 --> 00:08:35,900
 de la nueva moda

340
00:08:35,900 --> 00:08:36,520
 de atómica

341
00:08:36,520 --> 00:08:37,160
 de atómica

342
00:08:37,160 --> 00:08:37,680
 inmutable

343
00:08:37,680 --> 00:08:39,120
 yo seguramente

344
00:08:39,120 --> 00:08:39,640
 cuando esto

345
00:08:39,640 --> 00:08:41,980
 esté menos verde

346
00:08:41,980 --> 00:08:43,060
 le daré una prueba

347
00:08:43,060 --> 00:08:44,100
 porque con mi Arch

348
00:08:44,100 --> 00:08:45,220
 estoy muy contento

349
00:08:45,220 --> 00:08:46,240
 pero yo

350
00:08:46,240 --> 00:08:47,620
 estoy encantado

351
00:08:47,620 --> 00:08:48,040
 de que haya

352
00:08:48,040 --> 00:08:48,600
 distribuciones

353
00:08:48,600 --> 00:08:49,620
 súper sencillas

354
00:08:49,620 --> 00:08:50,940
 de mantenimiento

355
00:08:50,940 --> 00:08:51,580
 cero

356
00:08:51,580 --> 00:08:52,880
 para familiares

357
00:08:52,880 --> 00:08:53,600
 ahora mismo

358
00:08:53,600 --> 00:08:54,460
 lo que estoy probando

359
00:08:54,460 --> 00:08:55,560
 es openSUSE

360
00:08:55,560 --> 00:08:56,540
 Slow Roll

361
00:08:56,540 --> 00:08:57,840
 dado que el Lip

362
00:08:57,840 --> 00:08:59,100
 no me gusta

363
00:08:59,100 --> 00:08:59,920
 tener que estar

364
00:08:59,920 --> 00:09:00,680
 actualizando

365
00:09:00,680 --> 00:09:02,100
 así cosas cordas

366
00:09:02,100 --> 00:09:03,060
 de año en año

367
00:09:03,060 --> 00:09:03,680
 cuando voy a casa

368
00:09:03,680 --> 00:09:04,420
 de familiares

369
00:09:04,420 --> 00:09:05,700
 y Tumbleweed

370
00:09:05,700 --> 00:09:07,220
 era demasiado

371
00:09:07,220 --> 00:09:08,200
 son muchísimas

372
00:09:08,200 --> 00:09:08,680
 muchísimas

373
00:09:08,680 --> 00:09:09,720
 actualizaciones diarias

374
00:09:09,720 --> 00:09:10,660
 mucho más que en Arch

375
00:09:10,660 --> 00:09:11,040
 y Linux

376
00:09:11,040 --> 00:09:11,860
 y aparte

377
00:09:11,860 --> 00:09:12,300
 tienen que ser

378
00:09:12,300 --> 00:09:13,240
 por líneas de comando

379
00:09:13,240 --> 00:09:14,460
 no con Discover

380
00:09:14,460 --> 00:09:15,600
 no funcionaba bien

381
00:09:15,600 --> 00:09:16,100
 la última vez

382
00:09:16,100 --> 00:09:16,620
 que lo probé

383
00:09:16,620 --> 00:09:18,240
 y con Slow Roll

384
00:09:18,240 --> 00:09:19,060
 estoy viendo

385
00:09:19,060 --> 00:09:19,560
 a ver si

386
00:09:19,560 --> 00:09:20,840
 eso es estable

387
00:09:20,840 --> 00:09:21,760
 esto sería

388
00:09:21,760 --> 00:09:23,080
 una inmutable

389
00:09:23,080 --> 00:09:24,140
 o atómica

390
00:09:24,140 --> 00:09:25,140
 si se dice

391
00:09:25,140 --> 00:09:26,580
 más exactamente

392
00:09:26,580 --> 00:09:28,200
 sería de lujo

393
00:09:28,200 --> 00:09:29,600
 para ir a casa

394
00:09:29,600 --> 00:09:30,120
 de tu madre

395
00:09:30,120 --> 00:09:30,980
 dejarse la apuesta

396
00:09:30,980 --> 00:09:32,040
 que se actualice

397
00:09:32,040 --> 00:09:32,720
 de vez en cuando

398
00:09:32,720 --> 00:09:33,660
 y que sea súper sencillo

399
00:09:33,660 --> 00:09:34,280
 volver atrás

400
00:09:34,280 --> 00:09:35,060
 si pasa algo

401
00:09:35,060 --> 00:09:35,800
 y luego

402
00:09:35,800 --> 00:09:37,120
 tengo una serie

403
00:09:37,120 --> 00:09:37,780
 de novedades

404
00:09:37,780 --> 00:09:38,540
 de lo que viene

405
00:09:38,540 --> 00:09:39,660
 en Plasma 6.3

406
00:09:39,660 --> 00:09:40,500
 aunque no sea

407
00:09:40,500 --> 00:09:41,300
 mi parte favorita

408
00:09:41,300 --> 00:09:41,940
 porque a mí me gusta

409
00:09:41,940 --> 00:09:42,500
 hablar de cosas

410
00:09:42,500 --> 00:09:43,060
 que ya tenemos

411
00:09:43,060 --> 00:09:43,780
 entre las manos

412
00:09:43,780 --> 00:09:44,580
 la verdad es que

413
00:09:44,580 --> 00:09:45,320
 como no paran

414
00:09:45,320 --> 00:09:46,240
 de sacar noticias

415
00:09:46,240 --> 00:09:47,420
 o voy poniendo

416
00:09:47,420 --> 00:09:48,100
 algunas ya

417
00:09:48,100 --> 00:09:49,180
 o es que luego

418
00:09:49,180 --> 00:09:50,420
 cuando llegue el momento

419
00:09:50,420 --> 00:09:51,820
 va a ser tan abrumador

420
00:09:51,820 --> 00:09:52,820
 que voy a ser incapaz

421
00:09:52,820 --> 00:09:54,040
 de ir haciendo

422
00:09:54,040 --> 00:09:56,340
 detalladamente

423
00:09:56,340 --> 00:09:56,860
 cuáles son

424
00:09:56,860 --> 00:09:57,680
 las actualizaciones

425
00:09:57,680 --> 00:09:58,160
 entonces

426
00:09:58,160 --> 00:09:59,400
 aquí os voy haciendo

427
00:09:59,400 --> 00:10:00,020
 resúmenes

428
00:10:00,020 --> 00:10:00,440
 que al final

429
00:10:00,440 --> 00:10:01,220
 cuando los suméis

430
00:10:01,220 --> 00:10:02,620
 pues será el 90%

431
00:10:02,620 --> 00:10:03,300
 de lo que salga

432
00:10:03,300 --> 00:10:04,180
 y si no el día

433
00:10:04,180 --> 00:10:04,660
 que saliera

434
00:10:04,660 --> 00:10:05,420
 os tendría que decir

435
00:10:05,420 --> 00:10:05,800
 la mitad

436
00:10:05,800 --> 00:10:06,540
 entonces

437
00:10:06,540 --> 00:10:07,820
 Plasma 6.3

438
00:10:07,820 --> 00:10:09,200
 que seguramente

439
00:10:09,200 --> 00:10:09,640
 salga

440
00:10:09,640 --> 00:10:11,140
 el 11 de febrero

441
00:10:11,140 --> 00:10:11,960
 de 2025

442
00:10:11,960 --> 00:10:12,620
 o sea que

443
00:10:12,620 --> 00:10:14,000
 todavía le queda

444
00:10:14,000 --> 00:10:15,320
 tiempo

445
00:10:15,320 --> 00:10:15,800
 de que sigan

446
00:10:15,800 --> 00:10:16,620
 metiendo más cosas

447
00:10:16,620 --> 00:10:17,360
 trae

448
00:10:17,360 --> 00:10:19,180
 mejoras para tabletes

449
00:10:19,180 --> 00:10:20,200
 esto es la información

450
00:10:20,200 --> 00:10:21,500
 que va a sacar

451
00:10:21,500 --> 00:10:22,240
 de la tableta

452
00:10:22,240 --> 00:10:22,640
 tanto como

453
00:10:22,640 --> 00:10:24,280
 su configuración

454
00:10:24,280 --> 00:10:25,420
 esto es por la gente

455
00:10:25,420 --> 00:10:26,100
 que dibuja

456
00:10:26,100 --> 00:10:27,480
 hay gente

457
00:10:27,480 --> 00:10:27,900
 que ha hecho

458
00:10:27,900 --> 00:10:28,640
 alguna review

459
00:10:28,640 --> 00:10:29,240
 de que si había

460
00:10:29,240 --> 00:10:30,080
 bastantes cosas

461
00:10:30,080 --> 00:10:30,600
 mejorables

462
00:10:30,600 --> 00:10:31,240
 en Wayland

463
00:10:31,240 --> 00:10:31,740
 y las están

464
00:10:31,740 --> 00:10:32,380
 teniendo en cuenta

465
00:10:32,380 --> 00:10:32,660
 y están

466
00:10:32,660 --> 00:10:33,280
 haciendo mejores

467
00:10:33,280 --> 00:10:34,240
 vamos a tener

468
00:10:34,240 --> 00:10:35,240
 una notificación

469
00:10:35,240 --> 00:10:36,320
 cuando el sistema

470
00:10:36,320 --> 00:10:37,000
 ha detenido

471
00:10:37,000 --> 00:10:38,160
 algún proceso

472
00:10:38,160 --> 00:10:39,100
 por falta de memoria

473
00:10:39,100 --> 00:10:40,920
 esto es muy habitual

474
00:10:40,920 --> 00:10:42,360
 que hay un

475
00:10:42,360 --> 00:10:42,980
 System D

476
00:10:42,980 --> 00:10:43,640
 sobre todo

477
00:10:43,640 --> 00:10:45,100
 hay un automatismo

478
00:10:45,100 --> 00:10:45,680
 que si te quedas

479
00:10:45,680 --> 00:10:46,200
 sin memoria

480
00:10:46,200 --> 00:10:46,960
 empiezas a matar

481
00:10:46,960 --> 00:10:47,560
 procesos

482
00:10:47,560 --> 00:10:48,220
 para recuperar

483
00:10:48,220 --> 00:10:48,620
 memoria

484
00:10:48,620 --> 00:10:49,740
 el problema

485
00:10:49,740 --> 00:10:50,340
 es que

486
00:10:50,340 --> 00:10:51,320
 eso a veces

487
00:10:51,320 --> 00:10:52,300
 mata a lo bruto

488
00:10:52,300 --> 00:10:52,980
 y tú no sabes

489
00:10:52,980 --> 00:10:53,660
 que está matando

490
00:10:53,660 --> 00:10:54,340
 y que está pasando

491
00:10:54,340 --> 00:10:55,560
 entonces lo que van a hacer

492
00:10:55,560 --> 00:10:56,700
 es ponerte ahí

493
00:10:56,700 --> 00:10:58,540
 una notificación gráfica

494
00:10:58,540 --> 00:10:59,820
 que te diga

495
00:10:59,820 --> 00:11:00,700
 oye que

496
00:11:00,700 --> 00:11:01,280
 se ha matado

497
00:11:01,280 --> 00:11:01,740
 este proceso

498
00:11:01,740 --> 00:11:02,280
 por esto

499
00:11:02,280 --> 00:11:03,540
 por si tú quieres

500
00:11:03,540 --> 00:11:04,480
 manualmente

501
00:11:04,480 --> 00:11:05,460
 ir cerrando

502
00:11:05,460 --> 00:11:06,220
 cosas que sean

503
00:11:06,220 --> 00:11:07,040
 menos importantes

504
00:11:07,040 --> 00:11:07,700
 luego

505
00:11:07,700 --> 00:11:08,680
 Dolphin

506
00:11:08,680 --> 00:11:09,820
 va a tener

507
00:11:09,820 --> 00:11:10,480
 un interfaz

508
00:11:10,480 --> 00:11:11,200
 para dispositivos

509
00:11:11,200 --> 00:11:11,640
 móviles

510
00:11:11,640 --> 00:11:12,900
 desde la 2412

511
00:11:12,900 --> 00:11:14,460
 2412 de Gears

512
00:11:14,460 --> 00:11:15,620
 o sea que en diciembre

513
00:11:15,620 --> 00:11:17,180
 yo ya

514
00:11:17,180 --> 00:11:18,480
 probé

515
00:11:18,480 --> 00:11:19,400
 ocular

516
00:11:19,400 --> 00:11:21,100
 en el móvil

517
00:11:21,100 --> 00:11:22,120
 y parece ser

518
00:11:22,120 --> 00:11:22,600
 que Dolphin

519
00:11:22,600 --> 00:11:23,480
 lo han adaptado

520
00:11:23,480 --> 00:11:24,760
 para todas

521
00:11:24,760 --> 00:11:25,680
 las versiones

522
00:11:25,680 --> 00:11:26,080
 de móviles

523
00:11:26,080 --> 00:11:26,660
 que hay disponibles

524
00:11:26,660 --> 00:11:27,080
 con Linux

525
00:11:27,080 --> 00:11:28,180
 lo cual está muy bien

526
00:11:28,180 --> 00:11:28,860
 porque Dolphin

527
00:11:28,860 --> 00:11:29,700
 yo creo que como

528
00:11:29,700 --> 00:11:30,940
 explorador de ficheros

529
00:11:30,940 --> 00:11:32,540
 no tiene parangón

530
00:11:32,540 --> 00:11:33,420
 luego

531
00:11:33,420 --> 00:11:34,880
 el centro de información

532
00:11:34,880 --> 00:11:36,760
 mostrará todas tus GPUs

533
00:11:36,760 --> 00:11:37,520
 ya sabéis que podéis

534
00:11:37,520 --> 00:11:38,780
 tanto por línea de comandos

535
00:11:38,780 --> 00:11:39,620
 como gráficamente

536
00:11:39,620 --> 00:11:40,600
 decirle

537
00:11:40,600 --> 00:11:42,040
 a la configuración

538
00:11:42,040 --> 00:11:42,600
 del sistema

539
00:11:42,600 --> 00:11:43,060
 que os diga

540
00:11:43,060 --> 00:11:43,840
 que tenéis vosotros

541
00:11:43,840 --> 00:11:44,420
 instalados

542
00:11:44,420 --> 00:11:45,160
 viene muy bien

543
00:11:45,160 --> 00:11:45,520
 sobre todo

544
00:11:45,520 --> 00:11:46,460
 para hacer reportes

545
00:11:46,460 --> 00:11:47,500
 de hecho tiene un botoncito

546
00:11:47,500 --> 00:11:48,640
 para cópiamelo en inglés

547
00:11:48,640 --> 00:11:49,670
 que es justo lo que tienes que

548
00:11:49,670 --> 00:11:49,940
 pegar

549
00:11:49,940 --> 00:11:52,100
 en los bug trackers

550
00:11:52,100 --> 00:11:52,700
 y en los foros

551
00:11:52,700 --> 00:11:53,260
 pues

552
00:11:53,260 --> 00:11:54,120
 a la información

553
00:11:54,120 --> 00:11:54,860
 que ya había antes

554
00:11:54,860 --> 00:11:55,940
 ahora van a tener

555
00:11:55,940 --> 00:11:56,900
 todas las GPUs

556
00:11:56,900 --> 00:11:57,600
 que tengáis instaladas

557
00:11:57,600 --> 00:11:58,860
 luego también tenemos

558
00:11:58,860 --> 00:11:59,560
 que Discover

559
00:11:59,560 --> 00:12:00,320
 te informará

560
00:12:00,320 --> 00:12:01,020
 si la aplicación

561
00:12:01,020 --> 00:12:01,780
 viene directamente

562
00:12:01,780 --> 00:12:02,780
 de los desarrolladores

563
00:12:02,780 --> 00:12:03,760
 o de voluntarios

564
00:12:03,760 --> 00:12:04,800
 esto sobre todo

565
00:12:04,800 --> 00:12:05,200
 yo lo he visto

566
00:12:05,200 --> 00:12:05,940
 en Flatpak

567
00:12:05,940 --> 00:12:06,960
 tú en Flatpak

568
00:12:06,960 --> 00:12:08,080
 en la página web

569
00:12:08,080 --> 00:12:08,700
 Flash Hub

570
00:12:08,700 --> 00:12:09,800
 tú puedes ver

571
00:12:09,800 --> 00:12:10,500
 con un check

572
00:12:10,500 --> 00:12:12,520
 si quien está subiendo

573
00:12:12,520 --> 00:12:14,200
 y quien ha puesto

574
00:12:14,200 --> 00:12:14,880
 la receta

575
00:12:14,880 --> 00:12:15,960
 para que esté disponible

576
00:12:15,960 --> 00:12:16,740
 esa aplicación

577
00:12:16,740 --> 00:12:18,320
 son voluntarios

578
00:12:18,320 --> 00:12:18,940
 de Flash Hub

579
00:12:18,940 --> 00:12:19,720
 o de otras personas

580
00:12:19,720 --> 00:12:21,040
 o es directamente

581
00:12:21,040 --> 00:12:21,780
 el proyecto

582
00:12:21,780 --> 00:12:23,280
 el que está

583
00:12:23,280 --> 00:12:24,220
 manteniendo eso

584
00:12:24,220 --> 00:12:25,360
 así ya sabes

585
00:12:25,360 --> 00:12:26,160
 que si es directamente

586
00:12:26,160 --> 00:12:26,660
 el proyecto

587
00:12:26,660 --> 00:12:27,340
 pues tiene un plus

588
00:12:27,340 --> 00:12:28,420
 de confianza

589
00:12:28,420 --> 00:12:29,340
 yo hasta ahora

590
00:12:29,340 --> 00:12:30,060
 de Flash Hub

591
00:12:30,060 --> 00:12:30,660
 no me he encontrado

592
00:12:30,660 --> 00:12:31,300
 nada raro

593
00:12:31,300 --> 00:12:33,460
 pero se les puede escapar

594
00:12:33,460 --> 00:12:34,080
 de que alguien

595
00:12:34,080 --> 00:12:34,940
 suba

596
00:12:34,940 --> 00:12:35,440
 yo que sé

597
00:12:35,440 --> 00:12:36,280
 cualquier aplicación

598
00:12:36,280 --> 00:12:37,100
 que no la está subiendo

599
00:12:37,100 --> 00:12:38,940
 el programador principal

600
00:12:38,940 --> 00:12:40,360
 y de repente

601
00:12:40,360 --> 00:12:40,900
 meta algo

602
00:12:40,900 --> 00:12:41,260
 que todavía

603
00:12:41,260 --> 00:12:42,160
 no sabemos detectar

604
00:12:42,160 --> 00:12:43,280
 no me parece

605
00:12:43,280 --> 00:12:44,260
 un peligro muy gordo

606
00:12:44,260 --> 00:12:45,100
 pero sí me parece

607
00:12:45,100 --> 00:12:46,260
 que el camino a seguir

608
00:12:46,260 --> 00:12:47,080
 es premiar

609
00:12:47,080 --> 00:12:48,280
 a los que lo están

610
00:12:48,280 --> 00:12:49,140
 haciendo ellos mismos

611
00:12:49,140 --> 00:12:49,920
 luego

612
00:12:49,920 --> 00:12:50,980
 dos widgets

613
00:12:50,980 --> 00:12:52,540
 uno es el de impresora

614
00:12:52,540 --> 00:12:53,220
 ahora muestra

615
00:12:53,220 --> 00:12:53,740
 la información

616
00:12:53,740 --> 00:12:54,800
 de la cola directamente

617
00:12:54,800 --> 00:12:55,900
 no tienes que hacer

618
00:12:55,900 --> 00:12:57,260
 ese click extra

619
00:12:57,260 --> 00:12:58,040
 para saber

620
00:12:58,040 --> 00:12:59,560
 qué es lo que está imprimiendo

621
00:12:59,560 --> 00:13:00,460
 o si ha habido

622
00:13:00,460 --> 00:13:00,900
 un problema

623
00:13:00,900 --> 00:13:01,540
 con la cola

624
00:13:01,540 --> 00:13:02,720
 o si el filter

625
00:13:02,720 --> 00:13:03,340
 de caps

626
00:13:03,340 --> 00:13:04,480
 ha explotado

627
00:13:04,480 --> 00:13:05,680
 facilitando

628
00:13:05,680 --> 00:13:06,620
 el tener la información

629
00:13:06,620 --> 00:13:07,540
 y luego tenemos

630
00:13:07,540 --> 00:13:08,140
 que el widget

631
00:13:08,140 --> 00:13:09,620
 de teclado

632
00:13:09,620 --> 00:13:11,140
 ahora el indicador

633
00:13:11,140 --> 00:13:12,020
 muestra

634
00:13:12,020 --> 00:13:12,780
 cuando las teclas

635
00:13:12,780 --> 00:13:13,800
 modificadoras

636
00:13:13,800 --> 00:13:15,240
 están bloqueadas

637
00:13:15,240 --> 00:13:16,180
 o latchet

638
00:13:16,180 --> 00:13:17,000
 por accesibilidad

639
00:13:17,000 --> 00:13:18,040
 lo del latchet

640
00:13:18,040 --> 00:13:18,660
 no tengo muy claro

641
00:13:18,660 --> 00:13:19,060
 lo que es

642
00:13:19,060 --> 00:13:20,080
 lo del bloqueadas

643
00:13:20,080 --> 00:13:21,000
 y que tú

644
00:13:21,000 --> 00:13:22,040
 cuando tienes problemas

645
00:13:22,040 --> 00:13:22,600
 de escribir

646
00:13:22,600 --> 00:13:23,340
 con las dos manos

647
00:13:23,340 --> 00:13:24,440
 hay un modo

648
00:13:24,440 --> 00:13:25,080
 en el que tú haces

649
00:13:25,080 --> 00:13:25,660
 una combinación

650
00:13:25,660 --> 00:13:26,240
 de teclas

651
00:13:26,240 --> 00:13:26,900
 y ya se queda

652
00:13:26,900 --> 00:13:27,520
 como si estuvieras

653
00:13:27,520 --> 00:13:28,100
 pulsando

654
00:13:28,100 --> 00:13:28,960
 el control

655
00:13:28,960 --> 00:13:29,400
 o el alt

656
00:13:29,400 --> 00:13:30,480
 o lo que esté bloqueando

657
00:13:30,480 --> 00:13:31,520
 para poder

658
00:13:31,520 --> 00:13:32,120
 con una mano

659
00:13:32,120 --> 00:13:32,820
 ir pulsando

660
00:13:32,820 --> 00:13:33,160
 la otra

661
00:13:33,160 --> 00:13:34,000
 sin tener que tener

662
00:13:34,000 --> 00:13:35,020
 la segunda mano

663
00:13:35,020 --> 00:13:35,700
 dándole al control

664
00:13:35,700 --> 00:13:36,080
 o el alt

665
00:13:36,080 --> 00:13:37,660
 pues igual que teníamos

666
00:13:37,660 --> 00:13:38,440
 un notificador

667
00:13:38,440 --> 00:13:39,020
 que te dice

668
00:13:39,020 --> 00:13:39,800
 si está el bloqueo

669
00:13:39,800 --> 00:13:40,400
 mayúsculas

670
00:13:40,400 --> 00:13:41,200
 o si tienes

671
00:13:41,200 --> 00:13:42,200
 el numpad

672
00:13:42,200 --> 00:13:42,800
 puesto

673
00:13:42,800 --> 00:13:43,640
 el bloqueo numérico

674
00:13:43,640 --> 00:13:44,620
 pues ahora

675
00:13:44,620 --> 00:13:45,800
 como mejora

676
00:13:45,800 --> 00:13:46,460
 de accesibilidad

677
00:13:46,460 --> 00:13:47,460
 han puesto

678
00:13:47,460 --> 00:13:48,320
 que también sepas

679
00:13:48,320 --> 00:13:49,080
 si estás bloqueando

680
00:13:49,080 --> 00:13:49,520
 una tecla

681
00:13:49,520 --> 00:13:50,520
 estupendo

682
00:13:50,520 --> 00:13:51,780
 y con esto

683
00:13:51,780 --> 00:13:52,540
 es el listado

684
00:13:52,540 --> 00:13:53,400
 de noticias

685
00:13:53,400 --> 00:13:54,400
 que he podido salvar

686
00:13:54,400 --> 00:13:55,520
 de las que no se me han perdido

687
00:13:55,520 --> 00:13:56,180
 por más todo

688
00:13:56,180 --> 00:13:57,740
 espero que todos

689
00:13:57,740 --> 00:13:58,860
 os encontréis bien

690
00:13:58,860 --> 00:14:00,100
 que estéis disfrutando

691
00:14:00,100 --> 00:14:00,920
 del software libre

692
00:14:00,920 --> 00:14:02,560
 recordar donar

693
00:14:02,560 --> 00:14:04,000
 hacer copias de seguridad

694
00:14:04,000 --> 00:14:05,860
 participar en las comunidades

695
00:14:05,860 --> 00:14:07,600
 y sobre todo

696
00:14:07,600 --> 00:14:08,840
 disfrutar de la libertad

697
00:14:08,840 --> 00:14:09,840
 que nos da el software libre

698
00:14:09,840 --> 00:14:10,800
 un saludo

699
00:14:10,800 --> 00:14:11,680
 y nos escuchamos pronto

700
00:14:11,680 --> 00:14:12,480
 ¡Gracias por ver el video!

