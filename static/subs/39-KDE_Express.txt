bienvenidos KDE express tenemos un episodio especial en 
el que recuperamos las píldoras en 
este caso va a ser un monotema sobre cómo hacer streaming y 
opciones de accesibilidad dado que 
son útiles para los dos modos entonces lo primero que voy a 
hacer es abrir una aplicación que se 
llama show me the keys show me the keys la activo y entonces 
lo que hace es poner aquí cada cosa que yo tecle si yo pongo 
aquí sdf aparece por ahí vale esto lo minimito y ahora lo que 
vamos a hacer es abrir las opciones del 
sistema entonces primera opción para accesibilidad aparte que 
esto puesto yo de que me salgan las 
teclas y que también es útil para hacer un streaming por 
internet pues nos tenemos que ir a preferencias 
del sistema a gestión de ventanas y a efectos de escritorio 
pues yo escribo efectos y automáticamente me salen aquí entonces animación 
del clic del ratón esto se activa con meta asterisco para 
hacer el asterisco tiene que ser en el teclado de letras no 
vale el numérico y se hace con el 
shift entonces yo lo que hago es meta shift x ahí aparece y 
entonces si pincho se va a ver los clics en rojo una vez 
cuando pincho otra vez cuando suelto si lo hago muy rápido 
pues se ve así esto está bien para cuando tú estás diciendo 
algo que estás haciendo pues entonces yo pincho aquí clic y 
se ve perfectamente vale por supuesto tiene muchas opciones 
de los colores que pasa si haces el 
central si haces con el derecho este derecho este del central 
en preferencia avanzada pues todo lo que quieras dibujar 
incluso puedes poner el texto para que se vea el botón que 
está pinchando pero eso ya lo hace la utilidad que yo he 
puesto así que no hace falta 
siguiente opción seguimiento del ratón seguimiento del ratón 
es útil si la pantalla es muy grande y el ratón es pequeño 
en KDE tenemos la opción esta de que si lo menea se hace 
grande grande grande grande grande 
pero eso no es muy práctico para estar apuntando en sitio 
el seguimiento del ratón lo activamos con meta control P de 
manera definitiva o con meta control de manera temporal eso 
significa que si yo hago meta control mientras que lo tengo 
pulsado se va a ver estos circulitos alrededor del ratón 
pero si lo suelto se va en cambio si hago un meta control 
P se va a quedar fijo y no voy a tener que estar pulsando. 
y es una manera más sencilla de estar siguiendo el ratón 
ahora vuelvo a hacer meta control P y se quita este ya no 
tiene opciones simplemente desactivar o desactivar no puedes cambiar 
el color o la velocidad que lo hace también tenemos la opción 
de ampliación o lupa son esto viene siendo un zoom 
pero hay dos maneras de hacer el zoom puedes hacer un zoom 
con lupa en el que simplemente se vea por defecto a ver lo voy 
a aplicar entonces el zoom va a ser un zoom con lupa en el que 
simplemente se vea por defecto por defecto a ver lo voy a 
aplicar entonces el zoom va a ser un cuadradito alrededor del 
ratón en el que se va a ir viendo más grande esto se 
puede ir haciendo con el meta más si no recuerdo más si el 
meta más y el meta menos amplían o reducen 
el zoom meta más lo activa meta menos lo quita y cuando 
tienes el meta más muy grande si le das a meta cero vuelve a 
por defecto vuelve a por defecto en cambio 
si te vas a ampliación le damos a aplicar si yo hago el meta 
más lo que se hace grande es todo el escritorio el escritorio 
me va a seguir con el ratón entonces yo puedo ir moviéndome 
salgo por aquí y esto va a funcionar cuando hago la opción 
de seguir voy a hacer el meta cero que se quite así cuando hago 
la opción de ampliación tengo unas opciones extras 
por ejemplo yo lo activo con meta más otra vez el teclado 
numérico no funciona tiene que ser el que está al lado de la 
p si yo me voy a la derecha yo puedo hacer un 
a ver si me acuerdo un meta f5 y meta f6 vale si yo me voy al 
meta f5 me va a traer de golpe al centro de la pantalla y si 
hago el meta f6 me va a llevar el meta f6 a donde me llevaba 
el meta f5 al foco y el meta f6 al centro 
entonces si yo el foco lo pongo en este en esta aplicación que 
está aquí me voy acá y meta f5 me lleva ahí en medio del 
foco en cambio si me voy a la esquina y hago meta f6 me va al 
centro de la pantalla otra vez con meta menos puedo 
reducirlo o con meta cero hago un reseteo que es como quitarlo 
ampliación es el método que viene por defecto luego 
también tenemos hacer dibujos o marcas 
esto lo tenemos más para abajo marcar con el ratón marcar con 
el ratón tiene cuatro opciones uno es dibujar que se hace con 
meta mayúscula y entonces con el ratón te vas moviendo y haces 
un dibujo luego con meta mayúscula f11 
lo borras todo o si dibujas de nuevo con meta shift y haces un 
dibujito y haces otro dibujito después 
en vez de hacer el meta mayúsculas f11 que lo borra todo con meta 
mayúsculas f12 te va borrando los dibujos de tandas 
y luego hay otra opción que dibujar flecha directamente eso 
es meta control shift y entonces puedes hacer líneas 
desde donde lo pulses hasta donde lo sueltes ahí donde te 
va a dibujar la línea una vez que sueltes todo te lo 
va a quitar también podemos activar a la vez meta control p 
y lo dejamos activado que se vea y hacemos un meta control shift 
y hacemos una flecha una flecha y otra vez con meta 
mayúscula f12 se pueden ir borrando individualmente esto viene muy 
bien pues para hacer exposiciones tú puedes yo sobre todo veo 
interesante la de mayúscula meta en el que tú puedes estar 
subrayando cosas y se ve lo que estás diciendo 
el meta control p lo quito y hago luego esto no hace falta 
que se esté viendo esto es el dibujo que hayas hecho se va a 
quedar todo el rato en pantalla hasta que hagas el método 
shift f12 pues estas son tres o cuatro opciones de accesibilidad 
que a la vez te sirven como una manera de hacer 
presentaciones o streaming en directo más interactiva espero 
que os haya gustado esta píldora no sé cómo habrá quedado 
porque la he utilizado para hacer una prueba en PeerTube 
ahora veré si es usable y os la puedo publicar un saludo 
