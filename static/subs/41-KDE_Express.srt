1
00:00:00,000 --> 00:00:09,560
¡Buenas! Estás en KDE Express, episodio 41, igual que los años que tengo, y estoy grabando el viernes 21 de febrero de 2025.

2
00:00:09,560 --> 00:00:15,220
Hoy vengo con el monotema que os prometí, aunque tengo noticias preparadas, son para otro episodio.

3
00:00:15,220 --> 00:00:23,820
Hoy voy a hablaros del Fediverso. No sé si estáis al tanto de la movida que ha habido con la miertificación de las redes privativas, sobre todo TwitterX,

4
00:00:23,820 --> 00:00:34,220
y de los movimientos que se han hecho y todas las polémicas. Yo ahora mismo no voy a remover eso, que hay suficiente en redes sociales y en blogs.

5
00:00:34,220 --> 00:00:45,820
Pero sí vengo a daros a conocer o a darle promoción a una iniciativa que se llama Vámonos Juntas, en la que muchas asociaciones, empresas e individuos

6
00:00:45,820 --> 00:00:59,820
han decidido irse de esas redes sociales privativas y empezar a comunicarse por redes federadas como Mastodon, Miskei y todo el Fediverso, con PeerTube, Friendica...

7
00:00:59,820 --> 00:01:09,420
Toda la familia que hay en el Fediverso. La cosa es que el evento se propuso en un día hacer mucho ruido en redes sociales.

8
00:01:09,420 --> 00:01:18,420
Hay un blog donde se explica muy bien lo que es la filosofía y lo que se pretende, así que en el artículo que os voy a enlazar, aunque ya esté puesto en redes sociales,

9
00:01:18,420 --> 00:01:30,420
he pensado que esto hay que traérselo a un blog y de paso traerlo por aquí, por si algunos no estáis todos leyendo a Mastodon como estamos algunos ahí, como si fuera el antiguo Twitter.

10
00:01:30,420 --> 00:01:42,420
La parte que más atañe a este podcast o a la asociación KDE España es que mucha gente se ha venido al Fediverso, en Vámonos Juntas han hecho un listado de la gente que quiso apoyarla

11
00:01:42,420 --> 00:01:52,420
y tú puedes bajarte un ficherito y en tu aplicación de Mastodon o en la página web de Mastodon o Miskei, las compatibles, te puedes descargar y empezar a seguir a toda esa gente.

12
00:01:52,420 --> 00:02:05,420
Sobre todo eso es útil si no tienes a nadie, si te abres una cuenta nueva y no sabes a quién seguir, como en estas redes no hay algoritmos, pues a lo mejor cuando te des de alta como mucho te sugiere 3 o 4 personas que escriben mucho en tu instancia

13
00:02:05,420 --> 00:02:15,420
pero luego tu muro está vacío y solo ves lo que escriben la gente que sigue. Entonces para facilitar el tener interacciones decidieron hacer una importación de cuentas,

14
00:02:15,420 --> 00:02:26,420
lo bueno es tener software libre que hay muchas acciones y mucha libertad, nadie se quiere quedar con tus datos o encerrarte en su plataforma y a mí se me ocurrió que como yo soy una persona muy ordenada

15
00:02:26,420 --> 00:02:36,420
y suelo tener en Mastodon a mucha gente etiquetada en listas, lo bueno es que tú puedes ver solo una lista si quieres ver un tema en concreto o puedes decir que la gente que pones en una lista

16
00:02:36,420 --> 00:02:51,420
no te aparezca en el timeline o en el feed, en tu muro principal por si a lo mejor solo quieres verlo cuando quieres leer algo en concreto o si porque esas personas escriben mucho y de normal no te da la vida para leer tantas cosas

17
00:02:51,420 --> 00:03:03,420
y prefieres leer las 500 cuentas que escriben menos y cuando tienes tiempo leer la lista de los que escriben mucho. La cosa es que como yo ya tenía mucha gente etiquetada, pues me dediqué una semana a currármelo un poco más

18
00:03:03,420 --> 00:03:18,420
porque tampoco las tenía todo el mundo y he hecho listas por temáticas. En una página que tengo yo en tipo blog, os la dejaré en las notas del programa, hice una lista de podcasting en español, de accesibilidad, de medioambiente y activismo

19
00:03:18,420 --> 00:03:42,420
como activismo que venía aquí, sostenibilidad, medioambiente y sostenibilidad. Una lista de veganismo, anti-especismo y animalismo, una lista de software libre que es enorme, una de hardware, de makers, de 3D, de dispositivos como portátiles, linkbook, medios de comunicación, gente de la región de Murcia, ya que estoy yo por aquí, de la Unión Europea y también enlazo a la lista del Vámonos Juntas.

20
00:03:42,420 --> 00:04:04,420
Yo lo he hecho con una tabla en la que te dice, la primera columna es que casi todas son listas menos lo de Vámonos Juntas que son cuentas directamente. La ventaja de hacerlo como lista es que cuando tú lo importas en tu mastodón, por ejemplo, tú ya automáticamente sigues a esa gente pero se te queda etiquetada para saber que la sigues porque has importado esa lista.

21
00:04:04,420 --> 00:04:17,420
Y entonces puedes hacer lo que os he dicho antes de marcarla como que solo quieres verla dentro de la lista pero no quieres verla en tu muro porque de repente hay mucha gente. Por supuesto tú si hay alguien de la lista que no te interesa la puedes dejar de seguir y fuera.

22
00:04:17,420 --> 00:04:38,420
Luego la segunda columna es el nombre de la lista, de qué va. La tercera columna es un enlace para descargar el fichero. Luego un enlace a GitLab que es donde estoy alojando las listas con un listado del contenido por si tú lo que quieres es ver quién hay dentro de esa lista. Y luego un enlace a los cómics de GitLab donde puedes ver el historial. Cada vez que hago un cómic pues ahí se ve.

23
00:04:39,420 --> 00:04:54,420
David ha añadido a estas tres personas a esta lista. David ha añadido a estas cinco personas a esta lista. Entonces eso es un listado general. Pero luego ya que estaba hecho eso y que tenemos la cuenta de KDE España pues entonces me tiro otra semana y he hecho otra catalogación.

24
00:04:54,420 --> 00:05:17,420
Entonces en el meollo que realmente os puede interesar a los que sois de usuarios de KDE o estáis escuchando esto o os gusta el mundillo KDE pues tenemos un listado de usuarios KDE por el fe diverso. Ese lo llamo comunidad. En ese tenéis pues un montón de gente. De hecho os voy a comentar primero los demás que creo que tiene más sentido.

25
00:05:17,420 --> 00:05:42,420
Gente o proyectos implicados en KDE. Eso es gente de KDE España o gente que colabora activamente con KDE España. Podríamos decir que son socios Víctor HCK, Toscali, Podcast Linus cuando publicaba que también es socio, Leipold el presidente, Baltasar, Baltokien, la cuenta de KDE España, Brais, bastante gente.

26
00:05:42,420 --> 00:05:58,420
No, yo mismo. Las cuentas de los podcasts de KDE España, Iván. Gente como de la asociación y muy implicada. Luego tenemos a cuentas que suelen promocionar KDE. Ese lo tengo por aquí. Aquí lo tengo, colaboradores.

27
00:05:58,420 --> 00:06:20,420
Son gente que a lo mejor son blogs que publican noticias de KDE o es gente que suele darnos publicidad pero que no son usuarios en sí que ellos lo usen o que sean miembros de la asociación. Por ejemplo, tenemos a Tareao que es miembro de la asociación pero no es usuario en sí de KDE Plasma. Es muy buena gente y colabora con nosotros.

28
00:06:20,420 --> 00:06:42,420
O esLibre o YoYo308 que hace muchos vídeos y de vez en cuando te hace una review estupenda. Opensusees que es una cuenta que promociona mucho nuestras publicaciones. Slimbook, por ejemplo, que muchas veces nos patrocina y tiene el Slimbook KDE. Opensourcecode que es donde vamos a hacer la Akademy este año otra vez.

29
00:06:42,420 --> 00:06:53,420
Hay varias cuentas, muy Linux, que también hacen artículos. Cuentas que hablan de KDE de vez en cuando pero no son en sí personas o que están dentro de KDE o que son usuarios de KDE.

30
00:06:53,420 --> 00:07:20,420
Y luego tenemos la que al principio he leído porque estaba aquí en la página web pero quería dejar para el final que es la lista de comunidad. En la comunidad, ahí está. Todas las personas en Mastodon que suelen interactuar con la cuenta de KDE España. Todas las personas que suelen publicar activamente en los viernes de escritorio con un alt text porque si no, no podemos hacer retuit sus escritorios y la mayoría de la gente de las listas anteriores.

31
00:07:20,420 --> 00:07:41,420
Por ejemplo, si yo estoy en la lista de KDE, de que colaboro con KDE, pues también estoy en la lista de comunidad porque también soy comunidad de KDE. Entonces, si queréis tener un montón de contactos que están relacionados o son usuarios con KDE, pues os podéis instalar, os podéis importar la lista de comunidad.

32
00:07:42,420 --> 00:07:54,420
Si no queréis tanta gente random de golpe y sólo queréis cuentas que promocionen y cuentas de gente muy implicada, pues tenéis las otras dos listas, la de colaboradores y la de gente o proyectos implicados.

33
00:07:54,420 --> 00:08:18,420
Cada lista en una cuarta columna tiene una etiqueta, que es la etiqueta que yo uso en Mastodon cada vez que publico de nuevo y digo, oye, que he hecho actualizaciones o he publicado una lista nueva. Oye, que si alguien quiere que le añode a la lista, poner un mensaje con esta etiqueta que yo lo pueda ver y saber que queréis que apunte porque o soy usuario de KDE o porque colaboráis en algún proyecto.

34
00:08:18,420 --> 00:08:37,420
Por ejemplo, tenemos el vamos juntas comunidad con K, por eso de KDE, vamos juntas KDE-es, vamos juntas KDE-diff de difusión y luego el vamos juntas listados es el que uso para todas. Cuando hay algo nuevo con los listados, pues pongo eso porque a lo mejor he actualizado el artículo.

35
00:08:37,420 --> 00:08:56,420
En el artículo tengo una introducción con el contexto de cómo ha ido evolucionando el movimiento este con enlaces a gente que lo ha desarrollado más, un tutorial sobre cómo importar los listados y qué hacía más con una gráfica y unos cuantos consejos sobre el Fediverse por si soy usuario nuevo.

36
00:08:56,420 --> 00:09:19,420
Y luego tengo un cuarto listado, es KDE in English porque son KDE internacional con muchos desarrolladores y muchos proyectos. Aquí hay un montón de aplicaciones de KDE en plan Krita, Kdenlive, Kugentufocus, Tokodon, NeoChat, Ariana...

37
00:09:19,420 --> 00:09:47,420
Es una mezcla entre aplicaciones o proyectos de KDE que tienen su propia cuenta y desarrolladores de KDE que un alto tanto porciento escriben en inglés. Por ejemplo, tenéis a Volker Krause, perdonad mi alemán o idioma extranjero, a RedStraight, Aleix Pol está en las dos porque es el presidente a nivel internacional pero escribe en español también.

38
00:09:47,420 --> 00:10:01,420
Carl debería estar también aunque ahora mismo no lo veo. La cosa es que ahí tenéis la parte en inglés. Ahí hay mucho movimiento también porque en inglés y en las aplicaciones se mueve mucho. También están las cuentas que publicitan los blogs semanales que tenemos de aplicaciones y de KDE Plasma.

39
00:10:01,420 --> 00:10:30,420
La cosa es que en el enlace que os pongo al artículo tenéis tanto para descargar el fichero como para ver las cuentas que hay dentro que aparte las podéis copiar y pegar si preferís ponerlo en un mensaje de Telegram. En un mensaje de Mastodon podréis copiar el texto que yo pongo en la lista, sustituir el nombre de lista coma por un @ y tendréis ya los handlers, los usuarios para pegar en un mensaje y pincharle y seguir.

40
00:10:31,420 --> 00:10:45,420
Si no queréis usar el método de importar. Nosotros en KDE España hemos decidido unirnos a esta iniciativa. Ya hacía tiempo que no publicábamos mucho en Twitter pero en Mastodon sí que tenemos bastante movimiento.

41
00:10:45,420 --> 00:11:06,420
Sí que intentamos seguir con la fuerza y la energía que tenemos los voluntarios que tenemos en otras redes para seguir divulgando la palabra pero que sepáis que en el Fediverso estamos bastante presentes y si tenéis alguna recomendación de gente a la que seguir o de si vosotros no estáis a escribirnos y os añadimos a este listado.

42
00:11:07,420 --> 00:11:34,420
Lo bueno que tienen los listados es que tú lo coges hoy, te lo importas, sigues a toda esa gente y ahora tienes una lista. Dentro de dos semanas haces el mismo proceso, te vuelves a descargar el fichero, le importas y sigues a la gente nueva porque yo esto lo actualizo todas las semanas y entonces como no machaca tú cuando le das a importar le puedes decir a simplemente añádeme los nuevos, déjame lo que había pues así vais adquiriendo toda la gente y yo voy descubriendo del mundillo las cuentas que no tenía apuntadas.

43
00:11:34,420 --> 00:11:53,420
Y esto era lo que os quería comentar hoy, luego en próximos episodios hablaremos de cómo estamos empezando a organizar el Akademy que será en Málaga, ya haremos el Calfold Papers por si alguien quiere dar una charla, preguntaremos por ideas por si queréis hacer algo por Málaga, ese tipo de cosas, eso lo dejo para más adelante.

44
00:11:54,420 --> 00:12:12,420
También ahora os estoy preparando alguna entrevista, a ver si consigo hacerlo, es muy difícil cuadrar a gente, ya que estamos aquí haciendo listados de gente implicada con KDE, a ver si consigo traeros a alguien en audio para conocerlo un poco más porque hay muchos proyectos interesantes.

45
00:12:12,420 --> 00:12:27,420
Por ejemplo, Ernesto Acosta acaba de hacer un podcast nuevo que se llama Reset a raíz de que ha vuelto a Linux y él es usuario de KDE y es muy prolífico y está ahora mismo en Mastodon escribiendo un montón, todas las cosas que va descubriendo, la mayoría de cosas le gusta bastante.

46
00:12:27,420 --> 00:12:49,420
Con esto lo dejo por hoy, espero que os haya resultado interesante, ya sabéis que en Mastodon estamos para vuestros comentarios, aparte he hecho una modificación en la web que si en el mensaje de KDE España en el que se anuncia el episodio comentáis algo, esos comentarios salen directamente en la web de KDE. Un saludo y nos escuchamos, nos vemos, nos leemos, hasta luego!

