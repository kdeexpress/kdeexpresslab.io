Buenas, estás de vuelta en KDE Express Episodio 36. Este está 
grabado el 24 de noviembre de 2024 
y va a tener alguna noticia menos de lo habitual porque me 
he tirado unas semanas sin poder entrar en el fe diverso 
porque mi estancia de Mastodon estaba caída, tuvo problemas 
con la dana y aunque se ha recuperado 
no han podido coger la copia de seguridad y han empezado de cero, 
con lo cual he perdido los marcadores y los seguidores y la gente que 
seguía y las listas y todo, con lo cual unas cuantas cosas 
que tenía apuntadas se han quedado en el tintero. 
Por suerte los administradores de la instancia no han tenido 
ningún problema personal simplemente han tenido que 
empezar una instancia de cero, así que mucho ánimo para la 
gente que no ha tenido tanta suerte estas cosas en la tecnología 
pasan. Sirva de recordatorio de que es importante hacer copias 
de seguridad y tenerlas a salvo tanto cuando tenéis un 
servidor como cuando tenéis un cliente porque en Mastodon vosotros 
podéis exportar en csv las personas que seguís, 
los listados, los marcadores, todas esas cosas, no solo 
dependen de vuestra distancia sino que vosotros podéis 
salvarla y tenerla a salvo. Yo ya ahora que he recuperado mi 
cuenta y estoy en una distancia nueva y he seguido a todo el 
que he encontrado que seguía Por cierto, si os seguía y veis 
que no he seguido en masto.es, pegarme un toque porque seguía 
mucha mucha gente y ha sido difícil encontrar a todos 
La cosa es que ahora estoy haciendo copias de seguridad 
regulares y nunca es suficiente recordatorio de que hay que 
hacer copias de seguridad de todo, de todo y testearlas y 
tenerlas a buen recaudo Dicho lo cual, empiezo con las 
noticias de KDE que seguramente es a lo que habéis venido 
Lo primero es, Fedora 42, noticia bomba, va a tener una versión 
oficial de KDE y va a dejar de ser un simple spin 
Esto lo que significa es que tendrá su propia página web, 
que se le tendrá que dar un trato igualitario con la versión de 
Gnome que se llama Workstation, no se 
llama Gnome que la pondrán disponible en 
la descarga en el mismo sitio donde está la oficial 
Vamos, que va a tener los mismos recursos, la misma promoción 
y esto viene gracias a un trabajo muy grande de muchos 
voluntarios del equipo de Fedora que son los que llevaban el 
spin de KDE, que se lo han estado currando mucho 
que han conseguido una versión muy buena y han dicho que esa 
versión se merecía un trato igualitario 
Propusieron el día de los inocentes del año pasado 
cambiar la versión oficial de Gnome a KDE 
Eso evidentemente, siendo Fedora un proyecto paraguas de Red Hat, 
estaba difícil que pasara y entonces han llegado a un 
compromiso que bueno, en vez de hacer eso, 
pues vamos a poner las dos al mismo nivel 
Hubo una votación en los boards que tienen ahí en Fedora 
y hoy ya salió para adelante y parece ser que en la próxima 
versión pues ya deberíamos tener las 
dos disponibles Lo cual, oye, para todos los 
que nos gusta KDE, nos parece que es algo que se merecen 
y que se han ganado Luego aparte, en Fedora 42 
parece ser que van a activar una pantalla 
justo en la bienvenida en la que te va a dejar activar repositorios 
de terceros para directamente que no tengas 
tú que ir a mano y ir ahí por línea de comando 
y activar cosas que se ve que todo el mundo activa al inicio 
Luego, la Steam Deck también se ha actualizado 
solo que se ha actualizado a Plasma 52710 
que es un poco antiguo para nosotros 
pero ellos ya sabemos que van con pies así de plomo, muy 
despacio, asegurándose Incluso la última 52711 es de 
marzo de 2024 o sea, ni siquiera se han 
puesto a la última última de los parches de seguridad de la 
27 pero ellos se ven que cogen un 
Snapthost en algún momento y lo testean, lo testean, lo testean 
Yo la verdad es que no he tenido ningún bug ni ningún 
problema De hecho, os voy a decir que 
tengo por aquí apuntado, le he hecho una captura de pantalla 
cuáles son las versiones de cada cosa que tenía y que 
tiene ahora la Steam Deck La Steam Deck tenía la 527.5 
de Plasma y hemos pasado, como he dicho, 
a la 527.10 Tenía la 5.107.0 de los Frameworks 
Ahora tiene la 5.115 Tenía la 5.15.9 de la versión 
de Qt y ahora tiene la 5.15.12 
Y luego el Kernel han pasado de la 6.1.52 con los parches de 
Valve a la 6.5 con los parches de 
Valve También hay un buen salto ahí 
Bueno, la Steam Deck no es para tener lo último 
aunque esté basada en Arch pero vemos que la siguen manteniendo 
y es un proyecto que sigue y que tiene futuro 
y que incluso se plantean en algún momento 
que se hagan la Steam Deck 2 aunque dicen que todavía le 
tiene bastante vida a esta La cosa es que sigue el 
proyecto para adelante y poco a poco van llegando 
Así que yo contento de que funcione bien 
y de que el proyecto no solo está funcionando 
sino que llegan acuerdos con la comunidad de Arch 
para que implementen con personas que le están pagando 
algunas cosas como firmar actualizaciones digitalmente, 
automáticamente hacer más sencillo el proceso 
de construir Arch para que a la vez que sea más 
sencillo sea posible también hacerlo en 
otras arquitecturas porque se rumorea 
de que a lo mejor sacan una versión con ARM 
y como Arch ahora mismo no lo soporta oficialmente 
sino que hay un proyecto paralelo que sí que lo soporta 
pues se ve que están facilitando que sean los propios de Arch 
la propia comunidad la que lo haga oficialmente 
Valve ve ahí echando una mano como siempre 
al mundo de GNU Linux que aunque no sea una empresa 
de software libre mete pasta y mete soporte en la 
comunidad colabora 
y que seríamos en el gaming sin Proton 
luego volviendo a Fedora que la verdad es que es un buen 
mes para ellos no solo han anunciado lo del 
spin de KDE sino que han lanzado 
unos spins que son Plasma Mobile 
y Kinoite Mobile teníamos el spin de Kinoite 
que es la inmutable o realmente atómica 
pues con esa base lo que han hecho es decir 
bueno si hay gente que usa tabletas 
con la versión de KDE pues vamos a ponérselo más 
fácil y lo han hecho 
no versión de ordenador sino versión para móviles 
que realmente quien más usa esto 
es gente que tiene tabletas grandes 
pero bueno que ya son unos spins oficiales 
y que se están construyendo y que te los puedes descargar 
luego dentro de las aplicaciones tenemos que KDE Wave 
WAVE con V 
en español vuelve a estar en desarrollo 
antes estaba en mantenimiento básico 
que era que se seguía compilando para que saliera en el Gears 
pero no había ninguna mejora 
y ahora mismo han conseguido hacer 
la parte más gorda y fea que es que lo han portado entero 
a Qt6 que es lo difícil 
y alguien ha cogido el mantenimiento del proyecto 
con lo cual significa que si por alguna cosa 
Audacity no te va bien 
y no te gusta pues que sepas 
que esta alternativa que teníamos en KDE 
que estaba un poco abandonada vuelve a tener vida 
y que la van a seguir metiendo caña 
luego viene una noticia de KDE 
que está muy bien que es 
que en la en la pasada 
Akademy en la internacional 
dieron una charla e hicieron la propuesta 
de crear una distribución 
de KDE inmutable 
atómica hecha 
en Arch y BRTFS 
esto es que igual que esta Kinoite 
que estábamos hablando en Fedora 
pues la propia comunidad de KDE 
está pensando hacer igual que tienen 
la versión de Neon que es coger 
Ubuntu y hacerle los cambios 
para que tenga KDE a la última 
pues además esto no sustituye a nada 
simplemente un proyecto de probar 
y si cogemos un Arch igual que hace Valve 
la ponemos a nuestro gusto le hacemos 
inmutable y hacemos varias versiones 
igual que Neon tiene la estable 
la de desarrollador no sé qué 
pues van a hacer como tres ramas 
la de estar a la última a la última 
para que los programadores puedan ahí 
hacer sus cambios y testear 
la de los beta testers y la del usuario final 
entonces tendríamos de la mano 
de la comunidad de KDE 
una distro de la 
de la nueva moda de atómica 
de atómica inmutable 
yo seguramente cuando esto 
esté menos verde le daré una prueba 
porque con mi Arch estoy muy contento 
pero yo estoy encantado 
de que haya distribuciones 
súper sencillas de mantenimiento 
cero para familiares 
ahora mismo lo que estoy probando 
es openSUSE Slow Roll 
dado que el Leap no me gusta 
tener que estar actualizando 
así cosas cordas de año en año 
cuando voy a casa de familiares 
y Tumbleweed era demasiado 
son muchísimas muchísimas 
actualizaciones diarias mucho más que en Arch 
y Linux y aparte 
tienen que ser por líneas de comando 
no con Discover no funcionaba bien 
la última vez que lo probé 
y con Slow Roll estoy viendo 
a ver si eso es estable 
esto sería una inmutable 
o atómica si se dice 
más exactamente sería de lujo 
para ir a casa de tu madre 
dejarse la apuesta que se actualice 
de vez en cuando y que sea súper sencillo 
volver atrás si pasa algo 
y luego tengo una serie 
de novedades de lo que viene 
en Plasma 6.3 aunque no sea 
mi parte favorita porque a mí me gusta 
hablar de cosas que ya tenemos 
entre las manos la verdad es que 
como no paran de sacar noticias 
o voy poniendo algunas ya 
o es que luego cuando llegue el momento 
va a ser tan abrumador que voy a ser incapaz 
de ir haciendo detalladamente 
cuáles son las actualizaciones 
entonces aquí os voy haciendo 
resúmenes que al final 
cuando los suméis pues será el 90% 
de lo que salga y si no el día 
que saliera os tendría que decir 
la mitad entonces 
Plasma 6.3 que seguramente 
salga el 11 de febrero 
de 2025 o sea que 
todavía le queda tiempo 
de que sigan metiendo más cosas 
trae mejoras para tabletes 
esto es la información que va a sacar 
de la tableta tanto como 
su configuración esto es por la gente 
que dibuja hay gente 
que ha hecho alguna review 
de que si había bastantes cosas 
mejorables en Wayland 
y las están teniendo en cuenta 
y están haciendo mejores 
vamos a tener una notificación 
cuando el sistema ha detenido 
algún proceso por falta de memoria 
esto es muy habitual que hay un 
System D sobre todo 
hay un automatismo que si te quedas 
sin memoria empiezas a matar 
procesos para recuperar 
memoria el problema 
es que eso a veces 
mata a lo bruto y tú no sabes 
que está matando y que está pasando 
entonces lo que van a hacer es ponerte ahí 
una notificación gráfica que te diga 
oye que se ha matado 
este proceso por esto 
por si tú quieres manualmente 
ir cerrando cosas que sean 
menos importantes luego 
Dolphin va a tener 
un interfaz para dispositivos 
móviles desde la 2412 
2412 de Gears o sea que en diciembre 
yo ya probé 
ocular en el móvil 
y parece ser que Dolphin 
lo han adaptado para todas 
las versiones de móviles 
que hay disponibles con Linux 
lo cual está muy bien porque Dolphin 
yo creo que como explorador de ficheros 
no tiene parangón luego 
el centro de información mostrará todas tus GPUs 
ya sabéis que podéis tanto por línea de comandos 
como gráficamente decirle 
a la configuración del sistema 
que os diga que tenéis vosotros 
instalados viene muy bien 
sobre todo para hacer reportes 
de hecho tiene un botoncito para cópiamelo en inglés 
que es justo lo que tienes que pegar 
en los bug trackers y en los foros 
pues a la información 
que ya había antes ahora van a tener 
todas las GPUs que tengáis instaladas 
luego también tenemos que Discover 
te informará si la aplicación 
viene directamente de los desarrolladores 
o de voluntarios esto sobre todo 
yo lo he visto en Flatpak 
tú en Flatpak en la página web 
Flash Hub tú puedes ver 
con un check si quien está subiendo 
y quien ha puesto la receta 
para que esté disponible esa aplicación 
son voluntarios de Flash Hub 
o de otras personas o es directamente 
el proyecto el que está 
manteniendo eso así ya sabes 
que si es directamente el proyecto 
pues tiene un plus de confianza 
yo hasta ahora de Flash Hub 
no me he encontrado nada raro 
pero se les puede escapar de que alguien 
suba yo que sé 
cualquier aplicación que no la está subiendo 
el programador principal y de repente 
meta algo que todavía 
no sabemos detectar no me parece 
un peligro muy gordo pero sí me parece 
que el camino a seguir es premiar 
a los que lo están haciendo ellos mismos 
luego dos widgets 
uno es el de impresora ahora muestra 
la información de la cola directamente 
no tienes que hacer ese click extra 
para saber qué es lo que está imprimiendo 
o si ha habido un problema 
con la cola o si el filter 
de caps ha explotado 
facilitando el tener la información 
y luego tenemos que el widget 
de teclado ahora el indicador 
muestra cuando las teclas 
modificadoras están bloqueadas 
o latchet por accesibilidad 
lo del latchet no tengo muy claro 
lo que es lo del bloqueadas 
y que tú cuando tienes problemas 
de escribir con las dos manos 
hay un modo en el que tú haces 
una combinación de teclas 
y ya se queda como si estuvieras 
pulsando el control 
o el alt o lo que esté bloqueando 
para poder con una mano 
ir pulsando la otra 
sin tener que tener la segunda mano 
dándole al control o el alt 
pues igual que teníamos un notificador 
que te dice si está el bloqueo 
mayúsculas o si tienes 
el numpad puesto 
el bloqueo numérico pues ahora 
como mejora de accesibilidad 
han puesto que también sepas 
si estás bloqueando una tecla 
estupendo y con esto 
es el listado de noticias 
que he podido salvar de las que no se me han perdido 
por más todo espero que todos 
os encontréis bien que estéis disfrutando 
del software libre recordar donar 
hacer copias de seguridad participar en las comunidades 
y sobre todo disfrutar de la libertad 
que nos da el software libre un saludo 
y nos escuchamos pronto ¡Gracias por ver el video! 
