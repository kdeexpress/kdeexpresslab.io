 Buenas, 11 de diciembre de 2024,
 KDE Express está de vuelta con
 vuestra ración de noticias del
 mundillo, KDE y software libre
 en general.
 Hoy os traigo una cosa que a
 mí particularmente me gusta
 mucho, que es que una utilidad
 de línea de comandos que
 estaba muy chula,
 como OxyPNG, alguien de KDE, en
 este caso Carl Schwann, ha
 hecho una GUI, una aplicación
 gráfica, para poder usarla.
 No solo alrededor de esta aplicación,
 sino de varias.
 Y lo que ha hecho es OptiImage,
 es una aplicación que te
 permite comprimir imágenes de
 PNG, JPG, WebP, NSVG,
 y sin que tú visualmente prácticamente
 veas nada, le reducen el tamaño.
 Y aparte te dice cuánto ha
 conseguido reducir.
 Esto yo le he tirado bastantes
 horas para conseguir que la web
 y los logotipos que aparecen en
 el feed de KDE Express
 con tecnologías libres ocupen
 lo menos posible.
 Ya sabéis que esto hace que en
 vuestros móviles vaya más
 rápido, gasten menos batería
 y contamine menos al medio
 ambiente,
 aunque no sean kilos y kilos.
 Cualquier ayuda es buena, más
 cuando hace que todo vaya mejor.
 Pues que sepáis que ya están
 empaquetando la aplicación
 para que dentro de poco la podáis
 disfrutar en cualquier entorno
 de escritorio
 o sistema operativo Linux estará
 disponible.
 OptiImage.
 En la ronda habitual de actualizaciones,
 pues tenemos que digiKam trae
 mejoras de reconocimiento
 facial
 y han actualizado la librería
 interna de decodificación RAW.
 La que gusta la fotografía,
 sabréis de qué va esto.
 A mí lo que más me interesa
 es la parte del reconocimiento
 facial,
 porque yo lo uso como biblioteca
 local y como no me gusta que se
 suban ni me identifiquen por
 internet, Google y toda esta
 gente,
 digiKam es lo que mejor he
 encontrado para saber,
 encuéntrame todas las fotos de
 mi hermano o las fotos en las
 que sale mi hija con su prima.
 La verdad es que hasta ahora
 funcionaba bastante bien, pero
 requería un trabajo manual,
 evidentemente no es perfecto,
 pues siguen mejorando este tipo
 de cosas.
 Luego os traigo un enlace, que
 este sí que merece la pena que
 entráis sí o sí, no vale con
 mi simple comentario,
 y es un resumen, un listado de
 todas las cosas que puedes
 buscar con KRunner.
 Sabéis que KRunner es tanto
 el botón de inicio como el al
 espacio, lo tengo yo, que os
 sale ahí arriba del todo,
 aunque lo podéis configurar
 para que salga en el medio, hacéis
 combinación de teclas y empezáis
 a escribir.
 Aquí podéis abrir aplicaciones,
 ficheros, hacer búsquedas por
 internet,
 podéis cambiar la prioridad de
 cómo queréis que os salgan
 las búsquedas.
 Yo ahora que tengo un monitor
 muy largo y ya no tengo doble
 monitor,
 lo que he hecho es que pueda
 cambiar de ventana.
 Entonces cuando pongo console,
 no quiero que me abra una nueva
 instancia de console,
 lo que quiero es que me abra la
 ventana que tengo de console,
 pues tú ahí puedes cambiar
 las preferencias.
 Y en esta web os hacen un detalle
 de las 500 cosas que podéis
 buscar en plan,
 pues si pongo wp2.kde que me
 abra la Wikipedia,
 si abro un traductor, si abro
 cualquier cosa, está prácticamente
 ahí ya predefinido,
 o vosotros podéis poner las puestas
 propias.
 Y os hacen un listado muy concienzudo.
 En el típico artículo que suelen
 poner todas las semanas de
 Point Stick,
 hacen unas retrospectivas sobre
 cómo ha funcionado la ventana
 que se implementó hace poco en
 Plasma 6.2,
 de que una vez al año sale una
 ventanita que dice
 oye, el proyecto KDE está
 hecho por personas, esto requiere
 tiempo, estaría muy bien que
 donaras,
 si no lo puedes desactivar.
 Pues esto ha hecho que en el
 poco tiempo desde que salió la
 ventana,
 se han duplicado las donaciones
 habituales al proyecto KDE.
 Y esto teniendo en cuenta que
 simplemente le ha salido a la
 gente que usa Arch,
 o usa KDE Neon, o a lo mejor
 OpenSUSE Tumbleweed, o sea,
 gente que está muy muy a la
 última.
 O sea, quien esté con Kubuntu,
 no sé si Fedora, o desde luego
 Debian,
 vamos que la mayoría de gente
 no está en Plasma 6.2,
 o sea, desde la poca gente que
 está a la última y le ha salido,
 la recepción ha sido muy buena.
 También han hecho una
 investigación por internet, en
 los foros, en Ready, en Lemmy,
 y han visto que hay prácticamente
 casi nadie quejándose de que
 le haya salido una ventana.
 En Mastodon tuvimos un debate
 sobre el titular de MuyLinux,
 que el artículo estaba bien,
 pero el titular era un poco
 clickbait,
 y decía que si KDE tenía
 publicidad o hacía spam,
 aunque en el fondo decían que
 estaba muy bien,
 había gente que esto llamarlo
 spam y publicidad le parece excesivo,
 que un proyecto de software
 libre que se da gratis te ponga
 una ventana una vez al año
 que tú puedes quitar, llamar
 eso spam o publicidad.
 En el sentido estricto, un poco
 de publicidad podría ser que
 fuera,
 porque se le está dando publicidad
 a las opciones de donación,
 pero vamos, era simplemente por
 la semántica.
 En el fondo, la discusión,
 todo el mundo estaba de acuerdo
 de que ha sido una acción que
 ha funcionado
 y que puede marcar el camino de
 que a la gente hay que animarla
 y animarla fácil para que donen.
 Que no es que estemos todos
 ahí con el bolsillo agarrado
 y nos soltemos un duro, sino
 que a la gente hay que hacerle
 recordatorio
 y ponérselo fácil.
 También os traigo que, si, por
 una mala decisión o a sabiendas,
 tenéis una NVIDIA, que si ya
 os habéis descargado el driver
 565711,
 a partir de ahí, con Plasma 624
 volvéis a tener el HDR disponible.
 Ya sabéis que NVIDIA tiene
 miles de problemas en el
 software libre,
 ya sea por Wayland, ya sea por
 el driver propietario que no se
 lleva bien con el kernel,
 ya sea porque una actualización
 rompe una cosa o porque de
 repente le quitan el soporte,
 o porque el driver de software
 libre no ha tenido el apoyo que
 se ha tenido el DMD
 para que soporte todas las
 características del hardware.
 Bueno, la cosa es que últimamente
 parece que están haciendo las
 cosas algo mejor,
 desde luego no al nivel que
 otros fabricantes y no al nivel
 que nos gustaría,
 pero mejor, de estar lejos los
 tiempos de Linux Torval enseñándoles
 el dedo.
 Con lo cual, ya tenemos una
 cosa que se perdió y que hubiera
 estado bien
 que no hubiera desaparecido
 durante un tiempo, que es el
 HDR,
 cosa que yo ahora estoy empezando
 a probar porque mi nuevo
 monitor tiene
 y la verdad es que, al menos en
 KDE, es súper sencillo.
 Te vas a las opciones del
 monitor y activas el HDR.
 No tiene más.
 Si es verdad que yo nunca he
 sido de un ojo y un oído
 súper fino
 y todavía tengo pendiente
 hacer pruebas de si realmente
 lo estoy notando,
 pero funcionar funciona.
 Y dicen que en las próximas
 versiones todavía va a funcionar
 mejor.
 También tenemos un artículo
 interesante sobre cómo han
 mejorado el plasmoide de impresión.
 La cosa es que uno de los
 desarrolladores de KDE tenía
 un problema él mismo
 con su impresión y entonces
 decidió, digo, pues venga, le
 voy a dedicar mi tiempo libre
 a arreglar esto que me afecta a
 mí y puede ayudar a mucha
 gente.
 Y es que por cómo están hechos
 los plasmoides aquí en KDE,
 no se puede poner todo lo que
 tú quisieras.
 En plan, no puedo poner cuatro
 filas para poder meter esas
 cosas.
 La cosa es que había unos requerimientos,
 unos requisitos
 que no le permitía mostrar la
 información como quería.
 Y según ha ido avanzando la
 tecnología y las ideas de diseño
 que ha conseguido tener,
 ahora en el mismo plasmoide el
 resumen es que vaya a poder ver
 la cola de impresión del
 trabajo.
 Que no tenéis que hacer un
 clic más para meteros en la
 cola
 y entonces dentro de la cola
 pinchar en el trabajo
 y ver que pone error en el filtro,
 que se ha parado o si lo queréis
 cancelar.
 O sea, el resumen muy a grosso
 modo, pero el artículo está
 muy interesante.
 Está en inglés.
 Hablando de accesibilidad, que
 justo hace un momento acabo de
 grabar con Jorge
 accesibilidad con tecnologías
 libres, pero como esto es muy
 pequeñito
 como para comentarlo allí en
 general, es que Dolphin va a
 mejorar la accesibilidad
 con Orca 47 cuando salga Gears
 2412, que ya le queda poco.
 Han hecho varias mejoras a lo
 largo del tiempo y esta es la
 última.
 Esto es una de las cosas que a
 mí me gusta del proyecto KDE
 que tiene muy interiorizado el
 compromiso con, aunque saben
 que KDE Plasma
 ahora mismo no es súper accesible,
 están intentando cada día
 hacerlo mejor.
 Y esto es una de las cosas que
 han conseguido hacer.
 Luego me he encontrado que hay
 una aplicación nueva también,
 esta todavía más nueva que la
 de Optimage, que es Carp,
 que es un editor de PDFs al
 estilo PDF Arranger.
 Antes PDF Arranger se llamaba
 de otra manera, se ve que le
 hicieron un Forge
 porque quedó en desmantenimiento.
 Y ahora dentro del ecosistema
 de KDE, pues tenemos uno que es
 lo típico
 que te deja borrar una imagen o
 una página dentro de un PDF en
 concreto,
 cambiar las páginas de orden,
 hacer alguna cosita en concreto
 a las páginas.
 Está bien tener alternativas
 que se integren bien con el
 escritorio
 y por si volviera a quedar sin
 mantenimiento el PDF Arranger,
 que es lo que yo estaba usando
 hasta ahora.
 Y luego también tengo que
 dentro de todas las distros que
 se han ido sumando
 a poner Plasma 6.2 y dentro de
 las distros, estas son menos,
 que tienen Plasma por defecto,
 pues tenemos que se ha publicado
 openmandriba roma 2412, rome,
 que viene así con la actualización
 que venían de Plasma 5 y ya
 están en Plasma 6 para los que
 tengáis ahí
 el recuerdo de mandrake mandriba
 y openmandriba.
 Luego estarán las noticias que
 os quería contar hoy, pero
 luego quería hacer un recordatorio,
 por si acaso no estáis informados
 vía correo o se os ha pasado,
 que el sábado 21 de diciembre
 a las 10 de la mañana tenemos
 Asamblea General de KDE España.
 Con lo cual aún está ya
 tiempo de asociarse, creo que
 son 20 euros, una cantidad simbólica,
 y estaréis dentro de un
 proyecto con gente muy maja en
 el que tampoco se os pide que
 trabajéis,
 con el ser socio es suficiente
 y simplemente pues tiene acceso
 a la lista de distribución de
 correo,
 al grupo de Matrix y al grupo
 de Telegram.
 Es una manera de decir que apoyas
 el proyecto, aunque no tengas
 que trabajar,
 aunque estamos muy agradecidos
 a la gente como Iván, que
 viene pisando fuerte y viene
 también dedicándonos su
 tiempo.
 Y si no, pues ya os contaremos
 qué es lo que hemos estado
 hablando en la Asamblea General,
 a parte de los típicos rollos
 de cualquier asociación, de
 los presupuestos, las cuentas
 contables
 y todas esas cosas que ni os
 interesan a vosotros ni a mí.
 Pero luego sí que se hacen
 algunas cosas interesantes.
 Por ejemplo, este año vamos a
 proponer dedicar un tanto por
 ciento de los fondos de la asociación
 a algunos proyectos de software
 libre.
 Por ejemplo, ya hemos hecho una
 donación a OP3, que es el
 servicio de estadística que usamos
 en este podcast,
 y entonces de un listado que saquemos
 de todos los socios, en plan,
 pues, cosas que use sobre todo
 la asociación,
 ya sea el proyecto BBB o LibreOffice
 o algunas aplicaciones que usen
 mucho los socios,
 entre todos los fondos que
 tiene la Asociación Cada de
 España, de la Akademy,
 si conseguimos sacar algo con
 las camisetas o de las cuotas
 de los socios,
 un tanto por ciento, para no
 pillarnos los dedos con una
 cantidad exacta,
 lo dedicaremos al proyecto más
 óptimo, que sea como dar ejemplos
 en plan,
 siempre predicamos que hay que
 donar tiempo, conocimiento,
 boca a boca o dinero.
 Pues, KDE España dona mucho
 tiempo, conocimiento y esfuerzo
 de sus socios en divulgar el
 software libre y KDE,
 pero también vamos a donar un
 poco a proyectos que nosotros
 nos estamos nutriendo para
 funcionar como asociación.
 A ver qué os parece esta iniciativa,
 a ver si tenéis propuestas.
 Si no me da tiempo de volver a
 grabar, feliz año, felices fiestas
 a todos,
 y mientras tanto, nos vemos por
 el Fediverso.
 Un saludo.
 ¡Gracias!
