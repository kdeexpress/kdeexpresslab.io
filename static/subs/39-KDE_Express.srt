1
00:00:00,000 --> 00:00:01,520
 bienvenidos KDE express

2
00:00:01,520 --> 00:00:03,030
 tenemos un episodio especial en

3
00:00:03,030 --> 00:00:04,620
 el que recuperamos las píldoras

4
00:00:04,620 --> 00:00:05,220
 en

5
00:00:05,220 --> 00:00:07,120
 este caso va a ser un monotema

6
00:00:07,120 --> 00:00:09,080
 sobre cómo hacer streaming y

7
00:00:09,080 --> 00:00:11,410
 opciones de accesibilidad dado

8
00:00:11,410 --> 00:00:11,740
 que

9
00:00:11,740 --> 00:00:13,590
 son útiles para los dos modos

10
00:00:13,590 --> 00:00:15,370
 entonces lo primero que voy a

11
00:00:15,370 --> 00:00:17,100
 hacer es abrir una aplicación

12
00:00:17,100 --> 00:00:17,760
 que se

13
00:00:17,760 --> 00:00:20,410
 llama show me the keys show me

14
00:00:20,410 --> 00:00:23,070
 the keys la activo y entonces

15
00:00:23,070 --> 00:00:25,240
 lo que hace es poner aquí

16
00:00:26,240 --> 00:00:28,760
 cada cosa que yo tecle si yo pongo

17
00:00:28,760 --> 00:00:33,010
 aquí sdf aparece por ahí vale

18
00:00:33,010 --> 00:00:34,820
 esto lo minimito y ahora lo que

19
00:00:34,820 --> 00:00:35,060
 vamos a

20
00:00:35,060 --> 00:00:37,480
 hacer es abrir las opciones del

21
00:00:37,480 --> 00:00:40,380
 sistema entonces primera opción

22
00:00:40,380 --> 00:00:42,440
 para accesibilidad aparte que

23
00:00:42,440 --> 00:00:42,560
 esto

24
00:00:42,560 --> 00:00:44,300
 puesto yo de que me salgan las

25
00:00:44,300 --> 00:00:46,320
 teclas y que también es útil

26
00:00:46,320 --> 00:00:47,920
 para hacer un streaming por

27
00:00:47,920 --> 00:00:48,800
 internet pues

28
00:00:48,800 --> 00:00:50,820
 nos tenemos que ir a preferencias

29
00:00:50,820 --> 00:00:53,480
 del sistema a gestión de ventanas

30
00:00:53,480 --> 00:00:55,800
 y a efectos de escritorio

31
00:00:55,800 --> 00:00:59,530
 pues yo escribo efectos y automáticamente

32
00:00:59,530 --> 00:01:03,110
 me salen aquí entonces animación

33
00:01:03,110 --> 00:01:05,760
 del clic del ratón esto se

34
00:01:05,760 --> 00:01:07,980
 activa con meta asterisco para

35
00:01:07,980 --> 00:01:10,070
 hacer el asterisco tiene que

36
00:01:10,070 --> 00:01:11,990
 ser en el teclado de letras no

37
00:01:11,990 --> 00:01:12,660
 vale el

38
00:01:12,660 --> 00:01:14,910
 numérico y se hace con el

39
00:01:14,910 --> 00:01:17,270
 shift entonces yo lo que hago

40
00:01:17,270 --> 00:01:20,220
 es meta shift x ahí aparece y

41
00:01:20,220 --> 00:01:22,780
 entonces si pincho se va a ver

42
00:01:22,780 --> 00:01:24,500
 los clics en rojo una vez

43
00:01:24,500 --> 00:01:26,740
 cuando pincho otra vez cuando

44
00:01:26,740 --> 00:01:28,180
 suelto si lo hago muy rápido

45
00:01:28,180 --> 00:01:30,740
 pues se ve así esto está bien

46
00:01:30,740 --> 00:01:31,600
 para cuando tú estás diciendo

47
00:01:31,600 --> 00:01:33,350
 algo que estás haciendo pues

48
00:01:33,350 --> 00:01:35,170
 entonces yo pincho aquí clic y

49
00:01:35,170 --> 00:01:38,560
 se ve perfectamente vale por

50
00:01:38,560 --> 00:01:39,950
 supuesto tiene muchas opciones

51
00:01:39,950 --> 00:01:41,560
 de los colores que pasa si haces

52
00:01:41,560 --> 00:01:41,560
 el

53
00:01:41,560 --> 00:01:44,130
 central si haces con el derecho

54
00:01:44,130 --> 00:01:46,160
 este derecho este del central

55
00:01:46,160 --> 00:01:49,240
 en preferencia avanzada pues

56
00:01:49,240 --> 00:01:50,380
 todo lo que quieras dibujar

57
00:01:50,380 --> 00:01:51,880
 incluso puedes poner el texto

58
00:01:51,880 --> 00:01:53,520
 para que se vea el botón que

59
00:01:53,520 --> 00:01:54,840
 está pinchando pero eso ya lo

60
00:01:54,840 --> 00:01:55,520
 hace la utilidad que yo he

61
00:01:55,520 --> 00:01:55,520
 puesto

62
00:01:55,520 --> 00:01:57,520
 así que no hace falta

63
00:01:57,520 --> 00:02:00,570
 siguiente opción seguimiento

64
00:02:00,570 --> 00:02:03,570
 del ratón seguimiento del ratón

65
00:02:03,570 --> 00:02:05,780
 es útil si la pantalla es muy

66
00:02:05,780 --> 00:02:07,700
 grande y el ratón es pequeño

67
00:02:07,700 --> 00:02:09,630
 en KDE tenemos la opción esta

68
00:02:09,630 --> 00:02:10,480
 de que si lo menea se hace

69
00:02:10,480 --> 00:02:11,520
 grande grande grande grande

70
00:02:11,520 --> 00:02:11,520
 grande

71
00:02:11,520 --> 00:02:12,920
 pero eso no es muy práctico

72
00:02:12,920 --> 00:02:14,510
 para estar apuntando en sitio

73
00:02:14,510 --> 00:02:17,660
 el seguimiento del ratón lo

74
00:02:17,660 --> 00:02:20,040
 activamos con meta control P de

75
00:02:20,040 --> 00:02:22,510
 manera definitiva o con meta

76
00:02:22,510 --> 00:02:24,680
 control de manera temporal eso

77
00:02:24,680 --> 00:02:27,520
 significa que si yo hago meta

78
00:02:27,520 --> 00:02:29,090
 control mientras que lo tengo

79
00:02:29,090 --> 00:02:29,520
 pulsado se va a ver estos

80
00:02:29,520 --> 00:02:29,520
 circulitos alrededor del ratón

81
00:02:29,520 --> 00:02:29,520
 pero si lo suelto se va en

82
00:02:29,520 --> 00:02:29,520
 cambio si hago un meta control

83
00:02:29,520 --> 00:02:29,520
 P se va a quedar fijo y no voy

84
00:02:29,520 --> 00:02:29,520
 a tener que estar pulsando.

85
00:02:29,520 --> 00:02:30,690
 y es una manera más sencilla

86
00:02:30,690 --> 00:02:36,980
 de estar siguiendo el ratón

87
00:02:36,980 --> 00:02:44,520
 ahora vuelvo a hacer meta

88
00:02:44,520 --> 00:02:44,520
 control P y se quita este ya no

89
00:02:44,520 --> 00:02:44,520
 tiene opciones simplemente desactivar

90
00:02:44,520 --> 00:02:44,520
 o desactivar no puedes cambiar

91
00:02:44,520 --> 00:02:44,520
 el color o la velocidad que lo

92
00:02:44,520 --> 00:02:44,520
 hace también tenemos la opción

93
00:02:44,520 --> 00:02:44,520
 de ampliación o lupa

94
00:02:44,520 --> 00:02:52,490
 son esto viene siendo un zoom

95
00:02:52,490 --> 00:03:00,460
 pero hay dos maneras de hacer

96
00:03:00,460 --> 00:03:03,060
 el zoom puedes hacer un zoom

97
00:03:03,060 --> 00:03:03,730
 con lupa en el que simplemente

98
00:03:03,730 --> 00:03:04,420
 se vea por defecto a ver lo voy

99
00:03:04,420 --> 00:03:05,130
 a aplicar entonces el zoom va a

100
00:03:05,130 --> 00:03:07,040
 ser un zoom con lupa en el que

101
00:03:07,040 --> 00:03:09,520
 simplemente se vea por defecto

102
00:03:09,520 --> 00:03:10,980
 por defecto a ver lo voy a

103
00:03:10,980 --> 00:03:13,080
 aplicar entonces el zoom va a

104
00:03:13,080 --> 00:03:15,840
 ser un cuadradito alrededor del

105
00:03:15,840 --> 00:03:17,520
 ratón en el que se va a ir

106
00:03:17,520 --> 00:03:19,310
 viendo más grande esto se

107
00:03:19,310 --> 00:03:22,070
 puede ir haciendo con el meta

108
00:03:22,070 --> 00:03:24,020
 más si no recuerdo más si el

109
00:03:24,020 --> 00:03:24,520
 meta más

110
00:03:24,520 --> 00:03:27,960
 y el meta menos amplían o reducen

111
00:03:27,960 --> 00:03:30,640
 el zoom meta más lo activa

112
00:03:30,640 --> 00:03:32,140
 meta menos lo quita y cuando

113
00:03:32,140 --> 00:03:34,520
 tienes el meta más muy grande

114
00:03:34,520 --> 00:03:36,520
 si le das a meta cero vuelve a

115
00:03:36,520 --> 00:03:36,520
 por defecto

116
00:03:36,520 --> 00:03:38,810
 vuelve a por defecto en cambio

117
00:03:38,810 --> 00:03:40,190
 si te vas a ampliación le damos

118
00:03:40,190 --> 00:03:43,110
 a aplicar si yo hago el meta

119
00:03:43,110 --> 00:03:45,630
 más lo que se hace grande es

120
00:03:45,630 --> 00:03:47,920
 todo el escritorio el escritorio

121
00:03:47,920 --> 00:03:48,520
 me va a seguir con el ratón

122
00:03:48,520 --> 00:03:51,520
 entonces yo puedo ir moviéndome

123
00:03:51,520 --> 00:03:53,440
 salgo por aquí y esto va a

124
00:03:53,440 --> 00:03:56,320
 funcionar cuando hago la opción

125
00:03:56,320 --> 00:03:58,750
 de seguir voy a hacer el meta cero

126
00:03:58,750 --> 00:04:01,250
 que se quite así cuando hago

127
00:04:01,250 --> 00:04:03,200
 la opción de ampliación tengo

128
00:04:03,200 --> 00:04:03,520
 unas opciones extras

129
00:04:03,520 --> 00:04:06,440
 por ejemplo yo lo activo con

130
00:04:06,440 --> 00:04:07,940
 meta más otra vez el teclado

131
00:04:07,940 --> 00:04:09,240
 numérico no funciona tiene que

132
00:04:09,240 --> 00:04:10,560
 ser el que está al lado de la

133
00:04:10,560 --> 00:04:12,680
 p si yo me voy a la derecha yo

134
00:04:12,680 --> 00:04:15,520
 puedo hacer un

135
00:04:15,520 --> 00:04:19,080
 a ver si me acuerdo un meta f5

136
00:04:19,080 --> 00:04:21,790
 y meta f6 vale si yo me voy al

137
00:04:21,790 --> 00:04:25,230
 meta f5 me va a traer de golpe

138
00:04:25,230 --> 00:04:27,740
 al centro de la pantalla y si

139
00:04:27,740 --> 00:04:30,520
 hago el meta f6 me va a llevar

140
00:04:30,520 --> 00:04:33,510
 el meta f6 a donde me llevaba

141
00:04:33,510 --> 00:04:36,710
 el meta f5 al foco y el meta f6

142
00:04:36,710 --> 00:04:37,520
 al centro

143
00:04:37,520 --> 00:04:44,530
 entonces si yo el foco lo pongo

144
00:04:44,530 --> 00:04:45,230
 en este en esta aplicación que

145
00:04:45,230 --> 00:04:47,060
 está aquí me voy acá y meta

146
00:04:47,060 --> 00:04:49,350
 f5 me lleva ahí en medio del

147
00:04:49,350 --> 00:04:51,060
 foco en cambio si me voy a la

148
00:04:51,060 --> 00:04:54,000
 esquina y hago meta f6 me va al

149
00:04:54,000 --> 00:04:55,520
 centro de la pantalla

150
00:04:55,520 --> 00:04:56,960
 otra vez con meta menos puedo

151
00:04:56,960 --> 00:05:00,030
 reducirlo o con meta cero hago

152
00:05:00,030 --> 00:05:02,520
 un reseteo que es como quitarlo

153
00:05:02,520 --> 00:05:04,480
 ampliación es el método que

154
00:05:04,480 --> 00:05:07,840
 viene por defecto luego

155
00:05:07,840 --> 00:05:09,880
 también tenemos hacer dibujos

156
00:05:09,880 --> 00:05:10,520
 o marcas

157
00:05:10,520 --> 00:05:13,520
 esto lo tenemos más para abajo

158
00:05:13,520 --> 00:05:16,060
 marcar con el ratón marcar con

159
00:05:16,060 --> 00:05:19,520
 el ratón tiene cuatro opciones

160
00:05:19,520 --> 00:05:23,200
 uno es dibujar que se hace con

161
00:05:23,200 --> 00:05:26,090
 meta mayúscula y entonces con

162
00:05:26,090 --> 00:05:28,520
 el ratón te vas moviendo y haces

163
00:05:28,520 --> 00:05:29,520
 un dibujo

164
00:05:29,520 --> 00:05:32,240
 luego con meta mayúscula f11

165
00:05:32,240 --> 00:05:35,350
 lo borras todo o si dibujas de

166
00:05:35,350 --> 00:05:37,720
 nuevo con meta shift y haces un

167
00:05:37,720 --> 00:05:38,520
 dibujito

168
00:05:38,520 --> 00:05:41,510
 y haces otro dibujito después

169
00:05:41,510 --> 00:05:44,560
 en vez de hacer el meta mayúsculas

170
00:05:44,560 --> 00:05:45,940
 f11 que lo borra todo con meta

171
00:05:45,940 --> 00:05:49,240
 mayúsculas f12 te va borrando

172
00:05:49,240 --> 00:05:50,520
 los dibujos de tandas

173
00:05:50,520 --> 00:05:52,240
 y luego hay otra opción que

174
00:05:52,240 --> 00:05:54,710
 dibujar flecha directamente eso

175
00:05:54,710 --> 00:05:57,610
 es meta control shift y

176
00:05:57,610 --> 00:05:59,520
 entonces puedes hacer líneas

177
00:05:59,520 --> 00:06:01,920
 desde donde lo pulses hasta

178
00:06:01,920 --> 00:06:03,350
 donde lo sueltes ahí donde te

179
00:06:03,350 --> 00:06:03,520
 va a dibujar la línea

180
00:06:03,520 --> 00:06:05,810
 una vez que sueltes todo te lo

181
00:06:05,810 --> 00:06:07,440
 va a quitar también podemos

182
00:06:07,440 --> 00:06:09,570
 activar a la vez meta control p

183
00:06:09,570 --> 00:06:11,520
 y lo dejamos activado que se vea

184
00:06:11,520 --> 00:06:14,450
 y hacemos un meta control shift

185
00:06:14,450 --> 00:06:15,520
 y hacemos una flecha

186
00:06:15,520 --> 00:06:19,180
 una flecha y otra vez con meta

187
00:06:19,180 --> 00:06:21,520
 mayúscula f12 se pueden ir borrando

188
00:06:21,520 --> 00:06:24,010
 individualmente esto viene muy

189
00:06:24,010 --> 00:06:25,520
 bien pues para hacer exposiciones

190
00:06:25,520 --> 00:06:27,800
 tú puedes yo sobre todo veo

191
00:06:27,800 --> 00:06:30,520
 interesante la de mayúscula

192
00:06:30,520 --> 00:06:31,660
 meta en el que tú puedes estar

193
00:06:31,660 --> 00:06:33,560
 subrayando cosas y se ve lo que

194
00:06:33,560 --> 00:06:34,520
 estás diciendo

195
00:06:34,520 --> 00:06:38,450
 el meta control p lo quito y

196
00:06:38,450 --> 00:06:40,050
 hago luego esto no hace falta

197
00:06:40,050 --> 00:06:42,190
 que se esté viendo esto es el

198
00:06:42,190 --> 00:06:43,240
 dibujo que hayas hecho se va a

199
00:06:43,240 --> 00:06:44,520
 quedar todo el rato en pantalla

200
00:06:44,520 --> 00:06:46,520
 hasta que hagas el método

201
00:06:46,520 --> 00:06:50,090
 shift f12 pues estas son tres o

202
00:06:50,090 --> 00:06:51,520
 cuatro opciones de accesibilidad

203
00:06:51,520 --> 00:06:55,820
 que a la vez te sirven como una

204
00:06:55,820 --> 00:06:56,520
 manera de hacer

205
00:06:56,520 --> 00:06:58,080
 presentaciones o streaming en

206
00:06:58,080 --> 00:07:00,790
 directo más interactiva espero

207
00:07:00,790 --> 00:07:03,510
 que os haya gustado esta píldora

208
00:07:03,510 --> 00:07:04,520
 no sé cómo habrá quedado

209
00:07:04,520 --> 00:07:05,960
 porque la he utilizado para

210
00:07:05,960 --> 00:07:07,770
 hacer una prueba en PeerTube

211
00:07:07,770 --> 00:07:09,680
 ahora veré si es usable y os

212
00:07:09,680 --> 00:07:11,520
 la puedo publicar un saludo

