Buenas, estamos de vuelta en KDE Express, episodio 31, grabado 
el 11 de julio de 2024 y hoy, a punto de cumplir tres 
años de podcast, tenemos un regalo para vosotros, pero en 
especial para mí, que me da mucha alegría 
poder decir que tengo con vosotros a un nuevo socio de 
la Asociación KDE España, Iván G.J., buenas. 
Hola, ¿qué tal? Encantado de estar aquí. 
La verdad es que tenemos una buena racha últimamente en la 
asociación, hemos tenido unas nuevas 
altas, algunas con gente bastante joven, que siempre da 
alegría ver que hay relevo generacional. 
Bueno, joven, joven, no sé yo, ojalá pudiéramos también 
capturar a gente aún más joven, eso sería genial. 
Para quien no te conozca, yo por lo que sé, tienes una 
página web en construcción, tienes 
una cuenta, que es, si no me equivoco, Iván G.J., X, Y, Z. 
Efectivamente. Una cuenta en el Fediverso. 
En Fediverso TV de vídeos. Efectivamente. 
Que es Iván G.J., todo seguido. Y luego, creo que de un pasado 
bastante curioso, tienes una cuenta de YouTube, en el que 
también has colgado vídeos nuevos de 
software libre, pero tienes un pasado de skate, a ver, cuéntanos 
un poco más. Sí, sí, pues, a ver, yo la 
verdad es que desde hace muchísimos años, bueno, yo empecé, 
no sé si a alguno, a alguno le sonará por ahí, a lo mejor, 
de la era de los, de los inicios de, de YouTube, cuando 
empezó a petarlo fuerte en España, a nivel de videojuegos 
y tal, yo estaba, en esa época, pues, tendría 14, 15 años, y 
empecé un poco entrando por ahí, haciendo vídeos chorras 
de, del Call of Duty y estos, y estos juegos de la época, 
y en un momento determinado empecé a hacer skate, se me da fatal, 
¿vale?, se me da fatal, pero hice un par de vídeos y 
por alguna razón lo, lo petaron, lo petaron muy fuerte y, y 
luego como que sentía, me, me ponía nervioso 
el hecho de decir, pero si yo no, yo no sé de esto, o sea, 
yo no sé de skate ni nada, y tenía yo un poco la 
responsabilidad de eso, al final lo, lo acabé dejando, 
bueno, hoy en día están, casi todos los vídeos 
están realmente, eh, ocultos en el canal, no se pueden ver, 
solo he dejado como los vídeos que verdaderamente, 
con los que verdaderamente me siento más orgulloso y demás, 
que son un poco más divulgativos, un poquito 
más de, de, de este rollo que, que me encanta, bueno, y últimamente, 
pues estamos metiéndole, hemos recuperado 
un poco ese canal y hemos empezado también el, el canal en fediverse.tv 
para que haya también una oportunidad a la gente de 
utilizar software libre para, para visualizar 
todos estos vídeos. Yo, lo que he visto es que tiene unos 
vídeos que los vetan con un montón de 
visualizaciones, un parón y ya un nuevo advenimiento, que es 
la que ha recuperado el software libre, 
¿Y cuánto tiempo conoces o estás utilizando software 
libre? Pues mira, yo empecé literalmente como un mes 
antes de, de empezar el confinamiento, allí en 2020, fue cuando, mira, 
y esto es muy importante, quería también aprovechar y 
recalcarlo, lo importante que es que una compañera de 
trabajo, que yo no conocía personalmente, 
porque estábamos, incluso antes ya de la pandemia, 
estábamos un poco teletrabajando, y cuando la conocí por fin en 
la oficina, me di cuenta de que estaba usando Ubuntu, y yo me 
quedé súper extrañado, porque para mí fue como una 
sensación de decir, ¿cómo puede ser, no?, que, 
que esta persona esté usando Ubuntu y que yo ni siquiera me 
haya enterado, porque esta persona no ha 
tenido ningún tipo de problema a la hora de trabajar, todo ha 
ido bien, y eso fue un poco lo que encendió 
esa chispa, por eso digo que es muy importante, la gente no, no, 
creo que no es consciente de lo importante que es sencillamente 
usar el software libre, ¿no?, lo que puede causar eso, lo que 
puede provocar en la gente y hacer que, pues gente como yo, 
de repente, cuatro años después, esté 
haciendo vídeos y divulgando y, bueno, y colaborando en algunos 
proyectos. Entonces, bueno, yo empecé un 
poco ahí, como un mes antes de la pandemia, fue cuando se me 
rompió un portátil y decidí utilizarlo 
como media center para el televisor, y dije, pues lo formateo con 
un Ubuntu y a ver qué tal. Lo maravilloso, dentro de la desgracia 
que fue el confinamiento, fue que 
justo me cogió en ese momento en el que yo, se me había acabado 
de descubrir ese melón, el melón del software libre, el melón 
de Ubuntu, que era todo ese mundo, ¿no?, y entonces, 
durante el confinamiento, me lo pasé fantásticamente 
bien, metiéndome de lleno en todo ese mundo, descubriendo 
que no solo existía Ubuntu, que existían todas esas 
grandes distribuciones, que existía todo ese 
software, incluso que mucho de ese software ya lo estaba 
utilizando, y sin ser consciente de ello, 
y ya es que vamos, o sea, un par de semanas en el confinamiento, 
ya estaba con KDE Neon, ya estaba 
enamoradísimo de Plasma, y el resto, el resto es historia, ya 
en el ordenador del trabajo, en el 
ordenador personal, en el de mi madre... Fue un viaje rápido, 
¿no? No diste muchas vueltas, mucho 
distro hopping antes de caer aquí en Plasma, ¿y sigue 
usando KDE Neon? No, a día de hoy ya estoy 
usando Arch, porque soy una persona que, bueno, ya me iréis 
conociendo, pero soy una persona que me gusta 
mucho estar como a la última, me gusta mucho informarme, me 
gusta mucho aprender, y yo creo que 
Arch Linux te abre muchas puertas en ese aspecto, ¿no? A la hora 
de tener el último software, prácticamente 
a la semana de que haya salido la nueva versión de Plasma, 
pues ya la tienes disponible, no sé, además 
la experiencia también de haberlo instalado y de que te 
salga bien, y todo eso siempre es una experiencia 
que te llena mucho, ¿no? Y aprendes un montón, así que 
con Arch para mí, además la wiki, en fin, 
tengo un montón de razones, pero sí, yo estuve... Sí que 
hice bastante distro hopping al principio, 
pero ya te digo, fue como en un par de semanas, porque teníamos 
como todo el tiempo del mundo en 
la pandemia, entonces yo me ponía ahí una máquina virtual, 
empezaba a juguetear, luego con el Media 
Center este le instalé tres o cuatro distribuciones en tres o 
cuatro días, y bueno, al final, pues como 
le pasa a mucha gente, te quedas en Arch, porque tiene muchísimas, 
muchísimas ventajas para la gente 
como yo. Sí, sobre todo los que somos informáticos, que 
tenemos facilidad y nos gusta trastear, Arch es 
una maravilla para aprender, para jugar y para estar a la 
última, que también da un poco de 
gustirrinin. Y has dicho que empezaste con Ubuntu, con lo 
cual entiendo que empezarías con Gnome, 
¿has hecho también distro hopping con entornos de escritorio? 
Sí, efectivamente, yo creo que casi todo 
el mundo así que empieza un poco de chiripa, arranca con Ubuntu, 
¿no? Porque es lo que conocíamos, yo de 
hecho me acuerdo de incluso un par de años antes, incluso de 
empezar con todo esto, había una página 
web, no sé si te acuerdas, que Ubuntu tenía ahí donde podías 
como jugar con la interfaz de Ubuntu, 
¿no? Era una especie de cosa así, yo me acuerdo de jugar un 
poco con eso y tal, y es que, vamos, es que fue 
automático cuando, pues eso, empiezas a descubrir distribuciones, 
empiezas a ver que tienes opciones, que no solo tienes Ubuntu y 
descubres que cada Plasma, pues vamos, se ajusta perfectamente 
a lo que tú quieres, más que nada 
por el hecho de que lo puedes hacer completamente tuyo, ¿no? 
Y para mí es algo que toda mi vida siempre me ha 
gustado hacer las cosas mías, tener esa capacidad de customización 
y eso que no la llevo para nada al máximo, ¿no? 
Pero sí que me da la sensación de que para mí, pues, Gnome, 
por ejemplo, es excesivamente, te fuerza excesivamente 
a utilizar el ordenador de una manera y, claro, descubrí KDE Plasma 
y el hecho también de que KDE Plasma es que lo instalas y para una 
persona que ha usado Windows toda la vida es muy, muy 
sencillo, ¿no? Ya para empezar y eso que si hoy ves la 
configuración que tengo de KDE Plasma no se parece nada a lo que 
se usaría en Windows, pero eso es lo que me encanta, ¿no? Que 
empiezas así, empiezas pues con la interfaz normal y poco a 
poco vas diciendo, creo que me gustaría más a lo 
mejor tener la barra arriba, más fina, pues este menú no 
lo quiero, me gustaría poner otro, tal, y empiezas a ir jugando 
y te das cuenta que estás usando todo componentes, entre 
comillas, oficiales de KDE, ¿no? Y no tienes que jugar con 
extensiones de terceros y demás y consigues tener un 
un entorno de escritorio perfectamente adaptado a tu 
forma de trabajar. Al final, yo lo he dicho muchas veces 
también, cada persona usa el ordenador de una manera 
completamente diferente. Tú has dicho que eres informático, 
yo soy ingeniero industrial, entonces no tenemos, aunque nos 
parecemos en muchos, en muchas maneras 
de trabajar, a la vez probablemente también no tendremos, no coincidiremos 
en muchas de esas cosas, ¿no? Entonces, para mí Plasma ofrece 
esa posibilidad de adaptarse a cada usuario. 
Es uno de los motos de KDE que es sencillo por defecto, potente 
cuando es necesario. Yo tengo unos ciento y pico ordenadores 
en mi trabajo con Kubuntu y es muy intuitivo. Para la 
gente nueva, botón de inicio, abajo, es sencillote y la gente 
que quiere draftear tiene un mundo ahí de maravillas que 
hacen algunos. Yo soy como tú, yo personalizo, 
pero no tanto. Tengo la barra al lado, alguna cosa he cambiado, 
pero no le meto temas globales, bajado de por ahí, retoqueado, 
que son obras de arte, algunas cosas que hacen por ahí, con 
los peligros de estabilidad que tiene, tengo que tener tanto, 
pero la verdad es que maravillas visuales. 
Sí, sí, sí, totalmente. Sí, dime. 
Una duda que tengo, ¿en tu trabajo no te pusieron ninguna 
pega en que cada uno pongáis lo que queréis? 
La verdad es que tuve la maravillosa suerte de que no, no me dieron 
ningún problema. Además, yo ya en ese momento, cuando decidí 
instalarlo en el trabajo, ya un poco me había dado cuenta de 
que Ubuntu en particular, que es la típica, ¿no? Ubuntu, 
Kubuntu, bueno, todas las, todo lo que viene hasta debajo del 
paraguas de Canonical, que no me gustaba particularmente 
a lo mejor su filosofía y entonces me decanté sobre todo 
por Debian, porque es Debian, ¿no? Debian es indiscutible y, 
vamos, no me dieron ningún tipo de problema, así que en 
ese sentido tuve muchísima suerte y hasta el 
día de hoy usando Debian en el trabajo. 
¿A raíz de eso has encontrado más compañeros o compañeras 
que también usen Linux o sois vosotros dos por ahora la avanzadilla? 
No, o sea, efectivamente, después de descubrir a mi 
compañera de trabajo, en el equipo de software de la 
empresa y en el equipo del firmware también, hay varios, 
habían varios usuarios también de Linux, de GNU/Linux 
y todos usaban Ubuntu realmente. Y sí que después, cuando 
conseguimos contratar a un compañero nuevo en el departamento, 
me costó convencerle dos días de que efectivamente tenía que 
instalarse GNU/Linux y la verdad es que está, sinceramente, 
encantadísimo. De hecho, es que toda la gente 
a la que he conseguido, amigos y demás, a los que les he 
conseguido convencer, todo el mundo siempre me ha dicho, wow, 
o sea, es que esto, ¿cómo puede ser que no lo hubiera 
conocido antes, no? Con lo bien que funciona, los cero 
problemas que, que trae, al menos, cero 
problemas es mentira, porque al final todos sabemos que todos 
los sistemas operativos tienen sus movidas. Y eso es algo que 
no tenemos nunca que olvidar. Pero es que es así, me acuerdo 
de que este último compañero de trabajo es que se puso el típico 
dual boot, ¿no? De, vale, pues voy a ponerme uno con Windows y 
uno con Linux y tal, a la segunda vez que reinició a 
Windows y que Microsoft le borró no sé qué historia, 
de la partición, de no sé qué, que a algunos os sonará, 
las típicas historias estas. En ese momento dijo, mira, ¿sabes 
qué? A tomar por saco. Limpió el disco duro entero y ahora va 
100% Linux. Y nunca había usado el tío, nunca había usado New 
Linux para nada, ¿eh? Y ahora está 100%. 
Sí, la verdad es que hay gente que se encuentra bugs porque 
todos los softwares del mundo tienen bugs, pero yo creo que 
estamos en el mejor momento, en el momento más sencillo y que 
mejor funciona. Linux, KDE, Gnome, cualquier 
distribución, la verdad es que estamos muy bien. Y que haya 
tanta gente en tu trabajo usándolo, la verdad es que es muy buena 
señal. Y si encima estás tú para ir metiendo ahí la patita 
de, pero con KDE ya veréis qué bonito, mejor todavía. 
Sí, sí, de hecho, mira, justo ahora esta compañera de 
trabajo, de hecho, esta misma compañera de trabajo que me 
descubrió el mundo, sin querer prácticamente, porque ella 
tampoco es que sea ninguna fiel, seguidora férrima del mundo 
del software libre ni nada, pero esta misma compañera, el 
ordenador está en las últimas, está dándole muchos problemas 
y ya me ha comentado que probablemente el próximo ordenador 
le va a meter un Kubuntu o algo de ese estilo porque, bueno, al 
final también, ¿no? Eso también es una virtud 
súper importante de este mundo. Y en cuanto a aplicaciones, tú 
a nivel particular, ya sé por más todo aunque usas Kdenlive, 
pero ¿usas más algo del ecosistema de KDE o a nivel de trabajo usas 
alguna aplicación que te ha costado que funcione en Linux o 
has tenido todo una alfombra de rosas y te ha ido todo a la 
primera? Pues, obviamente, no sé cómo 
se dice Kdenlive o Kdenlive, no sé, bueno, es un nombre un 
poco... Un poco raro, tenemos que 
reconocerlo, pero sí, obviamente, Kdenlive es, vamos, 
es brutal, es brutal, para mí es una herramienta súper 
importante. Krita también, especialmente 
desde la charla que dio alguien en el Es Libre hace un par de mesecillos, 
estoy completamente enamorado, además yo era muy usuario de GIMP, 
incluso GIMP, incluso, como he dicho, 
lo que es el... antes de 2020, ¿no?, y la pandemia y todo 
esto, yo usaba GIMP porque funcionaba, o sea, funciona, 
bueno, funciona perfectamente, ¿no? 
Y un poco he descubierto también que, oye, que Krita es 
que funciona igual o mejor incluso casi que GIMP, ¿no?, 
tiene algunas características que van bastante más allá de 
lo que puede hacer GIMP. Entonces, esas son dos grandes 
herramientas. Kate, obviamente, para 
cualquier nota rápida que haya que sacar por ahí. 
Y, no sé, ocular, por supuestísimo, ocular es infalible. 
Y, no sé, software ya un poco, a lo mejor, más fuera de KDE, 
pues sí que a nivel del trabajo sí que usamos Visual 
Code porque al final es un poco... El estándar. 
Un poco el estándar, sí. Y, además, o sea, hay que 
reconocerlo, o sea, funcionar funciona bien, me gustaría 
herramientas a lo mejor que, o sea, me gustaría, wow, o sea, 
si KDE pudiera hacer algo así que creo que existe, 
o sea, hay que reconocerlo, o sea, hay que reconocerlo, o sea, 
hay que reconocerlo. Y, bueno, también me gusta 
otro software, uso mucho Helix o Helix, que es una especie de 
alternativa a Vim y Neovim, que lo está apretando muy 
fuerte, lo que pasa que aún no está muy completa, entonces 
por eso estoy un poco usando VS Code, que uso un plugin para 
que me ponga los comandos de Helix para ir un poco entrenando 
para cuando por fin el software esté listo y dejar ya el 
software de Microsoft completamente eliminado. 
Y, pues, no sé, ¿qué más cosillas? Pues, es que no sé, 
hay tantas cosas, la verdad, que es maravilloso. VLC 
también lo uso un montón, estoy usando también desde 
hace ya un par de años, se llama Hakura, puede ser, un 
software que es de KDE, que es un reproductor de vídeo. 
Ahí peco, ahí soy de VLC, tengo que admitirlo. 
Sí, pero yo VLC siempre lo tengo instalado porque es como, 
sabes que nunca va a fallar, pero luego tengo también 
algún otro software. Si el vídeo se puede abrir, VLC 
te lo abrirá. Efectivamente. Elisa, también, 
para reproducir música y demás, aunque, de nuevo, 
también muchas veces al fin y al cabo reproduciendo con VLC, 
porque es imbatible. VLC, si no me equivoco, está hecho con Qt 
también, ¿no? A ver, librerías Qt yo le he 
visto que tiene, no sé si es que tiene las dos, pero yo sí 
que lo he visto. Lo que pasa es que no está dentro del paraguas 
de la KDE e.V., es simplemente que usa las librerías. 
Aparte, creo que está hecho mayoritariamente en Francia, 
así que tiene algo Europeo que siempre está aquí. 
Ah, por cierto, claro, es que al final es que está tan, es 
tan natural ya que, pero Dolphin, Dolphin, es que claro, es que 
es, ya te digo, es tan obvio prácticamente que se me olvida, 
¿no? Y quería preguntarte, aparte 
de las aplicaciones, por tu vena divulgativa, porque está claro 
que tienes un canal de YouTube, te has abierto uno de Virtube, 
has entrado también en Mastodon, has entrado en la asociación, 
o sea, se ve aquí una entrada con energía. ¿Cuáles son tus 
planes? ¿Cuáles son tus planes? 
Sí, aparte te he visto que estás investigando, porque 
esto también es un crossover con GNU/Linux Valencia, que 
los dos somos socios también, y GNU/Linux Valencia ha difundido 
mucho GNU Taler, y he visto que tienes un vídeo, que aparte 
tiene bastante recorrido por lo que veo en el Fediverso, que se 
ha compartido mucho, que eso lo conocías de antes, lo has 
investigado ahora y... Es que tal cual, es que para 
mí hay un vídeo mío de esto, de hecho se acaba de publicar 
un... Un blog en KDE Blog, que un poco 
enlaza a este vídeo mío, donde explico que mi 
experiencia en Es Libre fue lo que abrió un poco este mar. 
Entonces, a partir de ahí es donde empecé a investigar las... 
Bueno, antes incluso de ir a Es Libre, viendo las asociaciones 
que participaban, pues empecé a investigarlas y tal, y fue 
como, guau, o sea, espérate, estoy... De repente he encontrado 
a un montón de gente. Que igual, oye, igual esto 
puede molar mucho. Y fue un poco a raíz de ahí 
que empecé a escuchar podcasts del KDE España, el KDE Express, 
y una de esas cosas que apareció por ahí fue la charla de 
Richard Stallman, que dio en Valencia en abril, hace unos 
meses, si no me equivoco, y pues escuché la charla y 
demás, y ahí hablaban de GNU Taler, y fue donde dije, espérate un 
momento, si yo estaba el otro día sacando a los perros y me... 
Y estaba pensando en ideas así de este estilo, ¿no? De cómo 
molaría que se desarrollara un software de ese estilo. Y fue 
genial encontrar que ya existía, la verdad. Y ojalá, ojalá que 
salga bien. Sí, yo recuerdo una charla de 
Javier Sepúlveda en Las Naves, en un FLISoL, si no recuerdo 
mal, que también hablaba de GNU Taler y parecía una cosa 
muy interesante. Y son de las cosas que cuestan porque hay 
muchos movimientos mayores a nosotros que tiran por otro 
camino. Pero ahí está el software 
libre, igual que hemos entrado, que ahora mismo nadie piensa 
que una multinacional pueda trabajar sin software libre, 
con suerte iremos abriendo otras vías. 
Sí, tal cual. Bueno, por si no lo conoce la gente, que 
investigue sobre GNU Taler, porque la verdad es que es una 
herramienta brutal que tiene un montón de futuro, al menos 
para mí, lo veo un poco así. Ojalá que podamos recuperar 
ese terreno también en el mundo del software. 
En las notas del programa dejaremos los enlaces a todas tus cuentas 
y ahí pueden ver el vídeo y hacerse una idea de lo que es. 
No le especifico aposta para que la gente le pique la curiosidad 
y vaya a verlo ahí. Genial, genial. 
En nivel comunidad de software libre, ¿te ha parecido que has 
encontrado otros cuatro gatos como tú o que parece que la 
cosa es más de lo que te pensabas cuando estabas solo investigando 
por internet? Pues, hombre, como dices, acabo 
de aterrizar un poco, entonces aún me queda mucho por 
aprender, pero la verdad es que me ha encantado. Solo el hecho 
de la organización durante el es libre y todas las participaciones. 
No sé, me flipa. Es que es justo lo que me encanta, la 
divulgación y demás. Y yo creo que KDE España es una 
herramienta maravillosa para conseguir ese objetivo, que la 
gente conozca este mundo. Y la verdad es que, bueno, trabajaremos 
a muerte para ello. Sí, porque la verdad es que es 
muy divertido estar en el mundo del software libre descubriendo 
cosas, pero yo desde luego lo disfruto mucho más cuando 
estoy en comunidad con gente afín. Y sabiendo que los esfuerzos 
los hacemos con partidas. De hecho, hoy me gustó mucho ver 
que tanto aquí como tú habíais hecho vídeos de experiencias 
de cómo estuve en libre. Es algo que a mí por tiempo no me 
ha sido posible, que otros eventos sí que tienen y que es 
algo que yo he echado en falta y que gracias a vosotros, pues, 
ahora esa parte está completada. Con dos personas más que hicieran 
otras dos cosas, iríamos rellenando los huecos y distribuyendo el 
trabajo, que en el fondo cada uno haciendo lo que a él le 
guste, le divierte, no es tanto trabajo. 
Sí, siempre para divulgar, para dar la sensación o para 
transmitir qué es lo que se siente cuando se está ahí. Al final, 
la gente tiene que poder ver que hay un mundo ahí fuera, 
que se pueden hacer otras cosas y yo creo que gente como vosotros, 
que para mí si sois jóvenes, al menos en comparación conmigo, 
viene muy bien que vayamos teniendo una escala generacional difundiendo 
de diferente manera y con diferentes visiones dentro de 
una cosa global. Ojalá que esto siga funcionando 
tan bien como lo he hecho hasta ahora. De hecho, viendo, por 
ejemplo, congresos como el Open South Code de Málaga, si no me 
equivoco, he visto también ideas muy interesantes también 
a nivel de divulgación y creo que, bueno, ahora tenemos un 
montón de herramientas, incluso también con la nueva 
inteligencia artificial y demás, que creo que nos van a 
permitir llegar bastante a la gente. Así que ojalá, ojalá 
que siga creciendo esto. Ha pasado volando esta media 
hora. Quiero hacer un llamamiento a que la gente que nos escucha 
haga como tú, hacer como Iván, asociarse, participar. 
Por favor. O sea, si hay algún congreso de software libre, ir. 
O sea, no tengáis ningún tipo de miedo, vais a conocer a 
gente maravillosa, gente que es completamente normal, no son 
extraterrestres. O sea que, por favor, haceros el ánimo porque 
hay gente detrás de todo esto, que eso es algo que yo descubrí 
también gracias al... Ales Libre. Y guau, o sea, es 
que os vais a encontrar como en casa y va a ser, de verdad, va 
a ser una experiencia maravillosa. Sí, porque es a la vez una 
comunidad diversa, porque somos gente de diferentes sitios, con 
diferentes backgrounds, con diferentes intereses, pero a la 
vez tenemos una cosa en común que hace que rápidamente hagas 
click con cualquiera que te cruces por un pasillo y tengas conversación 
y tengas tema para rato. Es que yo lo disfrute también 
muchísimo. Y no quiero terminar el episodio 
sin dar las gracias a nuestro editor de... 
A la vez será Jorge Lama, que ha vuelto de sus mini vacaciones 
y este episodio, por suerte para vosotros y sobre todo para 
mí, va a volver a editar él y a mí me va a dar la vida dándome 
ese tiempo para poder hacer otras cosas. Así que gracias, 
Jorge. Y también gracias a vosotros 
que nos estáis escuchando y que seguramente estáis yendo a 
correr, haciendo la compra o en la playa, pero estéis dedicando 
vuestro tiempo a informaros sobre software libre y CAD en 
particular. ¿Tú quieres decir unas últimas 
palabras, Iván, para despedirnos? Que muchísimas gracias, que viva 
CAD, viva el software libre y nos vemos por ahí, en las 
redes, por la calle, por donde sea. 
Venga, un saludo y gracias por vuestro tiempo. 
Gracias, chao. 