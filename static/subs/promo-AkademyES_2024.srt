1
00:00:00,000 --> 00:00:05,110
 El 24 y 25 de mayo la Asociación

2
00:00:05,110 --> 00:00:07,590
 KDE España celebrará Akademy

3
00:00:07,590 --> 00:00:09,760
 ES 2024 en Valencia,

4
00:00:09,760 --> 00:00:11,650
 el evento anual más importante

5
00:00:11,650 --> 00:00:13,520
 de la comunidad KDE que se enmarca

6
00:00:13,520 --> 00:00:15,360
 dentro de otro de los grandes

7
00:00:15,360 --> 00:00:17,460
 eventos libres como ES Libre,

8
00:00:17,460 --> 00:00:19,740
 coorganizado con la Asociación

9
00:00:19,740 --> 00:00:22,320
 Anfitriona GNU Linux Valencia y

10
00:00:22,320 --> 00:00:23,940
 gracias al patrocinio de la

11
00:00:23,940 --> 00:00:26,250
 Universidad de La Laguna y OpenSUSE.

12
00:00:26,250 --> 00:00:27,840
 El espacio elegido ha sido

13
00:00:27,840 --> 00:00:30,030
 Las Naves, el centro de innovación

14
00:00:30,030 --> 00:00:31,800
 social y urbana de la ciudad de

15
00:00:31,800 --> 00:00:33,300
 Valencia y La Mutant,

16
00:00:33,300 --> 00:00:34,930
 el espacio de artes vivas. Con

17
00:00:34,930 --> 00:00:36,520
 esta será la segunda ocasión

18
00:00:36,520 --> 00:00:38,140
 que los simpatizantes del

19
00:00:38,140 --> 00:00:40,240
 proyecto KDE visiten la histórica

20
00:00:40,240 --> 00:00:42,460
 capital de la comunidad Valenciana

21
00:00:42,460 --> 00:00:44,540
 tras Akademy-es del 2018.

22
00:00:44,540 --> 00:00:46,500
 Desde KDE España se alegran de

23
00:00:46,500 --> 00:00:48,970
 trabajar junto con los organizadores

24
00:00:48,970 --> 00:00:49,840
 de ES Libre,

25
00:00:49,840 --> 00:00:51,160
 uno de los eventos más

26
00:00:51,160 --> 00:00:53,020
 importantes para la promoción

27
00:00:53,020 --> 00:00:55,340
 y visualización de las tecnologías

28
00:00:55,680 --> 00:00:57,510
 Estamos seguros disfrutaremos

29
00:00:57,510 --> 00:00:59,320
 de unos días fantásticos, de

30
00:00:59,320 --> 00:01:01,140
 ponencias, presentaciones,

31
00:01:01,140 --> 00:01:03,540
 talleres, conversaciones, intercambio

32
00:01:03,540 --> 00:01:05,210
 de ideas y de cultura libre en

33
00:01:05,210 --> 00:01:05,820
 general.

34
00:01:05,820 --> 00:01:07,870
 Durante el evento, como es costumbre,

35
00:01:07,870 --> 00:01:09,390
 se realizarán charlas tanto

36
00:01:09,390 --> 00:01:10,240
 para usuarios

37
00:01:10,240 --> 00:01:11,920
 como para desarrolladores,

38
00:01:11,920 --> 00:01:13,470
 además de talleres prácticos

39
00:01:13,470 --> 00:01:15,480
 y otras actividades de carácter

40
00:01:15,480 --> 00:01:15,760
 más

41
00:01:15,760 --> 00:01:17,340
 social con las que se pretenden

42
00:01:17,340 --> 00:01:19,370
 cumplir los siguientes objetivos.

43
00:01:19,370 --> 00:01:20,800
 Poner en contacto a

44
00:01:20,800 --> 00:01:22,550
 los desarrolladores de KDE de

45
00:01:22,550 --> 00:01:24,230
 toda España para que puedan

46
00:01:24,230 --> 00:01:25,660
 hablar de los proyectos,

47
00:01:25,660 --> 00:01:27,240
 en que están trabajando,

48
00:01:27,240 --> 00:01:29,150
 compartir código, experiencias

49
00:01:29,150 --> 00:01:31,160
 y conocimiento. Dar a conocer

50
00:01:31,160 --> 00:01:33,300
 los proyectos KDE como el entorno

51
00:01:33,300 --> 00:01:35,590
 de escritorio a nuevos usuarios.

52
00:01:35,590 --> 00:01:37,160
 Divulgar acerca de las

53
00:01:37,160 --> 00:01:38,910
 tecnologías KDE tanto para

54
00:01:38,910 --> 00:01:40,680
 nuevos desarrolladores como

55
00:01:40,680 --> 00:01:42,180
 para usuarios que quieran

56
00:01:42,180 --> 00:01:43,840
 conocer mejor KDE.

57
00:01:43,840 --> 00:01:45,740
 El programa de ponencias, ya

58
00:01:45,740 --> 00:01:47,810
 publicado, se desarrollará el

59
00:01:47,810 --> 00:01:49,660
 viernes 24 de mayo a partir

60
00:01:49,660 --> 00:01:51,820
 de las 12 del mediodía. Componentes

61
00:01:51,820 --> 00:01:53,620
 de la talla de Lorenzo Carbonell,

62
00:01:53,620 --> 00:01:55,480
 el Atareao o Alien. Además,

63
00:01:55,480 --> 00:01:57,310
 de los ponentes tradicionales

64
00:01:57,310 --> 00:01:59,350
 del evento, Albert Astals,

65
00:01:59,350 --> 00:02:01,140
 Aleix Pol, Agustín Benito,

66
00:02:01,140 --> 00:02:03,630
 José Millán o Baltasar Ortega.

67
00:02:03,630 --> 00:02:05,760
 Para el sábado 25 de mayo,

68
00:02:05,760 --> 00:02:09,130
 Akademy-es 2024 se celebrará en

69
00:02:09,130 --> 00:02:09,340
 forma

70
00:02:09,340 --> 00:02:10,890
 de evento social alrededor de

71
00:02:10,890 --> 00:02:12,590
 su stand. En él se realizarán

72
00:02:12,590 --> 00:02:14,480
 concursos, entrevistas y charlas

73
00:02:14,480 --> 00:02:16,610
 distendidas entre los asistentes

74
00:02:16,610 --> 00:02:18,520
 y podréis encontrar una buena

75
00:02:18,520 --> 00:02:20,160
 muestra del software de

76
00:02:20,160 --> 00:02:21,890
 la comunidad KDE, así como de

77
00:02:21,890 --> 00:02:23,790
 hardware que los implementa por

78
00:02:23,790 --> 00:02:25,300
 defecto como ordenadores,

79
00:02:25,300 --> 00:02:27,330
 de la compañía Slimbook o la

80
00:02:27,330 --> 00:02:29,480
 Steam Deck, la famosa consola

81
00:02:29,480 --> 00:02:32,100
 portátil de Valve. Además de

82
00:02:32,100 --> 00:02:35,060
 merchandising de freeware, camisetas,

83
00:02:35,060 --> 00:02:37,620
 tazas, gorras, etc. Os esperamos

84
00:02:37,620 --> 00:02:39,840
 en Akademy-es 2024 de

85
00:02:39,840 --> 00:02:42,770
 Valencia, esLibre Edition. No

86
00:02:42,770 --> 00:02:44,750
 puedes faltar. Más

87
00:02:44,750 --> 00:02:52,640
 información en www.kde-Espana.org/Akademy-es-2024

88
00:02:55,120 --> 00:03:01,700
 ¡Hackers, you'll be free!

89
00:03:01,700 --> 00:03:08,040
 (música)

