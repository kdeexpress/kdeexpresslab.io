Buenas, estamos en KDE Express, el número 27. 
Hoy tenemos dos novedades, que es que va a ser un especial 
Akademy y que tenemos con nosotros, 
preparados todo el mundo, con Baltasar. Buenas, Baltasar. 
Hola, David. Esas presentaciones no tienes que hacerlas así, 
porque tampoco soy nadie de otro mundo. 
Bueno, pero es solo una muestra de una parte de la alegría 
que me da que tenerte por aquí. Que siempre... Leerte es bueno, 
pero verte y escucharte es todavía mejor. 
Esto es energético, porque últimamente en mi blog 
el nombre que más aparece es el tuyo. 
Así que, una cosa por la otra. Creo que hacemos un buen equipo. 
Cuando nos veamos presencialmente ni te cuento. 
Bueno, y hablando de presencialmente, pues hoy venimos a hablar de la 
Akademy-es en Valencia, que la haremos el viernes de 24, 
creo que es de mayo. Correcto. 
Correcto. Y venimos a contaros qué podéis esperar 
de la Akademy-es en Valencia. De las charlas que tenemos, ¿no, 
Baltas? Sí, la idea de este podcast, 
cada vez más podcast, pues es eso. 
Daros un poquito más de información, 
más que los simples títulos de las ponencias que hay. 
Y, bueno, quizás deberíamos explicar un poco 
por qué ha cambiado la estructura de la Akademy-es 
respecto a otros años, ¿no, David? 
Correcto, porque antiguamente hacíamos dos días 
y también antiguamente normalmente era un evento solo, 
independiente. Últimamente nos estamos juntando 
con más gente, que hemos visto que... 
Más divertido y más enriquecedor mezclarnos con el resto de la 
comunidad y, además, hemos decidido reducir 
a un solo día, ¿no? Sí, la idea es vamos a poner 
un solo día para todas las ponencias, 
centrándolas en aspectos interesantes de KDE 
y sin olvidar que KDE está abierto a otras aportaciones, 
lógicamente, y dejar el otro día que 
compartimos con Es Libre para estar en la mesa, 
aunque la mesa también estará el viernes, 
pues en la mesa habrá más gente el sábado 
y también, ¿por qué no?, por recorrer las salas y los 
pasillos de Es Libre y mezclarnos con la comunidad 
para que tengamos sangre mezclada, porque la sangre pura nunca es 
buena del todo y así, pues, crecemos como 
comunidad, es decir, intercambiamos genes, 
eso es la base de la evolución, pues más o menos vamos a hacer 
lo mismo, vamos a intercambiar genes, 
vamos a tener conocimiento con el resto de las comunidades 
libres. Por un lado nos mezclamos más 
y por otro lado los voluntarios tenemos la oportunidad de asistir 
a charlas que la verdad es que da un poco 
de envidia cuando estás todo el rato atendiendo 
a una charla de Akademy o atendiendo a la mesa 
y ver que hay tantas cosas que pasan por ahí, 
decir, hombre, por lo menos un día 
que nos podamos ahí relacionar y ver otras cosas 
sin desatender que siempre habrá alguien en la mesa 
y el viernes siempre habrá gente atendiendo a las charlas, 
pero hay que tener tiempo para todo 
porque el ser voluntario, lo haces con mucha ilusión, 
pero es un poco duro y hay que también darle margen 
a la gente que nos ayuda. Correcto, y además con la propuesta 
firme que tenemos de que el sábado en la mesa de 
KDE pues pasarán cosas, 
habrán concursos, habrán seguramente entrevistas, 
habrán lo que se nos vaya ocurriendo para dinamizar 
y que la gente que venga a vernos que se lleve una bonita impresión 
y seguramente alguna pegatina. Y luego, por supuesto, en la 
mesa, tanto viernes como sábado 
tendremos el merchandising típico de las camisetas, posiblemente 
gorras, los mismos tazas, cosas varias 
y los Konqis y las Katies que son nuestro elemento estrella 
sobre todo para mi hija que está deseando que vayamos 
a comprar alguno más. 
Seguro. Pues nada, ¿empezamos? Si quieres, ¿empiezas tú viernes 
24 de mayo 12 de la mañana? 
Pues sí, porque con el interés de no tener demasiadas charlas 
y hacerlo bastante concreto, pues, 
nos ha quedado un evento así condensado, yo diría la 
palabra sería condensado, y empieza 
justamente a las 12, 12.05 
con lo que se llama la ceremonia de apertura 
de Akademy-es, que como no somos políticos 
ni somos así muy prolíficos, pues con 5 minutos 
tenemos suficiente para que Adrián Cháves 
que es nuestro presidente de KDE España, pues, nos dé 
la bienvenida, nos dé 
los agradecimientos que justamente 
se tienen que dar a Añulis Valencia como asociación 
que nos ha ayudado a organizar esto 
y evidentemente a Es Libre, que también 
evidentemente tiene gran parte, 50% de culpa de esto 
de que nos veamos en mayo en Valencia 
y también dará información ya más detallada sobre los 
eventos sociales que tenemos previsto realizar. 
Enseguida empezaremos a las 12.05 con la primera charla en sí 
que es Craft, la distribución de KDE para plataformas no 
Linux, que la da Albert que es un clásico en 
Akademy y en todo lo que sea KDE 
y en esta charla se explicará qué es y cómo funciona Craft 
que es un sistema que usamos en KDE 
para generar los binarios, los instalables, por así decirlo, 
para sistemas Windows, Mac y Android 
o sea, todo lo que es fuera de GNU Linux. Es un tema 
realmente que a mí me ha interesado, es decir, que 
cómo hacer que un desarrollador haga algo y le sirva 
para casi todas las distribuciones y sean libres 
o privadas. Y no sé si Albert, el gran Albert, que no haya 
visto en la charla Albert esta es un must-have, 
nos lo explicará. Diría el subtítulo de 
cómo hacer menos infierno el hacer llegar nuestras aplicaciones 
a otros sitios. Sí, estaría muy bien. 
Cómo estar en todos los sistemas aplicativos 
sin morir en el intento. ¿Y la siguiente? 
La siguiente es a las 12.45 el horario siempre es aproximado, 
¿de acuerdo? De 12.45, hora y media, pues 
tenemos Python y Qt a cargo de 
José Millán Soto, otro de los que es tesoro de KDE España 
y es otro de los grandes desarrolladores 
de los integrantes del grupo de KDE España. 
En esta charla nos va a explicar 
una fructífera relación entre el lenguaje 
de programación Python y las librerías Qt 
¿vale? Sabemos que KDE se basa en librerías Qt 
y se hablará de las librerías específicas de 
PyQt y PySite. Mostrando algunos ejemplos. 
Esta sería una de las charlas técnicas que vamos a 
disfrutar. Bueno, los que sepáis, porque a mí 
realmente me interesa mucho pero se me da un poco alto. 
Siempre es interesante escuchar. Está un poco más enfocada a 
desarrolladores que creo que se hizo una parecida 
en Málaga y como tuvo buena acogida 
dentro del mundillo de desarrollador 
pues va a ser como una versión 2. 
Correcto, me parece súper interesante. 
¿La siguiente cuál es? Pues a las 1.30. 
De 1.30 a 2 de la tarde tenemos nuestra primera estrella 
invitada en nuestro elenco 
que es Types o TypST porque es un poco 
difícil de pronunciar como alternativa 
a Latex y Markdown. Y la da 
Lorenzo Carbonell, archiconocido como 
Atareao, en la que va a hacer una introducción 
a Types que es un sustituto, entre comillas, de 
Latex pero al estilo Markdown. Es una charla donde se contarán 
con detalle las ventajas del lenguaje, 
como Markdown, y todas las posibilidades que te ofrece Types. Esto es 
especialmente interesante para los que quieran elaborar 
documentos profesionales y de calidad. De Atareao 
creo que no hay que decir mucho, que 
está presentado él solo. Bueno, 
yo decir que estamos súper orgullosos de que Atareao 
esté en nuestra sala dando una charla. También hace 
otra charla en Es Libre, pero evidentemente es 
a otra hora porque a pesar de que Atareao 
todavía no tiene el don de la ubiquidad, aunque a veces 
creo que sí que lo tiene y pasa que lo esconde, 
a un script tendrá por ahí. Y es evidente que estamos 
súper contentos de que nos haya 
guardado media hora de su vida, de su atareada vida, para 
ilustrarnos sobre Types. Y en lo que atañe a mi 
persona, dado que estoy en el mundo académico y que 
tengo que hacer documentos profesionales 
y de calidad, pues me interesa muchísimo 
este tema por eso, porque Latex 
es un lenguaje excelente para hacer funciones gráficas 
y matemáticas y a mí me va a hacer falta, con lo 
cual esta no me esperéis en la mesa, 
que no voy a estar. Aparte, lo bueno es 
que como es la que termina la del 
mediodía, habrá algún momento para 
hacerle preguntas y hablar con él, porque ya lo siguiente 
es la comida y paramos hasta las tres y media. Correcto, y a 
las tres y media, pues 
se va a hacer una charla que se llama 
¿Qué es España? Para explicarlo. 
En un principio voy a estar yo en esa charla, pero seguramente 
pues igual invito a alguien que 
suba al estado conmigo y que lo presente, y en esa 
charla pues la idea que tenemos es explicar 
para qué sirve KDE España, si 
hacemos código o no hacemos, si organizamos eventos, evidentemente 
la respuesta es sí, si hacemos reuniones mensuales en 
algún sitio, sorpresa, si necesitas el 
programador para entrar, ya os digo yo que 
no, pues explicaremos más o menos qué es KDE España, 
qué se necesita para entrar y de paso os animaremos a que 
entres en KDE España, porque ya veréis que no hace falta 
nada en especial, simplemente voluntad 
y ganas de hacer cosas. Ahora es el momento de hacer 
énfasis en hacer comunidad. Hay un ratito de 
no solo es aprender cosas, sino también 
hay que hacer hincapié en juntarnos. No quiero salir del armario, 
sino salir de la pantalla, ¿de acuerdo? Una 
cosa que nos cuesta 
y a mí particularmente me costó bastante salir 
de ese mundo informático, comunicándome 
y siendo yo, yo, yo, yo, convirtiéndome en un 
producto y pasar a, no, que esto 
es, esto va de compartir cosas y de estar con gente y de hacer 
cosas con gente. Y acto seguido 
tenemos a nuestra segunda estrella invitada, que además 
es recurrente en las Akademy-es y le tenemos un 
cariño especial, que es alguien 
que además es también organizadora de esLibre, entre 
otras muchas cosas, que va a estar 
hablando de 4 a 4 y 45 
de Krita, con la charla Pintando con Krita, técnicas 
de ilustración, en la que va a 
hablar sobre cómo aplicar conocimiento 
y los pasos de ilustración tradicional a través del 
software. Durante la presentación se 
mostrará cómo trabajar y pensar por capas, 
qué colores son los más indicados según la capa 
de trabajo, cómo realizar paletas, personalizar pinceles y en 
general trabajar lo más cercano 
posible a la técnica tradicional, pero usando el 
programa Krita, que es uno de los softwares de 
KD. Una charla 
orientada para usuarios, entre comillas, ¿de 
acuerdo? No es una técnica, y yo creo 
que es súper interesante. Krita es una de esas 
aplicaciones que yo tengo ahí en la recámara, dándole un 
vistazo, porque creo que no le sacó el 
partido que le podría sacar. 
Yo también, esta me parece súper, súper interesante. 
Además, alguien tiene una capacidad 
comunicativa bastante elevada, y eso 
está bien. Yo creo que esa capacidad comunicativa 
también la tiene el siguiente ponente. La siguiente ponencia 
la tenemos a las 6.45 a las 5.30, y tenemos 
una ponencia que se llama ¿Cómo puedes utilizar OSSKB 
para desarrollar KD? por Agustín Benito, 
consultor del software libre. Y lo que decía, si alguien es 
una gran comunicadora, pues Agustín Benito 
es un gran, gran, gran comunicador. De hecho, si buscáis 
su nombre, Agustín Benito, en YouTube 
y charlas TED, veréis que tiene una charla TED 
con él como protagonista. Y en esta ocasión, pues nos 
trae algo que yo realmente desconozco 
mucho de este tema, con lo cual me parece súper interesante. 
Es de Sabia Nueva, que es el OSSKB, 
que es el Open Source Software Knowledge Basement. 
¿Veis? Base. Es un subconjunto de la base de 
conocimiento sobre software libre creado y 
mantenido y conocido por Scanos. 
Esta base de conocimiento, ofrecida a través 
del Software Transparency Foundation, 
permite a desarrolladores e investigadores ingenieros 
de licencia, compliance y herramientas 
transaccionadas con licencia de software acceder y mostrar 
información sobre licencias de software relacionadas 
con cualquier paquete de software libre. 
En otras palabras, una forma de saber cómo utilizar 
las licencias de software libre de forma eficiente. Es lo que 
yo entiendo. Para mí, 
lo bueno de esto es que es lo típico, de que tú leas el párrafo y dices, 
no me he enterado prácticamente de nada, pero 
vas a la charla y Agustín te lo va a dejar 
cristalino y va a decir, vale, ahora he entendido 
de una manera cristalina algo que me hubiera costado 
mucho más si tengo que ponerme a investigar, 
a verme todas las webs, a ver alguna 
demo. Es una manera de salir de las charlas diciendo, 
ya sé lo que es esto, puedo ver si me 
interesa o no. Correcto. De hecho, 
en esta charla, Agustín explicará 
de manera práctica cómo cualquier desarrollador 
y quiero remarcar de manera práctica 
cómo cualquier desarrollador de KDE puede 
utilizar OSS KDE a través del Workbench y también proporcionar 
información básica sobre el servicio. 
Así que, interesante para cualquier 
desarrollador que tenga interés. Y pasamos a un clásico 
de las Akademy-es, que son las charlas relámpago. Serán 
de cinco y media, seis y cuarto 
y son charlas de cinco minutos en el que cualquiera puede 
presentarse en cualquier momento y decir, 
oye, que quiero hablar de esto. Y mientras que 
haya un hueco, nosotros lo metemos. El año pasado, por ejemplo, 
tuvimos algún interesante sobre accesibilidad. Ya tenemos 
alguna petición, pero aún hay huecos, 
con lo cual podéis mandar un correo o estando 
allí hablar con alguien de la mesa o de la 
organización y decir, no, oye, que me gustaría hablar 
cinco o diez minutos de esto. Y nosotros vamos apuntando. 
Podría resaltar que ya tenemos un par de 
charlas relámpago preparadas, pero eso, que en todas las 
Akademy-es pasa que alguien se anima y habla de cualquier 
tema. ¿Vale? Paso en Madrid, en 
Valencia, en Almería siempre hay alguien 
y eso es muy interesante. Y cuando acabemos de echar las 
relámpagos, pues, tendríamos que hacernos 
la tradicional foto de grupo, que yo creo que 
es fundamental para poder promocionar este 
evento en redes sociales y 
en posteriores encuentros. Y después 
de la foto de grupo, tenemos un pequeño descanso 
y tendremos, creo que, una de las charlas que yo más 
espero, porque le tengo un gran aprecio 
a Aleix Pol, que es el presidente de KDE 
Internacional, que lamentablemente no puede asistir 
en presencial y en remoto, pues, va 
a hablarnos de Mirando Más Allá de Plasma 6. Va a ser, 
en sus palabras, pues, un repaso de lo que ha pasado 
este último año en el acervo de KDE. 
Recordemos que este último año de KDE ha sido 
especialmente fructífero. El 28 de febrero lanzamos 
KDE 6, que era un gran salto tecnológico 
y salió bien, que eso creo que sería lo más 
importante. Fue un gran salto y no fue 
un borrón, sino que fue un salto exitoso 
y lo que nos dicen todo el mundo que está en el 
mundo del desarrollo de KDE, que es que, en verdad, 
lo mejor está por llegar. Es decir, el salto ha 
sido positivo y ha sido un éxito, pero lo que vendrá 
a continuación, pues, puede ser 
realmente espectacular. Con lo cual, 
pues, esta charla, pues, la esperamos 
aunque sea en remoto, que se pierda un poquito, 
de calor, digámoslo así, ¿de acuerdo? Pero es 
una charla muy esperada. Escuchar a Alex siempre merece 
la pena y por él siempre se hace el 
esfuerzo de decir, bueno, si es en remoto, la voy a ver igual, 
porque tendremos algunos minutillos 
para preguntarle alguna cosa. No es lo mismo que 
verla grabada después. Y, además, 
yo estaba esta mañana con los dientes largos, viendo 
ya las novedades que van para Plasma 6.1 
o 6.2. O sea, esto no para, las bases han ido bien. 
Y lo que me gusta de Alex es que siempre 
te da como la perspectiva, la visión macro. 
O sea, no es leer unas noticias en concreto cada 
semana de un cambio, sino 
que ves por dónde va el rumbo. Y él tiene 
una visión muy global que siempre 
es enriquecedora. Creo que él y su equipo de KDV están 
haciendo un gran, gran esfuerzo para 
que la comunidad KDE siga un rumbo bastante fijo 
y estable. Y, bueno, no puedo estar más que agradecido 
a su trabajo, bueno, su trabajo y el trabajo 
de su equipo respecto a esto. Bueno, y la 
siguiente charla, de 7 y cuarta 
a 8, ¿qué podemos decir? Diez cosas que no sabías que 
podías hacer o sí en Plasma 6. Baltasar Ortega, 
el clásico. Esta vez me grabaréis, ¿o no? 
Esta vez te vamos a grabar y, además, para que veas, 
te vamos a grabar bien, porque la última vez 
te grabamos, pero eso era inescuchable. Tuvimos ahí unos problemas 
técnicos. Bueno, lo cierto es que se grabaron 
casi todas mal, pero con mi micrófono de solapa pudimos rescatar los 
audios. Excepto, ya te dije, 
desde luego fue una mala suerte impresionante, todas menos prácticamente 
solo las tuyas. Entonces, este año 
pondremos un especial cariño en 
que tu charla pueda propagarse por el universo una 
vez que termine el evento, porque 
son de las que a mí me gustan, porque 
cualquier usuario de, te voy a decir, 
GNU/Linux, no solo KDE, puede utilizarla, ver 
de uso, ver que hay maneras de productividad que no pensaba 
que se podían hacer tan fáciles y me 
parece súper práctica y súper útil y 
aparte es muy divertido ir viendo ahí, levantando 
la mano, pues esta no tenía ni idea, esta 
sí la sabía. Hostia, esto no sabía ni que 
era posible. Sí, he puesto el título de 10 por no poner 
20 o por no poner 5, porque en 
verdad todavía no tengo preparada, ¿de acuerdo? 
Si me pongo a rascar, saco, siempre 
saco mucho, pero bueno, claro, el 
problema es que cada vez no sé si 
las que cuento ya las saben, porque 
claro, ya sé, pero bueno, intentaremos buscar 
algunas novedades, algunas cosas que sean 
originales. Bueno, en Málaga yo 
recuerdo que la proporción, la mayoría de gente 
no se sabía la mayoría que dijiste. 
Es que ya tienen muchas, es que la Comunicade me ofrece 
mucha versatilidad, esa es la verdad. Y bueno, 
cuando acabe mi turra, pues 
se hará la ceremonia de clausura de Akademy-es, donde de nuevo 
en Madrián, Chávez, hará la despedida, 
los ofrecimientos de rigor y de justicia, y 
darás información actualizada sobre los eventos 
sociales del sábado y del domingo. Hay que decir, evidentemente, 
que estas charlas, como no somos 
profesionales, o los ponentes no son profesionales 
mayoritariamente, pues pueden sufrir 
pequeñas modificaciones, pues porque 
la vida es así de aleatoria en algunas ocasiones. Pero 
vamos, esos son los horarios 
orientativos que más o menos con un pequeño 
desvío se suelen cumplir, y se intentarán grabar, 
se intentarán que se escuche bien cuando 
las publiquemos, no tengo ni idea si las vamos a poder 
emitir en directo, yo creo que este año la prioridad 
es que se graben bien, más que emitirlas 
en streaming, y luego, si podéis ir, 
y si conseguimos publicarlas, pues las publicaremos 
presupuesto para que luego queden para la historia 
y cualquiera la pueda repasar en su momento, pero la prioridad 
va a ser el local, y luego el grabarlas para luego. 
Sí, porque está muy bien la virtualidad y toda la pesca, 
pero somos humanos 
y la presencialidad es importante, 
y hacer ese esfuerzo porque es un esfuerzo, hay que 
reconocerlo, coger el coche, coger el 
transporte público para ir a un evento 
el viernes o el sábado o cuando sea, pero lo que 
se gana es mucho, y eso a veces se nos olvida, 
el contacto con los humanos es importantísimo. 
Le voy a recordar que esto es un evento de voluntarios, 
y aunque emitir no sea difícil, 
en principio, o sea, yo en mi casa te monto 
la emisión sin ningún problema, 
la complicación viene cuando lo haces 
en un sitio en el que no es el tuyo, que no tienes los aparatos 
controlados y que lamentablemente no 
podemos ir dos días antes o un día 
antes a montar el chiringuito y testearlo. Tenemos que llegar 
ahí de prisa y corriendo y ver si funciona. 
Entonces, ese es el problema por el que no podemos asegurar 
a ciencia cierta que somos capaces de grabar, 
emitir y que todo funcione muy bien. 
Correcto. Jugamos fuera de casa y tenemos poco tiempo 
para prepararlo, porque normalmente lo que ocurre 
es que las primeras grabaciones o segundas grabaciones salen 
mal, pero las que... 
la tercera y cuarta empiezan a salir todas bien, 
porque ya hemos ido un tiempo para ir probándolo, 
pero bueno, son cosas. Jugamos con la ventaja de que 
esLibre va a empezar antes que nosotros, 
con lo cual es posible que en nuestra sala 
ya esté el chiringuito montado y si suena la flauta, pues 
tiraremos con la inercia de lo que tienen 
y si no, pues empezaremos nosotros 
a tocar lo que haga falta en el canal 
de PeerTube, a ver si lo podemos mandar, 
pero bueno, no hay que darle mucha vuelta. 
Si estáis por ahí, intentad ir y si no, pues si hay suerte 
las tendréis en nuestro canal del 
FEDIVERSO. No voy a decir el nombre de la instancia 
de PeerTube, pero ya sabéis, en nuestras redes 
lo tendréis. Bueno, pues estupendo, ¿no? 
Recordad simplemente que el sábado estaremos en la mesa 
y estaremos a vuestra exposición para hablar, charlar un ratito, 
para comentar cosas. Seguramente nos iremos desplazando 
por toda esLibre. Nos veréis porque casi todos 
llevamos las camisetas frikis de la K, de KD. 
Y el domingo sí que puedo adelantar que 
estamos a ver si podemos hacer una pequeña, 
una visita turística por Valencia matinal, porque la gente coge 
entre los trenes por la tarde o bienes por la tarde, 
pues quien esté todavía en Valencia 
el domingo, pues estoy intentando personalmente buscar a alguien 
que nos haga de guía, porque yo no soy 
demasiado buen guía de Valencia, no me conozco 
demasiado, pero tengo conocidos y a ver si, pues eso, damos un 
paseíto por Valencia, igual que 
hicimos el año pasado en Málaga, que fue una mañana 
pues fabulosa, muy, muy, muy productiva y muy chula. 
Las cosas que quedan en el recuerdo, al final, 
las cosas importantes son las que se quedan en el recuerdo 
y yo la visita matinal de Málaga el domingo, el año pasado, 
pues no se me va de la cabeza. Yo doy 
fe y recordar o añadir 
si alguien no lo sabe que es apta para familia, o sea, 
que yo fui con mujer e hija que 
a la actividad podéis venir con quien queráis, no hay 
falta que sea solo la gente que ha ido libre 
y que en medio del viernes y el sábado podéis pasaros 
por las mesas hablar con nosotros, preguntarnos 
si queréis venir a cenar a comer el sábado, o sea 
que si queréis hacer comunidad estamos ahí disponibles, tenéis 
las redes sociales, más todo en 
estar ellos atentos, tenemos el grupo de Telegram, por supuesto 
cada blog gira poniendo las novedades, eso no hay falta 
ni decirlo, y que 
tenéis varios medios para estar en contacto 
casi directo con nosotros por si queréis 
engacharse a comer, para hacer cualquier duda 
o apuntarse el domingo a la actividad. Pues nada 
chicos, yo creo que está todo dicho, 
¿no David? Yo creo que sí, tenéis 
evidentemente en la página ECA de España 
todos los programas que os hemos puesto 
precioso con colores, que ha quedado niquelado 
desde luego Baltasar, aquí se ha lucido 
queda precioso apuntarse si la queréis repasar de vez 
en cuando aunque nosotros lo hayamos ido 
leyendo tener la mano para 
repasar y ya nos vemos en Valencia dentro de nada 
Pues eso espero, impaciente estoy 
ya te lo digo. Venga pues un abrazo 
y gracias Jorge por editarnos esto y esperamos 
darte un abrazo allí. Correcto, que por fin 
veremos a Jorge, que yo no lo conozco personalmente 
Yo tampoco y tengo muchas ganas Hasta luego 
Saludos. 
