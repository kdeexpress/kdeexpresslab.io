1
00:00:00,000 --> 00:00:02,370
 Buenas estamos en el episodio

2
00:00:02,370 --> 00:00:04,360
 26 de KDE Express y estoy grabando

3
00:00:04,360 --> 00:00:05,320
 el 12 de abril.

4
00:00:05,320 --> 00:00:07,180
 Este episodio lo voy a titular

5
00:00:07,180 --> 00:00:08,800
 post-covid porque acabo de

6
00:00:08,800 --> 00:00:11,520
 salir de él así que si me notáis

7
00:00:11,520 --> 00:00:11,800
 la voz

8
00:00:11,800 --> 00:00:13,680
 rara seguramente sea por eso.

9
00:00:13,680 --> 00:00:15,280
 Hoy comienzo con una noticia

10
00:00:15,280 --> 00:00:17,800
 sobre KRUNNER que gracias a la

11
00:00:17,800 --> 00:00:18,120
 mega

12
00:00:18,120 --> 00:00:19,850
 release de Plasma 6 ha ganado

13
00:00:19,850 --> 00:00:21,520
 algunas funcionalidades.

14
00:00:21,520 --> 00:00:23,000
 Entonces esto lo voy a leer

15
00:00:23,000 --> 00:00:23,280
 directamente

16
00:00:23,280 --> 00:00:25,230
 de KDE Blog y es que ahora es

17
00:00:25,230 --> 00:00:26,970
 posible personalizar el orden

18
00:00:26,970 --> 00:00:28,830
 del resultado de búsqueda. O

19
00:00:28,830 --> 00:00:29,160
 sea que tú

20
00:00:29,160 --> 00:00:30,910
 puedes priorizar si primero

21
00:00:30,910 --> 00:00:32,730
 quieres que te muestre ficheros

22
00:00:32,730 --> 00:00:34,440
 o marcadores o resultados de

23
00:00:34,440 --> 00:00:35,040
 páginas web,

24
00:00:35,040 --> 00:00:36,530
 de todas las opciones que tiene

25
00:00:36,530 --> 00:00:37,890
 de cosas que mostrarte, tú

26
00:00:37,890 --> 00:00:39,800
 puedes ir priorizando cuáles

27
00:00:39,800 --> 00:00:39,920
 son

28
00:00:39,920 --> 00:00:41,310
 las más importantes para ti

29
00:00:41,310 --> 00:00:42,830
 para que lo primero que salga

30
00:00:42,830 --> 00:00:43,970
 sea algo que a ti te resulte

31
00:00:43,970 --> 00:00:44,320
 más

32
00:00:44,320 --> 00:00:45,800
 interesante. También es

33
00:00:45,800 --> 00:00:47,370
 posible convertir entre zonas

34
00:00:47,370 --> 00:00:49,760
 horarias o debido a unas refactorizaciones

35
00:00:49,760 --> 00:00:51,160
 del código que han hecho han

36
00:00:51,160 --> 00:00:52,530
 hecho que ahora sea bastante

37
00:00:52,530 --> 00:00:55,240
 más rápido a la hora de CPU,

38
00:00:55,240 --> 00:00:56,080
 gasta menos

39
00:00:56,080 --> 00:00:57,480
 y responde más rápido.

40
00:00:57,480 --> 00:00:59,060
 También es posible activar el

41
00:00:59,060 --> 00:01:00,960
 modo reposo híbrido desde KRUNNER

42
00:01:00,960 --> 00:01:01,960
 y también es

43
00:01:01,960 --> 00:01:04,180
 capaz de buscar en Codeberg y

44
00:01:04,180 --> 00:01:07,130
 en PyPI, que si sois desarrolladores

45
00:01:07,130 --> 00:01:08,880
 sabréis lo que es, si no,

46
00:01:08,880 --> 00:01:10,530
 no lo tendréis ni marcado. Y

47
00:01:10,530 --> 00:01:12,200
 luego también es posible ahora

48
00:01:12,200 --> 00:01:13,920
 encontrar resultados tanto en

49
00:01:13,920 --> 00:01:14,560
 el

50
00:01:14,560 --> 00:01:16,110
 idioma del sistema, en nuestro

51
00:01:16,110 --> 00:01:17,730
 caso seguramente español, y en

52
00:01:17,730 --> 00:01:19,170
 inglés. Así si hay alguna

53
00:01:19,170 --> 00:01:19,560
 cosa

54
00:01:19,560 --> 00:01:21,010
 que es muy sencilla de escribir

55
00:01:21,010 --> 00:01:22,480
 en inglés pero en español no

56
00:01:22,480 --> 00:01:23,960
 le encontráis exactamente la

57
00:01:23,960 --> 00:01:24,360
 palabra,

58
00:01:24,360 --> 00:01:25,790
 podéis probar con ella en

59
00:01:25,790 --> 00:01:27,720
 inglés. Luego recordar que por

60
00:01:27,720 --> 00:01:29,580
 defecto las teclas suelen ser

61
00:01:29,580 --> 00:01:30,960
 ALT + F2 o

62
00:01:30,960 --> 00:01:32,260
 ALT + barra espaciadora que es

63
00:01:32,260 --> 00:01:33,350
 la que uso yo, pero que es

64
00:01:33,350 --> 00:01:35,120
 completamente configurable, vosotros

65
00:01:35,120 --> 00:01:36,600
 la podéis cambiar. Esto

66
00:01:36,600 --> 00:01:38,360
 respecto a Dolphin si ya estáis

67
00:01:38,360 --> 00:01:40,010
 en Plasma 6. Luego quería

68
00:01:40,010 --> 00:01:41,040
 hablaros un poco

69
00:01:41,040 --> 00:01:43,270
 sobre un drama que ha habido

70
00:01:43,270 --> 00:01:46,080
 con los temas globales en la tienda

71
00:01:46,080 --> 00:01:48,600
 de aplicaciones y es que para

72
00:01:48,600 --> 00:01:49,320
 poder

73
00:01:49,320 --> 00:01:51,270
 hacer todo lo que hacen los

74
00:01:51,270 --> 00:01:53,370
 temas de Plasma les tienes que

75
00:01:53,370 --> 00:01:55,560
 dar bastante permisos para que

76
00:01:55,560 --> 00:01:56,040
 modifiquen

77
00:01:56,040 --> 00:01:57,960
 muchos ficheros, ejecuten

78
00:01:57,960 --> 00:01:59,800
 script y cosas de esas. El

79
00:01:59,800 --> 00:02:01,760
 problema es que cuando le das

80
00:02:01,760 --> 00:02:02,480
 permisos para que

81
00:02:02,480 --> 00:02:04,410
 ejecuten cosas, si hay algún

82
00:02:04,410 --> 00:02:06,430
 fallo pues los problemas pueden

83
00:02:06,430 --> 00:02:08,680
 ser grandes. Entonces la noticia

84
00:02:08,680 --> 00:02:10,210
 es que había un tema que

85
00:02:10,210 --> 00:02:12,340
 tenía un error típico muy tonto

86
00:02:12,340 --> 00:02:14,020
 en el software libre que es

87
00:02:14,020 --> 00:02:14,800
 cuando hace

88
00:02:14,800 --> 00:02:16,660
 un rm -fr no hace bien el

89
00:02:16,660 --> 00:02:18,590
 nombre del fichero que quiere

90
00:02:18,590 --> 00:02:21,040
 borrar, mete un espacio por en

91
00:02:21,040 --> 00:02:21,880
 blanco y te

92
00:02:21,880 --> 00:02:22,940
 cargas todo lo que hay antes

93
00:02:22,940 --> 00:02:24,090
 porque en realidad no ha dicho

94
00:02:24,090 --> 00:02:25,240
 el nombre del fichero sino que

95
00:02:25,240 --> 00:02:25,400
 ha

96
00:02:25,400 --> 00:02:27,340
 dicho la carpeta raíz. Eso si

97
00:02:27,340 --> 00:02:29,040
 no le has dado permisos de root

98
00:02:29,040 --> 00:02:30,560
 pues no se puede cargar el

99
00:02:30,560 --> 00:02:31,840
 sistema entero pero se puede

100
00:02:31,840 --> 00:02:33,110
 cargar todo tu home con todos

101
00:02:33,110 --> 00:02:34,840
 tus ficheros y tus configuraciones.

102
00:02:34,840 --> 00:02:35,280
 La

103
00:02:35,280 --> 00:02:36,720
 cosa es que por un lado hay que

104
00:02:36,720 --> 00:02:38,540
 tener cuidado con lo que instalemos

105
00:02:38,540 --> 00:02:39,960
 por mucho que sea una tienda

106
00:02:39,960 --> 00:02:41,460
 oficial y por otro lado tenemos

107
00:02:41,460 --> 00:02:42,950
 que intentar hacer las tiendas

108
00:02:42,950 --> 00:02:45,480
 oficiales mejores para que haya

109
00:02:45,480 --> 00:02:45,680
 algún

110
00:02:45,680 --> 00:02:47,520
 tipo de chequeo automático o

111
00:02:47,520 --> 00:02:49,410
 de revisión y que estas cosas

112
00:02:49,410 --> 00:02:51,320
 se pillen antes. Lo cierto que

113
00:02:51,320 --> 00:02:51,480
 fue

114
00:02:51,480 --> 00:02:52,990
 bastante rápido, alguien lo

115
00:02:52,990 --> 00:02:54,460
 reportó y enseguida quitaron

116
00:02:54,460 --> 00:02:55,890
 el tema para que nadie pudiera

117
00:02:55,890 --> 00:02:56,640
 bajárselo

118
00:02:56,640 --> 00:02:58,420
 pero por lo menos una persona

119
00:02:58,420 --> 00:03:00,280
 perdió bastante datos con lo

120
00:03:00,280 --> 00:03:01,920
 cual una cosa a recordar es

121
00:03:01,920 --> 00:03:02,520
 primero

122
00:03:02,520 --> 00:03:04,440
 siempre copia de seguridad si

123
00:03:04,440 --> 00:03:06,000
 no es por esto será por otra

124
00:03:06,000 --> 00:03:07,530
 cosa los datos se pierden

125
00:03:07,530 --> 00:03:08,160
 tienes que

126
00:03:08,160 --> 00:03:09,570
 tener copia de seguridad y

127
00:03:09,570 --> 00:03:11,100
 segundo y cuando está instalando

128
00:03:11,100 --> 00:03:12,480
 temas de terceros ya sea un

129
00:03:12,480 --> 00:03:13,080
 tema global

130
00:03:13,080 --> 00:03:16,380
 de Plasma ya sea un una aplicación

131
00:03:16,380 --> 00:03:19,110
 bajada hay que tener cuidado

132
00:03:19,110 --> 00:03:22,480
 ver los comentarios la review y

133
00:03:22,480 --> 00:03:24,100
 siempre siempre que ejecute un

134
00:03:24,100 --> 00:03:25,760
 script en copia de seguridad.

135
00:03:25,760 --> 00:03:27,490
 Pasando a la siguiente noticia

136
00:03:27,490 --> 00:03:28,280
 quería

137
00:03:28,280 --> 00:03:30,860
 hablaros de un artículo de linusadictos

138
00:03:30,860 --> 00:03:32,340
 que se llama le he instalado

139
00:03:32,340 --> 00:03:33,640
 Plasma big screen a mi

140
00:03:33,640 --> 00:03:35,280
 centro multimedia y es de lo

141
00:03:35,280 --> 00:03:36,910
 mejor que he podido hacerle

142
00:03:36,910 --> 00:03:38,760
 aunque tiene margen de mejora y

143
00:03:38,760 --> 00:03:39,040
 me ha

144
00:03:39,040 --> 00:03:40,410
 gustado porque yo la verdad es

145
00:03:40,410 --> 00:03:41,820
 que tengo pendiente de cogerme

146
00:03:41,820 --> 00:03:43,130
 una raspberry pi y probar

147
00:03:43,130 --> 00:03:43,800
 Plasma big

148
00:03:43,800 --> 00:03:45,780
 screen quien no lo conozca es

149
00:03:45,780 --> 00:03:48,030
 la versión de Plasma con botoncitos

150
00:03:48,030 --> 00:03:50,320
 gigantes hechos para poder usarlo

151
00:03:50,320 --> 00:03:52,020
 desde una tele sin tener que

152
00:03:52,020 --> 00:03:53,930
 estar con un teclado y un ratón

153
00:03:53,930 --> 00:03:55,600
 y moviéndote como si fuera un

154
00:03:55,600 --> 00:03:56,040
 ordenador

155
00:03:56,040 --> 00:03:57,240
 es como si fuera un centro

156
00:03:57,240 --> 00:03:58,730
 multimedia y por lo que cuentan

157
00:03:58,730 --> 00:03:59,880
 parece que le ha gustado

158
00:03:59,880 --> 00:04:00,480
 bastante

159
00:04:00,480 --> 00:04:02,030
 que por supuesto no es el

160
00:04:02,030 --> 00:04:03,850
 desarrollo principal que tiene

161
00:04:03,850 --> 00:04:05,460
 Plasma con lo cual tiene

162
00:04:05,460 --> 00:04:06,000
 algunas cosas

163
00:04:06,000 --> 00:04:07,750
 como algunas cosas duplicadas

164
00:04:07,750 --> 00:04:09,300
 que sería mejorable pero que

165
00:04:09,300 --> 00:04:11,020
 le parece bastante usable así

166
00:04:11,020 --> 00:04:11,280
 que os

167
00:04:11,280 --> 00:04:12,840
 recomiendo que le echéis un

168
00:04:12,840 --> 00:04:14,450
 vistazo al artículo y os dejo

169
00:04:14,450 --> 00:04:16,080
 el enlace por supuesto como el

170
00:04:16,080 --> 00:04:16,640
 resto de

171
00:04:16,640 --> 00:04:18,910
 artículos que voy recomendando

172
00:04:18,910 --> 00:04:20,800
 hay uno que es muy difícil de

173
00:04:20,800 --> 00:04:22,560
 contar pero que si os da bien

174
00:04:22,560 --> 00:04:22,920
 el inglés

175
00:04:22,920 --> 00:04:25,210
 es muy recomendable que es

176
00:04:25,210 --> 00:04:27,160
 Plasma 5 de early years que

177
00:04:27,160 --> 00:04:29,750
 viene siendo una retrospectiva

178
00:04:29,750 --> 00:04:31,620
 de kai uves un

179
00:04:31,620 --> 00:04:33,710
 desarrollador de KDE que viene

180
00:04:33,710 --> 00:04:35,560
 hablando de que ahora que ha

181
00:04:35,560 --> 00:04:37,680
 salido KDE Plasma 6 como fue

182
00:04:37,680 --> 00:04:38,440
 las

183
00:04:38,440 --> 00:04:40,380
 novedades y las cosas que se

184
00:04:40,380 --> 00:04:42,160
 que se pusieron en marcha

185
00:04:42,160 --> 00:04:44,290
 cuando pasamos de Plasma 4 a

186
00:04:44,290 --> 00:04:45,640
 Plasma 5 así

187
00:04:45,640 --> 00:04:47,920
 que si a alguno le interesa la

188
00:04:47,920 --> 00:04:50,090
 historia por así decirlo de

189
00:04:50,090 --> 00:04:52,280
 Plasma y de qt es un artículo

190
00:04:52,280 --> 00:04:52,720
 un poco

191
00:04:52,720 --> 00:04:54,680
 largo pero bastante interesante

192
00:04:54,680 --> 00:04:56,060
 y oye quieras que no te lo va

193
00:04:56,060 --> 00:04:57,470
 poniendo con capturas de

194
00:04:57,470 --> 00:04:58,320
 pantalla para

195
00:04:58,320 --> 00:04:59,500
 ilustrar cuáles son las

196
00:04:59,500 --> 00:05:01,100
 diferencias que hay hay algunas

197
00:05:01,100 --> 00:05:02,640
 partes que son un poco técnicas

198
00:05:02,640 --> 00:05:03,160
 que a lo

199
00:05:03,160 --> 00:05:04,730
 mejor las podéis saltar como

200
00:05:04,730 --> 00:05:05,960
 yo pero la idea en general la

201
00:05:05,960 --> 00:05:07,090
 verdad es que a mí me ha

202
00:05:07,090 --> 00:05:08,080
 gustado de

203
00:05:08,080 --> 00:05:09,930
 ver de todo el trabajo que se

204
00:05:09,930 --> 00:05:12,280
 ha hecho desde no desde los inicios

205
00:05:12,280 --> 00:05:14,400
 pero sí ya desde Plasma 4

206
00:05:14,400 --> 00:05:14,960
 hasta

207
00:05:14,960 --> 00:05:16,820
 Plasma 5 con el salto que hemos

208
00:05:16,820 --> 00:05:18,610
 dado ahora y luego también hay

209
00:05:18,610 --> 00:05:20,240
 unos artículos que me gustan

210
00:05:20,240 --> 00:05:20,600
 mucho

211
00:05:20,600 --> 00:05:24,070
 una review que hacen en debuffpoint.com

212
00:05:24,070 --> 00:05:26,470
 y ésta se llama KDE Plasma 6

213
00:05:26,470 --> 00:05:28,720
 experience a practical review

214
00:05:28,720 --> 00:05:31,030
 que viene siendo una visión

215
00:05:31,030 --> 00:05:34,080
 muy particular de cómo hace arimdang

216
00:05:34,080 --> 00:05:36,040
 es el handler que usa para

217
00:05:36,040 --> 00:05:37,820
 postear este chico la cosa es

218
00:05:37,820 --> 00:05:39,250
 que me gusta cómo hace la

219
00:05:39,250 --> 00:05:41,880
 review de las distribuciones la

220
00:05:41,880 --> 00:05:42,280
 hace con

221
00:05:42,280 --> 00:05:44,810
 tiempo con cabeza pensando en

222
00:05:44,810 --> 00:05:47,450
 diferentes tipos de usuarios y

223
00:05:47,450 --> 00:05:50,160
 este os paso el enlace también

224
00:05:50,160 --> 00:05:52,280
 la va adornando con capturas de

225
00:05:52,280 --> 00:05:53,680
 pantalla y está muy

226
00:05:53,680 --> 00:05:56,080
 interesante también en general

227
00:05:56,080 --> 00:05:56,800
 pone muy bien

228
00:05:56,800 --> 00:05:58,420
 a KDE Plasma si lo hubiera

229
00:05:58,420 --> 00:06:00,150
 puesto mal también hubiera

230
00:06:00,150 --> 00:06:02,440
 pasado el enlace pero yo recomiendo

231
00:06:02,440 --> 00:06:04,880
 mucho este blog debuffpoint.com

232
00:06:04,880 --> 00:06:07,010
 hacen reviews bastante buenas

233
00:06:07,010 --> 00:06:09,360
 de parte con cienzudas completas

234
00:06:09,360 --> 00:06:11,200
 con muchas secciones viendo

235
00:06:11,200 --> 00:06:12,740
 muchas cosas y luego por

236
00:06:12,740 --> 00:06:14,680
 supuesto también te dice lo

237
00:06:14,680 --> 00:06:15,600
 que no está bien

238
00:06:15,600 --> 00:06:17,590
 y los puntos de mejora y luego

239
00:06:17,590 --> 00:06:19,570
 una cosa para el futuro que es

240
00:06:19,570 --> 00:06:22,280
 que Plasma 6.1 porque desde que

241
00:06:22,280 --> 00:06:24,390
 salió Plasma 6 la verdad es

242
00:06:24,390 --> 00:06:27,000
 que han sacado un montón de features

243
00:06:27,000 --> 00:06:29,320
 de mejoras de funcionalidades

244
00:06:29,320 --> 00:06:32,040
 que van directamente a Plasma 6.1

245
00:06:32,040 --> 00:06:34,280
 y eso es desde que prácticamente

246
00:06:34,280 --> 00:06:36,360
 estaban a punto de terminar la

247
00:06:36,360 --> 00:06:38,480
 release el 28 de febrero ellos

248
00:06:38,480 --> 00:06:40,130
 ya habían metido un montón de

249
00:06:40,130 --> 00:06:42,120
 cosas que esto nuevo y la 6.1

250
00:06:42,120 --> 00:06:42,320
 esto

251
00:06:42,320 --> 00:06:43,570
 no hubiera 6.1 y una de las

252
00:06:43,570 --> 00:06:44,550
 cosas que me ha llamado la

253
00:06:44,550 --> 00:06:45,730
 atención porque yo no sabía

254
00:06:45,730 --> 00:06:46,360
 que se podía hacer

255
00:06:46,360 --> 00:06:49,290
 antes y la noticia es que se va

256
00:06:49,290 --> 00:06:52,450
 a poder volver a hacer es poner

257
00:06:52,450 --> 00:06:55,440
 un comando como como trigger

258
00:06:55,440 --> 00:06:56,000
 como

259
00:06:56,000 --> 00:06:57,830
 como se dice en español que

260
00:06:57,830 --> 00:07:00,350
 cuando aparezca una notificación

261
00:07:00,350 --> 00:07:02,880
 en Plasma se ejecute automáticamente

262
00:07:02,880 --> 00:07:05,140
 un comando en plan por ejemplo

263
00:07:05,140 --> 00:07:07,580
 pues si me sale la notificación

264
00:07:07,580 --> 00:07:09,840
 de que hay poca batería pues

265
00:07:09,840 --> 00:07:09,920
 yo

266
00:07:09,920 --> 00:07:11,790
 quiero lanzar a parte de un sonido

267
00:07:11,790 --> 00:07:13,420
 quiero lanzar un script pues

268
00:07:13,420 --> 00:07:15,400
 esto es algo que andreas es neider

269
00:07:15,400 --> 00:07:17,240
 publica en su blog que se va a

270
00:07:17,240 --> 00:07:19,790
 recuperar gracias a Plasma 6.1

271
00:07:19,790 --> 00:07:22,040
 y framework 6.1 y que él lo

272
00:07:22,040 --> 00:07:22,480
 usa por

273
00:07:22,480 --> 00:07:25,510
 ejemplo para cuando hay una notificación

274
00:07:25,510 --> 00:07:27,450
 de calendario lo pasa por un

275
00:07:27,450 --> 00:07:29,160
 texto speech un texto

276
00:07:29,160 --> 00:07:31,340
 a voz y así escucha la notificación

277
00:07:31,340 --> 00:07:33,110
 y no tiene que estar pendiente

278
00:07:33,110 --> 00:07:34,800
 de leerla y luego habla de que

279
00:07:34,800 --> 00:07:35,480
 hay

280
00:07:35,480 --> 00:07:37,260
 infinitas posibilidades que a

281
00:07:37,260 --> 00:07:38,920
 ti se te pueden ocurrir de que

282
00:07:38,920 --> 00:07:40,520
 cada vez que algún tipo de

283
00:07:40,520 --> 00:07:41,930
 notificación te salte que te

284
00:07:41,930 --> 00:07:43,690
 interesa hagas algo automáticamente

285
00:07:43,690 --> 00:07:44,640
 y la verdad es que me ha

286
00:07:44,640 --> 00:07:44,720
 gustado

287
00:07:44,720 --> 00:07:47,450
 cuando esté Plasma 6.1 lo probaré

288
00:07:47,450 --> 00:07:49,100
 yo diré qué tal me va luego

289
00:07:49,100 --> 00:07:50,960
 una mini noticia también es

290
00:07:50,960 --> 00:07:52,160
 que rust

291
00:07:52,160 --> 00:07:54,200
 desk que es parecido a ni desk

292
00:07:54,200 --> 00:07:56,090
 pero por lo menos software

293
00:07:56,090 --> 00:07:58,080
 libre o si te lo montas todo

294
00:07:58,080 --> 00:07:58,800
 tú pues es

295
00:07:58,800 --> 00:08:00,300
 completamente si no vos por lo

296
00:08:00,300 --> 00:08:01,740
 menos la parte del cliente si

297
00:08:01,740 --> 00:08:03,120
 usa su servidor en software

298
00:08:03,120 --> 00:08:04,040
 libre no

299
00:08:04,040 --> 00:08:06,200
 está tan pulido como Teamviewer

300
00:08:06,200 --> 00:08:08,120
 o como ni de pero yo lo he usado

301
00:08:08,120 --> 00:08:10,400
 funciona están implementando

302
00:08:10,400 --> 00:08:11,400
 el modo

303
00:08:11,400 --> 00:08:12,950
 Wayland también sea de los

304
00:08:12,950 --> 00:08:14,520
 más avanzados que hay y la

305
00:08:14,520 --> 00:08:16,230
 cosa es que cuando se actualizó

306
00:08:16,230 --> 00:08:17,300
 Plasma 6 parece

307
00:08:17,300 --> 00:08:18,660
 que se rompió alguna cosa

308
00:08:18,660 --> 00:08:19,740
 aunque parece que en Gnome

309
00:08:19,740 --> 00:08:21,100
 también se rompió hace poco

310
00:08:21,100 --> 00:08:21,760
 algo la cosa

311
00:08:21,760 --> 00:08:23,220
 es que la noticia es que lo han

312
00:08:23,220 --> 00:08:24,400
 actualizado y que ahora

313
00:08:24,400 --> 00:08:26,240
 funciona perfectamente en x11

314
00:08:26,240 --> 00:08:27,720
 con Plasma 6

315
00:08:27,720 --> 00:08:29,680
 Wayland funcionan algunas cosas

316
00:08:29,680 --> 00:08:31,220
 y otras no pero bueno está

317
00:08:31,220 --> 00:08:32,960
 bien que tan rápido en tal que

318
00:08:32,960 --> 00:08:33,120
 haya

319
00:08:33,120 --> 00:08:36,220
 salido Plasma 6 lo hayan mejorado

320
00:08:36,220 --> 00:08:38,800
 y luego os traigo una primicia

321
00:08:38,800 --> 00:08:41,200
 una novedad que ya sabéis

322
00:08:41,200 --> 00:08:42,600
 que yo creo que los últimos

323
00:08:42,600 --> 00:08:44,200
 tres episodios digo que esLibre

324
00:08:44,200 --> 00:08:45,840
 está aquí cerca que la Akademy

325
00:08:45,840 --> 00:08:46,080
 queda

326
00:08:46,080 --> 00:08:47,860
 nada que si no habéis reservado

327
00:08:47,860 --> 00:08:49,500
 hotel estáis tardando bueno

328
00:08:49,500 --> 00:08:51,400
 pues está a punto de publicarse

329
00:08:51,400 --> 00:08:52,980
 todavía no es oficial y es

330
00:08:52,980 --> 00:08:54,910
 posible que cambien los horarios

331
00:08:54,910 --> 00:08:56,760
 o algunas charlas pero ya se ha

332
00:08:56,760 --> 00:08:56,920
 cerrado

333
00:08:56,920 --> 00:08:58,790
 el call for paper ya la propuesta

334
00:08:58,790 --> 00:09:00,720
 de charlas están cerradas hay

335
00:09:00,720 --> 00:09:02,320
 tropecientas para esLibre en

336
00:09:02,320 --> 00:09:02,680
 general

337
00:09:02,680 --> 00:09:04,280
 y en Akademy ya más o menos

338
00:09:04,280 --> 00:09:06,340
 tenemos claro las que vamos a

339
00:09:06,340 --> 00:09:07,960
 tener entonces aunque cambien

340
00:09:07,960 --> 00:09:09,280
 de horario y

341
00:09:09,280 --> 00:09:10,830
 lo mismo hay aún en concreto

342
00:09:10,830 --> 00:09:12,180
 que cambia un poco os voy a

343
00:09:12,180 --> 00:09:13,800
 leer cuáles son las charlas

344
00:09:13,800 --> 00:09:14,240
 que podréis

345
00:09:14,240 --> 00:09:16,300
 ver el viernes tanto por la

346
00:09:16,300 --> 00:09:19,110
 mañana como por la tarde el sábado

347
00:09:19,110 --> 00:09:21,680
 dejaremos el día para mezclarnos

348
00:09:21,680 --> 00:09:23,210
 con la gente de esLibre e ir al

349
00:09:23,210 --> 00:09:24,900
 resto de charlas que también

350
00:09:24,900 --> 00:09:27,200
 van a estar muy bien y hay tropecientas

351
00:09:27,200 --> 00:09:29,440
 para elegir pero en Akademy en

352
00:09:29,440 --> 00:09:31,340
 sí el viernes tendremos una

353
00:09:31,340 --> 00:09:34,040
 charla sobre craft la distribución

354
00:09:34,040 --> 00:09:35,780
 de cada para plataformas no Linux

355
00:09:35,780 --> 00:09:37,460
 que es como generar los paquetes

356
00:09:37,460 --> 00:09:39,000
 o los instaladores python

357
00:09:39,000 --> 00:09:41,000
 y qt types como alternativa a

358
00:09:41,000 --> 00:09:42,810
 látex y Markdown una charla

359
00:09:42,810 --> 00:09:44,760
 sobre la asociación de cada

360
00:09:44,760 --> 00:09:45,640
 España en

361
00:09:45,640 --> 00:09:47,650
 general pintando concreta

362
00:09:47,650 --> 00:09:50,020
 técnica de ilustración cómo

363
00:09:50,020 --> 00:09:52,010
 puedes utilizar o ss kb para

364
00:09:52,010 --> 00:09:53,120
 desarrollar

365
00:09:53,120 --> 00:09:54,690
 cadena las charlas relámpago

366
00:09:54,690 --> 00:09:56,430
 que son las típicas estas cortas

367
00:09:56,430 --> 00:09:58,200
 en las que se hablan de cosas

368
00:09:58,200 --> 00:10:00,160
 diferentes mirando más allá

369
00:10:00,160 --> 00:10:02,250
 de Plasma 6 y 10 cosas que no

370
00:10:02,250 --> 00:10:04,280
 sabías que podías hacer o sí

371
00:10:04,280 --> 00:10:05,360
 en plan más 6

372
00:10:05,360 --> 00:10:07,080
 esa es la agenda que tenemos

373
00:10:07,080 --> 00:10:09,110
 desde media mañana del viernes

374
00:10:09,110 --> 00:10:11,000
 hasta el viernes por la tarde

375
00:10:11,000 --> 00:10:11,760
 nosotros

376
00:10:11,760 --> 00:10:13,900
 seguramente empecemos a las 12

377
00:10:13,900 --> 00:10:15,680
 y acabemos a las 8 de la tarde

378
00:10:15,680 --> 00:10:17,720
 esLibre empieza antes esLibre

379
00:10:17,720 --> 00:10:19,740
 seguramente a las 9 se abran

380
00:10:19,740 --> 00:10:22,000
 las puertas y a las 10 empieza

381
00:10:22,000 --> 00:10:23,920
 la primera charla con lo cual

382
00:10:23,920 --> 00:10:24,920
 ya sabéis

383
00:10:24,920 --> 00:10:26,680
 más o menos qué esperar de la

384
00:10:26,680 --> 00:10:28,590
 Akademy por supuesto tendremos

385
00:10:28,590 --> 00:10:30,320
 la mesa en frente de la sala

386
00:10:30,320 --> 00:10:31,600
 con pues

387
00:10:31,600 --> 00:10:33,500
 seguramente con una tablet con

388
00:10:33,500 --> 00:10:35,320
 una steam deck con Konqis con

389
00:10:35,320 --> 00:10:37,120
 camisetas como tuvimos en la

390
00:10:37,120 --> 00:10:37,360
 open

391
00:10:37,360 --> 00:10:38,830
 source code o sea que aún si

392
00:10:38,830 --> 00:10:40,700
 no vais a ninguna charla acercarse

393
00:10:40,700 --> 00:10:42,120
 por la mesa que seguro que

394
00:10:42,120 --> 00:10:43,850
 podemos hablar un rato y habrá

395
00:10:43,850 --> 00:10:45,640
 un montón de cosas y artilugios

396
00:10:45,640 --> 00:10:47,480
 para que juguete y luego una

397
00:10:47,480 --> 00:10:47,680
 vez que

398
00:10:47,680 --> 00:10:48,980
 estén publicadas las charlas

399
00:10:48,980 --> 00:10:50,430
 con su horario y las descripciones

400
00:10:50,430 --> 00:10:51,600
 más largas mirar a ver si os

401
00:10:51,600 --> 00:10:52,000
 interesa

402
00:10:52,000 --> 00:10:53,750
 alguno yo desde luego tengo que

403
00:10:53,750 --> 00:10:55,350
 hacerme una sesión de un par

404
00:10:55,350 --> 00:10:57,210
 de horas de ver todas las charlas

405
00:10:57,210 --> 00:10:57,240
 que

406
00:10:57,240 --> 00:10:58,660
 hay en esLibre y en Akademy

407
00:10:58,660 --> 00:11:00,140
 ver cómo me hago el horario

408
00:11:00,140 --> 00:11:01,490
 entre que estoy en la mesa

409
00:11:01,490 --> 00:11:02,160
 entre que estoy

410
00:11:02,160 --> 00:11:03,440
 en una charla o entre que estoy

411
00:11:03,440 --> 00:11:04,840
 haciendo de voluntario por otro

412
00:11:04,840 --> 00:11:06,560
 lado pero desde luego es un

413
00:11:06,560 --> 00:11:08,270
 lujo la de charla y la de gente

414
00:11:08,270 --> 00:11:10,380
 que nos vamos a juntar en Valencia

415
00:11:10,380 --> 00:11:12,920
 el 24 y el 25 de mayo hablando

416
00:11:12,920 --> 00:11:14,620
 de Valencia y esLibre en

417
00:11:14,620 --> 00:11:16,920
 general quería recordaros que

418
00:11:16,920 --> 00:11:19,320
 es libre este año quiere montar

419
00:11:19,320 --> 00:11:20,080
 si hay

420
00:11:20,080 --> 00:11:22,330
 peticiones una guardería o un

421
00:11:22,330 --> 00:11:24,860
 servicio de custodia para menores

422
00:11:24,860 --> 00:11:27,000
 con lo cual si tenéis pensado

423
00:11:27,000 --> 00:11:27,400
 viajar

424
00:11:27,400 --> 00:11:29,600
 con vuestros hijos familiares o

425
00:11:29,600 --> 00:11:31,900
 menores en general estaría

426
00:11:31,900 --> 00:11:33,320
 bien que os pusierais en

427
00:11:33,320 --> 00:11:34,000
 contacto vía

428
00:11:34,000 --> 00:11:35,720
 correo vía redes sociales o

429
00:11:35,720 --> 00:11:37,800
 como podáis con la organización

430
00:11:37,800 --> 00:11:39,520
 para decirle oye que yo voy a

431
00:11:39,520 --> 00:11:39,920
 ir el

432
00:11:39,920 --> 00:11:41,890
 viernes o voy a ir el sábado y

433
00:11:41,890 --> 00:11:43,950
 me gustaría poder dejar a mí

434
00:11:43,950 --> 00:11:46,040
 zagalo mizagala de esta hora

435
00:11:46,040 --> 00:11:46,440
 hasta ahora

436
00:11:46,440 --> 00:11:47,800
 si es sin compromiso pero es

437
00:11:47,800 --> 00:11:49,440
 para lo menos para ir haciéndose

438
00:11:49,440 --> 00:11:51,000
 una idea a la organización si

439
00:11:51,000 --> 00:11:51,160
 hay

440
00:11:51,160 --> 00:11:52,920
 demanda del servicio y poder

441
00:11:52,920 --> 00:11:54,880
 contratar acordes luego si vais

442
00:11:54,880 --> 00:11:56,270
 en tren también pedir

443
00:11:56,270 --> 00:11:57,240
 información

444
00:11:57,240 --> 00:11:59,180
 porque es posible que haya un

445
00:11:59,180 --> 00:12:01,180
 descuento del 5% con renfe y

446
00:12:01,180 --> 00:12:03,480
 luego si escucháis esto pronto

447
00:12:03,480 --> 00:12:03,640
 y

448
00:12:03,640 --> 00:12:05,010
 corriendo la página web del

449
00:12:05,010 --> 00:12:06,400
 libre a ver si todavía está

450
00:12:06,400 --> 00:12:08,080
 abierto la posibilidad de pedir

451
00:12:08,080 --> 00:12:09,160
 una beca si

452
00:12:09,160 --> 00:12:11,360
 sois un colectivo de los que

453
00:12:11,360 --> 00:12:13,750
 están marcados en la web pues

454
00:12:13,750 --> 00:12:15,880
 decir oye que es que soy un

455
00:12:15,880 --> 00:12:16,520
 estudiante

456
00:12:16,520 --> 00:12:18,240
 que tengo complicaciones o soy

457
00:12:18,240 --> 00:12:19,740
 de un grupo minoritario y me

458
00:12:19,740 --> 00:12:21,560
 gustaría que me subvencionar

459
00:12:21,560 --> 00:12:22,990
 el viaje o el alojamiento o

460
00:12:22,990 --> 00:12:24,780
 algo mirar las condiciones que

461
00:12:24,780 --> 00:12:26,240
 están en la web y mandar un

462
00:12:26,240 --> 00:12:27,910
 correo rápido para que de

463
00:12:27,910 --> 00:12:29,870
 tiempo a gestionarlo y con esto

464
00:12:29,870 --> 00:12:31,600
 yo creo que más o menos os he

465
00:12:31,600 --> 00:12:32,280
 contado

466
00:12:32,280 --> 00:12:33,980
 todo lo que quería contaros

467
00:12:33,980 --> 00:12:35,780
 hoy desde la alegría de haber

468
00:12:35,780 --> 00:12:37,400
 pasado bien el COVID y de estar

469
00:12:37,400 --> 00:12:37,880
 grabando

470
00:12:37,880 --> 00:12:39,400
 con menos de un mes, os dejo

471
00:12:39,400 --> 00:12:41,410
 hasta el siguiente episodio, un

472
00:12:41,410 --> 00:12:41,760
 saludo.

