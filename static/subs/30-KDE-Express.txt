Buenas, episodio 30 de KDE Express, se dice pronto 30 
episodios ya, hoy estamos grabando el 5 de julio de 2024 
y mientras que grabo tengo en el ordenador Plasma 6.1.2, 
Framework 6.3.0 y KDE Gears 24.05.1 Hoy os voy a traer una selección 
de noticias del mes de junio que me han ido llamando la 
atención La primera es que Fedora propone 
publicar una imagen con KDE Plasma Mobile, seguramente enfocada a 
Tabletas 2.1 Esto lo que quiere decir es que 
en Fedora tienen una metodología en la que tú haces propuestas 
y lo tienen que votar y lo aceptan 
Ellos tienen varios spins, que son las versiones que sacan de 
Fedora Igual que hay Fedoras con 
diferentes entornos de escritorio, hay Fedoras con diferentes tecnologías, 
las inmutables o atómicas mejor dicho 
Y ahora, la primera es que Fedora propone publicar una imagen con 
KDE Plasma Mobile, seguramente enfocada a Tabletas 2.1 
Y ahora han propuesto sacar una versión móvil que no solo lleve 
FOSS, la parte de Gnome, sino Plasma Mobile 
Porque se ve que hay gente interesada en usarla en algunas 
tabletas Así que es un buen signo de 
que se avanza por la parte mobile y que KDE Plasma tiene 
una buena evolución por ahí Hablando de cosas de movilidad 
como las tablets, pues PostMarket OS, que es una de las distros 
más famosas para meter Linux puro en tu móvil 
Se ha actualizado y es la 24.06 Ya tiene Plasma 6, lo cual 
está muy bien, que hayan hecho un cambio bastante gordo en tan 
poco tiempo Y quien también tiene Plasma 6 
ahora es OpenMandriva, que yo es una distro que no tengo muy 
controlada Desde los tiempos de Mandrake 
estoy un poco perdido Pues ya están probando Plasma 
6 Se van apuntando al carro poco 
a poco casi todas las distros Debían llegar en algún 
momento, no os preocupéis También he leído, porque no 
es algo que yo suelo usar Que Plasma Browser, la extensión 
de Firefox que integra Plasma 6 Que integra el escritorio con 
el navegador Tiene una nueva versión 
Por si no lo conocéis, lo que suele hacer o para lo que sirve 
Plasma Browser Es, te deja compartir enlaces 
Encontrar tabs que tengas en el navegador 
Te deja ver en el historial de páginas que ha visitado con 
KRunner Te deja monitorizar el proceso 
de las descargas Te integra las notificaciones 
Puedes controlar música y vídeo 
Está bastante bien, incluso lo puedes usar con KDE Connect 
O sea que no es un problema Porque si os gusta estar hiperconectado 
Tiene una nueva versión con mejoras 
Y os dejo el enlace, claro Y luego, una de las cosas que 
me ha llamado la atención últimamente Que yo sabía que podías hacer 
casi de todo con Plasma Pero no sabía que también podías 
poner un vídeo como fondo de pantalla 
Entonces, os dejo un proyecto que está en GitHub 
Que te dice como si lo instalas Puedes poner un vídeo y tenerlo 
ahí de fondo Mientras que se está moviendo 
Y dicen que es bastante estable Tiene versión para Aur, si 
estás en Arch El propio proyecto en GitHub 
Y luego está en la KDE Store Yo siempre recomiendo 
Leérselo primero bien Antes de instalarlo por si acaso 
Pero por lo que he leído, va bastante bien 
Y ya entramos en el menú principal 
De lo que va a ser este episodio Que es un resumen de las cosas 
que nos trae Plasma 6.1 
Que como ya os he comentado al principio 
Ya está entre nosotros Y lo he podido toquetear un 
poco La primera de las cosas 
Es que ya puedes activar el escritorio remoto 
Desde la propia configuración del sistema 
Y este es el primer paso Esto bajo Wayland 
Que no sabéis el dolor de cabeza 
Que a los administradores de sistemas 
Nos ha supuesto El paso a Wayland con el escritorio 
remoto Esto al final lo que usa es KRDP 
Y han automatizado la configuración Para que no tengas que estar toqueteando 
en varios sitios Yo he probado que funciona 
El funcionamiento de esto te va a las preferencias del sistema 
Escritorio remoto Tienes que crear un usuario 
Le pones una contraseña Lo activas, te pide unos permisos 
Y entonces ya te deja iniciarlo automáticamente 
Lees el puerto Y en las páginas que tiene KDE Plasma 
Os sale un vídeo de cómo funciona 
Una mejora que estábamos esperando 
Muchos administradores de sistemas 
Y ya está aquí Poco a poco Wayland está cogiendo 
La funcionalidad equivalente A la que tenía X11 
Aunque no está dentro de las noticias 
Pero sí que me he enterado Que Wayland ahora mismo tiene 
un fallo Bastante gordo con Orca 
Que es el reproductor o el lector de pantalla 
Que es el reproductor o el lector de pantalla 
Todavía no está fino Y yo creo que es donde deberían 
enfocarse mayoritariamente Para que la accesibilidad 
Pueda estar a la par tanto en Wayland como en X11 
Sé que están trabajando en ello 
Pero vamos, ahora mismo hay bastantes quejas 
Y yo creo que La mayoría de gente que necesite 
Orca Estará usando X11 
Volviendo a Plasma 6.1 Tenemos el modo de edición de 
escritorio mejorado Que os lo comenté en el episodio 
anterior Lo he probado y funciona muy 
bien La verdad es que es una mejora 
que se agradece También tenemos que ahora 
podemos recordar aplicaciones Abiertas en la sesión bajo 
Wayland Esto es algo que teníamos 
antes en X11 Y que perdimos con Wayland 
Que es que si tú cierras una sesión 
Se te vuelven a abrir todas las aplicaciones que tenías 
Y en el mismo sitio Ahora mismo creo que hemos 
conseguido solo Que recuerde las aplicaciones 
No todas se van a quedar en el mismo sitio 
Pero bueno, es un paso y está bastante bien 
Que sigamos avanzando Otra cosa que tiene, aunque no 
sé cuánto lo voy a poder disfrutar 
Es que Sincronizar colores LED de tu 
teclado es posible Pero para eso tienes que tener 
un teclado Que tenga luces LED 
Que sean configurables y que sea compatible 
Así que bueno Una característica de nicho 
para la gente de modding Pero bueno, seguro que hay 
gente que lo disfruta Y está gracioso 
Y luego una cosa que también os comenté hace tiempo 
Es que había un plugin Un efecto de escritorio 
En el que tú podías menear el ratón 
Y se hacía grande para que lo encontraras 
Y fuera más sencillo de ver Pues esto lo han puesto por 
defecto Y aparte lo han mejorado 
Para que cuando el icono El puntero se vea 
O se haga grande Se vea nítido 
No sea un icono horrible ahí con un zoom feísimo 
Por ahora solo va a funcionar con los que vienen por defecto 
Pero bueno, están trabajando Para que cualquiera pueda poner 
un SVG Que es escalable 
Y entonces se vean bien Esto está muy bien que venga 
por defecto Porque para el tema de accesibilidad 
Cuantas más cosas por defecto vengan 
Menos difícil será el primer uso que tengan que darle 
Las personas que tengan estos problemas 
Y luego os voy a hablar de la primera característica 
Que ha activado Plasma 
Que a mí no me ha gustado personalmente 
Aunque como podéis entender Es configurable 
Y ahora os cuento cómo se hace Se llama barrera 
Y sirve para no cambiar de monitor sin querer 
Cuando tú tienes dos monitores Y te estás moviendo entre 
ellos A lo mejor quieres tocar justo 
el borde de un monitor Y no querías pasar al 
siguiente Entonces lo que han hecho es 
como que tenga Un imán, una atracción, ese borde 
Y tienes que hacer un esfuerzo extra con el ratón 
Para pasar al siguiente monitor Si mueves el ratón 
De golpe No hay ningún problema 
Pero si vas despacito Te cuesta pasar de uno a otro 
Para mi gusto La configuración es demasiado 
fuerte Me veía muchas veces atascado 
Pasando de izquierda a derecha Entre dos monitores 
Entonces he estado buscando Y si escribí en la configuración 
En las preferencias del sistema Bordes de la pantalla 
Os saldrá en la misma configuración Donde podéis decir 
Que si vais a una esquina Se abre una aplicación 
O que si vais a otra esquina Se abre el kickoff, el menú de 
inicio Pues ahí mismo, por ahí abajo 
Tenéis una opción que se llama 
Barrera de bordes Que estará en 100 píxeles 
O algo así Esa es cuánta fuerza 
Cuánto movimiento tienes que hacer 
Para realmente pasar Yo lo que he hecho es dejarlo a 
cero Que lo transforma en ninguna 
Y con eso me he quedado cómodo Vosotros si veis que os molesta 
Podéis ir bajando el número Y ver si la funcionalidad 
Se ajusta más a vuestros gustos Como siempre 
Suelen hacer cosas La mayoría de cosas nos gustan 
Pero cuando no nos gustan En Plasma 
Las podemos cambiar Y una mejora que seguro 
Que le gustarán a todos Los que tengan un Intel 
Es que han hecho una activación Parcial del triple buffering 
Que lo que hace es Cuando detecta casos 
Que sería recomendable Lo activa 
Y se ve que en alguna gráfica Intel 
Muy antigua Esto es muy necesario 
Y han conseguido que el rendimiento Sea muchísimo mejor 
Al activar esto En Wayland por supuesto 
Porque en X11 ya estaba Estas serían las 
características principales Creo que no me dejo ninguna 
A ver... Bueno que en... 
En NVIDIA Como ahora tiene 
La sincronización explícita Volverá a ir mucho mejor 
Pero como yo no soy muy fan De NVIDIA 
No le he hecho mucho caso Y con esto 
Voy a pasar A la sección despedida del 
programa Que es el atajo recomendado 
El atajo recomendado de hoy Es 
META R La tecla con el Linux 
O la tecla de Windows Y R 
Esto lo que te hace Es que te va a abrir 
Espectacle En modo grabación 
Te va a dejar seleccionar un rectángulo 
Tú le pinchas Y ya está grabando automáticamente 
un vídeo Entonces saldrá 
Un icono rojo En la barra de tareas 
Para poder pararlo Tiene combinaciones de teclas 
Según si hace META SHIFT 
Más R O META ALT 
Más R Va a ser la pantalla completa 
Va a ser La ventana en concreto 
O el modo recuadro Pero está gracioso 
Pues si soléis hacer grabaciones O queréis hacer un videocast 
de algo Pues hay un... 
Un atajo de teclado Directamente 
Y ahora voy a añadir Un atajo recomendado 
Pero de accesibilidad Que es uno que me he encontrado 
sin querer Es que si le das a la tecla META 
Y al lado de la tecla R Y al Más 
Se te va a hacer un zoom de la pantalla 
Entonces tú te puedes ir moviendo con el ratón 
Y el zoom va siguiendo al ratón Para que no sea un cuadro fijo 
Esto realmente es que yo quería hacer 
META Asterisco Y el teclado numérico 
Para esto no funciona Tenéis que usar el teclado a 
partir del Enter y el Red Y entonces para hacer el META Asterisco 
No funciona Porque donde está el Asterisco 
está el Más Entonces tendrías que hacer META 
SHIFT Asterisco Y entonces al hacer META Más 
Pues va a ser Vas haciendo zoom 
Puedes seguir haciendo zoom También puedes darle al META 
Menos O al META Cero 
Y entonces el zoom se resetea Y vuelves de golpe a la 
pantalla como solía estar Cosas que son prácticas 
Yo os recomiendo iros a las opciones Ir a atajos de teclado 
Y ahí la verdad es que hay un montón 
Y ahí dedicarse a estar un ratito De vez en cuando mirar las 
aplicaciones que más usáis Seguro que encontráis funciones 
Que os alegran la vida del tiempo que recuperáis 
Con esto os dejo Espero que os haya gustado 
Espero conseguir hacer un episodio en julio 
Y no entrar en modo vacaciones Dado que sería el tercer aniversario 
de KDE Express Y si no ya sabéis que tenemos 
Una sala de Matrix Un grupo de Telegram 
Cuenta también del canal de Telegram 
Correo electrónico Estamos por el Fediverso 
Ya sabéis que nos gusta que nos escribáis por allí 
Y si no en cañas y bravas Estamos teniendo conversaciones 
siempre interesantes Un saludo 
Y una especial dedicatoria A nuestro editor en vacaciones 
Jorge Lama Que aunque estéis sufriendo 
que estáis de vacaciones Se lo merecía 
Y ahora mismo estoy editando yo esto 
Imagino que se notará un poco Un saludo Jorge y a todos vosotros 
Y hasta la próxima 