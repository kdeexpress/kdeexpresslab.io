 Buenas, KDE Express está de
 vuelta con el episodio 35 y
 estoy grabando el 23 de octubre.
 Lo primero comentar que Archive.org
 vuelve a estar funcionando,
 aunque solo en modo lectura.
 Durante una semana ha dejado a
 un montón de podcast y
 servicios y gente y organizaciones
 sin un montón de recursos
 súper importantes para la
 comunidad de software libre y
 de la cultura en general,
 con lo cual es un buen momento
 para donarle porque se están
 recuperando, pero se ha visto
 con qué facilidad
 podría borrarse de un plumazo
 un montón de información
 súper interesante.
 Nosotros en KDE lo sublimos
 porque tenemos servidores propios
 y estamos en camino de pasar
 todos los audios
 ahí para aligerar un poco el
 trabajo que le damos a Archive.org,
 pero no está de mal recordar
 que a veces
 damos por sentado servicios que
 si desaparecieran nos harían
 un gran agujero.
 Entrando ya en lo que es el
 episodio en sí, estoy en
 Plasma 6.2.1, ya fui hablando
 de las novedades
 que iban a llegar y han llegado.
 Ya no solo estamos en 6.2 sino
 que estamos en 6.2.1 y por lo
 que oigo
 la 6.2.1 está muy estabilizada,
 la 6.2.2 estará perfecta y por
 supuesto están trabajando ya
 en la 6.3.
 también tenemos el year 24.08.2
 con sus mejoras correspondientes,
 pero ahora voy a entrar en
 materia y os voy a ir contando
 las novedades que han pasado en
 este tiempo desde que grabé.
 Lo primero es que ya tenemos
 todas las charlas de la Akademy
 E 2024 de Valencia en nuestra
 instancia de virtud.
 De nuevo queremos agradecer a
 John por su colaboración, que
 hemos ido partiendo, separando,
 mejorando el audio.
 Luego con Whisper, igual que
 con este podcast, pues le he
 ido pasando los subtítulos y
 ya están todas en una playlist
 para que podáis disfrutarlas
 los que no tuvierais la inmensa
 fortuna de estar presencialmente
 allí como yo.
 También comentar que el 14 de
 octubre celebramos el 28 cumpleaños
 de KDE, se nos pasó entre
 episodios, pero sigue siendo
 ahí una efeméride
 que está bien saber que este
 proyecto tiene la miaja de 28
 años ya, no está nada mal y
 aparte de vez en cuando ver
 algunas fotos antiguas
 da así un gustirrinín de
 cómo ha avanzado la cosa y lo
 que disfrutábamos entonces y
 lo fácil que son las cosas
 ahora.
 También quería deciros que a
 las ya conocidas entradas de
 esta semana en KDE, que es un
 blog del que este podcast se
 nutre
 igual que todos los demás
 blogs del mundo que hablan de KDE,
 pues ahora como este Yasuo se
 centra en Plasma en sí
 tenemos la contrapartida para
 las aplicaciones del Blogs KDE,
 donde nos irán informando de
 que está por venir de las
 nuevas versiones de Gears.
 Siguiendo, tenemos que Ubuntu KDE
 Neon Core es una nueva distribución
 que está preparando Ubuntu,
 que sería una versión inmutable
 atómica de KDE con Plasma. Están
 de moda ahora mismo y está
 bien que tengamos versiones con
 KDE, porque hay algunas
 por ejemplo en otros sistemas
 operativos, pero siempre las de
 Gnome suelen estar más avanzadas.
 Con lo cual, esto me parece una
 buena iniciativa
 para los casos de uso que se vea
 que la inmutable o la atómica
 viene mejor. Hablando de KDE Neon,
 esta vez la versión normal y
 corriente
 pues tras salir Ubuntu 24-04
 estuvieron trabajando como locos
 y ya la tenemos disponible.
 Ya sabéis que son los repos
 oficiales de Ubuntu 24-04, pero
 con la paquetería por encima
 de todo lo necesario para tener
 Plasma a la última.
 Otro otro que también se han
 subido al carro de Plasma 6 es
 Ubuntu Studio, que le ha costado
 llegar a la 24-10, pero bueno
 ya sabemos que Ubuntu a partir
 de la 24-10 por fin tendrá
 nuestro querido Plasma 6, que
 es un salto sustancial y que yo
 aunque valoro mucho las distribuciones
 estables, me parece un viaje en
 el tiempo eso de tener que
 estar esperando tanto tanto
 tiempo para tener las mejoras
 que ya están publicadas hoy en
 día.
 Yo lo llamo que tengo versionitis
 y para eso tenemos distribuciones
 adecuadas y luego está quien
 no quiere tanta filigrana y
 quiere que le funcione tranquilamente
 y se queda con versiones más
 estables.
 Aunque yo tengo que decir que
 yo casi nunca tengo problemas y
 estoy a la última, así que
 ahí hay un buen debate sobre
 si que es más estable, algo
 muy antiguo que tienen que
 mantener los voluntarios o lo
 que va sacando el Upstream, el
 proyecto madre.
 Eso lo dejamos para otra ocasión.
 Una de las novedades que se me
 olvidó adelantar de Plasma 6.2,
 yo os fui comentando que iba a
 venir en Plasma 6.2 antes de
 que viniera, porque normalmente
 los desarrolladores van
 diciendo ya hemos hecho esto,
 ya hemos hecho lo otro, y hay
 una que se me olvidó y gracias
 a cada blog la tenemos siempre
 todas en mente, que es que han
 mejorado el manejo del brillo
 para los perfiles HDR e ICC,
 así como el rendimiento HDR.
 Lo cual a mí me viene de Perla
 porque tenía dos monitores,
 uno se lo he regalado a mi
 madre y estoy a punto de
 comprarme otro, y el que me voy
 a comprar creo que va a tener
 soporte HDR y estoy deseando
 empezar a jugar.
 Con suerte para cuando me lo
 compre estará Plasma 6.3 y
 seguro que estará todavía
 mejor implementado, pero bueno,
 la cosa es que ya en Linux hay
 soporte para HDR, que es algo
 que nos ha costado llegar.
 Por supuesto, esto será en Wayland,
 en X11 no creo que eso vaya a
 llegar.
 Una de las cosas de futuro, y
 ya dejamos Plasma 6.2, que esto
 ya lo tenemos aquí en las
 manos, es que Dolphin mejorará
 la búsqueda dentro de los ficheros
 en la próxima versión, que
 sea seguramente por diciembre,
 en la 2412 de Gears.
 Y la cosa es que quien conozcáis
 RipGrep, que no es una variante,
 es una alternativa a Grep, lo
 que pasa es que es súper
 rápido y es capaz de buscar en
 proyectos.
 Tiene mucha más funcionalidad
 y mucho más rendimiento.
 Bueno, pues Dolphin va a ser
 capaz de usarlo, si lo tenéis
 instalado en el sistema, para
 que las búsquedas sean mucho
 más rápidas.
 Esto creo que solo va a funcionar
 dentro de los ficheros, que es
 lo que hace RipGrep, busca
 contenido, y si lo tenéis
 activado.
 Lo que es el nombre de fichero
 va a seguir funcionando igual,
 y también vais a seguir teniendo
 la funcionalidad de Akonadi,
 eso no cambia.
 Una pequeña noticia de accesibilidad
 tangencialmente.
 Ya es posible crear y usar cursores
 SVG en Plasma.
 Esto, por ejemplo, si os acordáis,
 el efecto que ahora viene por
 defecto, de que sin meneer el
 ratón se va haciendo grande.
 La primera implementación, el
 ratón se hacía, el puntero se
 hacía grande, pero se hacía
 borroso y era un poco feo.
 Eso es porque estaba escalando
 una imagen.
 Ahora que tenemos iconos SVG
 que se escalan perfectamente,
 pues el ratón va a poder tener
 un puntero SVG.
 Ahora mismo no están todos
 disponibles, pero ya hay
 algunos dentro del Plasma, y
 luego se podrán ir añadiendo
 más, que escalen bien.
 Y esto viene muy bien a la
 accesibilidad, porque hay gente
 que necesita el ratón muy gordo,
 y está bien que sea fino y con
 líneas y no emborronado.
 Luego, dentro de noticias de la
 Asociación de KDE, pues esto
 ha pasado volando desde el
 episodio anterior, pero hemos
 hecho unas elecciones que las
 teníamos pendientes de la Akademy,
 porque a nuestro querido
 presidente Adrián se le acababa
 el plazo de mandato, son los
 estatutos, y entonces decidimos
 que para hacer unas elecciones
 solo a presidente,
 y al poco tener que hacer otras
 para la Junta, dijimos, mira,
 lo hacemos todo de golpe, dimitimos
 y renovamos Junta y Presidente.
 Y hemos cambiado un poco la
 distribución, aunque no tanto,
 porque lo que hemos hecho es
 que Adrián, que era presidente,
 desde aquí gracias por tu
 trabajo, pasa a vicepresidente.
 Rubén, que era vicepresidente,
 ahora pasa a presidente.
 Mucho ánimo, presidente, con
 el trabajo que tienes por delante.
 Repite el tesorero, José Millán,
 repite nuestro inigualable
 secretario, Baltasar Ortega,
 repetimos de vocales,
 Rosy Matthews y yo, y tenemos
 una nueva incorporación, que
 es Iván G.J., ya lo conocéis
 porque ha estado por aquí
 y también está divulgando por
 PeerTube y por YouTube, que
 entra en sustitución de José
 Picón, que fue el padre espiritual
 de este podcast,
 al que le debemos muchas iniciativas
 y mucho tiempo que ha echado de
 su vida personal en este
 proyecto y que ahora mismo
 necesita tomarse un pequeño
 descanso,
 pero que sigue con nosotros en
 la comunidad y al que le deseamos
 lo mejor.
 Y luego, una noticia fuera ya
 del entorno KDE, pero que
 nosotros lo usamos, es que hay
 una nueva versión de PeerTube,
 en la que tenemos ya audio y
 vídeo separado.
 Esto es de que puedes tener,
 aparte de que quiero ver a 1080
 el vídeo, quiero verlo a 720,
 pues ahora quiero ver solo una
 pantalla negra y tener el audio,
 porque a lo mejor tengo el móvil
 apagado o no me interesa y así
 ahorras datos.
 Esto además viene bien porque
 es una primera parte que hará
 falta para conseguir implementar
 que se puedan tener varios audios.
 Ahora mismo no lo puedes tener,
 pero es una petición que hay
 para poder tener o varios idiomas
 o audios con comentarios
 descriptivos para personas con
 deficiencias visuales.
 Pues es una primera piedra en
 el camino.
 Luego también trae un buscador
 en el widget de subtítulos.
 Tú puedes tener subtítulos en
 PeerTube, que los ves
 directamente en el vídeo.
 Ahora hay un widget que se pone
 en el panel lateral derecho
 abajo y entonces vas viendo
 todo el texto
 y ahora ahí es donde han
 puesto que puedes buscar
 palabras.
 Es una funcionalidad que muchas
 aplicaciones de podcast tienen
 y que viene muy bien cuando
 quieres saber algo en concreto
 que se ha dicho.
 Y luego hay una tercera novedad
 que a mí me sorprendió un
 poco,
 que es que tienes la posibilidad
 de configurar YouTube DL para
 importaciones.
 Yo me pensaba que YouTube DL
 estaba en mantenimiento flojo y
 que había uno nuevo,
 pero si lo han implementado
 ahora debe ser que les va bien.
 PeerTube tiene la opción de que
 tú puedes sincronizar un canal
 de YouTube con PeerTube
 y entonces te hace un espejo.
 Si borras, te lo vuelve a poner.
 Si lo que hay en YouTube se
 asegura de que esté en PeerTube.
 Pero como YouTube está siempre
 así haciendo un poco la puñeta
 en que esto no sea tan fácil,
 se ve que no es suficiente con
 tener una implementación
 directamente en PeerTube ahí
 escrita de golpe,
 sino que han hecho como dependencia
 un programa que se encargue de
 actualizarse cada vez que
 YouTube
 haga alguna de sus triquiñuelas
 para que esto deje de funcionar.
 Y ya sí que fuera de lo que es
 el Entorno KDE y casi el Show
 Art Libre,
 quería dar las gracias a los
 compañeros de Cañas y Poscas,
 que es un evento que hicieron
 los últimos de Feedlipinas y
 otros Poscas de Madrid,
 por ejemplo Borrachuzo y Elena.
 Es un evento de podcasting
 amateur, de la antigua usanza,
 de juntarse gente, escuchar
 directos, hablar de las cosas
 que les gustan.
 Y tiene un poco de parecido con
 el Show Art Libre porque esta
 gente defiende el feed libre.
 O esta gente y yo, que tengo
 ahí también una sección en
 los últimos de Feedlipinas
 con Roberto Ruizán, al que le
 mando un saludo, que fue un placer
 de virtualizar.
 Y la cosa es que tú puedes
 tener un podcast en Spotify sin
 RSS con aplicaciones propietarias
 o tú puedes usar el estándar
 RSS con un feed que esté colgado
 en un servicio codificado
 con aplicaciones libres, como
 se hace este podcast.
 Entonces son diferentes maneras
 de entender cómo publicar el
 contenido y cómo distribuirlo.
 Pues este grupo de gente defiende
 que no todo es el industrial,
 el comercial y el monetizar
 y el usar herramientas propietarias.
 Con lo cual es un evento que la
 verdad es que estuvo muy entretenido.
 Aparte me sirvió para desvirtualizar,
 que también le mando un saludo
 a Iván.
 Y ojalá pudiera ir a más.
 El problema es que el tiempo y
 la vida 1.0 no lo permite.
 También le voy a mandar un saludo,
 aunque en el evento lo vi muy
 poco,
 pero bueno, lo conseguí saludar
 antes de tener que irme a Papafriquei.
 Y de leer enhorabuena porque
 por la caída de Archive.org se
 ha cogido ahí los machos
 y ha decidido pasarse a Castopod
 y ha migrado todo su podcast
 con mucho trabajo y esfuerzo
 y trabajo manual a manopla de
 ir arreglando el feed.
 Y ahora autoaloja y lo hace en
 una plataforma hecha en
 software libre.
 Así que enhorabuena.
 Con esto me despido.
 Por supuesto, gracias a vosotros
 que estáis escuchando.
 A Jorge, que suele editar esto
 con mucha paciencia.
 Y a todas las personas amantes
 del software libre, si se lo hacéis
 llegar.
 Un saludo y nos vemos pronto.
 entonces
