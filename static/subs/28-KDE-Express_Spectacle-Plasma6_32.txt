Buenas, bienvenido de vuelta a
KDE Express. Hoy 17 de mayo,
viernes, estoy grabando el
episodio 28
y hoy no te voy a hablar de
Akademy, creo que hemos hecho
una buena promoción
y está la información ahí,
así que si os interesa estaréis
ya informados y si no, la tenéis
disponible.
Hoy vengo a hacer otro resumen
de noticias del mundo de KDE en
general
y voy a hacer una primera sección
que es distros, empezando por KDE
Neon
que han hecho un postmortem,
que viene siendo un resumen,
una review, una retrospectiva
sobre qué falló en el cambio
de Plasma 5 a Plasma 6, con el
framework, los gears
y dicen que es que hubo un par
de cosas que no capturaron las
pruebas continuas que hacen
y que encima como son pocos...
y querían fomentar la promoción
del evento, ya que era un
cambio muy gordo para la
comunidad
pues uno de los principales
mantenedores de la distro
estaba por España, en Málaga
con Paul Brown haciendo el lanzamiento
de la noticia
y entonces todo ese tiempo de
coche o de viaje que tuvo que
tener
pues le quitó tiempo de testeo
y entonces vieron rápidamente
cuáles eran los problemas
lo arreglaron, luego siguieron
arreglando alguna cosa
pero vamos, tienen un blog en
el que explican más o menos
qué es lo que pasó
y qué han hecho para que no
vuelva a pasar
luego también hablando de distros,
pues tenemos que Garuda, que es
una distro basada en Ars Linus
ya ha cogido Plasma 6, también
tenemos que Tuxedo OS también
ha dado el salto
Asahi Remix 40 KDE también
está en Plasma 6 porque está
basado en Fedora 40
y entonces ellos pues han adquirido...
han adquirido Plasma 6 igual
que los usuarios de Fedora
entonces la noticia en sí es
que ya hay muchas distros que
han pegado el salto
no Ubuntu las LTS, no Debian,
pero sí hay bastantes distros
por ahí
como Tumbleweed, que también
lo tiene, de OpenSUSE
y os quiero decir una cosa que
a mí me encanta, que es espectáculo
que antes era un capturador de
pantalla, ahora es prácticamente
un editor de imágenes
porque tiene tanta funcionalidad
que yo a veces lo uso como si
tuviera el Paint
pues una de las funcionalidades
que tenía era desenfocar
algún texto
por si tú hacías una captura
de pantalla de una aplicación
de mensajería
y no querías que se viera
algún texto
pues tenía la opción de desenforcarlo
directamente para no tener que
ir tú tachando ahí cutre
y eso se perdió en el cambio
de Plasma 6 y ya se ha recuperado
en la versión 24.05
también tiene la opción de
pixelar
y también tiene opciones de ir
marcando numeritos
y te va poniendo el solo del círculo
con el número por si quieres
hacer un tutorial
la verdad es que es una aplicación
que yo lo uso mucho en el
trabajo
para explicar cosas o hacer
tutoriales y es una maravilla
fotos, vídeos, te captura lo
que quieras, te deja editarlo
completamente recomendable
luego, hablando de recomendar,
me he enterado de que digiKam
tiene un libro electrónico que
se llama Recipes, Recetas
que está en oferta y que
además no tiene DRM
así que si soy usuario de
hacer ritmos de digiKam
yo soy usuario básico todavía
pues puede ser una buena adquisición
ayudáis al proyecto y tenéis
ahí un montón de funcionalidad
bien explicada
luego también contaros que en
abril fue el KDE Goal Spring
que viene siendo como una quedada
de desarrolladores
en la que se centran en
trabajar algo juntos
para poder acelerar
acelerar el desarrollo
porque cuando estás en remoto
se pueden hacer muchas cosas
pero siempre cuando te juntas
se aceleran y las cosas van
mejor
y entonces ahí se enfocaron en
sostenibilidad, accesibilidad y
automatización
lo cual está muy bien porque
son tres de mis temas fetiches
luego una de las cosas que yo
estaba esperando desde hace
mucho tiempo
a nivel profesional o empresarial
por así decirlo
es el tema del escritorio remoto
y parece ser que en Plasma 6.1
se va a facilitar el configurar
esto directamente
desde las opciones del sistema
en Wayland siempre ha sido un
poco más complicado
un bastante más complicado
configurar el escritorio remoto
de hecho no es posible en
muchas distros
ni con muchas aplicaciones
pero parece ser que ya hay maneras
de hacerlo
y KDE lo va a poner al alcance
de dos o tres clics
estoy seguro que OpenSUSE en
Yast
tiene un par de botones para
hacerlo
pero en Arch yo no lo había
encontrado tan fácil
y en KDE Neon seguro que
también está sencillo
si lo están haciendo los de KDE
y luego también tenemos que en
la versión 24.08 de Dolphin
os va a dejar seleccionar todos
los ficheros
o otras opciones que estamos en
KDE
que tú podrás configurar en
la configuración
cuando hablas doble clic en un
hueco donde no haya ningún fichero
tú abres Dolphin, tendrás el
listado de ficheros
en alguno de los huecos haces
doble clic
y por defecto te va a seleccionar
todos los ficheros que hay en
esa carpeta
tú podrás cambiar que cuando
hagas doble clic
sean otras acciones las que se
realicen
lo cual está bastante bien
y luego yo había pensado en
hacer un episodio especial de
atajos de teclado
porque la verdad es que hay un
montón y es súper configurable
y es súper bueno para la
productividad
pero he pensado que lo mismo es
un poco rollo todo de golpe en
un podcast sin verlo
y puede quedar mejor si lo hago
de uno en uno
entonces voy a terminar el
episodio
diciendo que uno de los que
más uso yo
que es con la tecla meta
en mi teclado es un pingüino
con la palabra Slimbook
pero en muchos teclados será
la tecla de Windows
si haces meta y la E se te abre
Dolphin
en Windows yo creo que se abriría
el explorador de ficheros
pero la verdad es que a mí es
una de las que Dolphin lo usa
un montón
es una herramienta suiza
y casi siempre lo abro con meta
E
si vosotros tenéis alguno que
usáis un montón y sea súper
útil
ponerlo por Mastodon porque a
lo mejor o seguramente
habrá muchos que no conozcamos
pues con esto
y dándole de nuevo las gracias
a esLibre por ayudarnos a
organizar el Akademy
y por supuesto a Openshusa
que es como se pronuncia Openshusa
la distro con mejores vídeos
musicales de promoción
de todo internet
y la Universidad de La Laguna
en Canarias
pues con esos agradecimientos
termino el episodio
y espero veros dentro de poco
por Akademy en Valencia
hasta luego
