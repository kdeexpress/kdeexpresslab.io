Buenas, esto es KDE Express y estoy grabando el 14 de agosto 
de 2024. Hoy me uno a los podcasters que 
aunque saben que en agosto la mayoría de la gente 
está haciendo otras cosas y no escucháis tantos podcats, 
que siguen queriendo contaros las novedades, en mi caso, de 
la comunidad KDE. Hoy os traigo unas cuantas noticias, 
la primera de accesibilidad, que es que en la próxima 
versión 6.2, que vendrá por octubre, 
en la página de configuración del sistema, si os vais a accesibilidad, 
ahí podéis cambiar el timbre del sonido de las alertas que 
salen. Pues ahora el selector de ficheros 
os va a dejar elegir ficheros.oga, que suelen ser los ogg también, 
pero con a de audio. Y aparte os dirá en un listado 
los formatos que soporta. Eso es una pequeña mejora, 
pero de estas del buen camino que a mí me gustan. 
Luego tenemos en... En nivel de aplicaciones, que 
digiKam se ha actualizado a la versión 8.4, 
que tiene muchos arreglos y algunas novedades, 
como que ahora tiene etiquetas automáticas con análisis profundo 
y traducción en línea de estas etiquetas. 
Esto quiere decir que tiene como un analizador interno en 
local que es capaz de detectar cosas. 
Y entonces te etiqueta porque si hay un perro, que si hay una 
nube, este tipo de cosas. Pero como las etiquetas trabajan 
en inglés, ahora tiene un traductor automático, 
que las puedes configurar tú, voluntariamente, 
si quieres que te lo analices y si quieres que te lo traduzcas. 
Luego también tengo por aquí apuntado que tiene 
una nueva documentación, tanto en la página web como en 
formato de DocBook. También añade mejoras de formato 
de ficheros de las cámaras, estilo de, pues, la Canon, no 
sé qué, la no sé cuánto. Cada una tiene sus peculiaridades 
y van añadiendo cámaras que leen mejor sus ficheros. 
También tenemos que tiene una nueva utilidad OCR, 
de... "Spell checking", de corrección 
gramatical y de configuración de idioma. 
También ponen por aquí que han mejorado la "metadata" y el 
"ExifTool tool", que eso es como si fuera el ID3 
de los audios. Es como decir que si la cámara 
ha grabado en vertical o en horizontal, 
que si ha grabado con no sé cuánto de exposición, 
todo ese tipo de cosas de la gente que entiende de cámaras, 
que no es mi caso. También tienen una utilidad 
que han mejorado para renombrar ficheros. 
Y qué más... El buscador y la base de datos 
también la han hecho mejorar. La calidad del clasificador de 
imágenes, que realmente no sé lo que es. 
También han mejorado la integración con el G’MIC, que es otra cosa 
que se me escapa. Y por supuesto, la migración a 
Qt 6 que va llegando a todas las aplicaciones del Paraguas KDE. 
Y ya lo demás son arreglos típicos. Eso era en cuanto a digiKam. 
Luego tenemos que Plasma 6 se... Sigue actualizando poco a poco, 
una vez que ya hicimos la migración gorda. 
Y yo ahora mismo estoy en Plasma 6.1.4 y en el framework 6.4.0. 
Pues en Plasma 6.1.2 han mejorado el manejo de los Flatpaks en 
Discover. No sé si Flatpak es una cosa 
que a mí me gusta mucho. Te simplifica buscar cosas, 
tiene el enjaulado para ser un pelín más seguro. 
Tampoco es la panacea. Pero Flatpak tiene el 
problema de que los runtime, como si fueran las dependencias 
gordas que todos usan, a veces empiezan a ocupar y una 
aplicación necesita una y otra aplicación necesita otra. 
Y entonces al final tienes tres de los mismos en versiones 
diferentes. Ocupa bastante espacio. 
Si tienes un disco duro grande, te da igual. 
Si no tienes un TOC como yo que te gusta tenerlo limpio. 
En línea de comando había maneras de hacer limpieza, es decir, oye, 
bórrame todas las que ya no uses o actualízame 
esta. Ahora lo que han hecho es que 
automáticamente Discover hace rebases de los runtimes y 
eliminan los que están. 
Que hacen mantenimiento más automático la aplicación 
gráfica que antes podías hacer 
tú por línea de consola. En Plasma 6.1.3 y 6.2.0, porque 
son arreglos y mejoras que van a venir por parte, han metido 
mucha caña en plan accesibilidad con las sticky case, que es 
algo que yo he aprendido lo que es para este episodio y es que 
hay personas que por discapacidad o por comodidad o productividad, 
no se les da bien hacer la combinación de teclas de tener 
una tecla pulsada y pulsar otra. 
Entonces las sticky case lo que te dejan es dejar pulsada una 
tecla como el control, el shift o el alt, fija que tú le 
das y ya se queda pulsada y entonces pulsar la siguiente 
tecla. Esto se ve que estaba controlado 
en X11, en Wayland se ha ido por el camino y le han 
metido muchísima caña y parece ser que en la 6.2 ya lo 
van a tener controlado y en la 6.1.3 hay algunas mejoras, pero 
todavía no está a la par con X11. 
Luego también os traigo un proyecto hecho por un padre que 
me da un aire porque tiene un crío de 3 años y pico y 
entonces ha ido explicando qué le ha hecho a su hijo para 
que pueda jugar un poco con una Raspberry Pi 5 o un par de 
accesorios más, cuenta en un post por las fotos y qué ha 
ido haciendo, el cable que ha elegido, una pantalla, todas 
esas cosas. Está gracioso, se gusta trastear 
y lo gracioso para este podcast es que lo que ha elegido 
es como aplicación principal. G-Comprise, es una maravilla 
con 5.000 mini aplicaciones y juegos dentro para todas las 
edades. K-Letters, que es específico 
para el tema de lectura y luego también Tuxpain para dibujar 
un poco. Os dejo el enlace por si os 
parece interesante. También tenemos un artículo 
en inglés en el que te dice que puedes contribuir a KDE 
en muchos lenguajes. Es famoso que KDE está hecho 
en C++ y algo de QML. Entonces, pues para mucha gente 
no es amigable dedicarse a intentar leer todo ese código 
y hacer algún mal recuerdo y aportar código porque ese no 
es su lenguaje de programación de cabecera. 
Pero lo que yo no sabía y este chico se ha dedicado a explicar 
es que tú realmente puedes usar código que no sea C++ en 
muchas partes del ecosistema KDE. Y entonces te hace un listado, 
entonces te dice que puedes usar Python, Ruby, Perl, 
Containers, tipo Docker y Podman, HTML, S, 
CSS y Javascript, WebAssembly, Flatpak y Snap, ZenMate, Java 
y Rust. En todos esos lenguajes hay 
proyectos de KDE en los que tú puedes colaborar. 
Y entonces el tema uno por uno, eso era el TOC, la guía del 
post. Si tú vas pinchando te va 
diciendo cómo encontrar proyectos que usan esos lenguajes 
y qué tipo de proyectos hay en cada uno de 
esos lenguajes. Pues mira, en Python hacemos 
esto, esto y esto que tiene que ver con el AT-SPI o con 
el Kantai Tracker, el Localize. Entonces te hace un resumen de 
las cosas que puedes echarle un vistazo por si te interesa 
echar una mano en ese lenguaje que tú sí que controles. 
Lo cual siempre hemos dicho que a KDE o al software libre en 
general se puede ayudar de miles de formas, con publicidad, 
ayudando, moderando foros, generando subtítulos o transcripciones 
de los vídeos, de los audios, con diseño gráfico, haciendo 
publicidad, un montón de maneras. 
Pero también es cierto que... Que se puede ayudar programando 
y no tiene por qué ser C++. Y luego también, no sé cuánta 
gente podrá, aquí sé que Kiva ha estado a punto, pero 
no ha podido por motivos que dentro de lo que hay no son malos, 
que es que consigue un trabajo nuevo, y es que tenemos 
la Akademy Internacional 2024. Va a ser en Würzburg, en  
Alemania, entre el 7 y el 12 de septiembre. 
Por si hay alguien que se anima y tiene la suerte de poder ir 
para allá, ya sabéis que la Akademy Internacional es en 
inglés, pero ahí hay gente muy pro. 
Así como en la Akademy España hay mucho más socialización, 
aplicación, nivel usuario, en la Akademy Internacional hay 
mucho desarrollador. Pero bueno, si sois desarrollador 
y queréis ir, seguramente ya lo sabríais, pero bueno, 
está bien que en este podcast lo recordemos. 
También, los que no podáis ir pero os interese, seguramente 
se retransmitirá online. Eso era en cuanto a noticias 
del mundillo KDE. También quería hablar un poco 
de mi libro, o lo que viene siendo mismo, de este podcast. 
Y es que, como todavía... Todavía las aplicaciones no 
tienen muy integradas las etiquetas del podcasting 2.0, 
que seguro que he hablado alguna vez, del Tag Person, 
para decir quién habla, o quién edita, 
o quién participa, o los subtítulos, o todo este tipo de cosas. 
Lo que he hecho es que en la página web de este podcast, y 
en la de accesibilidad con tecnologías libres, porque uso 
el mismo framework, he añadido la información que ya pongo en 
el feed, en el XML, me la he traído a la parte web. 
Entonces, si abrís el artículo de este episodio, por 
ejemplo, ahora abajo os saldrá subtítulos disponibles en 
y un enlace. Y luego, por si vuestra aplicación 
no lo implementa, y un enlace a un listado de aplicaciones 
que sí que lo implementan. Este en concreto ya lo había 
puesto, pero hace poco se me ha ocurrido que podría hacer 
lo mismo con los comentarios y con las personas. 
Entonces, ahora nosotros, en estos programas, siempre enlazamos 
un pod de Mastodon, donde si comentáis en las aplicaciones 
como Podverse, TrueFans y todas estas, ahí aparecen 
directamente los comentarios. 
De hecho, en TrueFans, creo que la semana que viene, se podrá 
comentar directamente desde la aplicación. 
Pues como estos lo usan muy pocas aplicaciones, lo que he hecho 
es poner un enlace. Enlace de comentarios os llevará 
al post de Mastodon, donde en KDE España, pues publicitamos 
este episodio, por si queréis decirnos algo directamente 
allí. También he puesto un click, en 
que si le dais a y transcripción completa aquí, os 
sale toda la transcripción del programa. 
Esto sale tanto en la web, como en las notas del programa, en 
vuestra aplicación. Así, si estáis en vuestra 
aplicación antigua. Y no podéis ver nada de esto, 
pues simplemente en un enlace en las notas del programa, 
pincháis y lo tenéis disponible. 
Y luego, lo que he puesto, pero que está solo en la web, es 
que a la derecha, si estáis en ordenador, y abajo, después 
de las notas del programa, si estáis en móvil, después 
de todo el texto, con todos los enlaces que yo dejo de las noticias, 
tendréis un participante dos puntos, y os saldrán unos 
circulitos con las imágenes, y el texto, y en calidad 
de las personas que participan. Normalmente, últimamente, pues 
David Marzal, el que habla principalmente, 
Jorge Lama es el que edita. Aunque en el anterior, si vais 
al episodio anterior, pues tenéis a Iván, que os dice 
que, a ver que lo tengo aquí, saldrá, participan, Iván como 
invitado, aparte de Jorge Lama como editor. 
Pequeñas mejoras, algunas de accesibilidad, algunas pijotadas 
que nos gustan a la gente del podcasting, y algunas que 
puede que os resulte útil, si os gusta comentar, o ver 
los subtítulos, o leer la transcripción, por si queréis 
ver algún nombre de un programa 
de algo que hemos dicho. Con esto termino por hoy, 
espero que estéis pasando buen verano, que tengáis 
salud, y que tengáis mucho software libre. 
