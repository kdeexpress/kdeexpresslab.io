1
00:00:00,000 --> 00:00:04,090
 KDE Express 21, si no me equivoco,

2
00:00:04,090 --> 00:00:05,050
 voy en coche así que no lo

3
00:00:05,050 --> 00:00:05,840
 tengo apuntado.

4
00:00:05,840 --> 00:00:10,200
 Quería contarte un par de

5
00:00:10,200 --> 00:00:13,520
 cosas cortas de las que se

6
00:00:13,520 --> 00:00:15,680
 pueden utilizar ya y de las que

7
00:00:15,680 --> 00:00:16,320
 vienen a futuro.

8
00:00:16,320 --> 00:00:20,090
 De las que pueden utilizar ya

9
00:00:20,090 --> 00:00:23,440
 es, en KDE tenemos unos repositorios

10
00:00:23,440 --> 00:00:24,800
 para F-Droid que tienen

11
00:00:24,800 --> 00:00:26,130
 aplicaciones extras de las que

12
00:00:26,130 --> 00:00:27,410
 hay en el repositorio oficial

13
00:00:27,410 --> 00:00:28,920
 de F-Droid. Yo lo uso

14
00:00:28,920 --> 00:00:31,800
 mucho por ejemplo para NeoChat

15
00:00:31,800 --> 00:00:34,560
 y para Itinerary creo que es y

16
00:00:34,560 --> 00:00:36,400
 pues con eso consigue la aplicación

17
00:00:36,400 --> 00:00:37,540
 en APK directamente, no te la

18
00:00:37,540 --> 00:00:38,780
 tienes que estar bajando, pero

19
00:00:38,780 --> 00:00:40,160
 eso hasta ahora tenía una pega

20
00:00:40,160 --> 00:00:40,600
 que

21
00:00:40,600 --> 00:00:41,950
 es que ese repositorio a pesar

22
00:00:41,950 --> 00:00:43,420
 de ser un repositorio estable,

23
00:00:43,420 --> 00:00:45,400
 porque hay un repositorio con

24
00:00:45,400 --> 00:00:46,600
 todos los

25
00:00:46,600 --> 00:00:48,380
 builds de la rama master, o sea

26
00:00:48,380 --> 00:00:49,690
 el código que se está

27
00:00:49,690 --> 00:00:51,920
 desarrollando, hay un repositorio

28
00:00:51,920 --> 00:00:52,080
 que

29
00:00:52,080 --> 00:00:53,440
 todas las noches se compila y

30
00:00:53,440 --> 00:00:54,860
 tú tienes pues la versión de

31
00:00:54,860 --> 00:00:56,440
 pruebas, pero el repositorio

32
00:00:56,440 --> 00:00:56,840
 estable

33
00:00:56,840 --> 00:00:58,390
 solo tiene las versiones que

34
00:00:58,390 --> 00:00:59,940
 están marcadas con el punto

35
00:00:59,940 --> 00:01:01,720
 algo, pero tenía una pega que

36
00:01:01,720 --> 00:01:01,960
 a mí me

37
00:01:01,960 --> 00:01:03,450
 volvía loco, que es que

38
00:01:03,450 --> 00:01:04,860
 también se compilaba todas las

39
00:01:04,860 --> 00:01:06,920
 noches. Todos los días tú tenías

40
00:01:06,920 --> 00:01:07,640
 dos

41
00:01:07,640 --> 00:01:09,190
 actualizaciones, bueno yo

42
00:01:09,190 --> 00:01:10,670
 porque tengo esas dos aplicaciones

43
00:01:10,670 --> 00:01:12,360
 instaladas fuera del repositorio

44
00:01:12,360 --> 00:01:13,960
 oficial, pero toda aplicación

45
00:01:13,960 --> 00:01:15,330
 que venga de ese repositorio

46
00:01:15,330 --> 00:01:16,720
 todas las noches te aparecía

47
00:01:16,720 --> 00:01:17,120
 en F-Droid

48
00:01:17,120 --> 00:01:18,270
 como que había una nueva

49
00:01:18,270 --> 00:01:19,740
 versión y si no querías tener

50
00:01:19,740 --> 00:01:21,280
 la notificación pues tenías

51
00:01:21,280 --> 00:01:21,800
 que decirle que

52
00:01:21,800 --> 00:01:23,360
 esa aplicación no te avisara y

53
00:01:23,360 --> 00:01:24,800
 tú de vez en cuando meterte y

54
00:01:24,800 --> 00:01:26,240
 actualizar a mano o estabas

55
00:01:26,240 --> 00:01:26,800
 instalando

56
00:01:26,800 --> 00:01:28,670
 todos los días la misma aplicación

57
00:01:28,670 --> 00:01:30,160
 porque era exactamente la misma

58
00:01:30,160 --> 00:01:31,560
 que se había compilado de

59
00:01:31,560 --> 00:01:33,000
 nuevo hasta el día que cambiaba

60
00:01:33,000 --> 00:01:34,330
 algo el código y entonces esa

61
00:01:34,330 --> 00:01:35,920
 sí era nueva, pero tú no veías

62
00:01:35,920 --> 00:01:36,040
 la

63
00:01:36,040 --> 00:01:37,350
 diferencia y por suerte a

64
00:01:37,350 --> 00:01:38,680
 partir de hoy que estáis

65
00:01:38,680 --> 00:01:40,630
 escuchando esto, eso ya no pasa,

66
00:01:40,630 --> 00:01:41,760
 porque han cambiado

67
00:01:41,760 --> 00:01:43,570
 la infraestructura en la que se

68
00:01:43,570 --> 00:01:45,450
 compilan, han pasado de Fabricator

69
00:01:45,450 --> 00:01:47,230
 o no sé qué, cómo se llama,

70
00:01:47,230 --> 00:01:47,500
 lo que

71
00:01:47,500 --> 00:01:48,690
 compilaba antes y ahora lo

72
00:01:48,690 --> 00:01:50,090
 hacen con GitLab, que es donde

73
00:01:50,090 --> 00:01:52,040
 se está centralizando la infraestructura

74
00:01:52,040 --> 00:01:54,440
 de KDE Internacional. Entonces

75
00:01:54,440 --> 00:01:56,800
 ahora este segundo repositorio

76
00:01:56,800 --> 00:01:58,560
 extra que no es el de F-Droid

77
00:01:58,560 --> 00:01:58,960
 oficial

78
00:01:58,960 --> 00:02:00,550
 y que tiene aplicaciones que no

79
00:02:00,550 --> 00:02:02,140
 están en el oficial, ya sólo

80
00:02:02,140 --> 00:02:03,640
 te va a dar actualizaciones

81
00:02:03,640 --> 00:02:04,040
 cuando

82
00:02:04,040 --> 00:02:05,720
 realmente se haya hecho una

83
00:02:05,720 --> 00:02:07,770
 versión nueva de la aplicación

84
00:02:07,770 --> 00:02:09,840
 y eso a mí la verdad es que me

85
00:02:09,840 --> 00:02:10,080
 viene

86
00:02:10,080 --> 00:02:11,970
 muy bien porque machaco menos

87
00:02:11,970 --> 00:02:14,040
 la memoria interna del teléfono,

88
00:02:14,040 --> 00:02:15,560
 el TOC que tengo de que no me

89
00:02:15,560 --> 00:02:15,800
 gusta

90
00:02:15,800 --> 00:02:17,640
 ver notificaciones me lo va a

91
00:02:17,640 --> 00:02:19,470
 arreglar y de paso voy a saber

92
00:02:19,470 --> 00:02:22,040
 realmente cuándo se ha actualizado

93
00:02:22,040 --> 00:02:22,160
 la

94
00:02:22,160 --> 00:02:23,300
 aplicación y cuándo hay

95
00:02:23,300 --> 00:02:24,720
 alguna novedad, así que esa es

96
00:02:24,720 --> 00:02:26,120
 una de las cosas que ya podéis

97
00:02:26,120 --> 00:02:27,280
 disfrutar y de

98
00:02:27,280 --> 00:02:29,660
 paso por si no lo sabíais para

99
00:02:29,660 --> 00:02:31,790
 que tengáis en cuenta que hay

100
00:02:31,790 --> 00:02:34,080
 un repositorio de KDE que pondré

101
00:02:34,080 --> 00:02:35,890
 las notas del programa cómo

102
00:02:35,890 --> 00:02:37,330
 llegar para poder tener instaladas

103
00:02:37,330 --> 00:02:39,040
 aplicaciones de KDE fuera del

104
00:02:39,040 --> 00:02:41,000
 repositorio oficial. Eso era lo

105
00:02:41,000 --> 00:02:42,600
 que es para hoy, luego antes de

106
00:02:42,600 --> 00:02:43,840
 lo que es para el futuro

107
00:02:43,840 --> 00:02:44,520
 quería

108
00:02:44,520 --> 00:02:46,750
 desde aquí mandar un abrazo

109
00:02:46,750 --> 00:02:48,980
 muy fuerte a Juan Febles que si

110
00:02:48,980 --> 00:02:51,080
 no estáis en Mastodon lo mismo

111
00:02:51,080 --> 00:02:51,240
 no

112
00:02:51,240 --> 00:02:53,670
 había enterado de que súbitamente

113
00:02:53,670 --> 00:02:55,560
 nos ha anunciado de que tiene

114
00:02:55,560 --> 00:02:58,170
 que parar el proyecto Linux

115
00:02:58,170 --> 00:02:58,560
 Express

116
00:02:58,560 --> 00:03:00,410
 y Podcast Linux por motivos

117
00:03:00,410 --> 00:03:02,460
 personales, sean los que sean

118
00:03:02,460 --> 00:03:04,560
 esperamos todos que le vaya muy

119
00:03:04,560 --> 00:03:04,800
 bien a

120
00:03:04,800 --> 00:03:06,460
 Juan porque se merece lo mejor,

121
00:03:06,460 --> 00:03:07,910
 porque en calidad humana no

122
00:03:07,910 --> 00:03:09,680
 sólo era de los mejores Podcasts

123
00:03:09,680 --> 00:03:09,860
 de

124
00:03:09,860 --> 00:03:11,690
 Linux de habla hispana sino que

125
00:03:11,690 --> 00:03:13,470
 era de las mejores personas en

126
00:03:13,470 --> 00:03:15,580
 la comunidad de Linux de habla

127
00:03:15,580 --> 00:03:15,960
 hispana,

128
00:03:15,960 --> 00:03:17,870
 así que sea lo que sea esperamos

129
00:03:17,870 --> 00:03:19,800
 que se solucione, estoy seguro

130
00:03:19,800 --> 00:03:21,520
 que todos tus oyentes Juan

131
00:03:21,520 --> 00:03:22,580
 esperan

132
00:03:22,580 --> 00:03:23,950
 que en el futuro dado que la

133
00:03:23,950 --> 00:03:25,870
 vida es muy larga pueda retomarlo.

134
00:03:25,870 --> 00:03:27,680
 Y ya volviendo a KDE quería

135
00:03:27,680 --> 00:03:30,600
 contaros que últimamente llevo

136
00:03:30,600 --> 00:03:33,220
 viendo varias noticias sobre

137
00:03:33,220 --> 00:03:35,840
 que Plasma 6 se pasa a Wayland por

138
00:03:35,840 --> 00:03:37,500
 defecto y según el artículo

139
00:03:37,500 --> 00:03:38,990
 la verdad es que he visto por

140
00:03:38,990 --> 00:03:40,880
 lo menos 12, todo no me ha dado

141
00:03:40,880 --> 00:03:40,960
 tiempo

142
00:03:40,960 --> 00:03:42,270
 a leérmelo entero porque ya ha

143
00:03:42,270 --> 00:03:43,380
 habido un momento que me he

144
00:03:43,380 --> 00:03:44,700
 saturado, había algunos que

145
00:03:44,700 --> 00:03:45,360
 estaban bien

146
00:03:45,360 --> 00:03:46,800
 y otros que estaban regular,

147
00:03:46,800 --> 00:03:48,520
 entonces os quiero hacer un resumen

148
00:03:48,520 --> 00:03:50,120
 por si habéis caído en alguno

149
00:03:50,120 --> 00:03:52,150
 de los que está regular. Plasma

150
00:03:52,150 --> 00:03:53,550
 6 ha decidido ser Wayland por

151
00:03:53,550 --> 00:03:55,160
 defecto pero eso es lo que no

152
00:03:55,160 --> 00:03:55,840
 significa

153
00:03:55,840 --> 00:03:57,920
 es que si tú tienes un ordenador

154
00:03:57,920 --> 00:04:00,270
 instalado con Plasma al actualizar

155
00:04:00,270 --> 00:04:01,600
 a Plasma 6 no te va a

156
00:04:01,600 --> 00:04:03,720
 desaparecer el X11 ni se te va

157
00:04:03,720 --> 00:04:05,590
 a cambiar por defecto a Wayland,

158
00:04:05,590 --> 00:04:07,680
 lo que significa es que para

159
00:04:07,680 --> 00:04:10,160
 nuevas instalaciones de nuevas

160
00:04:10,160 --> 00:04:12,740
 distribuciones que usen la compilación

161
00:04:12,740 --> 00:04:14,320
 por defecto de Plasma

162
00:04:14,320 --> 00:04:16,620
 esa compilación va a ir con Wayland,

163
00:04:16,620 --> 00:04:18,720
 se va a poder seguir instalando

164
00:04:18,720 --> 00:04:20,080
 la sesión X11,

165
00:04:20,080 --> 00:04:21,620
 simplemente la tendrás que

166
00:04:21,620 --> 00:04:23,130
 instalar dos paquetes y elegir

167
00:04:23,130 --> 00:04:24,520
 a la hora de inicio y

168
00:04:24,520 --> 00:04:26,460
 muchas distribuciones no compilan

169
00:04:26,460 --> 00:04:28,180
 Wayland por defecto, ellos harán

170
00:04:28,180 --> 00:04:29,120
 lo que les dé la gana,

171
00:04:29,120 --> 00:04:31,620
 o sea, igual que no se ve igual

172
00:04:31,620 --> 00:04:34,520
 Plasma en openSUSE, en Fedora,

173
00:04:34,520 --> 00:04:37,240
 en Arch o en Debian pues

174
00:04:37,240 --> 00:04:39,280
 cada una de esas distribuciones

175
00:04:39,280 --> 00:04:41,820
 eligen qué flags, qué etiquetas

176
00:04:41,820 --> 00:04:43,600
 activar y desactivar cuando

177
00:04:43,600 --> 00:04:44,080
 compilan,

178
00:04:44,080 --> 00:04:45,770
 para eso están las distribuciones,

179
00:04:45,770 --> 00:04:46,830
 para hacer elecciones y

180
00:04:46,830 --> 00:04:48,680
 configurar las cosas a su gusto,

181
00:04:48,680 --> 00:04:49,930
 quitando Gentoo que ahí te lo

182
00:04:49,930 --> 00:04:51,950
 haces todo tú completamente.

183
00:04:51,950 --> 00:04:53,800
 Entonces habrá distribuciones

184
00:04:53,800 --> 00:04:55,130
 que porque ellos lo consideran

185
00:04:55,130 --> 00:04:56,300
 que no está maduro o porque

186
00:04:56,300 --> 00:04:58,680
 les gusta, compilarán Plasma

187
00:04:58,680 --> 00:05:01,590
 con X11 y por defecto tú arrancarás

188
00:05:01,590 --> 00:05:03,950
 X11 y tendrás que activar Wayland

189
00:05:03,950 --> 00:05:04,920
 tú a mano,

190
00:05:04,920 --> 00:05:06,790
 que es lo que yo recomiendo,

191
00:05:06,790 --> 00:05:08,560
 porque está bastante maduro,

192
00:05:08,560 --> 00:05:10,080
 pruébalo y si hay algo en

193
00:05:10,080 --> 00:05:11,260
 concreto que no te va siempre

194
00:05:11,260 --> 00:05:12,570
 puede volver X11, pero hay

195
00:05:12,570 --> 00:05:13,950
 muchos artículos que te dicen

196
00:05:13,950 --> 00:05:15,120
 que Plasma

197
00:05:15,120 --> 00:05:17,000
 ha dejado obsoleto X11 y ya no

198
00:05:17,000 --> 00:05:18,670
 va a poder usarlo, si es verdad

199
00:05:18,670 --> 00:05:21,070
 que yo creo que como Fedora y

200
00:05:21,070 --> 00:05:21,560
 Gnome

201
00:05:21,560 --> 00:05:23,620
 están hablando de eso, están

202
00:05:23,620 --> 00:05:26,090
 proyectando a futuro cargarse X11

203
00:05:26,090 --> 00:05:27,400
 y hacerse solo Wayland,

204
00:05:27,400 --> 00:05:28,920
 pues yo no sé si alguno se ha

205
00:05:28,920 --> 00:05:30,580
 confundido y ha pensado que lo

206
00:05:30,580 --> 00:05:32,160
 de Plasma va a igual pero no,

207
00:05:32,160 --> 00:05:34,560
 Plasma va a seguir funcionando

208
00:05:34,560 --> 00:05:37,200
 con X11 durante mucho tiempo y

209
00:05:37,200 --> 00:05:39,400
 la 527 pues todo lo que dure la

210
00:05:39,400 --> 00:05:41,380
 527 y la Plasma 6 como va a

211
00:05:41,380 --> 00:05:43,500
 tener soporte de X11 también

212
00:05:43,500 --> 00:05:45,220
 cuando salga, también va a

213
00:05:45,220 --> 00:05:45,520
 haber una

214
00:05:45,520 --> 00:05:47,720
 chorra de años con X11 en Plasma

215
00:05:47,720 --> 00:05:49,390
 6, o sea que si os quedaba

216
00:05:49,390 --> 00:05:51,680
 alguna duda de qué pasa con Plasma

217
00:05:51,680 --> 00:05:51,720
 y

218
00:05:51,720 --> 00:05:53,360
 Wayland simplemente es el por

219
00:05:53,360 --> 00:05:55,230
 defecto en las compilaciones

220
00:05:55,230 --> 00:05:57,280
 por defecto, ¿eso qué es?

221
00:05:57,280 --> 00:05:57,760
 pues

222
00:05:57,760 --> 00:05:59,920
 KDE Neon por ejemplo, KDE Neon

223
00:05:59,920 --> 00:06:01,810
 si que compila como viene

224
00:06:01,810 --> 00:06:04,120
 configurado el código en los

225
00:06:04,120 --> 00:06:04,720
 repositorios,

226
00:06:04,720 --> 00:06:06,520
 pues KDE Neon si hace una

227
00:06:06,520 --> 00:06:08,950
 instalación nueva vendrá con

228
00:06:08,950 --> 00:06:11,240
 Plasma Wayland y lo dicho estará

229
00:06:11,240 --> 00:06:11,360
 en

230
00:06:11,360 --> 00:06:12,740
 la wiki cuáles son los dos o

231
00:06:12,740 --> 00:06:14,180
 tres paquetes que tienes que

232
00:06:14,180 --> 00:06:15,840
 instalar para tener el X11

233
00:06:15,840 --> 00:06:16,440
 disponible

234
00:06:16,440 --> 00:06:17,850
 si hay algo en concreto que no

235
00:06:17,850 --> 00:06:19,320
 te funcione. Y con estas dos

236
00:06:19,320 --> 00:06:20,840
 cosas terminamos por hoy la

237
00:06:20,840 --> 00:06:22,590
 versión coche, nos escuchamos

238
00:06:22,590 --> 00:06:23,640
 pronto, un saludo.

239
00:06:23,640 --> 00:06:27,000
 [MÚSICA]

240
00:06:27,000 --> 00:06:33,000
 [Música de cierre]

