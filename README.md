# Podcast KDE Express

Web para generar el feed del podcast


## Licencia
Todo el contenido propio del podcast se libera bajo Licencia [Creative Commons Atribución - Compartir igual](https://creativecommons.org/licenses/by-sa/4.0/)  
<img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" >
</a>
