---
title: "#05 - Fin de vacaciones"
date: 2021-09-10T01:05:05+02:00
categories: [kdeexpress]
tags: [temporada1, Steam, Kasts, Krita, Mobile]
author: "David Marzal"
img: 05/05_patak_1500_0.jpg
imgALT: "Fondo de pantalla ganador del Plasma 25th Anniversary Edition Wallpaper Contest"
podcast:
  audio: 05-kde-express-fin-de-vacaciones/05_KDE_Express-Fin_de_Vacaciones
  ##audio: 05_KDE_Express-Fin_de_Vacaciones
  olength : 13209526
  mlength : 14468300
  iduration : "00:20:05"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/05_KDE_Express-Fin_de_Vacaciones.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/05_KDE_Express-Fin_de_Vacaciones.asr.srt"
    trtype : text/srt
participantes :
  - person : Jose Picon
    role : Host
    group : Cast
    img : https://mpipeunstor.blob.core.windows.net/mpiuseravatars/86a37988-8272-4370-bc77-2c16d9ede0e5.jpg
    href : https://es.linkedin.com/in/jos%C3%A9-pic%C3%B3n
  - person : David Marzal
    role : Co-Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Brais Arias
    role : Co-Host
    group : Cast
    img : https://static.mastodon.gal/accounts/avatars/108/333/163/725/284/233/original/8a8b2905c9aed914.png
    href : https://mastodon.gal/@braisarias
comments: false
aliases:
  - /05
  - /5
---
Han vuelto, José y Brais estaban de merecidas vacaciones pero ya los tenemos por aquí para retomar una ronda de noticias de 20 minutos para empezar el curso poniéndonos al día. Hablaremos de Krita, PeerTube, Steam Deck, Gear, Kasts, puestos de trabajo, entre otras tantas cosas.

Artículo original en <https://kdeexpress.gitlab.io/05/>
<!--more-->

* José nos hace un buen resumen sobre las posibilidades de [Krita](https://krita.org/es/)

* KDE ahora está en PeerTube, en [esta instancia en concreto](https://tube.kockatoo.org/a/kde_community/video-channels)

* Plasma Mobile Gear 21.08 : [plasma-mobile.org](https://www.plasma-mobile.org/2021/08/31/plasma-mobile-gear-21-08/) - [linuxadictos](https://www.linuxadictos.com/plasma-mobile-gear-21-08.html)

* Actualización de Plasma 5.22.5: [kdeblog](https://www.kdeblog.com/lanzada-la-quinta-actualizacion-de-plasma-5-22-2.html) - [kde.org](https://kde.org/announcements/plasma/5/5.22.5/)

* Patak por Aron, [ganador](https://dot.kde.org/2021/09/01/announcing-winner-plasma-25th-anniversary-edition-wallpaper-contest) de" Plasma 25th Anniversary Edition Wallpaper Contest"

* KDE Gear 21.08.1 : [kdeblog](https://www.kdeblog.com/primera-actualizacion-de-kde-gear-21-08.html) - [kde.org](https://kde.org/announcements/gear/21.08.1/)

* KDE e.V. is [looking](https://ev.kde.org//2021/09/01/job-project-kde-hardware/) for a developer to help further hardware integration projects

* Steam Deck sigue generando [noticias](https://www.gamingonlinux.com/articles/category/Steam_Deck) e [ilusión](https://www.gamingonlinux.com/2021/08/i-look-forward-to-all-the-improvements-kde-plasma-will-get-with-the-steam-deck/page=2) en la comunidad

* YA puedes [presentar](https://www.kde-espana.org/akademy-es-2021-en-linea/presenta-tu-charla) tu charla para el Akademy-ES 2021!!
___
{{% linksubssteno %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}