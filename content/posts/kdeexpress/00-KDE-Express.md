---
title: "#00 - Nos Presentamos"
date: 2021-07-19T01:00:00+02:00
categories: [kdeexpress]
tags: [temporada1,nosotros]
author: "David Marzal"
img: 800px-Kde_dragons.png
imgALT: "Konqi junto a muchos de sus compis de diferentes colores, uno lleva una bandera de KDE"
podcast:
  audio: 00-kde-express-nos-presentamos/00_KDE_Express-Nos_Presentamos
  ##audio: 00_KDE_Express-Nos_Presentamos
  olength : 2905097
  mlength : 3075364
  iduration : "00:04:16"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/00_KDE_Express-Nos_Presentamos.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/00_KDE_Express-Nos_Presentamos.asr.srt"
    trtype : application/x-subrip
participantes :
  - person : Jose Picon
    role : Host
    group : Cast
    img : https://mpipeunstor.blob.core.windows.net/mpiuseravatars/86a37988-8272-4370-bc77-2c16d9ede0e5.jpg
    href : https://es.linkedin.com/in/jos%C3%A9-pic%C3%B3n
  - person : David Marzal
    role : Co-Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Brais Arias
    role : Co-Host
    group : Cast
    img : https://static.mastodon.gal/accounts/avatars/108/333/163/725/284/233/original/8a8b2905c9aed914.png
    href : https://mastodon.gal/@braisarias
comments: false
aliases:
  - /00
  - /0
---
Buenas, tenemos el placer de presentaros este nuevo podcast de la comunidad KDE España, en el que trataremos noticias y la actualidad de esta nuestra comunidad con un formato breve (max 30 minutos) para complementar los diferentes formatos en los que ya se divulga actualmente (video-podcast y blog).

En este episodio simplemente nos presentamos los participantes y os hacemos un resumen de lo que vais a poder encontrar en los siguientes episodios, esperemos que os resulten útiles o interesantes.

Artículo original en [https://kdeexpress.gitlab.io/00/](https://kdeexpress.gitlab.io/00/) con todos los enlaces.
<!--more-->
Actualmente estamos grabando 3 socios, aunque posiblemente ampliemos y hagamos colaboraciones, os dejamos también por aquí algunos enlaces sobre nosotros:

- __José Picon__: Apasionado de la tecnología y en especial del Software libre. En mis inicios empece con un Spectrum 128k y aprendí a programar en Basic. Empece con Linux a finales de los 90, y siempre he ido cambiando de distribuciones.
    - Vocal de [KDE España](https://www.kde-espana.org/junta-directiva), socio de GNU/Linux Valencia y podcaster en [Somos Tecnologic@s](https://www.ivoox.com/podcast-somos-tecnologicos_sq_f11062739_1.html).  
___
- __Brais Arias__: Es una persona comprometida con el software libre, socio de KDE España, ha participado y participa en otras comunidades, como por ejemplo [GPUL](https://gpul.org/).
    - También participa en [Mancomún Podcast](https://mancomun.gal/).  
___
- __David Marzal__: Administrador de sistemas GNU/Linux de profesión, apasionado por el [software libre](https://gitlab.com/Marzal) y la sostenibilidad [@ResiduoCero](https://t.me/ResiduoCero) y [@ResiduoCeroRM](https://t.me/ResiduoCeroRM) de vocación. Intentando poner mi granito de arena para tener un mundo y una sociedad mejor y más libre.
    - Mantengo un [Listado de Canales, Grupos, Bots y Recursos](https://gitlab.com/listados/awesome-telegram) de Telegram de todo tipo pero con un especial foco en el FLOSS y la sostenibilidad.
    - Colaborador adoptivo del Podcast [GNU/Linux Valencia](https://gnulinuxvalencia.org/).
    - Me podéis encontrar por [Mastodon](https://masto.es/@DavidMarzalC) o Telegram
___
{{% linksubssteno %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}
