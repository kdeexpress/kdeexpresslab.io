---
title: 'Vuelta de vacaciones cargado con novedades'
date: 2024-09-04T20:02:33+02:00
categories: [kdeexpress]
tags: [temporada4, flatpaks, Accesibilidad, Gears, Frameworks, Mapa, Neon, Calligra, Haruna]
author: "David Marzal"
img: 542px-Mascot_konqi-app-system.png
imgALT: "Konqi dentro de una rueda dentada, saluda con dos dedos en la frente"
podcast:
  guid : 33-2024-09-04-04-02-4aTemporada
  audio: 33-kde-express/33-KDE_Express
  ##audio: 33-KDE_Express
  olength : 17272634
  mlength : 14265979
  iduration : "00:14:51"
si :
  uri : "https://floss.social/@kde_espana/113080770894235164"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/33-KDE_Express.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/33-KDE_Express.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Jorge Lama
    role : Audio Editor
    group : Audio Post-Production
    img : https://files.mastodon.social/accounts/avatars/001/106/656/original/0c35a1ce0f0cec6b.jpg
    href : https://mastodon.social/@raivenra
itunes:
  explicit:   # por defecto false
comments: false
aliases:
  - /33
---
No paran los desarrolladores y voluntarios de la familia KDE ni en vacaciones. Pese a estar en periodo de playa o viajes (como ha hecho un servidor), siguen "trabajando" para tener un ecosistema cada día mejor y poder conquistar el mundo y vuestros corazones en algún momento.


Artículo original con los enlaces en <https://kdeexpress.gitlab.io/33/>
<!--more-->

* Tenemos un [mapa](https://www.kdeblog.com/actualizando-el-mapa-de-usuarios-kde-en-espana.html) con usuarios de KDE.
* [KDE Frameworks 6.5](https://9to5linux.com/kde-frameworks-6-5-released-with-plasma-wayland-and-dolphin-improvements) mejora la accesibilidad de la pantalla de configuración de los atajos de teclado.
* Mejora tu experiencia en KDE Plasma con [estos 15 widgets / plasmoides](https://itsfoss.com/kde-plasma-widgets/)
* [KDE Neon](https://blog.neon.kde.org/2024/08/27/kde-neon-rebase-progressing/) está casi listo para actualizarse con base en Ubuntu 24.04.
  * Y de paso os dejo [una guía y vídeo](https://soploslinux.com/guia-completa-de-kde-neon-2024/) sobre la instalación, configuración y personalización de esta distro.
* [Calligra](https://www.muylinux.com/2024/08/27/calligra-4-0-suite-ofimatica-kde/) resucita tras un largo letargo con su versión 4.0. Yo soy más de LO, pero oye que no sea por alternativas. Por cierto, ahora esta en formato Flatpak.
* [Haruna](https://haruna.kde.org/blog/2024-08-26-haruna-1.2.0/), reproductor del que nos habló Ivan hace poco, [se actualiza a la 1.2.0.](https://www.omgubuntu.co.uk/2024/08/qt-app-roundup-calligra-haruna-kdenlive)
* **KDE Gear** 24.08 nos trae [muchas novedades como](https://kde.org/es/announcements/gear/24.08.0/):
  * **Dolphin** permite mover a una nueva carpeta los archivos seleccionados o seleccionar todo con doble click en un hueco vacio.
  * **Filelight**, se puede instalar pulsando sobre el texto de espacio libre en la esquina inferior derecha de Dolphin y trae una pantalla de inicio renovada.
  * **Konsole** permite marcadores en la barra de desplazamiento.
  * [Kdenlive](https://kdenlive.org/en/2024/09/kdenlive-24-08-0-released/) ahora puede usar el nuevo editor de curvas de fotogramas clave para personalizar efectos y métodos de atenuación (entrada/salida cúbica y entrada/salida exponencial) para fundidos. 
  * **Tokodon** ahora puede avisar de los registros llevados a cabo en su servidor para una mejor gestión de usuarios entre otras mejoras.
  * **Kate** mejora el complemento de formateo de documentos con una mayor compatibilidad con archivos de bash, d, fish, configuración de Nix, opsi-script, QML y YAML. Como novedad relacionada, la funcionalidad del protocolo del servidor de lenguajes (LSP) añade compatibilidad con los lenguajes Gleam, PureScript y Typst.
  * **Okular**, el lector de documentos ecocertificado de KDE, mejora la compatibilidad con los formularios que se pueden rellenar en los documentos PDF
* Interesantisimo articulo y vídeo sobre [15 trucos para sacarle partido a Konsole](https://itsfoss.com/konsole-terminal-tweaks/). La mitad de las opciones no las conocía y alguna creo que la voy a utilizar bastante.
  * Abrir carpetas en Dolphin o ficheros (también activa las miniaturas en imágenes).
  * Comandos rápidos.
* Retrospectiva de mi tocayo Edmundson sobre las [métricas de Plasma](https://blog.davidedmundson.co.uk/blog/metrics-in-kde-are-they-useful/). Resumen: son útiles pero necesitan mejorar la infraestructura y los datos que se recogen. ¡Activarlas si queréis ayudar a mejorar el proyecto!
___
{{% linksubssteno %}}

{{< detalsum "Y transcripción completa al pinchar aquí" >}}
{{% includef "./static/subs/33-KDE_Express.txt" %}}
{{< /detalsum >}}
* Agradecimientos:
{{% jorgelama %}}
{{% Adrian %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}

