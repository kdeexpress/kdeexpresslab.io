---
title: "#13 - Car Edition + I love Free Software Day"
date: 2023-02-12T02:01:13+01:00
categories: [kdeexpress]
tags: [temporada2, Coche, ILoveFreeSoftware]
author: "David Marzal"
img: 542px-Mascot_konqi-app-system.png
imgALT: "Konqi dentro de una rueda dentada, saluda con dos dedos en la frente"
podcast:
  audio: 13-kde-express-car-edition/13-KDE_Express-Car_Edition
  ##audio: 13-KDE_Express-Car_Edition
  olength : 7115340
  mlength : 9255163
  iduration : "00:07:43"
si :
  uri : "https://floss.social/@kde_espana/110771151853208586"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/13-KDE_Express-Car_Edition.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/13-KDE_Express-Car_Edition.asr.srt"
    trtype : application/x-subrip
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Jorge Lama
    role : Audio Editor
    group : Audio Post-Production
    img : https://files.mastodon.social/accounts/avatars/001/106/656/original/0c35a1ce0f0cec6b.jpg
    href : https://mastodon.social/@raivenra
  - person : Frank Schroeter
    role : Music Contributor
    group : Audio Post-Production
    img : /images/person/erotic.avif
    href : https://filmmusic.io/song/10348-erotic
  - person : WinnieTheMoog
    role : Music Contributor
    group : Audio Post-Production
    img : /images/person/WinnieTheMoog.jpg
    href : https://filmmusic.io/song/5408-cinematic-emotional-inspiring
comments: false
aliases:
  - /13
---
Ante la falta de tiempo, pero con mucha ilusión con el proyecto de KDE, encontramos nuevas formas de conseguir haceros llegar información de nuestra comunidad. Inicialmente pensado para grabar andando al trabajo, pero finalmente de vuelta de la escuela de la peque, os dejamos un nuevo modelo de episodio "In itinere", con la inestimable ayuda de Jorge Lama, tanto como consejero "audífono", como nuevo editor y garante de un audio decente para vuestros oídos.

Artículo completo en <https://kdeexpress.gitlab.io/13/>
<!--more-->
* [Jorge Lama](https://mastodon.social/@raivenra)
* [I love Free Software Day](https://fsfe.org/activities/ilovefs/index.es.html)
* [Neodigit](https://neodigit.net/)
  * [Eduardo Collado](https://www.eduardocollado.com/)
* [Slimbook](https://linuxrocks.online/@slimbook) 
* [Junta Directiva KDE España](https://www.kde-espana.org/junta-directiva)
* [Brais Arias Rio ](https://mastodon.gal/@braisarias)
* [José Picon](https://twitter.com/_jmpicon)
* [Vosotros / comunidad](https://t.me/kde_canasbravas)
* Eventos
  * [Vídeos Akademy-es](https://tube.kockatoo.org/c/akademy_es/videos)
  * [esLibre 2023 en Zaragoza el 12 y 13 de Mayo (viernes y sábado)](https://eslibre.gitlab.io/2023/)
  * [OSC 2023 en Malaga el 9 y 10 de Junio (viernes y sábado)](https://www.opensouthcode.org/conferences/opensouthcode2023)
___
{{% linksubssteno %}}


* Agradecimientos:
{{% jorgelama %}}
{{% Adrian %}}
{{% m_cinematic %}}
{{% m_erotic %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}