---
title: "#11 - Puesta al día"
date: 2022-06-21T01:11:11+02:00
categories: [kdeexpress]
tags: [temporada1, novedades]
author: "David Marzal"
img: 495px-Mascot_konqi-support-search.png
imgALT: "Konqi subido a un archivador buscando documentos"
podcast:
  ##audio: "11-kde-express-puesta-al-dia/11 - KDE-Express - Puesta al dia"
  audio: "11-kde-express-puesta-al-dia/11%20-%20KDE-Express%20-%20Puesta%20al%20dia"
  olength : 5699923
  mlength : 7547042
  iduration : "00:09:37"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/11-KDE-Express-Puesta_al_dia.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/11-KDE-Express-Puesta_al_dia.asr.srt"
    trtype : application/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
comments: false
aliases:
  - /11
---
Primer episodio de 2022 que ya tocaba...

Ha pasado tanto tiempo que os ofrecemos un resumen de actualizaciones de los proyectos KDE con un montón de enlaces en las notas del programa.

Artículo original en <https://kdeexpress.gitlab.io/11/>
<!--more-->
* [La colección de parches KDE ahora se basa en Qt 5.15.5](https://tsdgeos.blogspot.com/2022/06/the-kde-qt5-patch-collection-has-been.html)
  * Con un toque de humor Albert, le da las gracias a los usuarios de la versión comercial por hacer de beta-tester.
* [KDE Plasma 5.25](https://kde.org/es/announcements/plasma/5/5.25.0/) - [KDEblog](https://www.kdeblog.com/lanzado-plasma-5-25-con-nuevos-conceptos.html)
  * Gestos y modo tactil, Colores de acentuación y personalización (paneles flotantes)
  * Posibilidad de moverse con el teclado por más elementos
  * Meta + W = Vista general
  * La Steam Deck aún está en 5.23!!
* [Frameworks 5.95.0](https://kde.org/announcements/frameworks/5/5.95.0/)  
* [KDE Gear 22.04.2](https://kde.org/announcements/gear/22.04.2/) - [KDEblog](https://www.kdeblog.com/segunda-actualizacion-de-kde-gear-22-04.html)
  * **Dolphin** más vistas previas/miniaturas e información, mejora de la conexión por MTP
  * **Konsole** implementa fragmentos de código y se mejora el complemento SSH
  * **Ark** renueva el soporte de ficheros 7zip
  * **Kalendar** se une a la familia Gear al ya estar suficiente maduro.
* [Kdenlive 22.04.2](https://kdenlive.org/en/2022/06/kdenlive-22-04-2-released/)
  * Implementadas las secciones, disponible en macOS con M1
* [Iniciativa: Bugs de 15 minutos](https://pointieststick.com/2022/06/17/this-week-in-kde-non-blurry-xwayland-apps/) - [Listado](https://tinyurl.com/plasma-15-minute-bugs)
* [Akademy será en Barcelona entre el 1 y el 7 de Octubre](https://akademy.kde.org/2022)
* [Akademy-ES se está organizando y pronto tendrá fecha](https://www.kde-espana.org/)
* [KDE Eco](https://eco.kde.org/es/) - [Vídeo presentación](https://tube.kockatoo.org/w/qd8djJo2Xb2ZpKz1i3jXiR) - [KDE Blog](https://www.kdeblog.com/season-of-kde-2022-y-kde-eco.html)
  * [Okular es el primer programa galardonado con la ecocertificación del gobierno aleman](https://eco.kde.org/es/blog/2022-03-16-press-release-okular-blue-angel/)
* Tiene que estar horneandose KDE Neon basada en Ubuntu 22.04
___
{{% linksubssteno %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}