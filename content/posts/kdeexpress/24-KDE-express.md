---
title: '#24 Grandes eventos se aproximan'
date: 2024-02-19T03:08:24+01:00
categories: [kdeexpress]
tags: [temporada3, eventos, akademy-es, esLibre, 24H24L]
author: "David Marzal"
img: 500px-Mascot_konqi-commu-mail.png
imgALT: "Konqi con muchos sobres de correo electronico"
podcast:
  guid :
  audio: 24-kde-express/24-KDE_Express
  ##audio: 24-KDE_Express
  olength : 8584261
  mlength : 7874379
  iduration : "00:10:56"
si :
  uri : "https://floss.social/@kde_espana/111958635377194454"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/24-KDE_Express-Eventos.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/24-KDE_Express-Eventos.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Jorge Lama
    role : Audio Editor
    group : Audio Post-Production
    img : https://files.mastodon.social/accounts/avatars/001/106/656/original/0c35a1ce0f0cec6b.jpg
    href : https://mastodon.social/@raivenra
itunes:
  explicit:   # por defecto false
comments: false
aliases:
  - /24
---
Quedan menos de 2 semanas para Plasma6, pero tenemos más eventos ya que se asoman. Hoy tenemos: 24H24L, esLibre, Akademy-es, KaOS, KDE Frameworks, el futuro de las Actividades de Plasma y articulo de opinión sobre GNOME Vs Plasma.

Artículo original con los enlaces en <https://kdeexpress.gitlab.io/24/>
<!--more-->

24H24L este año se publica como podcast tradicional, con charlas sueltas según se graban:
https://podcastindex.org/podcast/3076984

Ya puedes presentar tu charla a Akademy-es 2024, que estará dentro de esLibre Valencia:
https://www.kdeblog.com/presenta-tu-charla-a-akademy-es-2024-de-valencia-eslibre-edition-akademyes.html

KaOS 2024.01 ya lleva Plasma6!!
https://www.zdnet.com/article/the-first-linux-distribution-to-deliver-a-pure-kde-plasma-6-environment-is-here/

Y Kubuntu 24.04 no, se queda en 5.27 LTS:
https://www.omgubuntu.co.uk/2024/02/kubuntu-24-04-wont-use-kde-plasma-6

Ha salido KDE Frameworks 5.115:
https://kde.org/es/announcements/frameworks/5/5.115.0/
https://ubunlog.com/kde-realiza-varias-correcciones-de-errores-y-mejoras-en-la-interfaz-con-la-vista-puesta-en-su-mega-lanzamiento/

Las actividades de Plasma siguen por ahora en Plasma 6, pero...
https://www.linuxadictos.com/kde-se-plantea-eliminar-las-actividades-en-plasma-6-x-que-son-y-como-nos-afectaria.html

Comparar GNOME y Plasma no tiene sentido:
https://www.muylinux.com/2024/02/07/comparar-gnome-y-kde-plasma/

I Love Free Software Day : Gracias Juan, por tu tiempo y tu persona.
___
{{% linksubssteno %}}

{{< detalsum "Y transcripción completa al pinchar aquí" >}}
{{% includef "./static/subs/24-KDE_Express-Eventos-Transcripcion.txt" %}}
{{< /detalsum >}}
* Agradecimientos:
{{% jorgelama %}}
{{% Adrian %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}

