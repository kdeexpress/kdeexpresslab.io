---
title: 'Blue Angel, Tellico, Plasma 6.2, Donaciones, Slimbook, Agricultura'
date: 2024-09-19T15:03:34+02:00
categories: [kdeexpress]
tags: [temporada4, Blue Angel, Accesibilidad, Gears, Tellico, Plasma, Donaciones, Slimbook, agricultura]
author: "David Marzal"
img: 542px-Mascot_konqi-app-system.png
imgALT: "Konqi dentro de una rueda dentada, saluda con dos dedos en la frente"
podcast:
  guid : 34-2024-09-17-04-03-4aTemporada
  audio: 34-kde-express/34-KDE_Express
  ##audio: 34-KDE_Express
  olength : 11624104
  mlength : 10647955
  iduration : "00:11:05"
si :
  uri : "https://floss.social/@kde_espana/113164364988363971"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/34-KDE_Express.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/34-KDE_Express.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Jorge Lama
    role : Audio Editor
    group : Audio Post-Production
    img : https://files.mastodon.social/accounts/avatars/001/106/656/original/0c35a1ce0f0cec6b.jpg
    href : https://mastodon.social/@raivenra
itunes:
  explicit:   # por defecto false
comments: false
aliases:
  - /34
---
Buenas, el anterior venia cargado de novedades, pero este no se queda corto, aunque hoy hablamos menos del presente y más de un futuro muy cercano.

Artículo original con los enlaces en <https://kdeexpress.gitlab.io/34/>
<!--more-->
* Actualizada la certificación [Blue Angel](https://eco.kde.org/blog/2024-08-26-revised-blue-angel-criteria/) con más categorias y opciones más flexibles de medir la eficiencia energetica. Okular fue el primer programa que consiguio la etiqueta y en KDE Eco siguen trabajando para medir y certificar más programas.
* [Tellico 4.0](https://tellico-project.org/tellico-4-0-released/) disponible portado a Qt6. Para quien no lo conozca es una aplicación para gestionar colecciones capaz de obtener un monton de metadata sobre muchas categorias a organizar.
* Presentado el [Slimbook KDE VI](https://slimbook.com/kde), más, mejor, pero con el mismo compromiso con el proyecto KDE. Y ademas ahora Slimbook OS se puede [elegir en versión](https://slimbook.com/blog/noticias-1/post/slimbook-os-24-ya-esta-aqui-422) KDE Plasma. [MuyLinux](https://www.linuxadictos.com/slimbook-os-24.html), [LinuxAdictos](https://www.linuxadictos.com/slimbook-os-24.html).
* Para los que recordeis el efecto de accesibilidad que comente episodios atras, sobre mover el ratón y que crezca el puntero, por lo que he visto solo está disponible en Wayland. Que hay quien lo quiere quitar y quien lo quiere poner y no lo encontraba.
* [Algunas de las mejoras](https://pointieststick.com/2024/08/16/this-week-in-kde-system-settings-modernization-and-wayland-color-management/) a esperar en octubre con [Plasma 6.2](https://ubunlog.com/kde-6-2-esto-es-lo-que-se-tiene-preparado-hasta-el-momento/), ya que tenemos cerrado el plazo para añadir nueva funcionalidad al desarrollo.
  * Se permite [renombrar](https://invent.kde.org/plasma/plasma-pa/-/merge_requests/271) los dispositivos de audio y elegir que fuente de datos le da nombre.
  * El plasmoide del tiempo ahora mostrará la temperatura "se siente como".
  * Rediseño de la pantalla del teclado en la configuración.
  * [El plasmoide del brillo](https://pointieststick.com/2024/08/23/this-week-in-kde-per-monitor-brightness-control-and-update-then-shut-down/) permitirá configurar individualmente cada monitor.
  * Nueva opción para actualizar y apagar el equipo.
  * Nuevo icono sobre el perfil de energía para mostrar en un solo icono el perfil y la batería.
  * Discover mostrará la diferencias entre licencias propietarias y no-libres.
  * Los nuevos plasmoides apareceran arriba del menu de selección.
  * Las [opciones de accesibilidad](https://www.kdeblog.com/mejoras-en-la-accesibilidad-en-plasma-6-2.html) para problemas visuales de color, estará ahora en Accesibilidad, en vez de con el resto de efectos de escritorio. Y está pagina de configuración, ha sido mejorada en varios aspecto para hacerla más accesible.
  * Nuevo [recordatorio](https://pointieststick.com/2024/08/28/asking-for-donations-in-plasma/) de la posibilidad de donar al proyecto, será 1 vez al año, opcional y por supuesto se puede eliminar.
* Y en Gear 24.12
  * Okular permitira "hablar" el texto de la página mostrada
  * Las aplicaciones podrán bloquear que el equipo se ponga en modo reposo o se bloqué.
El número de bugs está en los niveles minimos desde 2015 desde que se ha hecho un esfuerzo en el QA.

* Articulo sobre si [KDE abarca demasiado](https://www.linuxadictos.com/soy-usuario-feliz-de-kde-desde-hace-anos-pero-a-veces-me-pregunto-por-que-pierden-el-tiempo-en-proyectos-innecesarios.html
)
* Campaña contra el [recorte de fondos](https://fsfe.org/news/2024/news-20240911-02.html) a proyectos de SL por la UE
* Articulo muy interesante sobre el [HW/SL y la agricultura](https://www.hwlibre.com/agricultura-2-0-nuevas-tecnologias-para-la-agricultura/)

¿Algún voluntario para ayudar a montar los vídeos de la Akademy-es de Valencia?

PD: El viernes 4 de octubre estaremos en directo en "[Las Charlas de 24H24L](https://t.me/evento24h24l/628)"
___
{{% linksubssteno %}}

{{< detalsum "Y transcripción completa al pinchar aquí" >}}
{{% includef "./static/subs/34-KDE_Express.txt" %}}
{{< /detalsum >}}
* Agradecimientos:
{{% jorgelama %}}
{{% Adrian %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}

