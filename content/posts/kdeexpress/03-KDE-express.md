---
title: "#03 - K: Juan Febles"
date: 2021-08-14T01:03:03+02:00
categories: [kdeexpress]
tags: [temporada1, comunidad, Juan Febles, kdenlive]
author: "David Marzal"
img: 600px-Mascot_20140702_konqui-group.png
imgALT: "Konqi junto al resto de sus compis de diferentes colores haciendo una piramide entre ellos"
podcast:
  audio: 03-kde-express-juan-febles/03-KDE_Express-Juan_Febles
  ##audio: 03-KDE_Express-Juan_Febles
  olength : 12135221
  mlength : 13714828
  iduration : "00:19:02"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/03-KDE_Express-Juan_Febles.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/03-KDE_Express-Juan_Febles.asr.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Juan Febles
    role : guest
    group : Cast
    img : https://podcastlinux.gitlab.io/images/avatar.jpg
    href : https://podcastlinux.gitlab.io/
comments: false
aliases:
  - /03
  - /3
---
Inauguramos un nuevo formato de episodio! La __Komunidad KDE__. Y no podíamos traeros a otra persona que no fuera Juan Febles de Podcast Linux y [Linux Express](https://podcastlinux.gitlab.io/), siendo este último una de las inspiraciones para crear el podcast que estaís escuchando.

En este episodio Juan nos cuenta como empezó con KDE, que cosas le tienen conquistado y que mejoraría, entre otras tantas cosas.

Artículo original en <https://kdeexpress.gitlab.io/03/>
<!--more-->

* Para colaborar en la traducción de la [web en español](https://kdenlive.org/es/) de KDenlive hay varias maneras, lo principal es traducir el texto y mandarlo junto con el enlace del artículo en inglés a algunas de estas opciones:
  * [Grupo de Telegram internacional](https://t.me/kdenlive), frdbr o Merlimau99 se encargarán de crear los articulos correspondientes en la versión española.
  * Lista de distribución kdenlive@kde.org, aquí teneís algo de info sobre la lista <https://community.kde.org/Kdenlive#Mailing_List>
  * Como comentario en la publicación de Telegram de este episodio.
  * A nuestro correo <kde_express@kde-espana.org>. Y ya nos encargamos nostros de hacersela llegar.
___
{{% linksubssteno %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}