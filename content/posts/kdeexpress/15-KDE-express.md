---
title: "#15 - Plasma 5.27"
date: 2023-03-09T02:03:15+01:00
categories: [kdeexpress]
tags: [temporada2, Plasma]
author: "David Marzal"
img: 495px-Mascot_konqi-support-search.png
imgALT: "Konqi subido a un archivador buscando documentos"
podcast:
  audio: 15-kde-express-plasma-5.27/15-KDE_Express-Plasma_5.27
  ##audio: 15-KDE_Express-Plasma_5.27
  olength : 16920715
  mlength : 19690444
  iduration : "00:27:21"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/15-KDE_Express-Plasma_5.27.vtt"
    trtype : text/vtt
#  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/15-KDE_Express-Plasma_5.27.asr.srt"
#    trtype : application/x-subrip
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Baltasar
    role : Co-Host
    group : Cast
    img : https://files.mastodon.social/accounts/avatars/000/924/299/original/e0a2b2e0270a6a27.png
    href : https://mastodon.social/@baltollkien
  - person : Jorge Lama
    role : Audio Editor
    group : Audio Post-Production
    img : https://files.mastodon.social/accounts/avatars/001/106/656/original/0c35a1ce0f0cec6b.jpg
    href : https://mastodon.social/@raivenra
  - person : Frank Schroeter
    role : Music Contributor
    group : Audio Post-Production
    img : /images/person/erotic.avif
    href : https://filmmusic.io/song/10348-erotic
aliases:
  - /15
---
En el anterior episodio nos pusimos al día, pero nos quedaba la guinda del pastel (del día de los enamorados). [KDE Plasma 5.27](https://kde.org/es/announcements/plasma/5/5.27.0/) es la última versión de la rama Qt 5 y será una para recordar por su estabilidad, su [madurez en Wayland](https://www.muylinux.com/2023/03/03/ahora-si-wayland-en-kde-plasma/) y sus mejoras que pasamos a resumir en el podcast.

Os recomendamos [esta](https://victorhckinthefreeworld.com/2023/03/07/entrevista-exclusiva-con-el-creador-del-fondo-de-plasma-5-27-de-kde/) genial entrevista con el creador del fondo de Plasma 5.27 de KDE.

Artículo original en <https://kdeexpress.gitlab.io/15/>
<!--more-->
Y no nos olvidamos de recordar que [Akademy-es 2023 se celebrará en Málaga](https://www.kde-espana.org/akademy-es-2023-se-celebrara-en-malaga) el 9 y 10 de junio dentro de [Open South Code](https://www.opensouthcode.org/conferences/opensouthcode2023)!!!
___
{{% linksubssteno %}}


* Agradecimientos:
{{% jorgelama %}}
{{% Adrian %}}
{{% m_erotic %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}