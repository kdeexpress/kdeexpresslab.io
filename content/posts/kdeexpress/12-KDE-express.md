---
title: "#12 - Reencuentro fin de curso"
date: 2022-07-06T01:12:12+02:00
categories: [kdeexpress]
tags: [temporada1, Akademy, Slimbook, Salmorejo]
author: "David Marzal"
img: 600px-Mascot_20140702_konqui-group.png
imgALT: "Konqi junto al resto de sus compis de diferentes colores haciendo una piramide entre ellos"
podcast:
  audio: 12-reencuentro-fin-de-curso/12-Reencuentro_fin_de_curso
  ##audio: 12-Reencuentro_fin_de_curso
  olength : 15685321
  mlength : 18208937
  iduration : "00:25:17"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/12-Reencuentro_fin_de_curso.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/12-Reencuentro_fin_de_curso.asr.srt"
    trtype : application/srt
participantes :
  - person : Jose Picon
    role : Host
    group : Cast
    img : https://mpipeunstor.blob.core.windows.net/mpiuseravatars/86a37988-8272-4370-bc77-2c16d9ede0e5.jpg
    href : https://es.linkedin.com/in/jos%C3%A9-pic%C3%B3n
  - person : David Marzal
    role : Co-Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Brais Arias
    role : Co-Host
    group : Cast
    img : https://static.mastodon.gal/accounts/avatars/108/333/163/725/284/233/original/8a8b2905c9aed914.png
    href : https://mastodon.gal/@braisarias
comments: false
aliases:
  - /12
---
Nos ha llevado tiempo, pero volvemos a juntarnos los tres integrantes principales del podcast.
Tenemos noticias, novedades y salseo.

Artículo original en <https://kdeexpress.gitlab.io/12/>
<!--more-->
* [KDE Plasma 5.25.2, lanzamiento de corrección de errores de junio](https://kde.org/es/announcements/plasma/5/5.25.2/)
    * [KDE Plasma 5.25.2 Released to Improve Flatpak Support, Present Windows Effect](https://9to5linux.com/kde-plasma-5-25-2-released-to-improve-flatpak-support-present-windows-effect)
* [Plasma Mobile Gear ⚙️ 22.06 is Out](https://plasma-mobile.org/2022/06/28/plasma-mobile-gear-22-06/)
* [Kdenlive 22.04.2](https://www.kdeblog.com/lanzado-kdenlive-22-04-2-puliendo-el-editor-de-video-de-kde.html)
* [DigiKam 7.7](https://www.kdeblog.com/lanzado-digikam-7-7-mejorando-los-paquetes-multisistemas.html)
* [WINE: Theming support for Qt5 applications.](https://www.phoronix.com/scan.php?page=news_item&px=Wine-7.12-Released)
* [Akademy será en Barcelona entre el 1 y el 7 de Octubre](https://akademy.kde.org/2022)
* [Akademy-ES 29 y 30 de septiembre](https://www.kde-espana.org/akademy-es-2022-se-realizara-en-barcelona-y-en-linea)
* [Podcast KDE España 7.5x01 - KDE Eco](https://tube.kockatoo.org/w/3DaFUmWxtWQLUk5ipKuxL2)
* [Slimbook KDE IV a la venta!](https://slimbook.es/noticias-notas-de-prensa-y-reviews/535-nueva-generacion-pro-x-y-kde-slimbook) - [KDE](https://kde.slimbook.es/) - Proximamente entrevista ;)
___
{{% linksubssteno %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}