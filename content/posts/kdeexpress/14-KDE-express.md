---
title: "#14 - Resumen final 2022"
date: 2023-03-01T02:02:14+01:00
categories: [kdeexpress]
tags: [temporada2, 2022, Recap, AkademyES]
author: "David Marzal"
img: 600px-Mascot_20140702_konqui-group.png
imgALT: "Konqi junto al resto de sus compis de diferentes colores haciendo una piramide entre ellos"
podcast:
  audio: 14-kde-express-2022-recap/14-KDE_Express-2022_Recap
  ##audio: 14-KDE_Express-2022_Recap
  olength : 16683862
  mlength : 20344204
  iduration : "00:28:15"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/14-KDE_Express-2022_Recap.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/14-KDE_Express-2022_Recap.asr.srt"
    trtype : application/x-subrip
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Baltasar
    role : Co-Host
    group : Cast
    img : https://files.mastodon.social/accounts/avatars/000/924/299/original/e0a2b2e0270a6a27.png
    href : https://mastodon.social/@baltollkien
  - person : Brais Arias
    role : Co-Host
    group : Cast
    img : https://static.mastodon.gal/accounts/avatars/108/333/163/725/284/233/original/8a8b2905c9aed914.png
    href : https://mastodon.gal/@braisarias
  - person : Jorge Lama
    role : Audio Editor
    group : Audio Post-Production
    img : https://files.mastodon.social/accounts/avatars/001/106/656/original/0c35a1ce0f0cec6b.jpg
    href : https://mastodon.social/@raivenra
  - person : Frank Schroeter
    role : Music Contributor
    group : Audio Post-Production
    img : /images/person/erotic.avif
    href : https://filmmusic.io/song/10348-erotic
comments: false
aliases:
  - /14
---
Antes de contaros todas las novedades recientes, queremos hacer un repaso a algunas de las más destacables (en nuestro día a día) de lo que ha pasado o actualizado en la Comunidad KDE desde el último episodio en grupo. Y traemos notición sobre la AkademyES.

Artículo completo en <https://kdeexpress.gitlab.io/14/>
<!--more-->
* [Nuevos goals](https://kde.org/goals/), de Wayland, Apps y Continuidad a -> Automatizar, Accesibilidad, Medioambiente.
  * [Wiki](https://community.kde.org/Goals)
* [Plasma 5.26](https://kde.org/es/announcements/plasma/5/5.26.0/)
  * Plasma ahora tiene más opciones de personalización con la selección automática de color de acento en función del fondo de pantalla.
  * Nuevos y mejores touch screen gestures.
  * Overview con opciones de búsquedas.
  * Panel flotante.
  * Cambiar el tamaño de los plasmoides como el calendario del reloj.
* Apps
  * KDE Gears [22.12](https://kde.org/es/announcements/gear/22.12.0/) y [22.08](https://kde.org/es/announcements/gear/22.08.0/)
  * Kate y Kwrite son ahora la misma app con diferente layout y ahora tiene una ventana de bienvenida
  * Spectacle
  * Neochat sigue implementando funciones de cifrado (cliente de Matrix )
* [Akademy-es 2023 se celebrará en Málaga](https://www.kde-espana.org/akademy-es-2023-se-celebrara-en-malaga) el 9 y 10 de junio dentro de [Open South Code](https://www.opensouthcode.org/conferences/opensouthcode2023)!!!
___
{{% linksubssteno %}}


* Agradecimientos:
{{% jorgelama %}}
{{% Adrian %}}
{{% m_erotic %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}