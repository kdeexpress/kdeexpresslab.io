---
title: 'Programa Akademy-es Valencia 2024'
date: 2024-05-09T03:11:27+02:00
categories: [kdeexpress]
tags: [temporada3, akademy-es, esLibre]
author: "David Marzal"
img: 626px-Mascot_konqi-commu-akademy.png
imgALT: "Konqi apuntando a una pizarra que pone Akademy"
podcast:
  guid : 27-2024-04-27-03-11-Akademyes
  audio: 27-kde-express/27-KDE-Express_Akademy-es_2024_Valencia
  ##audio: 27-KDE-Express_Akademy-es_2024_Valencia
  olength : 21433139
  mlength : 22568826
  iduration : "00:23:30"
si :
  uri : "https://floss.social/@kde_espana/112409650421937986"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/27-KDE-Express_Akademy-es_2024_Valencia.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/27-KDE-Express_Akademy-es_2024_Valencia.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Baltasar
    role : Co-Host
    group : Cast
    img : https://files.mastodon.social/accounts/avatars/000/924/299/original/e0a2b2e0270a6a27.png
    href : https://mastodon.social/@baltollkien
  - person : Jorge Lama
    role : Audio Editor
    group : Audio Post-Production
    img : https://files.mastodon.social/accounts/avatars/001/106/656/original/0c35a1ce0f0cec6b.jpg
    href : https://mastodon.social/@raivenra
location:
  - text : "Las Naves, Valencia"
    GEO : "39.46,-0.34"
    OSM : "R10843717"
    rel: subject
    country: es
itunes:
  explicit:   # por defecto false
comments: false
aliases:
  - /27
---
Episodio monografico sobre el programa de charlas y actividades de Akademy-es 2024 Valencia.

Os detallamos lo que podeís encontrar en <https://www.kde-espana.org/programa-akademy-es-2024> y recordamos que teneis varios métodos de contacto para estar informados en tiempo real los días del evento en <https://kdeexpress.gitlab.io/contacto/>

¡Nos vemos en la Akademy-es Valencia del 24 al 26 de mayo!

Gracias por el apoyo a [openSUSE](https://www.opensuse.org/), [La Universidad de La Laguna](https://www.ull.es/) y a todos los que vengáis o compartáis.

Artículo original en <https://kdeexpress.gitlab.io/27/>
___
{{% linksubssteno %}}

{{< detalsum "Y transcripción completa al pinchar aquí" >}}
{{% includef "./static/subs/27-KDE-Express_Akademy-es_2024_Valencia.txt" %}}
{{< /detalsum >}}
* Agradecimientos:
{{% jorgelama %}}
{{% Adrian %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}

