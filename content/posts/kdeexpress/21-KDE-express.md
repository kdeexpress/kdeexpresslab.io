---
title: '#21 F-Droid y Plasma-Wayland'
date: 2023-11-21T03:05:21+02:00
categories: [kdeexpress]
tags: [temporada3, Plasma6, F-Droid, Wayland]
author: "David Marzal"
img: 542px-Mascot_konqi-app-system.png
imgALT: "Konqi dentro de una rueda dentada, saluda con dos dedos en la frente"
podcast:
  guid :
  audio: 21-kde-express/21-KDE_Express-F-droid_y_PlasmaWayland
  ##audio: 21-KDE_Express-F-droid_y_PlasmaWayland
  olength : 4011529
  mlength : 4662028
  iduration : "00:07:14"
si :
  uri : "https://floss.social/@kde_espana/111449899146828423"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/21-KDE_Express-F-droid_y_PlasmaWayland.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/21-KDE_Express-F-droid_y_PlasmaWayland.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Jorge Lama
    role : Audio Editor
    group : Audio Post-Production
    img : https://files.mastodon.social/accounts/avatars/001/106/656/original/0c35a1ce0f0cec6b.jpg
    href : https://mastodon.social/@raivenra
  - person : Frank Schroeter
    role : Music Contributor
    group : Audio Post-Production
    img : /images/person/erotic.avif
    href : https://filmmusic.io/song/10348-erotic
itunes:
  explicit:   # por defecto false
comments: false
aliases:
  - /21
---
Mejoras a la hora de obtener las aplicaciones de KDE para Android gracias a un repo estable de F-Droid y qué podeis esperar de X11/Wayland en Plasma6.

Un abrazo muy grande para Juan Febles.

Artículo original en <https://kdeexpress.gitlab.io/21/>
<!--more-->
* F-Droid
  * https://community.kde.org/Android/F-Droid
* Juan Febles
  * https://www.kdeblog.com/podcast-linux-cierra-sus-emisiones-y-yo-no-puedo-estar-mas-triste.html
* Wayland
  * https://www.kdeblog.com/akademy-2023-plasma-6-is-coming.html
  * https://www.gamingonlinux.com/2023/11/kde-plasma-6-goes-wayland-by-default-initial-hdr-gaming-support/
  * https://tuxcare.com/es/blog/kde-plasma-6-with-five-new-default-settings/
  * https://pointieststick.com/2023/11/10/this-week-in-kde-wayland-by-default-de-framed-breeze-hdr-games-rectangle-screen-recording/
  * https://victorhckinthefreeworld.com/2023/09/19/wayland-vs-x11-en-linux/
___
{{% linksubssteno %}}

{{< detalsum "Y transcripción completa al pinchar aquí" >}}
{{% includef "./static/subs/21-KDE_Express-F-droid_y_PlasmaWayland.txt" %}}
{{< /detalsum >}}
* Agradecimientos:
{{% jorgelama %}}
{{% Adrian %}}
{{% m_erotic %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}