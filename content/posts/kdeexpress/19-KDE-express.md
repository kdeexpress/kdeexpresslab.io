---
title: '#19 ¿Conoces "KDE es para..."?'
date: 2023-08-19T03:03:19+02:00
categories: [kdeexpress]
tags: [temporada3, ParaTi, Promo]
author: "David Marzal"
img: 625px-Mascot_konqi-app-presentation.png
imgALT: "Konqi apuntando a una pizarra que pone E=MC²"
podcast:
  guid : '#19 Conoces KDE para'
  audio: 19-kde-express-parati/19-KDE_Express-Parati
  ##audio: 19-KDE_Express-Parati
  olength : 17525120
  mlength : 21991290
  iduration : "00:30:32"
si :
  uri : "https://floss.social/@kde_espana/110915781588231822"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/19-KDE_Express-Parati.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/19-KDE_Express-Parati.asr.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Baltasar
    role : Co-Host
    group : Cast
    img : https://files.mastodon.social/accounts/avatars/000/924/299/original/e0a2b2e0270a6a27.png
    href : https://mastodon.social/@baltollkien
  - person : Jorge Lama
    role : Audio Editor
    group : Audio Post-Production
    img : https://files.mastodon.social/accounts/avatars/001/106/656/original/0c35a1ce0f0cec6b.jpg
    href : https://mastodon.social/@raivenra
  - person : Frank Schroeter
    role : Music Contributor
    group : Audio Post-Production
    img : /images/person/erotic.avif
    href : https://filmmusic.io/song/10348-erotic
itunes:
  explicit:   # por defecto false
comments: false
aliases:
  - /19
---
El proyecto KDE es un paraguas para una gran cantidad de aplicaciones y proyectos, por lo que el equipo de Promo se esfuerza en dar a conocer las bondades de esta cantidad de recursos.

Una manera de conocer aplicaciones enfocadas a determinados sectores es <https://kde.org/es/for/> y hoy Baltasar nos propone darle un repaso a todos los apartados de la web para que descubráis alguna aplicación nueva.

Artículo original en <https://kdeexpress.gitlab.io/19/>
___
{{% linksubssteno %}}

<!--more-->
{{< detalsum "Y transcripción completa al pinchar aquí" >}}
{{% includef "./static/subs/19-KDE_Express-Parati.txt" %}}
{{< /detalsum >}}
* Agradecimientos:
{{% jorgelama %}}
{{% Adrian %}}
{{% m_erotic %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}