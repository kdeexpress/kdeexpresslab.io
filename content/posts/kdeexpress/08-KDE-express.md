---
title: "#08 - Komunidad: Ignacio"
date: 2021-10-15T01:08:08+01:00
categories: [kdeexpress]
tags: [temporada1, Comunidad, Bravas, Ignacio]
author: "David Marzal"
img: 649px-Mascot_konqi-app-utilities.png
imgALT: "Konqi junto a una caja de herramientas con una llave inglesa en la mano"
podcast:
  audio: 08-kde-k-ignacio/08_KDE_K-Ignacio
  ##audio: 08_KDE_K-Ignacio
  olength : 12450799
  mlength : 19296035
  iduration : "00:26:47"
transcript :
  - trurl : "https://kdeexpress.gitlab.io/subs/08_KDE_K-Ignacio.vtt"
    trtype : text/vtt
  - trurl : "https://kdeexpress.gitlab.io/subs/08_KDE_K-Ignacio.asr.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Ignacio Serantes Iglesias
    role : guest
    group : Cast
    img : https://images.pling.com/img/00/00/00/38/98/eusonig.gif
    href : https://store.kde.org/u/eusonig
comments: false
aliases:
  - /08
  - /8
---
Seguimos con __Komunidad KDE__, cambiando esta vez a un compañero de [KDE - Cañas y Bravas](https://t.me/kde_canasbravas) y socio de KDE España, Ignacio Serantes Iglesias.
Como es habitual hacemos un recorrido histórico por su experiencia con Linux en general y con algunas vivencia de KDE en particular.

Artículo original en <https://kdeexpress.gitlab.io/08/>
<!--more-->
Algunas de las cosas mencionadas:
  * https://es.wikipedia.org/wiki/NEPOMUK
    * https://community.kde.org/Baloo
  * https://www.opensuse.org/
    * https://es.wikipedia.org/wiki/YaST
    * https://openbuildservice.org/

PD: Feliz [25 aniversario](https://25years.kde.org/es/) de KDE!
___
{{% linksubssteno %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}