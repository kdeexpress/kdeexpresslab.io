---
title: "#04 - Komunidad: Walter"
date: 2021-08-28T01:04:04+02:00
categories: [kdeexpress]
tags: [temporada1, comunidad, Walter, HSL]
author: "David Marzal"
img: 600px-Mascot_20140702_konqui-group.png
imgALT: "Konqi junto al resto de sus compis de diferentes colores haciendo una piramide entre ellos"
podcast:
  audio: 04-kde-express-walter/04_KDE_Express_Walter
  ##audio: 04_KDE_Express_Walter
  olength : 17448809
  mlength : 19461258
  iduration : "00:27:01"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/04_KDE_Express_Walter.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/04_KDE_Express_Walter.asr.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Walter (HSL)
    role : guest
    group : Cast
comments: false
aliases:
  - /04
  - /4
---
Continuamos con el formato __Komunidad KDE__, en esta ocasión con un compañero de [Home Studio Libre](https://t.me/HomeStudioLibre), Walter.
Que nos contara sus comienzos en GNU/Linux y en KDE, su camino hasta llegar a esta comunidad, las aplicaciones que más utiliza y algunas de las cosas que más le gustan de Plasma.

Artículo original en <https://kdeexpress.gitlab.io/04/>
<!--more-->

Algunos de los proyectos mencionados:
* [Puppy Linux](https://es.wikipedia.org/wiki/Puppy_Linux) - Una _minidistro_ para equipos con muy muy pocos recursos.
* [INTER Y NET PODCAST](https://open.spotify.com/show/4ofWeCvtkdNomrpaQgl7EB?si=RDom&nd=1)

PD: Un saludo a Fran y a todo HSL ;)
___
{{% linksubssteno %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}