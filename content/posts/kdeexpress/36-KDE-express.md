---
title: 'Las distros KDE están on fire'
date: 2024-11-19T15:04:36+01:00
categories: [kdeexpress]
tags: [temporada4, Fedora, Steam Deck, Inmutables, KDE Wave, Plasma 6.3]
author: "David Marzal"
img: 36/StemDeck_2024-11-14.jpg
imgALT: "Centro de información de Plasma mostrando las versiones de los componentes"
podcast:
  guid : 36-2024-11-18-04-05-4aTemporada-KDE_express
  audio: 36-kde-express/36-KDE_Express
  ##audio: 36-KDE_Express
  olength : 10515711
  mlength : 13657327
  iduration : "00:14:13"
si :
  uri : "https://floss.social/@kde_espana/113510209428225580"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/36-KDE_Express.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/36-KDE_Express.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Jorge Lama
    role : Audio Editor
    group : Audio Post-Production
    img : https://files.mastodon.social/accounts/avatars/001/106/656/original/0c35a1ce0f0cec6b.jpg
    href : https://mastodon.social/@raivenra
itunes:
  explicit:   # por defecto false
comments: false
aliases:
  - /36
---
Hacer copia de seguridad de todo, incluido de vuestros marcadores, listas y cuentas que seguis de Mastodon. Este episdio ha tardado más y tiene menos noticias por no hacer esas copias más regularmente. Y si os seguía y no teneis una petición, pegarme un toque a https://masto.es/@DavidMarzalC, que hice todo lo posible por buscar todas las cuentas que estaba siguiendo. Dicho lo cual, empezamos con las noticias KDE.

Artículo original con los enlaces en <https://kdeexpress.gitlab.io/36/>
Si no ves nada después de esta linea en tu aplicación de podcast, es que no sabe leer bien el feed RSS, prueba otra ;)
<!--more-->
* En Fedora 42 KDE se convertirá en una versión oficial y deja de ser un simple "spin". [Español](https://www.muylinux.com/2024/11/08/fedora-kde-workstation/) / [English](https://linuxiac.com/fedora-linux-elevates-kde-to-edition-status/)
  * Y [añade](https://blog.marcdeop.com/?p=289) la opción de activar los repositorios de terceros en la pantalla de bienvenida 
* Steam Deck [actualiza](https://store.steampowered.com/news/app/1675200/view/4676514574283544994) a Plasma 5.27.10 (la última hasta la fecha en 5 es la 5.27.11 de marzo de 2024)
* Fedora [lanza](https://fedoraproject.org/wiki/Changes/Fedora_KDE_Plasma_Mobile) los spins Plasma Mobile y Kinote Mobile
* KDE Wave [vuelve a estar en desarrollo](https://markpenner.space/blog/2024-10-28-kwave-update/) más allá de mantenimiento básico 
* Proyecto [Banana](https://linuxiac.com/kde-announced-its-kde-linux-distro/), una distro atómica/inmutable en desarrollo por KDE. [Basada en Arch y Brtfs](https://blog.desdelinux.net/project-banana-la-apuesta-de-kde-por-tener-una-distro-propia/).
* Plasma 6.3 (2025-02-11):
  * [Mejoras para tabletas](https://pointieststick.com/2024/10/18/this-week-in-plasma-hardware-is-hard/)
  * Notificación cuando el sistema ha tenido que [matar algún proceso](https://blogs.kde.org/2024/11/02/this-week-in-plasma-spoooooky-ooooooooom-notifications/) por falta de memoria.
  * Dolphin tiene una nueva [interfaz para dispositivos moviles](https://blogs.kde.org/2024/10/20/this-week-in-kde-apps/) desde la 24.12
  * El centro de información mostrará [todas tus GPUs ](https://blogs.kde.org/2024/11/09/this-week-in-plasma-everything-you-wanted-and-more/)
  * Discover te informará si la aplicación viene directamente de los desarrolladores o de voluntarios.
  * El widget de impresora ahora muestra la información de la cola directamente
  * El widget de indicador de teclado ahora muestra cuando las teclas modificadores están bloqueadas o "latched" por accesibilidad.

___
{{% linksubssteno %}}

{{< detalsum "Y transcripción completa al pinchar aquí" >}}
{{% includef "./static/subs/36-KDE_Express.txt" %}}
{{< /detalsum >}}
* Agradecimientos:
{{% jorgelama %}}
{{% Adrian %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}

