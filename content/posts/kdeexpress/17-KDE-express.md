---
title: "#17 - Resumen AkademyES 2023"
date: 2023-08-10T03:01:17+02:00
categories: [kdeexpress]
tags: [temporada3, AkademyES]
author: "David Marzal"
img: 626px-Mascot_konqi-commu-akademy.png
imgALT: "Konqi apuntando a una pizarra que pone Akademy"
podcast:
  audio: 17-kde-express-akademy-es-2023/17-KDE_Express-AkademyES2023
  ##audio: 17-KDE_Express-AkademyES2023
  olength : 16128565
  mlength : 20339324
  iduration : "00:28:15"
location:
  - text : "Centro La Térmica, Málaga"
    GEO : "36.68911,-4.44548;u=100"
    OSM : "R3060459"
    rel: subject
    country: es
si :
  uri : "https://floss.social/@kde_espana/110865566288752407"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/17-KDE_Express-AkademyES2023.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/17-KDE_Express-AkademyES2023.asr.srt"
    trtype : application/x-subrip
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Baltasar
    role : Co-Host
    group : Cast
    img : https://files.mastodon.social/accounts/avatars/000/924/299/original/e0a2b2e0270a6a27.png
    href : https://mastodon.social/@baltollkien
  - person : Brais Arias
    role : Co-Host
    group : Cast
    img : https://static.mastodon.gal/accounts/avatars/108/333/163/725/284/233/original/8a8b2905c9aed914.png
    href : https://mastodon.gal/@braisarias
  - person : Jorge Lama
    role : Audio Editor
    group : Audio Post-Production
    img : https://files.mastodon.social/accounts/avatars/001/106/656/original/0c35a1ce0f0cec6b.jpg
    href : https://mastodon.social/@raivenra
  - person : Frank Schroeter
    role : Music Contributor
    group : Audio Post-Production
    img : /images/person/erotic.avif
    href : https://filmmusic.io/song/10348-erotic
itunes:
  explicit:   # por defecto false
comments: false
aliases:
  - /17
---
Nunca nos fuimos, pero hemos vuelto con un episodio monotematico sobre Akademy-es 2023 en Málaga. Os contamos un resumen con todas las charlas que tuvimos y nuestras impresiones.

Por cierto, ahora KDE Express implementa el podcasting 2.0 con varias opciones nuevas, pero necesitas una aplicación que las soporte, [aquí tenéis un listado](https://podcastindex.org/apps?appTypes=open+source%2Capp). Y [aquí una app web](https://podverse.fm/es/podcast/kkDmfJUu-r) que soporta muchas por si queréis probar a escuchar teniendo los subtítulos del episodio por ejemplo.

Artículo original en <https://kdeexpress.gitlab.io/17/>
<!--more-->

Aquí tenéis el [programa del evento](https://www.kde-espana.org/akademy-es-2023/programa) con los enlaces a las charlas de Peertube.
___
{{% linksubssteno %}}


* Agradecimientos:
{{% jorgelama %}}
{{% Adrian %}}
{{% m_erotic %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}