---
title: '#25 Eventos, Plasma6 y muchas noticias'
date: 2024-03-28T03:09:25+02:00
categories: [kdeexpress]
tags: [temporada3, eventos, akademy-es, esLibre, 24H24L, Plasma6]
author: "David Marzal"
img: 626px-Mascot_konqi-commu-akademy.png
imgALT: "Konqi apuntando a una pizarra que pone Akademy"
podcast:
  guid : 25-Eventos-Plasma6-y-muchas-noticias
  audio: 25-kde-express/25-KDE_Express-Akademyes
  ##audio: 25-KDE_Express-Akademyes
  olength : 18788183
  mlength : 14118580
  iduration : "00:14:42"
si :
  uri : "https://floss.social/@kde_espana/112174087910630054"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/25-KDE_Express-Akademyes.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/25-KDE_Express-Akademyes.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Jorge Lama
    role : Audio Editor
    group : Audio Post-Production
    img : https://files.mastodon.social/accounts/avatars/001/106/656/original/0c35a1ce0f0cec6b.jpg
    href : https://mastodon.social/@raivenra
itunes:
  explicit:   # por defecto false
comments: false
aliases:
  - /25
---
Hoy hablamos de esLibre y akademy-es Valencia, KDE en general, varias noticias de Plasma y os dejo bastantes enlaces para seguir la actualidad de este último mes en nuestra comunidad.

Artículo original con los enlaces en <https://kdeexpress.gitlab.io/25/>
<!--more-->

* Akademy-es: El 1 de abril cierra el plazo para enviar propuestas o solicitar asistencia para alojamiento de ponentes. El de patrocinadores continua abierto.
  * https://www.kde-espana.org/akademy-es-2024/asistencia-para-alojamiento-en-akademy-es-2024
  * https://www.kde-espana.org/akademy-es-2024/patrocina-akademy-es-2024
  * Podcast en Mancomún hablando de esLibre : https://mancomun.gal/podcasts/mancomun-podcast-eslibre-2024/

* Estado de situación:
  * KDE Gear 24.02.1 : Dolphin recupera el cambio de color de su icono según el escritorio : https://9to5linux.com/kde-gear-24-02-1-improves-dolphin-spectacle-okular-and-other-kde-apps
  * KDE Plasma 6.0.3 : Esto ya es tan estable como para ponerselo a mi madre : https://kde.org/announcements/plasma/6/6.0.3/

* Plasma 6:
  * https://kde.org/es/announcements/megarelease/6/
  * Vídeo en Peertube con un Te lo enseño y te lo cuento: https://fediverse.tv/videos/watch/e97efa42-118a-49fe-a7d1-648953eef5d5
  * User-facing changes : https://community.kde.org/Plasma/Plasma_6#User-facing_changes
  * https://debugpointnews.com/kde-plasma-6-release/
  * En KDE Neon : https://www.muylinux.com/2024/02/28/kde-neon-6/
  * En openSUSE Tumbleweed y Kalpa : https://news.opensuse.org/2024/03/22/plasma-arrives-in-os-distributions/
  * Tambien en postmarketOS : https://www.muylinux.com/2024/03/06/postmarketos-systemd-gnome-kde-plasma/
  * Charla con José Picon en su nuevo podcast (Whihax) sobre privacidad, KDE y Plasma6 : https://www.ivoox.com/entrevista-a-david-marzal-kde-plasma-6-y-audios-mp3_rf_126405406_1.html

* MuyLinux nos habla de como ver el tamaño de los archivos en Dolphin : https://www.muylinux.com/2024/02/19/tamano-archivos-en-dolphin-kde-plasma/

* La nlnet financia la mejora de la accesibiliad en KDE Plasma con Wayland : https://nlnet.nl/project/KDEPlasma-Wayland/

* Nuevo KDE Slimbook : https://www.muylinux.com/2024/02/22/slimbook-excalibur-kde-amd/

* 8 Nuevas actividades en Gcompris (Android, Flatpak...): https://www.muylinux.com/2024/02/22/gcompris-4/

* digiKam 8.3 incorporta un descodificador RAW interno : https://www.kdeblog.com/tercera-actualizacion-de-digikam-8-ahora-con-descodificador-raw-interno.html

* Fedora 40 vendrá pronto con Plasma6 pero sin opción de X11 : https://www.muylinux.com/2024/03/27/fedora-40-beta/

* XRay OS, una especie de ArchLinux "Vanilla" enfocado en juegos con Plasma por defecto : https://ubunlog.com/xray-os/
___
{{% linksubssteno %}}

{{< detalsum "Y transcripción completa al pinchar aquí" >}}
{{% includef "./static/subs/25-KDE_Express-Akademyes.txt" %}}
{{< /detalsum >}}
* Agradecimientos:
{{% jorgelama %}}
{{% Adrian %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}

