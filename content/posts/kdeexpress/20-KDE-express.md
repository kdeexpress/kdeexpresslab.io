---
title: '#20 En 110d vuelve el cubo'
date: 2023-11-08T03:04:20+02:00
categories: [kdeexpress]
tags: [temporada3, Plasma6, Cubo]
author: "David Marzal"
img: 482px-Mascot_konqi-support-bughunt.png
imgALT: "Konqi con una red intentado cazar un bug (mariposa)"
podcast:
  guid :
  audio: 20-kde-express-vuelve-el-cubo/20-KDE_Express-VuelveElCubo
  ##audio: 20-KDE_Express-VuelveElCubo
  olength : 12775090
  mlength : 18285445
  iduration : "00:21:29"
si :
  uri : "https://floss.social/@kde_espana/111376903121675330"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/20-KDE_Express-VuelveElCubo.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/20-KDE_Express-VuelveElCubo.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Baltasar
    role : Co-Host
    group : Cast
    img : https://files.mastodon.social/accounts/avatars/000/924/299/original/e0a2b2e0270a6a27.png
    href : https://mastodon.social/@baltollkien
  - person : Jorge Lama
    role : Audio Editor
    group : Audio Post-Production
    img : https://files.mastodon.social/accounts/avatars/001/106/656/original/0c35a1ce0f0cec6b.jpg
    href : https://mastodon.social/@raivenra
  - person : Frank Schroeter
    role : Music Contributor
    group : Audio Post-Production
    img : /images/person/erotic.avif
    href : https://filmmusic.io/song/10348-erotic
itunes:
  explicit:   # por defecto false
comments: false
aliases:
  - /20
---
Empezado el curso escolar y acabada la temporada opositora respectivamente, Baltasar y David comentán en este episodio la fecha de la MEGA publicación de KDE6 en Febrero, la recuperación del efecto cubo, la campaña de donaciones y membresía de KDE Internacional y algunas cosas más que tendréis que escuchar para estar al día de que se mueve en la comunidad KDE.

Artículo original en <https://kdeexpress.gitlab.io/20/>
<!--more-->
* https://kde.org/announcements/kdes-6th-megarelease-alpha/
* https://kde.org/es/fundraisers/plasma6member/
* https://www.kdeblog.com/vuelve-el-efecto-cubo-a-plasma.html
* https://www.kdeblog.com/concurso-fondo-de-pantalla-para-plasma-6.html

* Podcasting 2.0 desde la web sin necesidad de cambiar de aplicación:
  * Subtitulos: https://podverse.fm/es/podcast/kkDmfJUu-r
  * Podroll: https://www.podfriend.com/podcast/kde-express5/ y https://podnews.net/podcast/iaqqv
  * Participantes: https://www.podfriend.com/podcast/kde-express5/ o https://www.steno.fm/show/a9a56b87-575a-5f6f-9636-cdf7b73e6230 (entrando en un cápitulo y dandole a PEOPLE) 
___
{{% linksubssteno %}}

{{< detalsum "Y transcripción completa al pinchar aquí" >}}
{{% includef "./static/subs/20-KDE_Express-VuelveElCubo.txt" %}}
{{< /detalsum >}}
* Agradecimientos:
{{% jorgelama %}}
{{% Adrian %}}
{{% m_erotic %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}