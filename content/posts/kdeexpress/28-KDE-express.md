---
title: 'Spectacle, Dolphin, Plasma 6 y otras noticias al moton'
date: 2024-05-17T03:12:28+02:00
categories: [kdeexpress]
tags: [temporada3, akademy-es, esLibre, Spectacle, Dolphin, Plasma6, KDE Goals, Digikam]
author: "David Marzal"
img: 500px-Mascot_konqi-commu-mail.png
imgALT: "Konqi con muchos sobres de correo electronico"
podcast:
  guid : 28-2024-05-17-03-12-Akademyes
  audio: 28-kde-express/28-KDE-Express_Spectacle-Plasma6
  ##audio: 28-KDE-Express_Spectacle-Plasma6
  olength : 7862381
  mlength : 6519686
  iduration : "00:06:47"
si :
  uri : "https://floss.social/@kde_espana/112458217106391840"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/28-KDE-Express_Spectacle-Plasma6.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/28-KDE-Express_Spectacle-Plasma6.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
itunes:
  explicit:   # por defecto false
comments: false
aliases:
  - /28
---
Spectacle es una navaja suiza, Plasma 6 llega a un montón de distros, digiKam tiene un libro, trucos de Dolphin, Spring de KDE Goals, escritorio remoto y alguna cosa más podrás encontrar en este episodio 28 de KDE Express.

Artículo original con los enlaces en <https://kdeexpress.gitlab.io/28/>
<!--more-->
* Distros
  * Post-mortem de los problemas de la actualización de KDE Neon a Plasma 6 : https://blog.neon.kde.org/2024/04/29/kde-neon-post-plasma-6-updates-review/
  * Nueva versión de Garuda (ArchLinux) con Plasma6
    * https://9to5linux.com/arch-linux-based-garuda-linux-bird-of-prey-distro-lands-with-kde-plasma-6
    * https://www.muylinux.com/2024/04/29/garuda-linux/
  * TUXEDO OS también : https://alternativeto.net/news/2024/4/tuxedo-updates-its-linux-based-os-with-kde-plasma-6-and-enhanced-visual-features/
  * Asahi Remix 40 KDE Plasma 6 por defecto : https://www.phoronix.com/news/Fedora-Asahi-Remix-40

* Spectacle recupera el retoque de desenfocar en 24.05 : https://ubunlog.com/kde-devuelve-la-funcion-factor-de-desenfoque-a-spectacle-e-introduce-un-buen-numero-de-mejoras-en-la-interfaz/

* Libro digital sobre digiKam publicado: Recipes 2024-05-13 sin DRM y en oferta : https://www.digikam.org/news/2024-04-13_digikam_recipes_2024-05-13_released/

* KDE Goals Spring de abril sobre sostenibilidad, accesibilidad y automatización
  * https://www.volkerkrause.eu/2024/04/27/kde-goal-sprint-april-2024.html
  * https://tsdgeos.blogspot.com/2024/05/kde-goals-april-2024-sprint.html

* Configurar el escritorio remoto será posible desde la configuración del sistema en Plasma 6.1
* Dolphin dejara seleccionar todos los ficheros u otras acciones en 24.08 : https://pointieststick.com/2024/05/10/this-week-in-kde-our-cup-overfloweth-with-cool-stuff-for-you/

* Atajo recomendado: Meta+e = Abrir Dolphin

* Nos entrevistan en Mancomun : https://mancomun.gal/es/podcasts/mancomun-podcast-akademy-es-2024/
___
{{% linksubssteno %}}

{{< detalsum "Y transcripción completa al pinchar aquí" >}}
{{% includef "./static/subs/28-KDE-Express_Spectacle-Plasma6.txt" %}}
{{< /detalsum >}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}

