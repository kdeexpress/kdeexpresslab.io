---
title: "#06 - Komunidad: Adolfo"
date: 2021-09-16T01:06:06+02:00
categories: [kdeexpress]
tags: [temporada1, Comunidad, HSL, Adolfo]
author: "David Marzal"
img: 526px-Mascot_konqi-base-framework.png
imgALT: "Konqi dentro de una K que representa KDE Frameworks"
podcast:
  audio: 06-kde-express-adolfo/06-KDE_Express-Adolfo
  ##audio: 06-KDE_Express-Adolfo
  olength : 15523778
  mlength : 18197452
  iduration : "00:25:16"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/06-KDE_Express-Adolfo.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/06-KDE_Express-Adolfo.asr.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Adolfo Barrios
    role : guest
    group : Cast
    img : https://mstdn.es/system/accounts/avatars/108/232/360/982/919/446/original/1bcbe2bcd3a178a4.jpeg
    href : https://mstdn.es/@HicSuntDracones
comments: false
aliases:
  - /06
  - /6
---
Seguimos con el formato __Komunidad KDE__, otra vez con un compañero de [Home Studio Libre](https://t.me/HomeStudioLibre), Adolfo Barrios.
Hablamos de por donde ha pasado, como ha llegado a KDE Plasma y sus aplicaciones favoritas entre otras tantas cosas relacionadas con el Software Libre.

Artículo original en <https://kdeexpress.gitlab.io/06/>
<!--more-->
Algunas de las aplicaciones mencionadas:
  * https://apps.kde.org/es/
___
{{% linksubssteno %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}