---
title: "#07 - Peertube y Planeta KDE"
date: 2021-10-07T01:07:07+01:00
categories: [kdeexpress]
tags: [temporada1, Peertube, Akademy, Planet, Kate]
author: "David Marzal"
img: 495px-Mascot_konqi-support-search.png
imgALT: "Konqi subido a un archivador buscando documentos"
podcast:
  audio: 07-kde-express-peertube-y-planeta-kde/07-KDE_Express-Peertube_y_Planeta_KDE
  ##audio: 07-KDE_Express-Peertube_y_Planeta_KDE
  olength : 12930415
  mlength : 13794359
  iduration : "00:19:09"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/07-KDE_Express-Peertube_y_Planeta_KDE.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/07-KDE_Express-Peertube_y_Planeta_KDE.asr.srt"
    trtype : text/srt
participantes :
  - person : Jose Picon
    role : Host
    group : Cast
    img : https://mpipeunstor.blob.core.windows.net/mpiuseravatars/86a37988-8272-4370-bc77-2c16d9ede0e5.jpg
    href : https://es.linkedin.com/in/jos%C3%A9-pic%C3%B3n
  - person : David Marzal
    role : Co-Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Brais Arias
    role : Co-Host
    group : Cast
    img : https://static.mastodon.gal/accounts/avatars/108/333/163/725/284/233/original/8a8b2905c9aed914.png
    href : https://mastodon.gal/@braisarias
comments: false
aliases:
  - /07
  - /7
---
Se acerca el aniversario de KDE, pero antes tenemos tiempo de tratar temas de todo tipo sobre todo relacionados con la promoción del trabajo de la comunidad. Si tienes 20 minutos (o 12 a 1.75x) escucharas en que andamos metidos y eventos que vienen de camino entre otros temas de los que os dejamos a continuación los enlaces.

Artículo original en <https://kdeexpress.gitlab.io/07/>
<!--more-->
Enlaces mencionados:
* https://tube.kockatoo.org/a/kde_es
* https://www.kde-espana.org/akademy-es-2021-en-linea
* https://t.me/kdeplanet_es
  * https://planet.kde.org/es/

* https://24h24l.org/
* https://republicaweb.es/
* https://www.kdeblog.com/
___
{{% linksubssteno %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}