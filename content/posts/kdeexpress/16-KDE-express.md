---
title: "#16 - KDE en Telegram"
date: 2023-03-14T02:04:16+01:00
categories: [kdeexpress]
tags: [temporada2, Telegram]
author: "David Marzal"
img: 542px-Mascot_konqi-app-system.png
imgALT: "Konqi dentro de una rueda dentada, saluda con dos dedos en la frente"
podcast:
  audio: 16-kde-express-kde-en-telegram/16-KDE_Express-KDE_en_Telegram
  ##audio: 16-KDE_Express-KDE_en_Telegram
  olength : 5580158
  mlength : 6617252
  iduration : "00:09:11"
si :
  uri : "https://floss.social/@kde_espana/110771151853208586"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/16-KDE_Express-KDE_en_Telegram.vtt"
    trtype : text/vtt
  #- trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/16-KDE_Express-KDE_en_Telegram.asr.srt"
  #  trtype : application/x-subrip
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Jorge Lama
    role : Audio Editor
    group : Audio Post-Production
    img : https://files.mastodon.social/accounts/avatars/001/106/656/original/0c35a1ce0f0cec6b.jpg
    href : https://mastodon.social/@raivenra
  - person : Frank Schroeter
    role : Music Contributor
    group : Audio Post-Production
    img : /images/person/erotic.avif
    href : https://filmmusic.io/song/10348-erotic
aliases:
  - /16
---
Episodio temático sobre Telegram, grabado en movilidad, en el que podréis descubrir las diferentes formas de participar y/o estar al día sobre la comunidad de KDE desde esta aplicación, no del todo libre pero con mucho uso dentro del mundo de la informática y el SL.

Artículo original en <https://kdeexpress.gitlab.io/16/>
<!--more-->
Aquí tenéis la [entrada de la wiki con casi todos los recursos en Telegram](https://community.kde.org/Telegram). De los cuales destacamos en el podcast los siguiente:
* [Canal: KDE Express Podcast](https://t.me/KDEexpress) - En el que además podéis comentar los episodios.
* [Canal: Planet KDE en Español](https://t.me/kdeplanet_es)
* [Canal: Planet KDE in english](https://t.me/kdeplanet)
* [Grupo y foro: KDE - Cañas y Bravas](https://t.me/kde_canasbravas) - Por favor decir algo al entrar que se sepa que sois humanos y no bots.
___
{{% linksubssteno %}}


* Agradecimientos:
{{% jorgelama %}}
{{% Adrian %}}
{{% m_erotic %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}