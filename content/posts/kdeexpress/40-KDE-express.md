---
title: '40 episodios y no paran las noticias'
date: 2025-02-11T15:04:40+01:00
categories: [kdeexpress]
tags: [temporada4, Accesibilidad, Plasma6, Gears, Flatpak, Gcompris, Peertube, F-Droid, KDEconnect]
author: "David Marzal"
img: 542px-Mascot_konqi-app-system.png
imgALT: "Konqi dentro de una rueda dentada, saluda con dos dedos en la frente"
podcast:
  guid : 40-2025-02-07-04-09-4aTemporada-KDE_express
  audio: 40-kde-express/40-KDE_Express
  ##audio: 40-KDE_Express
  olength : 9649676
  mlength : 13618162
  iduration : "00:14:11"
si :
  uri : "https://floss.social/@kde_espana/113985828110154405"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/40-KDE_Express.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/40-KDE_Express.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Jorge Lama
    role : Audio Editor
    group : Audio Post-Production
    img : https://files.mastodon.social/accounts/avatars/001/106/656/original/0c35a1ce0f0cec6b.jpg
    href : https://mastodon.social/@raivenra
itunes:
  explicit:   # por defecto false
comments: false
aliases:
  - /40
---
KDE Express llega a la cuarentena con energía, no suficiente para fiestas, pero si para noticias y una promo de un podcast amigo.

* Mujeres con Historia: [Feed](https://www.ivoox.com/podcast-mujeres-historia_fg_f1878350_filtro_1.xml) - [@Mujeresconhistoria@mas.to](https://mas.to/@Mujeresconhistoria) en Mastodon - [PodcastIndex](https://podcastindex.org/podcast/363798)

Artículo original con todos los enlaces en <https://kdeexpress.gitlab.io/40/>
Si no ves nada después de esta linea en tu aplicación de podcast, es que no sabe leer bien el feed RSS, prueba otra ;)
<!--more-->

* Loas
  * El [escritorio + popular](https://www.muylinux.com/2025/01/17/kde-plasma-es-el-escritorio-mas-popular-de-2024/) en MuyLinux
  * Y [el favorito](https://www.muylinux.com/2025/01/21/comunidad-arch-linux-favoritos-2024/) en ArchLinux con un 43% de votos.
  * ¿Por qué KDE Plasma sigue [liderando como la mejor opción](https://laboratoriolinux.es/index.php/-noticias-mundo-linux-/escritorios/37405-por-que-kde-plasma-sigue-liderando-como-la-mejor-opcion-de-escritorio-para-linux.html) de escritorio?
* [Plasma 6.3](https://blogs.kde.org/2025/01/11/this-week-in-plasma-final-plasma-6.3-features/)
  * Al llegar notificaciones con el "No molestar activado" cuando se vuelva al modo normal habrá una única notificación el número, en vez de una cascada de notificaciones
  * Los iconos que sean de enlaces simbólicos ahora tendrán la opción en el menu contextual de ver a donde apuntan.
  * Podrás elegir en el menu de inicio si quieres iconos simbólicos o volver a iconos normales los cuales puedes seleccionar en el editor del menu.
* [Gears 25.04](https://blogs.kde.org/2025/01/12/this-week-in-kde-apps-usability-improvements-new-features-and-updated-apps/)
  * Dolphin: Recortará el nombre por en medio en vez del final si es demasiado largo.
  * Skrooge: Administrador de finanzas personal, ha sido actualizado a Qt6
* Fuera de Gears pero siguiendo con Apps:
  * Lanzado [Gcompris 25](https://gcompris.net/news/2025-01-30-es.html) con 5 [nuevas actividades](https://www.muylinux.com/2025/01/30/gcompris-25-suite-educativa/), una de dibujo libre, calcular con complemento a diez, suma vertical y dos variantes de resta vertical.
  * Flatpak lanza [mayor-version](https://www.linuxadictos.com/flatpak-1-16-llega-mas-de-dos-anos-despues-con-mejoras-en-su-integracion-y-estas-novedades.html) tras [dos años](https://feaneron.com/2025/01/14/flatpak-1-16-is-out)
* F-droid
  * Peertube [ya disponible](https://f-droid.org/es/packages/org.framasoft.peertube/) 
  * KDE Connect 1.32.10 [con arreglos](https://f-droid.org/es/packages/org.kde.kdeconnect_tp/) 
* FLOSS en general
  * Wine llega a 10 con [muchas mejoras](https://9to5linux.com/wine-10-released-with-experimental-bluetooth-driver-new-hid-pointer-driver), sobre todo en Wayland 
  * [Thunderbird 134](https://www.thunderbird.net/en-US/thunderbird/134.0/releasenotes/) llega con arreglos y notificaciones de escritorio en tiempo real.

___
{{% linksubssteno %}}

{{< detalsum "Y transcripción completa al pinchar aquí" >}}
{{% includef "./static/subs/40-KDE_Express.txt" %}}
{{< /detalsum >}}
* Agradecimientos:
{{% jorgelama %}}
{{% Adrian %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}

