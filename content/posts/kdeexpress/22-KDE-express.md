---
title: '#22 La mejor distro para KDE'
date: 2023-12-12T03:06:22+01:00
categories: [kdeexpress]
tags: [temporada3, Plasma, Distribuciones, Krita, Slimbook]
author: "David Marzal"
img: 500px-Mascot_konqi-commu-mail.png
imgALT: "Konqi con muchos sobres de correo electronico"
podcast:
  guid :
  audio: 22-kde-express/22-KDE_Express-La_mejor_distro_para_KDE
  ##audio: 22-KDE_Express-La_mejor_distro_para_KDE
  olength : 6019252
  mlength : 5934723
  iduration : "00:08:14"
si :
  uri : "https://masto.es/@ruisan/111499296802097617"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/22-KDE_Express-La_mejor_distro_para_KDE.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/22-KDE_Express-La_mejor_distro_para_KDE.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Jorge Lama
    role : Audio Editor
    group : Audio Post-Production
    img : https://files.mastodon.social/accounts/avatars/001/106/656/original/0c35a1ce0f0cec6b.jpg
    href : https://mastodon.social/@raivenra
  - person : Frank Schroeter
    role : Music Contributor
    group : Audio Post-Production
    img : /images/person/erotic.avif
    href : https://filmmusic.io/song/10348-erotic
itunes:
  explicit:   # por defecto false
comments: false
aliases:
  - /22
---
Reflexión sobre "la mejor distro", Krita, feed de Compilando Podcast, fiesta por Plasma6, Slimbook tiene nueva tienda y Promo de Disperso Tiempo Escaso.

PD: Reunión de socios KDE España el 20 de diciembre, es decir el miércoles de la semana que viene.

Artículo original con enalces en <https://kdeexpress.gitlab.io/22/>
<!--more-->
* La mejor distro para KDE : https://masto.es/@ruisan/111499296802097617
* https://krita.org/en/item/broken-rules-sponsors-krita/
* Cambio de URL en Compilando Podcast : https://compilando.es/index.php/feed/podcast/
* https://www.kdeblog.com/slimbook-renueva-su-pagina-web.html
* Eventos por la MegaRelease de Plasma6: https://community.kde.org/Promo/Events/Parties/KDE_6th_Megarelease
* https://disperso.tiempoescaso.com/
___
{{% linksubssteno %}}

{{< detalsum "Y transcripción completa al pinchar aquí" >}}
{{% includef "./static/subs/22-KDE_Express-La_mejor_distro_para_KDE.txt" %}}
{{< /detalsum >}}
* Agradecimientos:
{{% jorgelama %}}
{{% Adrian %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}