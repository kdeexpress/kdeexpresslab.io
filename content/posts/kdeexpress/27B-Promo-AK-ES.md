---
title: 'Promo Akademy-es Valencia 2024'
date: 2024-05-21T03:51:01+02:00
categories: [kdeexpress]
tags: [temporada3, akademy-es, esLibre, promo]
author: "David Marzal"
img: AkademyES/akademy_2024_logos_propios_900x700.png
imgALT: "Konqi apuntando a una pizarra que pone Akademy"
podcast:
  guid : 27B-2024-05-09-03-11-Akademyes
  audio: promo-AkademyES_2024/promo-AkademyES_2024
  ##audio: promo-AkademyES_2024
  olength : 
  mlength : 4536777
  iduration : "00:03:07"
si :
  uri : "https://floss.social/@kde_espana/112384692814143721"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/promo-AkademyES_2024.srt"
    trtype : text/srt
participantes :
  - person : Paco Estrada
    role : Host
    group : Cast
    img : https://compilando.es/wp-content/uploads/2017/04/cropped-cropped-cropped-400x400-texto.jpg
    href : https://compilando.es/
  - person : David Marzal
    role : Content Manager
    group : Administration
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
itunes:
  explicit:   # por defecto false
comments: false
aliases:
  - /AkademyES-2024
---
Gracias a Paco Estrada de [Compilando Podcast](https://compilando.es/) os podemos compartir la promo con la que pretendemos resumir lo que podéis esperar de nuestro evento en Valencia.

Compartirla tanto como queráis y podáis ;)

https://www.kde-espana.org/akademy-es-2024/programa-akademy-es-2024

Y gracias de nuevo a [openSUSE](https://www.opensuse.org/), [La Universidad de La Laguna](https://www.ull.es/), a Paco y a todos los que vengáis o compartáis.

Artículo original en <https://kdeexpress.gitlab.io/AkademyES-2024/>
___
{{% linksubssteno %}}

{{< detalsum "Y transcripción completa al pinchar aquí" >}}
{{% includef "./static/subs/promo-AkademyES_2024.txt" %}}
{{< /detalsum >}}
* Agradecimientos:
{{% jorgelama %}}
{{% Adrian %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}

