---
title: 'Feliz 28 cumpleaños'
date: 2024-10-24T08:04:35+02:00
categories: [kdeexpress]
tags: [temporada4, AkademyES, Aniversario, KDE Neon, Junta]
author: "David Marzal"
img: 501px-Mascot_konqi-commu-journalist.png
imgALT: "Konqi levantando un microfono, sostiene un bloc de notas en la otra mano"
podcast:
  guid : 35-2024-10-23-04-04-4aTemporada-KDE_express
  audio: 35-kde-express/35-KDE_Express
  ##audio: 35-KDE_Express
  olength : 9224245
  mlength : 12478825
  iduration : "00:12:59"
si :
  uri : "https://floss.social/@kde_espana/113360907551665559"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/35-KDE_Express.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/35-KDE_Express.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Jorge Lama
    role : Audio Editor
    group : Audio Post-Production
    img : https://files.mastodon.social/accounts/avatars/001/106/656/original/0c35a1ce0f0cec6b.jpg
    href : https://mastodon.social/@raivenra
itunes:
  explicit:   # por defecto false
comments: false
aliases:
  - /35
---
Archive.org vuelve a funcionar, aunque nosotros estamos en migración de nuestros audios a nuestros propios servidores para ayudar un poco a que tenga menos carga de trabajo este gran servicio, en el que seguiremos dejando copia de los diferentes recursos que generemos.

Plasma 6.2.1 (aunque hoy ya está 6.2.2), Gears [24.08.2](https://kde.org/es/announcements/gear/24.08.2/)

Artículo original con los enlaces en <https://kdeexpress.gitlab.io/35/>
<!--more-->
* Ya están disponibles todas las charlas de Akademy-es 2024 Valencia en [nuestra instancia de Peertube](https://tube.kockatoo.org/w/p/ouhPEyy4NkHxZuYDL6G83z). Gracias de nuevo Jon.
* El 14 de octubre celebramos el [28 cumpleaños de KDE](https://www.kdeblog.com/fiestas-por-el-28-aniversario-kde.html)
* A las ya conocidas entradas de "Esta semana en KDE" que ahora solo se centra en Plasma, les ha salido su [contrapartida para las aplicaciones](https://blogs.kde.org) donde se nos irá informando de lo que está por venir en las nuevas versiones de Gears.
* Ubuntu KDE Neon Core. Ubuntu [está preparando](https://www.muylinux.com/2024/09/20/kde-neon-core-ubuntu-snap/) una versión inmutable/atomica con KDE Plasma
* Mientras tanto KDE Neon [ya tiene bajo los motores](https://www.muylinux.com/2024/10/14/kde-neon-ubuntu-24-04-lts/) los repos de [24.04 LTS](https://blog.neon.kde.org/2024/10/10/kde-neon-rebased-on-ubuntu-24-04-lts/)
* Por otro lado Ubuntu Studio 24.10 [sube a Plasma 6](https://ubunlog.com/ubuntu-studio-24-10-sube-a-plasma-6-se-pasa-al-kernel-mainline-y-actualiza-su-meta-paquete-para-creadores-de-contenido/), se pasa al kernel mainline y actualiza su meta-paquete para creadores de contenido.
* Una de las novedades que se me olvido adelantar de Plasma 6.2 es la mejora en el [manejo del brillo](https://www.kdeblog.com/gestion-de-color-en-plasma-6-2.html) para los perfiles HDR e ICC, así como el rendimiento HDR.
* Dolphin mejorara la búsqueda dentro de los ficheros en la [próxima versión](https://blogs.kde.org/2024/10/02/use-ripgrep-all-/-ripgrep-to-improve-search-in-dolphin/)
* Ya es posible crear o usar [cursores SVG](https://blog.vladzahorodnii.com/2024/10/06/svg-cursors-everything-that-you-need-to-know-about-them/) en Plasma. Cosa muy útil para la accesibilidad y que no se vea borroso.
* Hemos renovado la Junta (un poco)
  * Presidente: Rubén Gómez Antolí
  * Vicepresidente: Adrián Chaves Fernández
  * Tesorero: José Millán Soto
  * Secretario: Baltasar Ortega Bort
  * Vocales: Rosie Matthews, Ivan Gregori Jorques y David Marzal (un servidor)
* Nueva versión de [Peertube](https://joinpeertube.org/news/release-6.3), con audio y video separado, buscador en el widget de subtitlos y la posibilidad de configurar Youtube-dl para importaciones.
* Breve mención a Cañas y Podcast de [LUDF](https://masto.es/@ultimosfeed), [Papa Friki](https://mas.to/@PapaFriki) y [Ruisan](https://tiempoescaso.com/).

___
{{% linksubssteno %}}

{{< detalsum "Y transcripción completa al pinchar aquí" >}}
{{% includef "./static/subs/35-KDE_Express.txt" %}}
{{< /detalsum >}}
* Agradecimientos:
{{% jorgelama %}}
{{% Adrian %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}

