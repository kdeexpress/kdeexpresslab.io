---
title: 'Accesibilidad, digiKam, flatpaks en Discover, Sticky keys y mejoras en la web'
date: 2024-08-16T08:01:32+02:00
categories: [kdeexpress]
tags: [temporada4, flatpaks, Accesibilidad, digiKam]
author: "David Marzal"
img: 
imgALT: ""
podcast:
  guid : 32-2024-08-14-04-01-4aTemporada
  audio: 32-kde-express/32-KDE_Express
  ##audio: 32-KDE_Express
  olength : 13247887
  mlength : 10547997
  iduration : "00:10:59"
si :
  uri : "https://floss.social/@kde_espana/112970419767708109"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/32-KDE_Express.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/32-KDE_Express.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Jorge Lama
    role : Audio Editor
    group : Audio Post-Production
    img : https://files.mastodon.social/accounts/avatars/001/106/656/original/0c35a1ce0f0cec6b.jpg
    href : https://mastodon.social/@raivenra
itunes:
  explicit:   # por defecto false
comments: false
aliases:
  - /32
---
Haciendo frente al calor veraniego del sur, aquí tenemos otra tanda de nocitas de nuestra comunidad, esperamos que os lo estéis pasando bien los que estéis de vacaciones o al fresquito. Accesibilidad, digiKam, flatpaks en Discover, Sticky keys, RPi, Akademy, mejoras en la web y alguna otra cosa más.

Artículo original con los enlaces en <https://kdeexpress.gitlab.io/32/>
<!--more-->

* Enlaces de las cosas mencionadas en el episodio:
  * En la página de accesibilidad de la configuración del sistema, el [selector de ficheros del timbre del sistema, en Octubre cuando salga Plasma 6.2 soporta audios en .oga y ademas te indica que foramtos soporta.](https://pointieststick.com/2024/07/05/this-week-in-kde-autoscrolling/)
  * digimKam se actualiza a la 8.4.0 con muchos arreglos y algunas novedades como: [etiquetas automaticas con análisis profundo y traducción en linea de estas etiquetas.](https://www.kdeblog.com/cuarta-actualizacion-de-digikam-8-ahora-con-traductor-automatico-de-etiquetas.html)
  * Plasma 6.1.2 [mejora el manejo de flatpaks en Discover. Ahora se hacen automáticamente los rebases de los runtimes y se eliminan los que están EOL](https://9to5linux.com/kde-plasma-6-1-3-improves-flatpak-support-in-discover-fixes-more-bugs)
  * Plasma 6.1.3 y 6.2.0 trae muchas [mejoras de accesibilidad en Wayland](https://pointieststick.com/2024/07/19/this-past-two-weeks-in-kde-fixing-sticky-keys-and-the-worst-crashes/) sobre todo con las ["Sticky keys"](https://es.wikipedia.org/wiki/Sticky_Keys)
  * [Proyecto hecho por un padre con Gcompris y RPi5](https://itsfoss.com/raspberry-pi-kids-set-up/)
  * [Puedes contribuir a KDE en muchos lenguajes](https://rabbitictranslator.com/contribute-to-kde-without-cpp/)
  * [Akademy Internacional en Würzburg, entre el 7 y el 12 de septiembre](https://akademy.kde.org/2024/), será online y presencial.
  * Sección Podcasting 2.0: Mejoras en la web del podcast y la de [ATL](https://accesibilidadtl.gitlab.io/), para usar la información que ponemos en el feed para tambien mostrar aquí un enlace a los subtitulos y transcripciones, los comentarios y las personas que participan en el podcast, por si aún usais aplicaciones que no lo implementan directamente.

___
{{% linksubssteno %}}

{{< detalsum "Y transcripción completa al pinchar aquí" >}}
{{% includef "./static/subs/32-KDE_Express.txt" %}}
{{< /detalsum >}}
* Agradecimientos:
{{% jorgelama %}}
{{% Adrian %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}

