---
title: 'Opciones de Accesibilidad y Streaming en Plasma 6.2'
date: 2025-01-18T11:04:39+01:00
categories: [kdeexpress]
tags: [temporada4, Accesibilidad, Plasma6, Streaming, Presentaciones]
author: "David Marzal"
img: 625px-Mascot_konqi-app-presentation.png
imgALT: "Konqi apuntando a una pizarra que pone E=MC²"
podcast:
  guid : 39-2025-01-18-04-08-4aTemporada-KDE_express
  audio: 39-kde-express/39-KDE_Express
  ##audio: 39-KDE_Express
  olength : 4230036
  mlength : 6915062
  iduration : "00:07:12"
si :
  uri : "https://tube.kockatoo.org/videos/watch/d7d1a9cf-64d9-4c0e-bed9-2f995900395a"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/39-KDE_Express.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/39-KDE_Express.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
itunes:
  explicit:   # por defecto false
comments: false
aliases:
  - /39
---
Retomamos las píldoras de KDE Express con un combo de opciones de Accesibilidad y presentación en Plasma. La mayoría son útiles tanto hacer el uso de Plasma más accesible, como para realizar presentaciones o Streamings en directo.

Este episodio en concreto es posible que os sea útil ver el vídeo que tenemos colgado en:
https://tube.kockatoo.org/c/pildoras/videos?s=1

Estas son las opciones comentadas:
* Preferencias del sistema -> Gestión de ventanas -> Efectos del escritorio:
  * Animación del click del ratón : Meta+* (con el Shift+ )
  * Seguimiendo del ráton :
    * Meta + Ctrl + P = Activar y desactivar permanente
    * Meta + Ctrl = Activado mientras pulsadok
  * Ampliación/ Lupa: 
    * Meta+[=/+] (Ampliar) o Meta+- (Reducir) y Meta+0 (Resetear)
    * Meta+F5 (Mover ratón al foco) [Estos dos solo en Amplicación]
    * Meta+F6 (Mover ratón al centro)
    * No valen las teclas del numpad
  * Marcar con el ratón
    * Mayúsculas + Meta + ratón = Dibujo libre
    * Mayúsculas + Meta + Ctrl + ratón = Dibujo de flechas
    * Meta + Mayúsculas + F12 = Borrar la última marca del ratón
    * Meta + Mayúsculas + F11 = Borrar marcas del ratón

¿Las conocíais? ¿Os parecen útiles? ¿Conocéis más?

PD: Al haber grabado este episodio en vídeo con OBS, no le hemos hecho sufrir a nuestro querído Jorge Lama con la edición, así que si notais el audio con algo de menos calidad, es ese el motivo.
Gracias Jorge por hacer KDE Express posible.
___
{{% linksubssteno %}}

{{< detalsum "Y transcripción completa al pinchar aquí" >}}
{{% includef "./static/subs/39-KDE_Express.txt" %}}
{{< /detalsum >}}
* Agradecimientos:
{{% jorgelama %}}
{{% Adrian %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}

