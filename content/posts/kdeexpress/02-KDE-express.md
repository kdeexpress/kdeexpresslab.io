---
title: "#02 - Novedades de verano"
date: 2021-08-02T01:02:02+02:00
categories: [kdeexpress]
tags: [temporada1, consola, gear,frameworks,plasma, kasts]
author: "David Marzal"
img: 542px-Mascot_konqi-app-system.png
imgALT: "Konqi dentro de una rueda dentada, saluda con dos dedos en la frente"
podcast:
  audio: 02-kde-express/02-KDE_Express
  ##audio: 02-KDE_Express
  olength : 12962200
  mlength : 13556465
  iduration : "00:18:49"
transcript:
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/02-KDE_Express.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/02-KDE_Express.asr.srt"
    trtype : text/srt
participantes :
  - person : Jose Picon
    role : Host
    group : Cast
    img : https://mpipeunstor.blob.core.windows.net/mpiuseravatars/86a37988-8272-4370-bc77-2c16d9ede0e5.jpg
    href : https://es.linkedin.com/in/jos%C3%A9-pic%C3%B3n
  - person : David Marzal
    role : Co-Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
comments: false
aliases:
  - /02
  - /2
---
Segundo episodio lanzado, en esta ocasión estamos solo José y David a los micros, pero Brais sigue en la sala de máquinas revisando noticias y volverá para los próximos (es lo que tienen las vacaciones de verano...).

Artículo original en [https://kdeexpress.gitlab.io/02/](https://kdeexpress.gitlab.io/02/) con todos los enlaces.
<!--more-->

* Novedades en KDE
  * De Plasma 5.22.2 a 5.22.4 : Noticia en [kdeblog](https://www.kdeblog.com/lanzada-la-cuarta-actualizacion-de-plasma-5-22.html) y [anuncio oficial](https://kde.org/announcements/plasma/5/5.22.4/)
  * Frameworks 5.84.0 : [kdeblog](https://www.kdeblog.com/actualizacion-de-julio-del-2021-de-kde-frameworks.html) - [KDE.org](https://kde.org/announcements/frameworks/5/5.84.0/)
  * [Gear](https://tsdgeos.blogspot.com/) 21.04.3 : [kdeblog](https://www.kdeblog.com/tercera-actualizacion-de-kde-gear-21-04.html) - [KDE.blog](https://kde.org/announcements/gear/21.04.3/) - 21.08 saldrá el [12 de agosto](https://tsdgeos.blogspot.com/2021/07/kde-gear-2108-releases-branches-created.html) (en principio).
  * Plasma Mobile 21.07 : [kdeblog](https://www.kdeblog.com/actualizacion-de-plasma-mobile-julio-de-2021.html) - [plasma-mobile.org](https://plasma-mobile.org/2021/07/20/plasma-mobile-gear-21-07/)

* [Kasts](https://apps.kde.org/kasts/) : Gestor de podcast para móvil y escritorio(flatpak)
  * [kdeblog](https://www.kdeblog.com/gestor-de-podcast-kasts-nueva-aplicacion-kde.html)

* El desarrollo de KDE no para en vacaciones, web recomendada para ver que se está haciendo:
  * https://pointieststick.com/

* [Steam Deck](https://store.steampowered.com/steamdeck) : Consola portatil de Valve usando Arch Linux y Plasma
  * [kdeblog 1](https://www.kdeblog.com/steam-deck-y-el-escritorio-plasma-de-kde.html) - [kdeblog 2](https://www.kdeblog.com/kde-plasma-en-steam-deck-la-consola-gnu-linux-de-valve.html) - [gamingonlinux.com](https://www.gamingonlinux.com/2021/07/valve-has-formally-announced-the-steam-deck-a-portable-handheld-console-with-steamos/)

* [Kubuntu 20.10 deja de tener soporte](https://kubuntu.org/news/kubuntu-20-10-groovy-gorilla-reaches-end-of-life/)

* Como se hace el podcast:
  * [Retoque final al audio para normalizar el volumen](https://gitlab.com/Marzal/dmc-scripts/-/blob/master/bin/dmc_ffkde.sh)
  * [Proyecto de la web y el feed](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io)
  * [Plantilla del proyecto reproducible](https://gitlab.com/Marzal/hugo-cards-podcast)
___
{{% linksubssteno %}}


{{% licencia %}}
{{% podverse kkDmfJUu-r %}}