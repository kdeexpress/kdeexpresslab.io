---
title: "#01 - Primer episodio"
date: 2021-07-20T01:01:01+02:00
categories: [kdeexpress]
tags: [temporada1, Plasma, Akademy]
author: "David Marzal"
img: 626px-Mascot_konqi-commu-akademy.png
imgALT: "Konqi apuntando a una pizarra que pone Akademy"
podcast:
  audio: 01-kde-express-empezamos/01_KDE_Express-Empezamos
  ##audio: 01_KDE_Express-Empezamos
  olength : 15108092
  mlength : 16155415
  iduration : "00:22:26"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/01_KDE_Express-Empezamos.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/01_KDE_Express-Empezamos.asr.srt"
    trtype : text/srt
participantes :
  - person : Jose Picon
    role : Host
    group : Cast
    img : https://mpipeunstor.blob.core.windows.net/mpiuseravatars/86a37988-8272-4370-bc77-2c16d9ede0e5.jpg
    href : https://es.linkedin.com/in/jos%C3%A9-pic%C3%B3n
  - person : David Marzal
    role : Co-Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Brais Arias
    role : Co-Host
    group : Cast
    img : https://static.mastodon.gal/accounts/avatars/108/333/163/725/284/233/original/8a8b2905c9aed914.png
    href : https://mastodon.gal/@braisarias
comments: false
aliases:
  - /01
  - /1
---
Aquí tenemos el primer episodio en si de KDE Express, nos ha costado un poco publicarlo así que algunos programas ya han sacado nueva versión, pero las comentaremos en los siguientes. Esperamos que lo que son nuestras experiencias y opiniones no envejezcan tan rápido como KDE saca nuevas releases :)

Artículo original en [https://kdeexpress.gitlab.io/01/](https://kdeexpress.gitlab.io/01/) con todos los enlaces.
<!--more-->
* Akademy : [https://conf.tube/accounts/kde/videos](https://conf.tube/accounts/kde/videos) \[DAVID\]  
  * [The KDE Qt 5.15 patch collection](https://conf.tube/videos/watch/b8c68a1a-a2dc-491a-ba9e-be7904d32585)  
  * [Addressing wayland robustness](https://conf.tube/videos/watch/23d063c3-2a66-403b-842a-2ea602930767 "Addressing wayland robustness")  
  * [Towards sustainable computing](https://conf.tube/videos/watch/6c88c1fb-3b51-4d5e-be7b-5347cd141798 "Towards sustainable computing")  
  * [Kdenlive](https://conf.tube/videos/watch/ff564eec-7e4b-474d-98c1-6adc95340b15 "Kdenlive")  
  * [SPDX License Markers in KDE - Progress Update](https://conf.tube/videos/watch/9daaa96a-ec91-4b47-9d38-e33512fc235c "SPDX License Markers in KDE - Progress Update")  
  * [Health Analysis of the KDE Community](https://conf.tube/videos/watch/b47ecac3-4ece-48b8-8739-298f5dcabdff "Health Analysis of the KDE Community")  

* Akademy Award 2021: \[BRAIS\]  
  * [https://www.kdeblog.com/akademy-award-2021-los-premios-de-la-comunidad-kde.html](https://www.kdeblog.com/akademy-award-2021-los-premios-de-la-comunidad-kde.html)  

* [https://planet.kde.org/es/](https://planet.kde.org/es/) \[JOSÉ\]  
  
* KDE Plasma 5.22.2 (8 junio) \[DAVID\]  
  * [5.22.0](https://kde.org/announcements/plasma/5/5.22.0/) - [5.22.2](https://kde.org/announcements/plasma/5/5.22.2/)  
  * [https://www.kdeblog.com/disponible-plasma-5-22-mas-estable-mas-usable-y-mas-flexible.html](https://www.kdeblog.com/disponible-plasma-5-22-mas-estable-mas-usable-y-mas-flexible.html)  
  * [https://www.preining.info/blog/2021/06/kde-plasma-5-22-for-debian/](https://www.preining.info/blog/2021/06/kde-plasma-5-22-for-debian/)  
  * [https://9to5linux.com/you-can-now-install-kde-plasma-5-22-on-kubuntu-21-04-heres-how](https://9to5linux.com/you-can-now-install-kde-plasma-5-22-on-kubuntu-21-04-heres-how)  
  * [https://alpinelinux.org/posts/Alpine-3.14.0-released.html](https://alpinelinux.org/posts/Alpine-3.14.0-released.html)  
  * KDE Neon 5.22 (9 junio)  
    * [https://blog.neon.kde.org/2021/06/09/kde-neon-5-22-is-here/](https://blog.neon.kde.org/2021/06/09/kde-neon-5-22-is-here/)  

* \[EVENTO\] [akademy-ES](https://akademy.kde.org/2021/) (online 20-21 noviembre): \[JOSE\]  
  * [Anunciado Akademy-es 2021 en línea #akademyes](https://www.kdeblog.com/anunciado-akademy-es-2021-en-linea-akademyes.html)  

* KDE España: [https://www.kde-espana.org/](https://www.kde-espana.org/) \[JOSÉ\]
* \[EVENTO\] [Congreso esLibre](https://eslib.re/2021/) (online 25 y 26 de junio)  \[DAVID\]  
  * [https://propuestas.eslib.re/2021/charlas/coleccion-parches-kde-qt-5-15](https://propuestas.eslib.re/2021/charlas/coleccion-parches-kde-qt-5-15)  
___
{{% linksubspodverse kkDmfJUu-r %}}
{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}