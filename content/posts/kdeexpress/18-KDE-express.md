---
title: "#18 - Plasma6 en camino"
date: 2023-08-11T03:02:18+02:00
categories: [kdeexpress]
tags: [temporada3, Plasma6]
author: "David Marzal"
img: 649px-Mascot_konqi-app-utilities.png
imgALT: "Konqi junto a una caja de herramientas con una llave inglesa en la mano"
podcast:
  audio: kde-express-plasma-6/KDE_Express-Plasma6
  ##audio: KDE_Express-Plasma6
  olength : 12839426
  mlength : 15518759
  iduration : "00:21:33"
si :
  uri : "https://floss.social/@kde_espana/110887010937746826"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/18-KDE_Express-Plasma6.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/18-KDE_Express-Plasma6.asr.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Baltasar
    role : Co-Host
    group : Cast
    img : https://files.mastodon.social/accounts/avatars/000/924/299/original/e0a2b2e0270a6a27.png
    href : https://mastodon.social/@baltollkien
  - person : Jorge Lama
    role : Audio Editor
    group : Audio Post-Production
    img : https://files.mastodon.social/accounts/avatars/001/106/656/original/0c35a1ce0f0cec6b.jpg
    href : https://mastodon.social/@raivenra
  - person : Frank Schroeter
    role : Music Contributor
    group : Audio Post-Production
    img : /images/person/erotic.avif
    href : https://filmmusic.io/song/10348-erotic
itunes:
  explicit:   # por defecto false
comments: false
aliases:
  - /18
---
Plasma 6 está cada vez más cerca, así que hoy os ponemos al día sobre como va el desarrollo.
Y como siempre os dejamos unas cuantos enlaces por si quereis ampliar información.

Artículo original en <https://kdeexpress.gitlab.io/18/>

* Noticias de Baltasar
  * https://pointieststick.com/2023/06/18/on-the-road-to-plasma-6/
  * https://pointieststick.com/2023/08/03/august-plasma-6-progress-update/
  * https://www.kdeblog.com/estado-de-progreso-de-plasma-6.html
* [Wiki oficial de Plasma6](https://community.kde.org/Plasma/Plasma_6)
* https://blog.broulik.de/2023/08/on-the-road-to-plasma-6-contd/
* [Remote Desktop by KDE](https://quantumproductions.info/articles/2023-08/remote-desktop-using-rdp-protocol-plasma-wayland)

{{% linksubssteno %}}

<!--more-->
___
* Agradecimientos:
{{% jorgelama %}}
{{% Adrian %}}
{{% m_erotic %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}