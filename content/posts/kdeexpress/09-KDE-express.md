---
title: "#09 - Akademy-es 2021"
date: 2021-11-19T01:09:09+01:00
categories: [kdeexpress]
tags: [temporada1, Akademy-ES]
author: "David Marzal"
img: 626px-Mascot_konqi-commu-akademy.png
imgALT: "Konqi apuntando a una pizarra que pone Akademy"
podcast:
  audio: kde-express-akademy-es/KDE_Express_AkademyES
  ##audio: KDE_Express_AkademyES
  olength : 7974902
  mlength : 8765991
  iduration : "00:12:10"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/09-KDE_Express_AkademyES.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/09-KDE_Express_AkademyES.asr.srt"
    trtype : application/srt
participantes :
  - person : Jose Picon
    role : Host
    group : Cast
    img : https://mpipeunstor.blob.core.windows.net/mpiuseravatars/86a37988-8272-4370-bc77-2c16d9ede0e5.jpg
    href : https://es.linkedin.com/in/jos%C3%A9-pic%C3%B3n
  - person : David Marzal
    role : Co-Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
comments: false
aliases:
  - /09
  - /9
---
Episodio especial para el Akademy-ES 2021.

Podeis encontrar el taller de este podcast en https://tube.kockatoo.org/w/b3uvJcozZSt6Kfr3VHkz9s

Artículo original en <https://kdeexpress.gitlab.io/09/>
___
{{% linksubssteno %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}