---
title: 'Krunner es una Maravilla, Opt KDE, Gear, webapps, HDR, Plasma6, Kdenlive'
date: 2024-06-11T03:13:29+02:00
categories: [kdeexpress]
tags: [temporada3, akademy-es, esLibre, Plasma6, KDE Gear, Opt KDE, Kdenlive]
author: "David Marzal"
img: 500px-Mascot_konqi-commu-mail.png
imgALT: "Konqi con muchos sobres de correo electronico"
podcast:
  guid : 29-2024-06-10-03-13-Akademyes
  audio: 29-kde-express/29-KDE-Express
  ##audio: 29-KDE-Express
  olength : 13761759
  mlength : 11103462
  iduration : "00:11:34"
si :
  uri : "https://floss.social/@kde_espana/112596510618968097"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/29-KDE-Express.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/29-KDE-Express.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
itunes:
  explicit:   # por defecto false
comments: false
aliases:
  - /29
---
Resumen de noticias de este mes más reflexiones sobre la Akademy-es y esLibre Valencia 2024.

Artículo original con los enlaces en <https://kdeexpress.gitlab.io/29/>
<!--more-->

* Opt Green es la nueva iniativa de KDE Eco : https://www.muylinux.com/2024/05/29/kde-eco-opt-green/

* Webapps en KDE, como mejorar sus iconos en Wayland: https://www.muylinux.com/2024/05/27/webapps-kde-plasma-wayland/

* KDE Gear 24.05, trae nuevas aplicaciones y varias mejoras :
  * https://www.muylinux.com/2024/05/23/kde-gear-24-05/
  * https://www.linuxadictos.com/kde-gear-24-05.html

* KDE trabaja en arreglar los iconos dentro de GNOME
  * https://ubunlog.com/kde-toma-medidas-para-que-sus-aplicaciones-se-vean-bien-fuera-de-plasma-entre-las-novedades-de-esta-semana/
  * https://www.linuxadictos.com/kde-ya-trabaja-en-los-problemas-de-iconos-en-entornos-que-no-son-kde.html

* Kdenlive 24.04 con efectos en grupos, Renderizado multi-formato, subtitulos con Whisper y otras muchas mejoras: 
https://kdenlive.org/en/2024/05/kdenlive-24-05-0-released/

* El HDR mejorará mucho en Plasma 6.1
https://www.phoronix.com/news/KDE-HDR-Colors-Part-3

* En Plasma 6.1 será más sencillo editar tu Escritorio, añadir plasmoides...
https://notmart.org/blog/2024/05/new-plasma-edit-mode-in-6-1/

* Debate sobre si los kernels super viejos son más inseguros que los LTS/estables:
https://www.theregister.com/2024/05/21/longterm_supported_distros_kernel_policies/

* Distros que reciben Plasma6 :
  * Alpine Linux 3.20, ¿quien usa alpine con un DE? Los moviles: https://www.alpinelinux.org/posts/Alpine-3.20.0-released.html
  * Manjaro : https://www.linuxadictos.com/manjaro-lanza-gran-actualizacion-con-plasma-6-gnome-46-lxqt-2-0-y-linux-6-9.html
  * NixOS 24.05 : https://www.muylinux.com/2024/06/05/nixos-24-05/

* Primeras reflexiones sobre Akademy-es 2024 : https://www.kdeblog.com/primeras-reflexiones-sobre-akademy-es-2024-de-valencia-eslibre-edition.html

* Atajo recomendado: Alt+espacio = Krunner
___
{{% linksubssteno %}}

{{< detalsum "Y transcripción completa al pinchar aquí" >}}
{{% includef "./static/subs/29-KDE-Express.txt" %}}
{{< /detalsum >}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}

