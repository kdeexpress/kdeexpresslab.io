---
title: 'El Fediverso es divertido'
date: 2025-02-21T19:05:41+01:00
categories: [kdeexpress]
tags: [temporada4, Fediverso, Mastodon, Misskey, VamonosJuntas]
author: "David Marzal"
img: 500px-Mascot_konqi-commu-mail.png
imgALT: "Konqi con muchos sobres de correo electronico"
podcast:
  guid : 41-2025-02-07-04-09-4aTemporada-KDE_express
  audio: 41-kde-express/41-KDE_Express
  ##audio: 41-KDE_Express
  olength : 9659004
  mlength : 12364010
  iduration : "00:12:52"
si :
  uri : "https://floss.social/@kde_espana/114043188109751306"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/41-KDE_Express.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/41-KDE_Express.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Jorge Lama
    role : Audio Editor
    group : Audio Post-Production
    img : https://files.mastodon.social/accounts/avatars/001/106/656/original/0c35a1ce0f0cec6b.jpg
    href : https://mastodon.social/@raivenra
itunes:
  explicit:   # por defecto false
aliases:
  - /41
---
Episodio especial sobre el Fediverso y el camapaña #VamonosJuntas.

Hoy solo hay un enlace, pero que dentro tiene mucha información otros muchos enlaces.

https://marzal.gitlab.io/mundolibre/blog/2025/01/27/el-fediverso-es-divertido/

Anímate a pasarte por el Fediverso y de paso coméntanos algo.

Artículo original en <https://kdeexpress.gitlab.io/41/>

___
{{% linksubssteno %}}

{{< detalsum "Y transcripción completa al pinchar aquí" >}}
{{% includef "./static/subs/41-KDE_Express.txt" %}}
{{< /detalsum >}}
* Agradecimientos:
{{% jorgelama %}}
{{% Adrian %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}

