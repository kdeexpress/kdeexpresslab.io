---
title: "#P01 - Cómo ayudar a KDE"
date: 2022-07-12T01:02:00+02:00
categories: [kdeexpress]
tags: [Pildoras]
author: "David Marzal"
img: 
podcast:
  ##audio: 
  olength : 2743159
  mlength : 4049752
  iduration : "00:08:33"
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
comments: false
aliases:
  - /P01
---
Primera píldora y puede que de las más importantes.
Una forma rápida, fácil y barata de ayudar; y luego un abanico de posibilidades de otros métodos en los que puedes colaborar con KDE y con muchos proyectos de SL.

Artículo original en <https://kdeexpress.gitlab.io/P01/>
<!--more-->
Enlaces:
* https://www.kde-espana.org/
  * [Foro oficial en español](https://discuss.kde.org/c/local-communities/kde-en-espanol/18)
* https://kde.org/
  * https://kde.org/community/donations/
  * https://kde.org/support/
* https://community.kde.org/Main_Page
  * https://community.kde.org/Get_Involved
  * https://userbase.kde.org/
* https://bugs.kde.org/

* https://wiki.archlinux.org/title/KDE
  * https://wiki.archlinux.org/title/KDE_(Espa%C3%B1ol)

* EXTRAS
  * https://pointieststick.com/2021/10/13/25-ways-you-can-contribute-to-kde/
  * https://www.kdeblog.com/como-colaborar-con-kde.html
___
{{% licencia %}}
