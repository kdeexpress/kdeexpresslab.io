---
title: 'Trailer: KDE Express. Comunidad y Software Libre'
date: 2025-02-27T16:01:01+01:00
categories: [kdeexpress]
tags: [temporada4, Trailer]
author: "David Marzal"
img: logo_kdee.png
imgALT: "KDE EXPRESS, una K en una rueda dentada y encima una flecha dando sensación de movimiento"
podcast:
  guid : T04-2025-02-27-01-01-4aTemporada-KDE_express
  audio: t04-kde-express-trailer/T04-KDE_Express-Trailer
  ##audio: T04-KDE_Express-Trailer
  olength : 272540
  mlength : 381518
  iduration : "00:00:23"
  trailer : 4
si :
  uri : "https://floss.social/@kde_espana/114076998113045512"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/T04-KDE_Express-Trailer.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/T04-KDE_Express-Trailer.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
itunes:
  explicit:   # por defecto false
aliases:
  - /T04_Trailer
---
Desde Cartagena, de la mano de David Marzal, descubre más sobre la comunidad KDE y de Software libre con noticias, eventos, entrevistas, tips e información de calidad del mundillo.

Con una periodicidad quincenal, David, con ánimo divulgador y el espíritu Linuxero de compartir conocimiento, da un repaso a las aplicaciones y actualizaciones del KDE, pero también de otras tecnologías relacionadas, accesibilidad, etc. A veces también trae a algún invitado con papeles. 

Si quieres aprender estas tecnologías, con las explicaciones tan claras y entretenidas de David, este es tu podcast. ¡Pon la Oreja!

Artículo original en <https://kdeexpress.gitlab.io/T04_Trailer/>

___
{{% linksubssteno %}}

{{< detalsum "Y transcripción completa al pinchar aquí" >}}
{{% includef "./static/subs/T04-KDE_Express-Trailer.txt" %}}
{{< /detalsum >}}
* Agradecimientos:
{{% Adrian %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el trailer aquí mismo:" >}}

