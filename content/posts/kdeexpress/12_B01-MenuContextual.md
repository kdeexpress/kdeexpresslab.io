---
title: "#B01 - Presentación proyecto Pildoras y bugs y sus apaños"
date: 2022-07-12T01:01:01+02:00
categories: [kdeexpress]
tags: [Pildoras, Bugs-Apaños]
author: "David Marzal"
img: 482px-Mascot_konqi-support-bughunt.png
imgALT: "Konqi con una red intentado cazar un bug (mariposa)"
podcast:
  audio: b-01-menu-contextual-doble-monitor/B01_MenuContextualDobleMonitor
  ##audio: B01_MenuContextualDobleMonitor
  olength : 2743159
  mlength : 4049752
  iduration : "00:04:48"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/B01_MenuContextualDobleMonitor.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/B01_MenuContextualDobleMonitor.asr.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
comments: false
aliases:
  - /B01
  - /B1
---
Os presentamos un nuevo formato y sección: Las PÍLDORAS y los bugs y sus apaños.

En este audio teneis la presentación del proyecto y el primer bug.
Si en audio solo no se entiene bien lo teneis disponible el vídeo en
https://tube.kockatoo.org/w/bP5eQCo7Bb3VT7UYtQQ2NT


Te salen los menús contextuales desplazados hacia abajo, aquí tienes un apaño hasta que este corregido por nuestros incansables desarrolladores.

Por cierto, grabando este vídeo he visto otro bug, al cambiar la disposición de los monitores para enseñar el problema OBS también se lía con la posición del ratón y sale desplaza hacia abajo y queda un poco más raro todavía, espero que se entienda.

https://bugs.kde.org/show_bug.cgi?id=429338

Artículo original en <https://kdeexpress.gitlab.io/B01/>
___
{{% licencia %}}
