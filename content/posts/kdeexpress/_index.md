---
title: "KDE Express. Comunidad y Software Libre"
description: |
    David Marzal nos sumerge en la comunidad KDE con noticias, entrevistas e información sobre Software Libre en general. Y en particular sobre KDE Plasma, Dolphin, Okular, Kdenlive, Konsole, Neochat, Kate, Krita, Akademy y la infinidad de proyectos de esta familia.

    Logo por Adrian Marzal
podcast_image: "logo_kdee.png"

author:
    name: "David Marzal" # <itunes:author|itunes:name>, <item>-<itunes:author>
    email: "kde_express@kde-espana.org" # <managingEditor|webMaster|itunes:email> <item>-<author>
    fediverse: "@KDE_espana@floss.social" # web metadata

#URLprefix : https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/media.blubrry.com/3752801/
URLprefix : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/"
#URLhost : "gnulinuxvalencia.org/audio-video/audio/"
#URLhost : "kdeexpress.kde-espana.org/"
URLhost : "archive.org/download/"

# https://github.com/Podcastindex-org/podcast-namespace/blob/main/docs/1.0.md
p20:
# Datos que deben estar aqui definidos si se quieren usar
    guid: "a9a56b87-575a-5f6f-9636-cdf7b73e6230"        # channel
    guid_r: "5da26ee2-8dbb-5dfc-a574-c5747753178b"
    protocol: "activitypub"     # https://github.com/Podcastindex-org/podcast-namespace/blob/main/socialprotocols.txt
# Datos especificos que pueden sustituir a los globales de hugo.toml
    locked: "yes"                # ¿Feed bloqueado para otras plataformas? yes|no
    funding: "Hazte socio/a"    # Texto para animar a colaborar
    fundingURL: "https://www.kde-espana.org/involucrate" # Enlace
    location : 
    - text : "España"          # channel
      GEO : "40.416944,-3.703333;u=1000000"
      OSM : "R1311341"     # España
      country: es          # Country
      rel : creator        # subject | creator
    medium :                    # podcast (default) https://podcastindex.org/namespace/1.0#medium
    transcript :
        rel : captions
        language : es
    participantes :
      - person : David Marzal
        role : Host
        group : Cast
        img : /images/person/DavidMC.jpg
        href : https://masto.es/@DavidMarzalC
      - person : Jorge Lama
        role : Audio Editor
        group : Audio Post-Production
        img : https://files.mastodon.social/accounts/avatars/001/106/656/original/0c35a1ce0f0cec6b.jpg
        href : https://mastodon.social/@raivenra
    podroll : [e7fc1a59-fb05-5202-a94c-4e18750cddba, 68b52970-05d8-5cb1-8bd3-d15fa3961e09, bd8f7280-b35a-5bfd-bbde-829cf1b12682, 1f792690-bccb-534e-b95d-52dc00426822, 2a4a6311-ddad-5735-9ed7-09d734d8d041, 610e9ea8-edf0-407f-9e6c-72375a0e17db, 093ce4f4-ee17-5746-b334-a423d54dd018]
    podping : [6571346]
    websub : "https://hub.livewire.io/"
    publisher :
      feedGuid : 003af0a0-6a45-55bf-b765-68e3d349551a
      feedUrl : https://marzal.gitlab.io/mundolibre/Marzal.rss
    chat:
        server : kde.org
        protocol : matrix
        accountId : "@dmarzal:matrix.org"
        space : "#kde-es:kde.org"
    follow : followlinks
    images: #
      - href : logo_kdee.png
        alt : "KDE EXPRESS, una K en una rueda dentada y encima una flecha dando sensación de movimiento"
        purpose : artwork
        aspect_ratio : "1/1"
        type : image/png
        width : 3000
      - href : FlyingKonqui_1200x630.webp
        alt : "Konqi volando sobre unas burbujas"
        purpose : social
        aspect_ratio : "1.9"
        type : image/webp
        width: 1200
        height : 630
      - href : logo_kde-esp_512x512.webp
        alt : "Logo de KDE con la K y el texto ESPAÑA debajo"
        purpose : icon
        aspect_ratio : "1/1"
        type : image/webp
        width : 512
      - href : SafeLandig_2560x1440.webp
        alt : "KDE Plasma 5.25 wallpaper cropped by Louis Durrant"
        purpose : cover
        aspect_ratio : "16/9"
        type : image/webp
        width : 2560
        height : 1440
skipHours: [0, 1, 2, 3, 4, 5, 20, 21, 22, 23]
skipDays: [Monday, Friday, Saturday, Sunday] # [Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday]
itunes:
    explicit:                   # por defecto false
    #newfeedurl: "https://kdeexpress.gitlab.io/feed" # https://podcasters.apple.com/support/837-change-the-rss-feed-url
    category: 
    - Technology :
    - News : Tech News
    - Society & Culture : Philosophy

# https://www.calculatorsoup.com/calculators/technology/aspect_ratio-calculator.php
# cover : 16/9 2560x1440 min 
# social : 1.91 1200x630 min - Episodes.fm -> og:image
# Mastodon banner : 3:1 1500x500
# A continuación poner la descripción del podcast con html
---
Podcast en español sobre la comunidad [KDE](https://www.kde-espana.org/). Audios de entre 15-30 minutos con noticias, eventos, novedades e información en general.

También puedes encontrarnos en [Telegram](https://t.me/KDEexpress)

Licencia CC-BY-SA-4.0

Logo por Adrian Marzal
