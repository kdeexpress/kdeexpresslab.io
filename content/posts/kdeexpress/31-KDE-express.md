---
title: 'Komunidad con Ivan GJ'
date: 2024-07-15T03:15:31+02:00
categories: [kdeexpress]
tags: [temporada3, Komunidad, Ivan GJ]
author: "David Marzal"
img: 600px-Mascot_20140702_konqui-group.png
imgALT: "Konqi junto al resto de sus compis de diferentes colores haciendo una piramide entre ellos"
podcast:
  guid : 31-2024-07-11-03-15-Akademyes
  audio: 31-kde-express/31-KDE_Express-IvanGJ
  ##audio: 31-KDE_Express-IvanGJ
  olength : 24045338
  mlength : 25276006
  iduration : "00:26:19"
si :
  uri : "https://floss.social/@kde_espana/112786929183406566"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/31-KDE_Express-IvanGJ.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/31-KDE_Express-IvanGJ.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Ivan G.J.
    role : guest
    group : Cast
    img : https://files.mastodon.social/accounts/avatars/001/298/544/original/393b446c08ad020e.webp
    href : https://mastodon.social/@ivangj
  - person : Jorge Lama
    role : Audio Editor
    group : Audio Post-Production
    img : https://files.mastodon.social/accounts/avatars/001/106/656/original/0c35a1ce0f0cec6b.jpg
    href : https://mastodon.social/@raivenra
itunes:
  explicit:   # por defecto false
comments: false
aliases:
  - /31
---
Vuelven por fin los episodios de comunidad, en esta ocasión contamos con una de las últimas incorporaciones a la Asociación KDE España.

Hablamos con Iván, sobre como llegó al Software Libre y a KDE, su camino por la divulgación en YT y su nueva etapa enfocada en la cultura y SL.

Artículo original con los enlaces en <https://kdeexpress.gitlab.io/31/>
<!--more-->

* Enlaces de las cosas mencionadas en el episodio:
  * Mastodon: https://mastodon.social/@ivangj
  * Peertube: https://fediverse.tv/a/ivangj/
  * Web: https://ivangj.xyz/
  * Repoductor Haruna : https://haruna.kde.org/ 

¡Seguidle que tiene muchas cosas que contar!
___
{{% linksubssteno %}}

{{< detalsum "Y transcripción completa al pinchar aquí" >}}
{{% includef "./static/subs/31-KDE_Express-IvanGJ.txt" %}}
{{< /detalsum >}}
* Agradecimientos:
{{% jorgelama %}}
{{% Adrian %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}

