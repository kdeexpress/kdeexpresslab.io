---
title: '#23 Feliz 2024 y popurri de noticias'
date: 2024-01-13T03:07:23+01:00
categories: [kdeexpress]
tags: [temporada3, Noticias, Distribuciones, esLibre]
author: "David Marzal"
img: 500px-Mascot_konqi-commu-mail.png
imgALT: "Konqi con muchos sobres de correo electronico"
podcast:
  guid :
  audio: 23-kde-express/23-KDE_Express_Feliz2024
  ##audio: 23-KDE_Express_Feliz2024
  olength : 7880736
  mlength : 8084628
  iduration : "00:11:13"
si :
  uri : "https://floss.social/@kde_espana/111748008278079892"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/23-KDE_Express-Feliz2024_y_popurri_de_noticias.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/23-KDE_Express-Feliz2024_y_popurri_de_noticias.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Jorge Lama
    role : Audio Editor
    group : Audio Post-Production
    img : https://files.mastodon.social/accounts/avatars/001/106/656/original/0c35a1ce0f0cec6b.jpg
    href : https://mastodon.social/@raivenra
  - person : Yoyo Fernandez
    role : Music Contributor
    group : Audio Post-Production
    img : https://salmorejogeek.files.wordpress.com/2022/02/salmorejo-geek-v2-400px.png
    href : https://mastodon.social/@salmorejogeek
itunes:
  explicit:   # por defecto false
comments: false
aliases:
  - /23
---
Quedan 46 días para la megapublicación de KDE 6 y eso hace que tengamos muchas noticias que comentar, así que en este episodio se trata el proximo evento de esLibre y de 24H24L, el paso de Nobara a KDE Plasma, un vídeo sobre las razones por las que no hay más distros con Plasma por defecto, la campaña de donaciones de KDE, tres maneras de abrir aplicaciones, una encuesta de uso de Muylinux y como bonus uno de los temas de Yoyo.

Artículo original con los enlaces en <https://kdeexpress.gitlab.io/23/>
<!--more-->

* Eventos
  * esLibre Valencia 2024 ( 24 y 25 de Mayo ): https://eslib.re/2024/
  * 24H24L 2024 : https://24h24l.org/

* Nobara se pasa a KDE Plasma por :
  * Tasa de refresco variable (VRR), 
  * (DRM) requerido para soportar la realidad virtual en Wayland,
  * Escalado fraccional 
  * Posibilidad de arrastrar y soltar!
  * Mejor integración con Steam. 
  * https://www.muylinux.com/2023/12/27/nobara-39/
  * https://nobaraproject.org/2023/12/26/december-26-2023/

* Why Isn't Every Linux Distro Shipping KDE? (Brodie Robertson)
  * https://odysee.com/@BrodieRobertson:5/why-isn't-every-linux-distro-shipping:e
  * https://www.youtube.com/watch?v=VXe2gGpg1cw

* Campaña de donaciones : 
  * https://www.gamingonlinux.com/2024/01/kde-plasma-6-hits-initial-funding-goal/
  * https://kde.org/es/fundraisers/plasma6member/

* Tres maneras de abrir programas en Plasma
  - Escribir el nombre en un escritorio vacío
  - Krunner : Alt + F2 o Alt + barra espaciadora
  - Con la tecla «super» (o tecla windows)
  - https://victorhckinthefreeworld.com/2022/09/21/tres-maneras-de-abrir-programas-en-plasma-de-kde-sin-usar-el-raton/

* GNOME vs KDE en Muylinux
  * Gana Gnome en la encuesta por un 2% 30/32
  * https://www.muylinux.com/2024/01/04/escritorios-2023/

* Amo a Plasma por Yoyo Fernandez : https://www.youtube.com/watch?v=Qokta5Bu400
___
{{% linksubssteno %}}

{{< detalsum "Y transcripción completa al pinchar aquí" >}}
{{% includef "./static/subs/23-KDE_Express-Feliz2024_y_popurri_de_noticias.txt" %}}
{{< /detalsum >}}
* Agradecimientos:
{{% jorgelama %}}
{{% Adrian %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}
