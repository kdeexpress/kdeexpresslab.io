---
title: 'OptiImage, KDenlive, Krunner, HDR, Dolphin, Karp, OpenMadriva, Impresión, Donaciones'
date: 2024-12-12T14:04:37+01:00
categories: [kdeexpress]
tags: [temporada4, OptiImage, digiKam, Krunner, HDR, Dolphin, Karp, OpenMadriva, Impresión, Donaciones]
author: "David Marzal"
img: 501px-Mascot_konqi-commu-journalist.png
imgALT: "Konqi levantando un microfono, sostiene un bloc de notas en la otra mano"
podcast:
  guid : 37-2024-12-12-04-06-4aTemporada-KDE_express
  audio: 37-kde-express/37-KDE-Express
  ##audio: 37-KDE-Express
  olength : 8887755
  mlength : 11798053
  iduration : "00:12:17"
si :
  uri : "https://floss.social/@kde_espana/113640825783664552"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/37-KDE_Express.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/37-KDE_Express.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Jorge Lama
    role : Audio Editor
    group : Audio Post-Production
    img : https://files.mastodon.social/accounts/avatars/001/106/656/original/0c35a1ce0f0cec6b.jpg
    href : https://mastodon.social/@raivenra
itunes:
  explicit:   # por defecto false
comments: false
aliases:
  - /37
---
Puede que el último episodio de este año, pero con la ración habitual de noticias de la comunidad KDE.

Artículo original con los enlaces en <https://kdeexpress.gitlab.io/37/>
Si no ves nada después de esta linea en tu aplicación de podcast, es que no sabe leer bien el feed RSS, prueba otra ;)
<!--more-->
* OptiImage 1.0.0 [lanzado](https://carlschwan.eu/2024/11/30/optiimage-1.0.0-is-out/)!
* [digiKam](https://www.digikam.org/news/2024-11-16-8.5.0_release_announcement/): Mejoras en el reconocimiento facial, actulizado la libreria interna de decodificación RAW
* Buscar en la web [desde Krunner](https://blog.marcdeop.com/?p=302):
* Los recordatorios [funcionan](https://pointieststick.com/2024/12/02/i-think-the-donation-notification-works/):
* Con 6.2.4 quien tenga NVIDIA 565.71.1 en adelante vuelve a tener [HDR disponible](https://9to5linux.com/kde-plasma-6-2-4-re-enables-hdr-mode-for-users-on-nvidia-565-and-linux-6-11).
* Mejorado el [plasmoide de impresión](https://blog.broulik.de/2024/12/hardware-shenanigans/)
* Dolphin mejora su [accesibilidad](https://blogs.kde.org/2024/12/08/this-week-in-kde-apps-gear-24.12.0-incoming/) con Orca 47 en Gear 24.12 después de otras mejoras. 
* Karp una nueva aplicación para editar PDFs al estilo PDFArrenger
* [OpenMadriva](https://www.phoronix.com/news/OpenMandriva-ROME-24.12) se suma a Plasma 6.2 como escritorio por defecto
* Asamblea general KDE el 21 de diciembre a las 10.00. [Asociate](https://www.kde-espana.org/involucrate)!

___
{{% linksubssteno %}}

{{< detalsum "Y transcripción completa al pinchar aquí" >}}
{{% includef "./static/subs/37-KDE_Express.txt" %}}
{{< /detalsum >}}
* Agradecimientos:
{{% jorgelama %}}
{{% Adrian %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}

