---
title: 'Feliz 2025. Mucha salud y FLOSS'
date: 2025-01-08T20:04:38+01:00
categories: [kdeexpress]
tags: [temporada4, Gears, Plasma6, Wrap, Amarok, KDE Connect, Pidgin, Accesibilidad, HDR, Q4OS, openSUSE]
author: "David Marzal"
img: 501px-Mascot_konqi-commu-journalist.png
imgALT: "Konqi levantando un microfono, sostiene un bloc de notas en la otra mano"
podcast:
  guid : 38-2025-01-08-04-07-4aTemporada-KDE_express
  audio: 38-kde-express/38-KDE_Express
  ##audio: 38-KDE_Express
  olength : 15036037
  mlength : 20447600
  iduration : "00:21:17"
si :
  uri : "https://floss.social/@kde_espana/113794354450749938"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/38-KDE_Express.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/38-KDE_Express.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Jorge Lama
    role : Audio Editor
    group : Audio Post-Production
    img : https://files.mastodon.social/accounts/avatars/001/106/656/original/0c35a1ce0f0cec6b.jpg
    href : https://mastodon.social/@raivenra
itunes:
  explicit:   # por defecto false
comments: false
aliases:
  - /38
---
El mundo puede que esté cada vez más loco, pero en nuestro oasis de paz, la comunidad KDE avanza viento en popa.

Artículo original con los enlaces en <https://kdeexpress.gitlab.io/38/>
Si no ves nada después de esta linea en tu aplicación de podcast, es que no sabe leer bien el feed RSS, prueba otra ;)
<!--more-->
* [Análisis](https://www.dedoimedo.com/computers/fedora-41-kinoite.html) de Fedora Kinoite, una inmutable que apunta maneras, pero aún no lista para el usuario medio.
* KDE está en 6 de las 20 noticias más populares de [Phoronix en 2024](https://www.phoronix.com/news/Linux-Open-Source-News-2024)
* MuyLinux considera que KDE Plasma 6 es lo [mejor de 2024](https://www.muylinux.com/2025/01/03/lo-mejor-de-2024-kde-plasma-6/)
* Según las [estadísticas](http://blog.davidedmundson.co.uk/blog/i-measured-kdes-commit-stats-and-the-results-surprised-me/) de los commits, el proyecto tiene buena salud en cuanto a desarrolladores
* KDE [Gear 24.12](https://kde.org/announcements/gear/24.12.0/) viene con varias [novedades](https://www.muylinux.com/2024/12/12/kde-gear-24-12/).
  * KDE Connect ya funciona con Bluetooh https://www.kdeblog.com/novedades-de-kde-connect-en-kde-gear-24-12.html
  * [Amarok](https://blogs.kde.org/2024/12/29/amarok-3.2-punkadiddle-released/) 3.2 con una compilación funciona en [QT6](https://www.linuxadictos.com/amarok-3-2-nueva-version-del-reproductor-de-musica-de-codigo-abierto-que-despide-2024-con-qt6-y-otras-novedades.html) y con filtro para colecciones sin etiquetas.
  * KDE tiene en desarrollo una utilidad para [Home Assistant](https://blog.davidedmundson.co.uk/blog/adding-home-automation-to-kde/): [Kiot](https://www.phoronix.com/news/KDE-Kiot-Home-Assistant)
* Plasma 6.3
  * Mejora sustancial en el [escalado](https://blogs.kde.org/2024/12/14/this-week-in-plasma-better-fractional-scaling/)
  * Varias mejoras para las tabletas gráficas
  * Discover mostrara que permisos cambian en las aplicaciones con sandbox (Flatpak)
  * Mejorar de [accesibilidad](https://blogs.kde.org/2025/01/04/this-week-in-plasma-artistry-and-accessibility/): En la navegación con teclado de la funcionalidad de aplicaciones con componentes Kirigami, Discover y KDE XDG portal. También mejorado el subrayado de las letras que marcan el acceso rápido con la tecla Alt.
  * Mejoras en el modo nocturno bajo [HDR](https://zamundaaa.github.io/wayland,/colormanagement/2025/01/07/fixing-night-light.html) 
*  [Q4OS](https://soploslinux.com/q4os-una-distribucion-ligera/) una distro ligera basada en KDE Plasma / Trinity
* [openSUSE](https://news.itsfoss.com/opensuse-gaming-gpu-tool/) se pasa a swicheroo para GPUs hibridas
* [Pidgin](https://linuxiac.com/after-16-years-pidgin-3-takes-its-first-steps/) revive!

* Recomendación de Los últimos de FEEDlipinas ([@ultimosfeed@masto.es](https://masto.es/@ultimosfeed)): [Web](https://ultimosfeed.blogspot.com/) y [Feed](https://feeds.redcircle.com/610e9ea8-edf0-407f-9e6c-72375a0e17db)
* Y de [Accesibilidad con Tecnologias Libres](https://accesibilidadtl.gitlab.io/): [Feed](https://accesibilidadtl.gitlab.io/feed)

___
{{% linksubssteno %}}

{{< detalsum "Y transcripción completa al pinchar aquí" >}}
{{% includef "./static/subs/38-KDE_Express.txt" %}}
{{< /detalsum >}}
* Agradecimientos:
{{% jorgelama %}}
{{% Adrian %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}

