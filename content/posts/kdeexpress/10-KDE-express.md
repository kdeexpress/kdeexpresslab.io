---
title: "#10 - Novedades fin de 2021"
date: 2021-12-30T01:10:10+01:00
categories: [kdeexpress]
tags: [temporada1, novedades]
author: "David Marzal"
img: 542px-Mascot_konqi-app-system.png
imgALT: "Konqi dentro de una rueda dentada, saluda con dos dedos en la frente"
podcast:
  audio: 10-kde-express-novedades-fin-de-2021/10-KDE_Express-Novedades_fin_de_2021
  ##audio: 10-KDE_Express-Novedades_fin_de_2021
  olength : 15378418
  mlength : 19220367
  iduration : "00:26:41"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/10-KDE_Express-Novedades_fin_de_2021.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/10-KDE_Express-Novedades_fin_de_2021.asr.srt"
    trtype : application/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Baltasar
    role : Co-Host
    group : Cast
    img : https://files.mastodon.social/accounts/avatars/000/924/299/original/e0a2b2e0270a6a27.png
    href : https://mastodon.social/@baltollkien
comments: false
aliases:
  - /10
---
Último episodio de 2021 en el que contamos con la aparición estelar de Baltasar Ortega de KDE Blog (entre otros tantos quehaceres).

Tenéis un audio cargado de novedades el ecosistema KDE y un par de noticias de FLOSS en general, a continuación os dejo un resumen con los enlaces.

Artículo original en <https://kdeexpress.gitlab.io/10/>
<!--more-->
* KDE
  * [Actualización de diciembre del 2021 de KDE Frameworks](https://www.kdeblog.com/actualizacion-de-diciembre-del-2021-de-kde-frameworks.html)
  * [Disponible Plasma Mobile 21.12 con muchas mejoras](https://www.kdeblog.com/disponible-plasma-mobile-21-12-con-muchas-mejoras.html)
  * [Disponible KDE Gear 21.12, un regalo anticipado](https://www.kdeblog.com/disponible-kde-gear-21-12-un-regalo-anticipado.html)
  * [Novedades de Kdenlive de KDE Gear 21.12](https://www.kdeblog.com/novedades-de-kdenlive-de-kde-gear-21-12.html)
  * [17/12 Lanzado GCompris 2.0, otro regalo de la Comunidad KDE](https://gcompris.net/news/2021-12-17-es.html)
  * [23/12 Lanzado Krita 5.0](https://krita.org/es/krita-5-0-notas-de-lanzamiento/)
* No KDE
  * [Presentación de campaña ¿Dinero público? ¡Código público!… ¡ahora en Español!](https://www.kdeblog.com/presentacion-de-campana-dinero-publico-codigo-publico-ahora-en-espanol.html)
    * [Folleto y presentaciones](https://download.fsfe.org/campaigns/pmpc/ES/)
  * [Blender 3.0](https://www.blender.org/download/releases/3-0/)

¡Que tengáis buena entrada de año!
___
{{% linksubssteno %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}