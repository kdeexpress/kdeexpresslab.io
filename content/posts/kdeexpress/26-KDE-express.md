---
title: 'Krunner, Temas peligrosos, reviews, comandos en notificaciones y Akademy-es'
date: 2024-04-15T03:10:26+02:00
categories: [kdeexpress]
tags: [temporada3, akademy-es, esLibre, Plasma6]
author: "David Marzal"
img: 626px-Mascot_konqi-commu-akademy.png
imgALT: "Konqi apuntando a una pizarra que pone Akademy"
podcast:
  guid : 26-2024-04-12-18-32-Akademyes
  audio: 26-kde-express/26-KDE_Express_20240415
  ##audio: 26-KDE_Express_20240415
  olength : 15091600
  mlength : 12197208
  iduration : "00:12:42"
si :
  uri : "https://floss.social/@kde_espana/112275512150432717"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/26-KDE_Express_20240415.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/26-KDE_Express_20240415.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
  - person : Jorge Lama
    role : Audio Editor
    group : Audio Post-Production
    img : https://files.mastodon.social/accounts/avatars/001/106/656/original/0c35a1ce0f0cec6b.jpg
    href : https://mastodon.social/@raivenra
itunes:
  explicit:   # por defecto false
comments: false
aliases:
  - /26
---
Hoy hablamos de nuevas funciones de Krunner, Temas peligrosos, Plasma Bigscreen, reviews, comandos en notificaciones y como exclusiva la programación de la Akademy-es en esLibre. 

Artículo original con los enlaces en <https://kdeexpress.gitlab.io/26/>
<!--more-->

* Nuevas funcionalidades de Krunner: https://www.kdeblog.com/busquedas-mas-rapidas-y-eficientes-en-plasma-6.html
* Los temas globales de Plasma de la tienda necesitan revision: https://www.muylinux.com/2024/03/29/malware-en-la-kde-store-el-tema-de-la-seguridad-y-sus-multiples-caras/
* Plasma Bigscreen : https://www.linuxadictos.com/le-he-instalado-plasma-bigscreen-a-mi-centro-multimedia-y-es-de-lo-mejor-que-he-podido-hacerle-aunque-tiene-margen-de-mejora.html
* Historia de Plasma5: https://blog.broulik.de/2024/03/plasma-5-the-early-years/
* Review in English de Plasma 6 : https://www.debugpoint.com/kde-plasma-6-review/
* Plasma 6.1 recuperar la opción de añadir comandos a las notificaciones: https://blog.cryptomilk.org/2024/04/05/kde-run-a-command-on-notification-trigger/
* Ruskdek añade soporte a Plasma6 en x11 : https://github.com/rustdesk/rustdesk/pull/7389

¡Nos vemos en la Akademy-es Valencia el 25 y 26 de mayo!

___
{{% linksubssteno %}}

{{< detalsum "Y transcripción completa al pinchar aquí" >}}
{{% includef "./static/subs/26-KDE_Express_20240415.txt" %}}
{{< /detalsum >}}
* Agradecimientos:
{{% jorgelama %}}
{{% Adrian %}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}

