---
title: '#30 Plasma6.1, Mobile, Vídeos en el escritorio e Integración'
date: 2024-07-05T03:14:30+02:00
categories: [kdeexpress]
tags: [temporada3, Plasma6, Mobile]
author: "David Marzal"
img: 500px-Mascot_konqi-commu-mail.png
imgALT: "Konqi con muchos sobres de correo electronico"
podcast:
  guid : 30-2024-07-05-03-14-Akademyes
  audio: 30-kde-express/30-KDE-Express
  ##audio: 30-KDE-Express
  olength : 12417745
  mlength : 10631571
  iduration : "00:11:04"
si :
  uri : "https://floss.social/@kde_espana/112735344447499205"
transcript :
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/30-KDE-Express.vtt"
    trtype : text/vtt
  - trurl : "https://op3.dev/e,pg=a9a56b87-575a-5f6f-9636-cdf7b73e6230/kdeexpress.gitlab.io/subs/30-KDE-Express.srt"
    trtype : text/srt
participantes :
  - person : David Marzal
    role : Host
    group : Cast
    img : /images/person/DavidMC.jpg
    href : https://masto.es/@DavidMarzalC
itunes:
  explicit:   # por defecto false
comments: false
aliases:
  - /30
---
Selección noticias de este mes de junio. A fecha de grabación estamos con Plasma 6.1.2, Frameworks 6.3.0 y Gear 24.05.1.

Artículo original con los enlaces en <https://kdeexpress.gitlab.io/30/>
<!--more-->

* [Fedora propone publicar una imagen con KDE Plasma Mobile, para tablets seguramente](https://www.phoronix.com/news/Fedora-41-KDE-Mobile-Proposal)

* Y hablando de movilidad, [postmarketOS 24.06 se actualiza a KDE Plasma 6](https://9to5linux.com/postmarketos-24-06-linux-mobile-os-brings-kde-plasma-6-gnome-mobile-46), al igual que [openMandriva](https://www.phoronix.com/news/OpenMandriva-ROME-24.07-RC)

* [Integración de Plasma Browser](https://blog.broulik.de/2024/06/plasma-browser-integration-1-9-1/)

* [Puedes tener vídeos de fondo de pantalla en Plasma6](https://github.com/luisbocanegra/plasma-smart-video-wallpaper-reborn)

* [Plasma 6.1](https://kde.org/announcements/plasma/6/6.1.0/) nos [trae](https://9to5linux.com/kde-plasma-6-1-desktop-environment-officially-released-heres-whats-new):
  * Activar el escritorio remoto desde la configuración del sistema (funciona en Wayland)
  * Modo edición de escritorio mejorado (comentado en el anteior episodio)
  * Recordar aplicaciones abiertas en la sesión bajo Wayland
  * Sincronizar colores LED de tu teclado (si es compatible)
  * Menear el ratón para que crezca activado por defecto (accesibilidad)
  * Barrera para no cambiar de monitor sin querer con el ratón : Bordes de la pantalla -> Barrera de bordes.
  * [Mejora de rendimiento sustancial en viejos Intel](https://www.phoronix.com/news/KDE-Plasma-6.1-Intel-iGPU) gracias al Triple buffering

* Atajo recomendado:
  * Meta + R = Grabar región con Spectacle
  * Meta + + / Meta + - / Meta + 0 = Zoom del escritorio con seguimiento de ratón
___
{{% linksubssteno %}}

{{< detalsum "Y transcripción completa al pinchar aquí" >}}
{{% includef "./static/subs/30-KDE-Express.txt" %}}
{{< /detalsum >}}

{{% licencia %}}
{{< audiohtml5 caption="Puedes escuchar el episodio aquí mismo:" >}}

