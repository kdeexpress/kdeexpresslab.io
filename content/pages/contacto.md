---
title: Contacto
tags: [contacto]
aliases:
    - /contacto
    - /contact
---
Buenas, tenemos el placer de presentaros este podcast de la comunidad [KDE España](https://www.kde-espana.org/), en el que trataremos noticias y la actualidad de esta nuestra comunidad con un formato breve (max 30 minutos) para complementar los diferentes formatos en los que ya se divulga actualmente (video-podcast y [blog](https://www.kdeblog.com/)).

Os dejamos algunos enlaces de interes aquí centralizados.

* ¿Quienes somos?
    * En el [episodio 0](https://kdeexpress.gitlab.io/posts/kdeexpress/00-kde-express/) lo comentamos.

* Feeds RSS: https://kdeexpress.gitlab.io/feed

* Correo:
    * kde_express@kde-espana.org

* RRSS/Medios:
    * KDE España en el Fediverso : [@kde_espana@floss.social](https://floss.social/@kde_espana)
        * Notificaciones de episodios en el Fediverso : @6571346@ap.podcastindex.org
    * Canal de Telegram : [@KDEexpress](https://t.me/KDEexpress)

* Puedes ver en [Podnews](https://podnews.net/podcast/iaqqv) enlaces a todas las plataformas disponibles. Nosotros recomendamos:
    * [Podverse.fm](https://podverse.fm/es/podcast/kkDmfJUu-r) - Es Software Libre y soporta: Cápitulos, transcripción y comentarios de Mastodon

* Pero por si usas estas y quieres valorar y dar amor en ellas (pese a que recomendamos las libres):
    * [Spotify](https://open.spotify.com/show/4bbVNOQPlCJ5qiTANv0NMR)
    * [iVoox](https://www.ivoox.com/podcast-kde-express_sq_f11332151_1.html)
    * [Apple Podcast](https://podcasts.apple.com/es/podcast/kde-express/id1580277859)
    * [Amazon Music](https://music.amazon.es/podcasts/ff19911b-8402-4781-b202-bd4cc8d33793/kde-express)

* Código fuente del proyecto:
    * [Gitlab](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io)